ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine correct(sx,sy,sxy,phi,coh,angle)
c
c***** This subroutine correct the out-of-balance stress ***************
c********** by fixing the minimum principal stress    ******************
c
       Implicit real*8 (a-h, o-z)
       real*8 sx,sy,sxy
	   id = 0
c	   st1 = (sx+sy)*0.5D0 + dsqrt(0.25D0*(sx-sy)**2 + sxy**2)
c	   st3 = (sx+sy)*0.5D0 - dsqrt(0.25D0*(sx-sy)**2 + sxy**2)
 	   call transform1(sx,sy,sxy,st3,st1,angle)

c	   theta = datan(2.0D0*dabs(sxy)/(sx-sy))
           if (id .eq. 1) go to 3
cc-------------------------------------------------------------------------------------------
cc
cc  This section assumes the minimum stress is unchanged
cc
cc-------------------------------------------------------------------------------------------
	   sn = (1.0D0 + dsin(phi))/(1.0D0 - dsin(phi))
!	   sc = 2.0D0*coh*dsqrt(sn)
	!!Shunfu.Z 2010.03.12
		sc = 2.0D0*coh*dcos(phi)/(1.0D0 - dsin(phi))
	!!Shunfu.Z 2010.03.12
	   s3 = st3
	   s1 = sn*s3 + sc
 	   call tranback1(sx,sy,sxy,s3,s1,angle)
	
	
	   go to 2
c**** the following block is wrong-----june---EQ
         rr = 0.5D0*s3*dsin(phi)*(1.0D0 - sn)
	   sp = dsqrt(((st1 - s1)/2.D0)**2.D0*(dcos(theta)-1.D0)**2
	1        +rr**2.D0) - rr*dcos(theta)
	   ss = 0.5D0*(st1 + st3)
	 IF ( sx .lt. sy) THEN
	   sy  = ss + sp*dcos(theta)
 	   sx  = ss - sp*dcos(theta) - st1 + s1
	   sxy = sp*dsin(theta)
	 ELSE
	   sx  = ss + sp*dcos(theta)
 	   sy  = ss - sp*dcos(theta) - st1 + s1
	   sxy = -sp*dsin(theta)
	 END IF
 	   go to 2
cc-------------------------------------------------------------------------------------------
cc
c  This portion assumes over-balanced stresses will be scaled down with the same proportion
cc
cc-------------------------------------------------------------------------------------------
c
3	   ss = 0.5D0*(st1 + st3)
	   RR = 0.5D0*(st1 - st3)
  	   sx = sx - (sxy - ss*Dsin(phi))*Dcos(theta)
	   sy = sy + (sxy - ss*Dsin(phi))*Dcos(theta)
	   sxy= sxy* (1.0 - Dsin(theta)) + ss*Dsin(phi)*Dsin(theta)
	   sx = ss + RR*Dcos(phi)
	   sy = ss - RR*Dcos(phi)
	  sxy = RR*Dsin(phi)
2             return
              end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine transform(sx,sy,sxy,sr,st,ag)
c
c***** Transform stresses or strains in x-y direction with an angle ****
c      **** "angle" to the maximum principal stress direction  ****
c
       Implicit real*8 (a-h, o-z)
	 pi=3.1415926d0
		  sr = sx*dcos(ag)**2.D0 + 
	1	       sy*dsin(ag)**2.D0 + dsin(2.0D0*ag)*sxy
	      st = sx*dsin(ag)**2.D0 + 
	1	       sy*dcos(ag)**2.D0 - dsin(2.0D0*ag)*sxy
       return 
	 end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine tranback(sx,sy,sxy,sr,st,angle)
c
c***** Transform stresses or strains in r-t principal direction  ****
c         **** into x-y direction with an angle "angle"  ****
c
       Implicit real*8 (a-h, o-z)
		  sx = sr*dcos(-angle)**2.D0 + st*dsin(-angle)**2.D0 
	      sy = sr*dsin(-angle)**2.D0 + st*dcos(-angle)**2.D0 
           sxy = 0.5D0*(st-sr)*dsin(-2.0D0*angle)
       return 
	 end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine bacsub(bk,loads,n,iw)
c
c
c*** This subroutine completes Gaussian forward and backwrad substitutions on factors BK, having N rows
c*** and half-bandwidth IW. Right hand side LOADS overwritten by solution
c
       Implicit real*8 (a-h, o-z)
       real*8 bk(*),loads(*)
       loads(1)=loads(1)/bk(1)
       do 1 i=2,n
               sum=loads(i)
               i1=i-1
               nkb=i-iw
            if(nkb) 2,2,3
  2         nkb=1
  3         do 4 k=nkb,i1
               jn=(i-k)*n+k
               sum=sum-bk(jn)*loads(k)
  4         continue
               loads(i)=sum/bk(i)
  1    continue
       do 5 jj=2,n
               i=n-jj+1
               sum=0.0D0
               i1=i+1
               nkb=i+iw
            if(nkb-n) 7,7,6
  6         nkb=n
  7         do 8 k=i1,nkb
               jn=(k-i)*n+i
  8         sum=sum+bk(jn)*loads(k)
               loads(i)=loads(i)-sum/bk(i)
  5    continue
       return
       end 
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine fmkdke(km,ikm,kp,ikp,c,ic,ke,ike,kd,ikd,
     *                      dof,nod,tot,theta,dtkQ,biot,c2)
c
c*** This subroutine forms coupled elemental matrices KE and KD from ***
c*** elastic stiffness KM,fluid "stiffness" KP and coupling matrix C ***
c
       Implicit none
       integer ikm,ikp,ic,ike,ikd,dof,nod,tot,i,j,k,m
       real*8 c(16,4),ke(20,20),kd(20,20),c2(16,4)
       real*8 theta,km(16,16),kp(4,4),dtkQ(4,4),biot
c	nod=4
c	c=0.0d0
c 	dtkq=0.0d0
c	kp=-kp
c	dtkq=0.0d0
c	dtkq=100.0d0*dtkq
c	 write(4999,*) 
c	 write(4999,*) dtkq(1,1),kp(1,1)
       do 11 i=1,dof
		do 12 j=1,dof
 12		ke(i,j)=km(i,j)
		do 13 k=1,nod
			ke(i,dof+k)=biot*c(i,k)
 13		ke(dof+k,i)=biot*c(i,k)
 11    continue
       do 14 i=1,nod
		do 15 j=1,nod
 15		ke(dof+i,dof+j)=kp(i,j)
 14    continue
       do 17 i=1,dof
       do 17 j=1,tot
		kd(i,j)=(theta-1.0D0)*ke(i,j)
 17    ke(i,j)=theta*ke(i,j)
       m=dof+1
       do 18 i=m,tot
       do 18 j=1,dof
		kd(i,j)=ke(i,j)*theta
 18    ke(i,j)=kd(i,j)
       do 19 i=m,tot
       do 19 j=m,tot

		kd(i,j)= -ke(i,j)*(theta-1.0)*theta - dtkQ(i-dof,j-dof)*theta
 19     ke(i,j)=-ke(i,j)*theta*theta -  dtkQ(i-dof,j-dof)*theta
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine gauss(samp,gp)
c 
c*** This subroutine forms Gaussian quadrature abscissae and weights ***
c
       Implicit real*8 (a-h, o-z)
       integer gp
       real*8 samp(gp,2)
       go to(1,2,3,4,5,6,7),gp
  1    samp(1,1)=0.0D0
       samp(1,2)=2.0D0
       go to 100
  2    samp(1,1)=1.D0/dsqrt(3.D0)
       samp(2,1)=-samp(1,1)
       samp(1,2)=1.D0
       samp(2,2)=1.D0
       go to 100
  3    samp(1,1)=0.2D0*dsqrt(15.D0)
       samp(2,1)=0.0D0
       samp(3,1)=-samp(1,1)
       samp(1,2)=5.D0/9.D0
       samp(2,2)=8.D0/9.D0
       samp(3,2)=samp(1,2)
       go to 100
  4    samp(1,1)=.861136311594053D0
       samp(2,1)=.339981043584856D0
       samp(3,1)=-samp(2,1)
       samp(4,1)=-samp(1,1)
       samp(1,2)=.347854945137454D0
       samp(2,2)=.652145154862546D0
       samp(3,2)=samp(2,2)
       samp(4,2)=samp(1,2)
       go to 100
  5    samp(1,1)=.906179845938664D0
       samp(2,1)=.538469310105683D0
       samp(3,1)=0.0D0
       samp(4,1)=-samp(2,1)
       samp(5,1)=-samp(1,1)
       samp(1,2)=.236926885056189D0
       samp(2,2)=.478628670499366D0
       samp(3,2)=.568888888888889D0
       samp(4,2)=samp(2,2)
       samp(5,2)=samp(1,2)
       go to 100
  6    samp(1,1)=.932469514203152D0
       samp(2,1)=.661209386466265D0
       samp(3,1)=.238619186083197D0
       samp(4,1)=-samp(3,1)
       samp(5,1)=-samp(2,1)
       samp(6,1)=-samp(1,1)
       samp(1,2)=.171324492379170D0
       samp(2,2)=.360761573048139D0
       samp(3,2)=.467913934572691D0
       samp(4,2)=samp(3,2)
       samp(5,2)=samp(2,2)
       samp(6,2)=samp(1,2)
       go to 100
  7    samp(1,1)=.949107912342759D0
       samp(2,1)=.741531185599394D0
       samp(3,1)=.405845151377397D0
       samp(4,1)=0.0D0
       samp(5,1)=-samp(3,1)
       samp(6,1)=-samp(2,1)
       samp(7,1)=-samp(1,1)
       samp(1,2)=.129484966168870D0
       samp(2,2)=.279705391489277D0
       samp(3,2)=.381830050505119D0
       samp(4,2)=.417959183673469D0
       samp(5,2)=samp(3,2)
       samp(6,2)=samp(2,2)
       samp(7,2)=samp(1,2)
 100   continue
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine formln(derf,ider,funf,samp,isamp,i,j)
c
c***** This subroutine forms the shape functions FUN and their derivatives          *****
c***** DER for 4-Noded quadrilateral elements at Gaussian point(I,J)                *****
c
       Implicit real*8 (a-h, o-z)
       integer  ider,isamp,i,j
       real*8 derf(2,4),funf(4),samp(isamp,2)
       real*8 eta,xi,etam,etap,xim,xip
       
	
	if(j.eq.12) then
		xi=-1.0
		eta=0.0
	elseif(j.eq.23) then
		xi=0.0
		eta=1.0
	elseif(j.eq.34) then
		xi=1.0
		eta=0.0
	elseif(j.eq.41) then
		xi=0.0
		eta=-1.0
	else
		xi=samp(j,1)
		if (i.eq.0) then                                                                            n
			eta=1.0
		else
			eta=samp(i,1)
		endif
	endif
			
       etam=0.25D0*(1.0D0-eta)
       etap=0.25D0*(1.0D0+eta)
       xim =0.25D0*(1.0D0-xi)
       xip= 0.25D0*(1.0D0+xi)
       funf(1)=4.0D0*xim*etam
       funf(2)=4.0D0*xim*etap
       funf(3)=4.0D0*xip*etap
       funf(4)=4.0D0*xip*etam
       derf(1,1)=-etam
       derf(1,2)=-etap
       derf(1,3)=etap
       derf(1,4)=etam
       derf(2,1)=-xim
       derf(2,2)=xim
       derf(2,3)=xip
       derf(2,4)=-xip
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine fmquad(der,ider,fun,samp,isamp,i,j,nxi)
       Implicit real*8 (a-h, o-z)
c
c***** This subroutine computes local coordinates shape functions ******
c***** and their derivatives for an 8-node quadrilateral          ******
c
       integer ider,isamp,i,j
       real*8 der(2,8),fun(8),samp(isamp,2)
       real*8 eta,xi,etam,etap,xim,xip
       eta=samp(i,1)
       xi=samp(j,1)
	if (nxi.eq.2) xi=-1.d0

       etam=0.25D0*(1.D0-eta)
       etap=0.25D0*(1.D0+eta)
       xim=0.25D0*(1.D0-xi)
       xip=0.25D0*(1.D0+xi)
       der(1,1)=etam*(2.D0*xi+eta)
       der(1,2)=-8.D0*etam*etap
       der(1,3)=etap*(2.D0*xi-eta)
       der(1,4)=-4.D0*etap*xi
       der(1,5)=etap*(2.D0*xi+eta)
       der(1,6)=8.D0*etap*etam
       der(1,7)=etam*(2.D0*xi-eta)
       der(1,8)=-4.D0*etam*xi
       der(2,1)=xim*(xi+2.D0*eta)
       der(2,2)=-4.D0*xim*eta
       der(2,3)=xim*(2.D0*eta-xi)
       der(2,4)=8.D0*xim*xip
       der(2,5)=xip*(xi+2.D0*eta)
       der(2,6)=-4.D0*xip*eta
       der(2,7)=xip*(2.D0*eta-xi)
       der(2,8)=-8.D0*xim*xip
       fun(1)=4.D0*etam*xim*(-xi-eta-1.d0)
       fun(2)=32.D0*xim*etam*etap
       fun(3)=4.D0*etap*xim*(-xi+eta-1.D0)
       fun(4)=32.D0*xim*xip*etap
       fun(5)=4.D0*xip*etap*(xi+eta-1.D0)
       fun(6)=32.D0*xip*etap*etam
       fun(7)=4.D0*etam*xip*(xi-eta-1.D0)
       fun(8)=32.D0*xim*xip*etam
      return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine formb(bee,ibee,deriv,ideriv,nod)
c
c**This subroutine forms strain-displacement matrix for plane problem**
c
       Implicit none
	 integer ibee,ideriv,nod
       real*8 bee(ibee,16),deriv(2,8)
       integer k,l,m
       do 1 m=1,nod
       k=2*m
       l=k-1
       bee(1,l)=deriv(1,m)
       bee(3,k)=deriv(1,m)
       bee(2,k)=deriv(2,m)
       bee(3,l)=deriv(2,m)
   1   continue
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine fmdeps(dee,idee,e,v)
c
c************ This suboutine forms the elastic plane strain  ***********
c************ stress/strain matrix                           ***********
c
       Implicit real*8 (a-h, o-z)
       integer idee
       real*8 dee(idee,idee),e,v,v1
       v1=1.D0-v
       c=E/((1.D0+v)*(1.D0-2.D0*v))
       dee(1,1)=v1*c
       dee(2,2)=v1*c
       dee(3,3)=.5D0*c*(1.D0-2.D0*v)
       dee(1,2)=v*c
       dee(2,1)=v*c
       dee(1,3)=0.D0
       dee(3,1)=0.D0
       dee(2,3)=0.D0
       dee(3,2)=0.D0
       return
       end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine formkv(bk,km,ikm,g,n,dof)
c
c*****This subroutine assembles element matrices into symmetrical*****
c***** band global matrix(stored as a vector)                    *****
c
       Implicit NONE
       real*8 bk(800000),km(20,20)
       integer ikm,g(20),n,dof,i,j,cd,val
       do 1 i=1,dof
       if(g(i).eq.0)go to 1
       do 5 j=1,dof
       if(g(j).eq.0)go to 5
       cd=g(j)-g(i)+1
       if(cd-1)5,4,4
    4  val=n*(cd-1)+g(i)
	 bk(val)=bk(val)+km(i,j)
    5  continue
    1  continue
      continue
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine matmul(a,ia,b,ib,c,ic)
c
c*********** This subroutine performs matrix multiply A*B=C ************
c
       Implicit NONE
       integer ia,ib,ic,l,m,n,i,j,k
       real*8 a(ia,ib),b(ib,ic),c(ia,ic),x
       do 1 i=1,ia
       do 1 j=1,ic
       x=0.0
       do 2 k=1,ib
   2   x=x + a(i,k)*b(k,j)
       c(i,j)=x
   1   continue
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine mvmult(m,im,v1,k,y)
c
c********* This subroutine performs multiply A matrix by a vector ******
c
       Implicit none
       integer im,k,l,i,j
       real*8 m(im,k),v1(k),y(im),x
       do 1 i=1,im
       x=0.0d0
       do 2 j=1,k
  2    x=x+m(i,j)*v1(j)
       y(i)=x
       if(im.ne.k)go to 1
  1    continue	 
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine twoby2(jac,ijac,jac1,ijac1,det)
c
c********** This subroutine inverts the 2x2 Jacobian Matrix ************
c
       Implicit real*8 (a-h, o-z)
       integer ijac,ijac1,k,l
       real*8 jac(2,2),jac1(2,2), det
       det=jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
       if( det .ne. 0.0) go to 65
65     jac1(1,1)=jac(2,2)
       jac1(1,2)=-jac(1,2)
       jac1(2,1)=-jac(2,1)
       jac1(2,2)=jac(1,1)
       do 1 k=1,2
       do 1 l=1,2
   1   jac1(k,l)=jac1(k,l)/det
       return
       end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine null( a,m,n)
c
c**************** This subroutine  nulls a mxn matrix *****************
c
       Implicit none
       integer m,n,i,j
       real*8 a(m,n)
       do 1 i=1,m
       do 1 j=1,n
   		a(i,j)=0.0
1	 continue
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine matran( a,ia,b,ib)
c
c*****************This subroutine transposes a mxn matrix **************
c
       Implicit real*8 (a-h, o-z)
       integer ia,ib,m,n,i,j
       real*8 a(ia,ib), b(ib,ia)
       do 1 i=1,ib
       do 1 j=1,ia
  1    a(j,i)=b(i,j)
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine matadd(a,ia,b,ib)
c
c**************** This subroutine adds two matrices A = A + B ***********
c
       Implicit NONE
       integer ia,ib,i,j
       real*8 a(ia,ib),b(ia,ib)

       do 1 i=1,ia
       do 1 j=1,ib
  1    a(i,j)=a(i,j)+b(i,j)
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
       subroutine formtb(kb,ikb,km,ikm,g,w,dof)
c
c******** This subroutine assembles element matrices into        *******
c******** unsymmetrical band globle matix(stored as a rectangle) *******
c
       Implicit NONE
       real*8 kb(5000,5000),km(20,20)
       integer ikb,ikm,g(20),w,dof,i,j,cd

       do 1 i=1,dof
       if(g(i).eq.0) go to 1
       do 2 j=1,dof
       if(g(j).eq.0) go to 2
       cd=g(j)-g(i)+w+1
c	print*,"cd=",cd,", i=",i,", j=",j,", g(i)=",g(i),"g(j)=",g(j),"w=",w
       kb(g(i),cd)=kb(g(i),cd)+km(i,j)
	if (i.eq.j) then
	endif
  2    continue
  1    continue
c	!!stop '======'
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine nulvec(vec,n)
c
c******************** This subroutine nulls a vector *******************
c
       Implicit real*8 (a-h, o-z)
	 integer i,n
       real*8 vec(n)
       do 1 i=1,n
   1   vec(i)=0.0
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         subroutine banred(bk,n,iw)
c
c******* This subroutine performs gaussian reduction of       *********
c******* the stiffness matrix; stored as a vector bk(n*(iw+1)) *********
c
         Implicit real*8 (a-h, o-z)
         real*8 bk(*)

         do 1 i=2,n
         il1=i-1
         kbl=il1+iw+1
         if (kbl-n) 3, 3, 2
    2    kbl=n
    3    do 1 j=i,kbl
         ij=(j-i)*n+i
         sum=bk(ij)
         nkb=j-iw
         if(nkb)4,4,5
    4    nkb=1
    5    if(nkb-il1)6,6,8
    6    do 7 m=nkb,il1
         ni=(i-m)*n+m
         nj=(j-m)*n+m
	 if ( bk(ni) .eq. 0.0D0 .or. bk(nj) .eq. 0.0D0) go to 7
        sum=sum-bk(ni)*bk(nj)/bk(m)
    7  continue
    8    bk(ij)=sum
    1    continue
         return
         end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine bantml(kb,ikb,loads,ans,n,iw)
c
c******** This suborutine multiplies an unsymmetric banded ***********
c******** matrix 'kb' by the vector 'loads'                *********** 
c
      Implicit none
	integer ikb,n,iw
      real*8 kb(5000,5000),loads(*),ans(*)
	integer i,k,l,j
	real*8 x
!	print*,"n,iw=",n,iw
      do 1 i=1,n

		x=0.0d0
		k=iw+2
		l=iw+iw+1
         do 2 j=1,l
         k=k-1
         if(i-k+1.gt.n)goto 2
         if(i-k+1.lt.1)goto 2
         x=x+kb(i,j)*loads(i-k+1)
    2    continue
         ans(i)=x
    1    continue
         return
         end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
        subroutine MSMULT(A,C,M,N)
c
c        This Subroutine Multiples a Matrix by A Scalar
c
       Implicit NONE
	 INTEGER M,N
	 Real*8 A(m,n),C
	 INTEGER I,J

	 Do 1 I=1,M
	 Do 1 j=1,N
1       A(I,J) = A(I,J)*c
        return
        end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine VOL2D(BEE,IBEE,VOL,NOD)
c
c        This Subroutine Forms a Vector containing the Derivatives of the Shape Function
c        (Plane 2D)
c
       Implicit NONE
	 INTEGER IBEE,NOD
c	 Real*8 BEE(ibee,16), VOL(16)
	Real*8 BEE(ibee,*), VOL(*)
	INTEGER M,K,L

	 do 1 M = 1, NOD
	 k = 2*M
	 L = K-1
	! VOL(L) = BEE(2,L)
	! VOL(K) = BEE(1,k)
	VOL(L) = BEE(1,L)
	 VOL(K) = BEE(2,k)
1       continue
	 return
	 end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c***********************************************************************
        subroutine invar(s1,s2,s3,s4,amean,dsbar,alode)
c******************* Forms invariants of the stress tensor *************
        Implicit none
	 real*8 s1,s2,s3,s4,amean,dsbar,alode
	 real*8 aa,dev1,dev2,dev3,sj3,sine
          aa=(0.5D0*((s1-s2)**2.D0+(s2-s3)**2.D0+(s3-s1)**2.D0
	1        +6.D0*s4**2.D0))
	  if(aa.lt.0.d0) then
	     !!stop
	  end if
        dsbar=dsqrt(0.5D0*((s1-s2)**2.D0+(s2-s3)**2.D0+(s3-s1)**2.D0
	1        +6.D0*s4**2.D0))
        amean=(s1+s2+s3)/3.D0
        if(dsbar.eq.0.0) then
        alode=0.0D0
        else
        dev1=(2.D0*s1-s2-s3)/3.D0
        dev2=(2.D0*s2-s3-s1)/3.D0
        dev3=(2.D0*s3-s1-s2)/3.D0
        sj3=dev3*(dev1*dev2-s4*s4)
        sine = -13.5D0*sj3/dsbar**3.0D0
        if(sine.gt.1.D0)  sine = 1.0D0
        if(sine.lt.-1.D0) sine = -1.0D0
 	  alode = dasin(sine)/3.D0
        end if
        return
        end

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc***********************************************************************
      subroutine fmbrad(bee,ibee,deriv,ideriv,fun,
     *                         coord,icoord,vol,sum,nod)
c******** Forms strain-displacement matrix for axisymmetric strain *****
      Implicit none
	integer ibee,ideriv,icoord,nod
      real*8 bee(ibee,*),deriv(ideriv,*),fun(*),coord(icoord,*),vol(*)
	real*8 sum
	integer k,m,l
c      real*8 bee(ibee,16),deriv(2,8),fun(8),coord(8,2),
c     *            vol(16)
        sum=0.D0
        do 1 k=1,nod
 1      sum=sum+fun(k)*coord(k,1)
        do 2 m=1,nod
        k=2*m
        l=k-1
        bee(1,l)=deriv(1,m)
        bee(3,k)=deriv(1,m)
        vol(k)=deriv(2,m)
        bee(2,k)=deriv(2,m)
        bee(3,l)=deriv(2,m)
        bee(4,l)=fun(m)/sum
        vol(l)=bee(1,l)+bee(4,l)
 2      continue
        return
        end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc***********************************************************************
        subroutine fmdrad(dee,idee,e,v)
c**** Form 4x4 stress-strain matrix DEE for axisymmetric elasticity ****
        Implicit real*8 (a-h, o-z)
        dimension dee(4,4)
        v1=v/(1.D0-v)
        vv=(1.D0-2.D0*v)/(1.D0-v)*0.5D0
        dee(1,1)=1.D0
        dee(2,2)=1.D0
        dee(4,4)=1.D0
        dee(3,3)=vv
        dee(1,2)=v1
        dee(2,1)=v1
        dee(1,4)=v1
        dee(4,1)=v1
        dee(2,4)=v1
        dee(4,2)=v1
        do 1 i=1,4
        do 1 j=1,4
        dee(i,j)=dee(i,j)*e*(1.D0-v)/(1.D0-2.D0*v)/(1.D0+v)
  1     continue
        return
        end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc***********************************************************************
        subroutine fmdpl(dpl,idpl,dee,idee,s1,s2,s3,s4,dsbar,sbary,e,v)
c*********** Form von Mises plastic stress-strain matrix ***************
        Implicit real*8 (a-h, o-z)
        dimension dpl(4,4),dee(4,4),pl(4,4)
        sp=(s1+s2+s3)/3.D0
        sr1=s1-sp
        st1=s2-sp
        sz1=s3-sp
        sbar=dsqrt(0.5D0*((s1-s2)**2.D0+(s2-s3)**2.D0+(s3-s1)**2.D0+
	1       6.D0*s4**2.D0))
        if(sbar.gt.sbary)fac=1.D0
        if(sbar.le.sbary)fac=(dsbar-sbary)/(dsbar-sbar)
        pl(1,1)=sr1*sr1
        pl(1,2)=sr1*sz1
        pl(2,1)=sr1*sz1
        pl(1,3)=sr1*s4
        pl(3,1)=sr1*s4
        pl(1,4)=sr1*st1
        pl(4,1)=sr1*st1
        pl(2,2)=sz1*sz1
        pl(2,3)=sz1*s4
        pl(3,2)=sz1*s4
        pl(2,4)=sz1*st1
        pl(4,2)=sz1*st1
        pl(3,3)=s4*s4
        pl(3,4)=st1*s4
        pl(4,3)=st1*s4
        pl(4,4)=st1*st1
        do 1 i=1,4
        do 1 j=1,4
        pl(i,j)=pl(i,j)*3.D0*e/(2.D0*sbar*sbar*(1.D0+v))
  1     dpl(i,j)=dee(i,j)-fac*pl(i,j)
        return
        end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc***********************************************************************
        subroutine MOCOPL(phi,psi,E,v,sx,sy,sz,txy,pl)
c*********** This  subroutine forms the plastic stress/strain **********
c*********** matrix for a Mohr-Coulomb material (phi,PSI IN DEGREES )*********
c
        Implicit real*8 (a-h, o-z)
        dimension pl(4,4), col(4), row(4)
	PI   = 4.D0*DATAN(1.D0)
      SNPH = Dsin(phi)
	SNPS = Dsin(psi)
	SQ3  = Dsqrt(3.D0)
	CC   = 1.D0 - 2.D0*v
	Dx = (2.D0*sx - Sy - sz)/3.D0
	Dy = (2.D0*sy - Sz - sx)/3.D0
	Dz = (2.D0*sz - Sx - sy)/3.D0
	D2 = Dsqrt(-DX*DY - DY*DZ -DZ*DX+TXY*TXY)
	D3 = DX*DY*DZ - DZ*TXY*TXY
	TH = - 3.D0*SQ3*D3/(2.D0*D2**3.D0)
	IF(TH .gt. 1) TH =  1.D0
	IF(TH .lt. -1) TH = -1.D0
	TH = DASIN(TH)/3.D0
	SNTH = DSIN(TH)
c
c   If ABS(SNTH) > 0.49 the corner is smoothed
c
	IF( DABS(SNTH) .GT. .49D0) THEN
                	 SIG = -1.D0
	IF(SNTH .LT. 0.) SIG =  1.D0
	RPH = SNPH*(1.D0 + v)/3.D0
	RPS = SNPS*(1.D0 + v)/3.D0
	CPS = 0.25D0*SQ3/D2*(1.D0 + SIG*SNPS/3.D0)
	CPH = 0.25D0*SQ3/D2*(1.D0 + SIG*SNPH/3.D0)
	COL(1) = RPH + CPH*((1.D0 - v)*DX + v*(DY + DZ))
	COL(2) = RPH + CPH*((1.D0 - v)*DY + v*(DZ + DX))
	COL(3) = CPH*CC*TXY
	COL(4) = RPH + CPH*((1.D0 - v)*DZ + v*(DX + DY))
	ROW(1) = RPS + CPS*((1.D0 - v)*DX + v*(DY + DZ))
	ROW(2) = RPS + CPS*((1.D0 - v)*DY + v*(DZ + DX))
	ROW(3) = CPS*CC*TXY
	ROW(4) = RPS + CPS*((1.D0 - v)*DZ + v*(DX + DY))
	   EE = E/((1.D0+v)*CC*(RPH*SNPS+2.D0*CPH*CPS*D2*D2*CC))
	ELSE
	ALP = DATAN(DABS((SX - SY)/(2.D0*TXY)))
      CA  = DCOS(ALP)
	SA  = DSIN(ALP)
	DD  = CC*SA
	S1  = 1.D0
	S2 = 1.D0
	IF((SX-SY) .LT. 0.0D0) S1 = -1.D0
	IF(TXY .LT. 0) S2 = -1.D0
	COL(1) = SNPH + S1*DD
	COL(2) = SNPH - S1*DD
	COL(3) = S2*CC*CA
	COL(4) = 2.D0*v*SNPH 
	ROW(1) = SNPS + S1*DD
	ROW(2) = SNPS - S1*DD
	ROW(3) = S2*CC*CA
	ROW(4) = 2.*v*SNPS 
	   EE = E/(2.D0*(1.D0+v)*CC*(SNPH*SNPS+CC))
        ENDIF
	Do 1 i =1, 4
	Do 1 j =1, 4
1	pl(i,j) = EE*ROW(i)*COL(j)
	return
	end
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine MOCOUF(phi,c,sigm, dsbar,theta,f)
c*********** This subroutine calculate the value of the yield function****
c*********** for a Mohr-Coulomb material (phi in radian)  *****************
c                  
      Implicit none
	real*8 phi,c,sigm, dsbar,theta,f
	real*8 SNPH,csph,csth,snth,temp
	   SNPH = dsin(phi)
     	   csph = dcos(phi)
	   csth = dcos(theta)
	   snth = dsin(theta)
         temp = dcos(theta)/dsqrt(3.D0)+dsin(theta)*dsin(phi)/3.D0
cMa.Z          temp = dcos(theta)/dsqrt(3.D0)-dsin(theta)*dsin(phi)/3.D0
cSf.Z         temp = dcos(theta)/dsqrt(3.D0)+dsin(theta)*dsin(phi)/3.D0
         F = sigm*dsin(phi) - dsbar*temp + c*dcos(phi)
	return
	end
c********
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc***********************************************************************
        subroutine MCY(phi,c,sx,sy,sxy,f)
c*********** This subrouti4ne calculate the value of the yield function****
c*********** for a Mohr-Coulomb material (phi in radian)  ****************
c
c                Note: f < 0 represents plastic yielding
c
         Implicit real*8 (a-h, o-z)
	   s1 = (sx+sy)*0.5D0+dsqrt(0.25D0*(sx-sy)**2.D0 + sxy**2.D0)
	   s2 = (sx+sy)*0.5D0-dsqrt(0.25D0*(sx-sy)**2.D0 + sxy**2.D0)
	   f  = (s1+s2)*0.5D0*dsin(phi) - (s1-s2)*0.5D0 + c*dcos(phi)
c	   f  = (s1+s2)*0.5D0*dsin(phi) - (s1-s2)*0.5D0 - c*dcos(phi)
	return
	end
c*cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc**********************************************************************
        subroutine vecadd(a,b,c,n)
        Implicit real*8 (a-h, o-z)
        real*8 a(n), b(n), c(n)
	do 1 i=1, n
1       c(i) = a(i) + b(i)
	return
	end
Ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.................Carl Change.................
      SUBROUTINE INTERP(X,Y,N,XO,YO)
	 IMPLICIT NONE
C	 REAL*8  X(200),Y(200),XO,YO
       INTEGER N,I
	 REAL*8  X(N),Y(N),XO,YO
      IF(XO.GE.X(N)) YO=Y(N)
      IF(XO.GE.X(N)) RETURN
	IF(XO.LE.X(1)) YO=Y(1)
      IF(XO.LE.X(1)) RETURN
      DO 11 I=2,N
      IF(XO.GE.X(I)) GO TO 11
      YO=Y(I-1) +(XO-X(I-1))*(Y(I)-Y(I-1))/(X(I)-X(I-1))
      RETURN
  11  CONTINUE
      END
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc***********************************************************************
        subroutine checon1(loads, oldlds, n, tol, icon,ns,iter,il)
c        subroutine checon1(loads, oldlds, n, tol, icon,diff,ns,iter,il)
c*********** This subroutine sets icon to zero if the relative *********
c*********** change in vectors 'loads' and 'oldlds' > 'tol'    *********
      Implicit none
	integer n, icon,ns,iter,il
      real*8 loads(5000),oldlds(5000),tol
	real*8 temp(5000)
	real*8 bigd,aa
	integer i

	 icon = 1
	 bigd = 0.0D0
	 do i=1,n
		aa=dabs(loads(i))
		if (aa.le.1.0d-30)   aa=1.d0
c	  temp(i)=(dabs(loads(i))-dabs(oldlds(i)))/aa
		temp(i)=dabs(loads(i)-oldlds(i))/aa
	 end do
	 bigd=temp(1)
	 do i=2,n
		if (temp(i).ge.bigd) bigd=temp(i)
	 end do
	 if (bigd.le.tol) then
		icon=1
	 else
		icon=0
	 end if

	 do i=1,n
		oldlds(i)=loads(i)
	 end do
	 
	 return
	 end
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine quotca(samp,isamp,det,term,quot,i,j)
c
c********* This subroutine performs multiply A matrix by a vector ******
c
       Implicit real*8 (a-h, o-z)
       integer im,k,l,i,j
       real*8 samp(isamp,2)
         quot=det*Samp(i,2)*Samp(j,2)*term
       return
       end


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
 		!!检查过了，跟马博士的结果一样
       subroutine loadps(temp1,temp3,pwci,pwco,force,isamp,nk,ns,
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf
     1   ,ijac,ijac1,term)
c
c********* This subroutine performs multiply A matrix by a vector ******
c
       Implicit real*8 (a-h, o-z)

       integer p,q,nxe,nye,icoord,icordf,g(20),ider,gp,isamp
     1             ,ao,al,am,an,ap,aq,ar,as,ns,nk(3000),tot
       real*8 sl,cl,sr,cr,ang,pi
       real*8 ifacl,ifacr,ofacr,ofacl
       real*8 press(3,2)
       real*8 coord(8,2),coordf(4,2),elcod(2,3)
       real*8 x(5000),Y(5000),pgash(2),dgash(2)
	 real*8 der(2,8),fun(8)
       real*8 samp(isamp,2)
	 real*8 force(5000)
	 real*8 jac(2,2),jac1(2,2),det,quot
	integer nodep(40,80,8),nodeg(40,80,20)
C	COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)
c	COMMON/LOAD1/radius(51),pofix(1000),point(1000,2),gtheta,
c	1gravy,pressa(1000,6),spoke(41),dofix(1000,3)
	COMMON/LOAD1/radius(82),rad2(42),pofix(1000),point(1000,2),gtheta,
	1gravy,pressa(1000,6),spoke(41),dofix(1000,3)
	COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)
	1,lodpt(1000),nbound,ndrill,ndirect
	COMMON/flowc/nflowi,nflowo
	integer noprs(3)        
      ang=3.1415926/(2.0*nxe)
	ang=atan(1.0d0)/nxe*2.0d0
c***********setting input data. These input data should be 
c           readen from data file
        do 9201 iedge=1,nxe
		ao=2*iedge-1
		al=ao+1
		am=al+1
c*************nopre  number of node
		noprs(1)=ao
		noprs(2)=al
		noprs(3)=am
c*********** pressure in each node, 1---x direction, 2--y direction
		t3=iedge*ang
!	t3=0.0d0
		t2=iedge*ang-ang/2.d0
		t1=iedge*ang-ang
	press(1,1)=-temp3*dcos(t1)**2.d0/dr-temp1*dsin(t1)**2.d0/dr
	press(1,2)=-temp1*dcos(t1)*dsin(t1)/dr+temp3*dsin(t1)*dcos(t1)/dr
	press(2,1)=-temp3*dcos(t2)**2.d0/dr-temp1*dsin(t2)**2.d0/dr
	press(2,2)=-temp1*dcos(t2)*dsin(t2)/dr+temp3*dsin(t2)*dcos(t2)/dr
	press(3,1)=-temp3*dcos(t3)**2.d0/dr-temp1*dsin(t3)**2.d0/dr
	press(3,2)=-temp1*dcos(t3)*dsin(t3)/dr+temp3*dsin(t3)*dcos(t3)/dr

c	press(1,1)=-temp1*dcos(t1)**2.d0/dr-temp3*dsin(t1)**2.d0/dr
c	press(1,2)=-temp3*dcos(t1)*dsin(t1)/dr+temp1*dsin(t1)*dcos(t1)/dr
c	press(2,1)=-temp1*dcos(t2)**2.d0/dr-temp3*dsin(t2)**2.d0/dr
c	press(2,2)=-temp3*dcos(t2)*dsin(t2)/dr+temp1*dsin(t2)*dcos(t2)/dr
c	press(3,1)=-temp1*dcos(t3)**2.d0/dr-temp3*dsin(t3)**2.d0/dr
c	press(3,2)=-temp3*dcos(t3)*dsin(t3)/dr+temp1*dsin(t3)*dcos(t3)/dr
c	  call tungeo(iedge,1,nxe,nye,spoke,radius,coord,icoord,
c	1         coordf,icordf,g,x,y,angl,angm)
c******avoid to call tungeo again and again*****************************
		do in=1,tot
			g(in)=nodeg(iedge,1,in)
		end do
		do in=1,icoord
			coord(in,1)=x(nodep(iedge,1,in))
			coord(in,2)=y(nodep(iedge,1,in))
		end do
		do in=1,icordf
			coordf(in,1)=x(nodep(iedge,1,(2*in-1)))
			coordf(in,2)=y(nodep(iedge,1,(2*in-1)))
		end do
c*************************************************************************
		do 9102 iodeg=1,3
          elcod(1,iodeg)=x(noprs(iodeg))
9102		elcod(2,iodeg)=y(noprs(iodeg))
c*************** get B matrix for solid, der here = deriv in Owen
		gp=2
		do 9105 ig=1,gp 
			call gauss(samp,gp)
c************** xi=weigh in gauss in Owen
			call fmquad(der,ider,fun,samp,gp,ig,1,2)

			do 9110 idofn=1,2
				pgash(idofn)=0.d0
				dgash(idofn)=0.d0
				do 9110 iodeg=1,3
				pgash(idofn)=pgash(idofn)+press(iodeg,idofn)*fun(iodeg)
9110			dgash(idofn)=dgash(idofn)+elcod(idofn,iodeg)*der(2,iodeg)

			dvolu=1.d0
			pxcom=dgash(1)*pgash(2)-dgash(2)*pgash(1)
			pycom=dgash(1)*pgash(1)+dgash(2)*pgash(2)
			continue
			do 9120 knode=1,3
				if(g(2*knode+3).ne.0) then
c	    nk(g(2*knode+3))=g(2*knode+3)
		force(g(2*knode+3))=force(g(2*knode+3))+fun(knode)*pxcom*dvolu
				end if
				if(g(2*knode+4).ne.0) then
c	    nk(g(2*knode+4))=g(2*knode+4)
		force(g(2*knode+4))=force(g(2*knode+4))+fun(knode)*pycom*dvolu
				end if
9120			continue
			force(g(18))=pwci
			force(g(19))=pwci
9105		continue
9201    continue

c       if ns=0, set intinal bondary condition
c    when outside edge is in q input, porepressure in outside
c     edge have to be calculated -------Aug.9
	if (ns.eq.0.or.nflowo.eq.1) then
        do 9301 iedge=1,nxe
		aq=nye*(3*nxe+2)+2*iedge-1
		ar=aq+1
		as=ar+1
c*************nopre  number of node
		noprs(1)=as
		noprs(2)=ar
		noprs(3)=aq
c*********** pressure in each node, 1---x direction, 2--y direction
		t3=iedge*ang
!	t3=0.0d0
		t2=iedge*ang-ang/2.d0
		t1=iedge*ang-ang
c  only if ns.eq.0 and no fixed node, add pressure boudary conditions.
c  Otherwise, only add porepressure in outside edge according to q --Aug.9
		if(ns.eq.0) then
	press(3,1)=-temp3*dcos(t1)**2.d0/dr-temp1*dsin(t1)**2.d0/dr
	press(3,2)=-temp1*dcos(t1)*dsin(t1)/dr+temp3*dsin(t1)*dcos(t1)/dr
	press(2,1)=-temp3*dcos(t2)**2.d0/dr-temp1*dsin(t2)**2.d0/dr
	press(2,2)=-temp1*dcos(t2)*dsin(t2)/dr+temp3*dsin(t2)*dcos(t2)/dr
	press(1,1)=-temp3*dcos(t3)**2.d0/dr-temp1*dsin(t3)**2.d0/dr
	press(1,2)=-temp1*dcos(t3)*dsin(t3)/dr+temp3*dsin(t3)*dcos(t3)/dr

	press(3,1)=-temp1*dcos(t1)**2.d0/dr-temp3*dsin(t1)**2.d0/dr
	press(3,2)=-temp3*dcos(t1)*dsin(t1)/dr+temp1*dsin(t1)*dcos(t1)/dr
	press(2,1)=-temp1*dcos(t2)**2.d0/dr-temp3*dsin(t2)**2.d0/dr
	press(2,2)=-temp3*dcos(t2)*dsin(t2)/dr+temp1*dsin(t2)*dcos(t2)/dr
	press(1,1)=-temp1*dcos(t3)**2.d0/dr-temp3*dsin(t3)**2.d0/dr
	press(1,2)=-temp3*dcos(t3)*dsin(t3)/dr+temp1*dsin(t3)*dcos(t3)/dr
		else
			press(3,1)=0.d0
			press(3,2)=0.d0
			press(2,1)=0.d0
			press(2,2)=0.d0
			press(1,1)=0.d0
			press(1,2)=0.d0
		end if
c	  call tungeo(iedge,nye,nxe,nye,spoke,radius,coord,icoord,
c	1         coordf,icordf,g,x,y,angl,angm)
c******avoid to call tungeo again and again*****************************
		do in=1,tot
			g(in)=nodeg(iedge,nye,in)
		end do
		do in=1,icoord
			coord(in,1)=x(nodep(iedge,nye,in))
			coord(in,2)=y(nodep(iedge,nye,in))
		end do
		do in=1,icordf
			coordf(in,1)=x(nodep(iedge,nye,(2*in-1)))
			coordf(in,2)=y(nodep(iedge,nye,(2*in-1)))
		end do
c*************************************************************************

		do 9202 iodeg=1,3
			elcod(1,iodeg)=x(noprs(iodeg))
9202		elcod(2,iodeg)=y(noprs(iodeg))

c*************** get B matrix for solid, der here = deriv in Owen
		gp=2
		do 9205 ig=1,gp 
			call gauss(samp,gp)
c************** xi=weigh in gauss in Owen
			call fmquad(der,ider,fun,samp,gp,ig,1,2)

			do 9210 idofn=1,2
				pgash(idofn)=0.d0
				dgash(idofn)=0.d0
				do 9210 iodeg=1,3
			pgash(idofn)=pgash(idofn)+press(iodeg,idofn)*fun(iodeg)
9210			dgash(idofn)=dgash(idofn)+elcod(idofn,iodeg)*der(2,iodeg)

			dvolu=1.d0
			pxcom=dgash(1)*pgash(2)-dgash(2)*pgash(1)
			pycom=dgash(1)*pgash(1)+dgash(2)*pgash(2)
			
			do 9220 knode=1,3
				kn=2*knode+11
				if(kn.ge.17) kn=1
				if(g(kn).ne.0) then
					force(g(kn))=force(g(kn))+fun(knode)*pxcom*dvolu
				end if
				kn=2*knode+12
				if(kn.ge.17) kn=2
				if(g(kn).ne.0) then
					force(g(kn))=force(g(kn))+fun(knode)*pycom*dvolu
				end if
	
9220			continue
			force(g(20))=pwco
			force(g(17))=pwco
9205      continue
9301    continue
      end if
777	continue
9333	continue	

        do 9302 iedge=1,nxe
		do in=1,tot
			g(in)=nodeg(iedge,nye,in)
		end do
c*************** get B matrix for solid, der here = deriv in Owen
			force(g(20))=pwco
c		nk(g(17))=g(17)
			force(g(17))=pwco
9302    continue
c********we think that extra force is loaded when ns=0 ********
c note: this part is just for extra load. It means that the normal load is 
c force pressure and pore pressure in inside and outside of well( it is done as 
c above code). Except normal load condition, if user want to  
c load orther force, use following code.
      if (ns.eq.0) then
c**********nodal load*********************
		if(nplod.eq.0) go to 11
		do 10 ip=1,nplod
			inode=nf(nodpt(ip),1)
			if (inode.ne.0) then
				force(inode)=force(inode)+point(ip,1)
			end if
			jnode=nf(nodpt(ip),2)
			if (jnode.ne.0) then
				force(jnode)=force(jnode)+point(ip,2)
			end if
  10		continue	
  11		continue
c***********gravity load*******************
c********** loop over each element********
c  set thickness=1 and material mass density=2
c note:  in fact, they should be input data********modify after***July,EQ
c        thick=term
		if(ngrav.eq.0) go to 21
c********average dansity of stone is about 2650kg/m^3=2.65*10^-6kg/mm^3
c  Dancy*gravy=szg*10e6  **************** Sept3,2000 EQ
c****default gtheta=0.d0
		gtheta=0.d0

		do 20 ii=1,nxe
		do 20 jj=1,nye

c	  gxcom=dense*gravy*dsin(gtheta)
c	  gycom=-dense*gravy*dcos(gtheta)
			gxcom=szg*1.0d6*dsin(gtheta)
			gycom=-szg*1.0d6*dcos(gtheta)
c******advoice to call tungeo again and again*****************************
			do in=1,tot
				g(in)=nodeg(ii,jj,in)
			end do
			do in=1,icoord
				coord(in,1)=x(nodep(ii,jj,in))
				coord(in,2)=y(nodep(ii,jj,in))
			end do
			do in=1,icordf
				coordf(in,1)=x(nodep(ii,jj,(2*in-1)))
				coordf(in,2)=y(nodep(ii,jj,(2*in-1)))
			end do
c*************************************************************************
			gp=2

			do 30 i=1,gp
			do 30 j=1,gp
				call gauss(samp,gp)
				call fmquad(der,ider,fun,samp,gp,i,j,1)
c
c************ Convert local coordinates to global coordinates ***********
				call matmul(der,ider,coord,icoord,jac,ijac)
				call twoby2(jac,ijac,jac1,ijac1,det)
				call quotca ( samp,gp,det,term,quot,i,j )
c** if axial symmetry, it is another fomular
				do 40 in=1,icoord
					inp=nodep(ii,jj,in)
					if (nf(inp,1).ne.0) then
				force(nf(inp,1))=force(nf(inp,1))+gxcom*fun(in)*quot
					end if
					if (nf(inp,2).ne.0) then
				force(nf(inp,2))=force(nf(inp,2))+gycom*fun(in)*quot
					end if
 40				continue
 30			continue
 20		continue
 21		continue
c************distributed edge loads**********************************8
c
		if(nedge.eq.0) go to 51
		do 50 in=1,nedge
     			noprs(1)=nopress(in,1)
			noprs(2)=nopress(in,2)
			noprs(3)=nopress(in,3)
c*********** pressure in each node, 1---x direction, 2--y direction
			press(1,1)=pressa(in,1)
			press(1,2)=pressa(in,2)
			press(2,1)=pressa(in,3)
			press(2,2)=pressa(in,4)
			press(3,1)=pressa(in,5)
			press(3,2)=pressa(in,6)
	  
c******avoid to call tungeo again and again*****************************
			do 902 iodeg=1,3
			elcod(1,iodeg)=x(noprs(iodeg))
902			elcod(2,iodeg)=y(noprs(iodeg))
c*************** get B matrix for solid, der here = deriv in Owen
			gp=2
			do 905 ig=1,gp 
				call gauss(samp,gp)
c************** xi=weigh in gauss in Owen
				call fmquad(der,ider,fun,samp,gp,ig,1,2)

				do 910 idofn=1,2
					pgash(idofn)=0.d0
					dgash(idofn)=0.d0
					do 910 iodeg=1,3
			pgash(idofn)=pgash(idofn)+press(iodeg,idofn)*fun(iodeg)
910			dgash(idofn)=dgash(idofn)+elcod(idofn,iodeg)*der(2,iodeg)

				dvolu=1.d0
				pxcom=dgash(1)*pgash(2)-dgash(2)*pgash(1)
				pycom=dgash(1)*pgash(1)+dgash(2)*pgash(2)

				do 920 knode=1,3
				if(nf(noprs(knode),1).ne.0) then
				force(nf(noprs(knode),1))=force(nf(noprs(knode),1))
	1                           +fun(knode)*pxcom*dvolu
		
				end if
				if(nf(noprs(knode),2).ne.0) then
				force(nf(noprs(knode),2))=force(nf(noprs(knode),2))
	1                            +fun(knode)*pycom*dvolu
				end if
920				continue
905			continue
  50		continue
 51		continue
      end if

       return
       end
C***********************************************************************
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc      
	SUBROUTINE correctDP1(NLIM,RTOL,FTOL,H,sx,sy,sxy,sz,v,phi,coh,
     +                      ag, COUNT)
C-----------------------------------------------------------------------
      IMPLICIT DOUBLE PRECISION (A-H, O-Z)
      REAL*8 RTOL, FTOL
      REAL*8 R1, R2, R3, F1, F2, F3, H, H1, H2, HRATIO, C1, C2, C3
      REAL*8 DISC, FCNRP, DELR, T
      INTEGER NLIM, COUNT, NTERM, FLAG
	real*8 ff(301)

C
C       Compute sequence of points converging to the root
C
          call transform1(sx,sy,sxy,sr,st,ag)
         
c**** if sz<sr, re-get ramnian stress and return  Oct09,2000 EQ
       
      if( sz.le.st) then
         sima1=st
	   sima2=sz
	   sima3=sr
         ddx=(sima1-sima3)/300.d0
	   nnt=0
 100	   aav=10000.d0
	   do ii=1,300
	     xx=sima3+ii*ddx
c            call tranback1(sx,sy,sxy,sr,xx,ag)
            call invar(sr,xx,sz,0.d0,sigm,dsbar,theta)
            call mocouf(PHI,COH,sigm,dsbar,theta,f1)
	      ff(ii)=f1
	          if(dabs(ff(ii)).le.aav) then
	            aav=ff(ii)
	            nii=ii
	          end if
	   end do
	   nnt=nnt+1
	   pr=sima3+nii*ddx
	   if(pr.le.sz) then 
	      sz=pr
	      if(nnt.ge.1) go to 101
	      go to 100
	   end if
 101      call tranback1(sx,sy,sxy,sr,pr,ag)
          return

	else
c****** deal with tha case when sz>st
         sima1=sz
	   sima2=st
	   sima3=sr
         ddx=(sima1-sima3)/300.d0
	   nnt=0
 200	   aav=10000.d0
	   do ii=1,300
	     xx=sima3+ii*ddx
            call invar(sr,st,xx,0.d0,sigm,dsbar,theta)
            call mocouf(PHI,COH,sigm,dsbar,theta,f1)
	      ff(ii)=f1
	          if(dabs(ff(ii)).le.aav) then
	            aav=ff(ii)
	            nii=ii
	          end if
	   end do
	   nnt=nnt+1
	   pr=sima3+nii*ddx
	   if(pr.le.st) then 
	      st=pr
	      call tranback1(sx,sy,sxy,sr,st,ag)
	      return
	      if(nnt.ge.1) go to 201
	      go to 200
	   end if
 201     sz=pr 
          return

      end if
c*********
      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	subroutine displa1(n_angbkfix,iangbkfix,angbkfix,
     *	n_bkfix,iabkfix,
     * loads,tload,bk,n,nxe,nye,d0,mcc,dofix,nvfix,
c	1     mt2,mt3,loads0,nbound,ns)
 	1     mt2,mt3,loads0,nbound,ns,iw)
	IMPLICIT REAL*8(A-H,O-Z)
	real*8 loads(5000),tload(5000),bk(50000),dofix(1000,3)
c	real*8 loads(40000),tload(40000),dofix(1000,3)
	real*8 loads0(5000)
	integer mcc(1000,4)                                
C       COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)

	integer iabkfix(500,0:3)
	integer iangbkfix(500)
	real*8 angbkfix(500)

cMa.Z	m=2*nxe+1
cMa.Z      do 10 i=1,m  
cMa.Z	 ange=(i-1)*3.1414926d0/4.d0/nxe
cMa.Z	  if (nf(i,1).ne.0) then
cMa.Z	    dx=tload(nf(i,1))
cMa.Z	  else
cMa.Z	    dx=0.d0
cMa.Z	  end if
cMa.Z	  if (nf(i,2).ne.0) then
cMa.Z	    dy=tload(nf(i,2))
cMa.Z	  else
cMa.Z	    dy=0.d0
cMa.Z	  end if
c	fix displacement should be in r-direction***sept7,2000
c       d=dsqrt(dx**2.d0+dy**2.d0)
cMa.Z        d=dx*dcos(ange)+dy*dsin(ange)
cMa.Z	  dt=dy*dcos(ange)-dx*dsin(ange)
cMa.Z	  if(d.ge.0.95d0*d0) then
cMa.Z	  d0x=d0*dx/d
cMa.Z	  d0y=d0*dy/d
c        d0x=d0*dcos(ange)-dt*dsin(ange)
c	  d0y=d0*dsin(ange)+dt*dcos(ange)

	do 10 i=1,n_angbkfix !nodes around wells
	 ange=angbkfix(i)
	  if (nf(iangbkfix(i),1).ne.0) then
	    dx=tload(nf(iangbkfix(i),1))
	  else
	    dx=0.d0
	  end if
	  if (nf(iangbkfix(i),2).ne.0) then
c	    dy=tload(nf(i,2))
		dy=tload(nf(iangbkfix(i),2))
	  else
	    dy=0.d0
	  end if
c	fix displacement should be in r-direction***sept7,2000
c       d=dsqrt(dx**2.d0+dy**2.d0)
        d=dx*dcos(ange)+dy*dsin(ange)
	  dt=dy*dcos(ange)-dx*dsin(ange)
	  if(d.ge.0.95d0*d0) then
	  d0x=d0*dx/d
	  d0y=d0*dy/d
c        d0x=d0*dcos(ange)-dt*dsin(ange)
c	  d0y=d0*dsin(ange)+dt*dcos(ange)
	  if(nf(iangbkfix(i),1).ne.0) then
c	  loads(nf(i,1))=d0x*1.d200
c	  bk(nf(iangbkfix(i),1),nf(iangbkfix(i),1)+iw+1)=1.d200
	 bk(nf(iangbkfix(i),1))=1.d200
        end if
	  if(nf(iangbkfix(i),2).ne.0) then
c	  loads(nf(i,2))=d0y*1.d200
c       bk(nf(iangbkfix(i),2),nf(iangbkfix(i),2)+iw+1)=1.d200
	  bk(nf(iangbkfix(i),2))=1.d200
	  end if
	  end if
10    continue
c***********add extra fixed imformation
      !by Bashar: nvfix, is never defined and is zero.
	if(nvfix.ne.0) then
      do 20 i=1,nvfix
	mp=mcc(i,1)
	do jj=1,3
	 if(mcc(i,jj+1).eq.1) then
	  if(nf(mp,jj).ne.0) then
c	   bk(nf(mp,jj),nf(mp,jj)+iw+1)=1.d200
		bk(nf(mp,jj))=1.d200
	  end if
	 end if
	end do
 20   continue
      end if

c*******add initial displacement fix********** next mt2~mt3 will be replaced by ibkfix(i,3)
      !by Bashar:NBOUND: INITIAL CONDITION CODE (0-SPECIFY DISPLACEMENT, 1-SPECIFY STRESS)
      if(nbound.eq.0.and.ns.ne.0) then
cMa.Z	 do jj=mt2,mt3
       do jj=1,n_bkfix
cMa.Z	  if(nf(jj,1).ne.0) then
	  if(iabkfix(jj,1).EQ.1.AND.nf(iabkfix(jj,0),1).ne.0) then
c	   bk(nf(iabkfix(jj,0),1),nf(iabkfix(jj,0),1)+iw+1)=1.d200
	   bk(nf(iabkfix(jj,0),1))=1.d200
	  end if
cMa.Z	  if(nf(jj,2).ne.0) then
	  if(iabkfix(jj,1).EQ.2.AND.nf(iabkfix(jj,0),2).ne.0) then
c	   bk(nf(iabkfix(jj,0),2),nf(iabkfix(jj,0),2)+iw+1)=1.d200
	   bk(nf(iabkfix(jj,0),2))=1.d200
	  end if
	 end do
	end if
c in the above loop, for the outer boundaries (not well nodes nor centerline nodes)
c if their nf is not zero : the coefficent for Ux and Uy is set to infinity
c nf of those nodes is usually zero.
c***********************************
	return
	end

c
c**********************************************************************
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	subroutine displa2(n_angbkfix,iangbkfix,angbkfix,
     *	n_bkfix,iabkfix,
     * loads,tload,bk,n,nxe,nye,d0,mcc,dofix,nvfix,
c	1     mt2,mt3,loads0,nbound,ns)
 	1     mt2,mt3,loads0,nbound,ns,iw)
	IMPLICIT REAL*8(A-H,O-Z)
	real*8 loads(5000),tload(5000),bk(50000),dofix(1000,3)
c 	real*8 loads(40000),tload(40000),dofix(1000,3)
	real*8 loads0(5000)
	integer mcc(1000,4)
C       COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)
c	real*8 bk(40000,851)


	integer iabkfix(500,0:3)
	integer iangbkfix(500)
	real*8 angbkfix(500)

	do 10 i=1,n_angbkfix
	 ange=angbkfix(i)
	  if (nf(iangbkfix(i),1).ne.0) then
	    dx=tload(nf(iangbkfix(i),1))
	  else
	    dx=0.d0
	  end if
	  if (nf(iangbkfix(i),2).ne.0) then
c	    dy=tload(nf(i,2))
		dy=tload(nf(iangbkfix(i),2))
	  else
	    dy=0.d0
	  end if

c	fix displacement should be in r-direction***sept7,2000
c       d=dsqrt(dx**2.d0+dy**2.d0)
        d=dx*dcos(ange)+dy*dsin(ange)
	  dt=dy*dcos(ange)-dx*dsin(ange)
	  if(d.ge.0.95d0*d0) then
	  d0x=d0*dx/d
	  d0y=d0*dy/d
c        d0x=d0*dcos(ange)-dt*dsin(ange)
c	  d0y=d0*dsin(ange)+dt*dcos(ange)
	  if(nf(iangbkfix(i),1).ne.0) then
	  loads(nf(iangbkfix(i),1))=d0x*1.d200
        end if
	  if(nf(iangbkfix(i),2).ne.0) then
	  loads(nf(iangbkfix(i),2))=d0y*1.d200
	  end if
	  end if
10    continue

c***********add extra fixed imformation
      if (nvfix.ne.0) then
      do 20 i=1,nvfix
	mp=mcc(i,1)
	do jj=1,3
	 if(mcc(i,jj+1).eq.1) then
	  if(nf(mp,jj).ne.0) then
	   loads(nf(mp,jj))=dofix(i,jj)*1.d200
	  end if
	 end if
	end do
 20   continue
      end if
c*******add initial displacement fix**********
      if(nbound.eq.0.and.ns.ne.0) then
cMa.Z	 do jj=mt2,mt3
       do jj=1,n_bkfix
cMa.Z	  if(nf(jj,1).ne.0) then
	  if(iabkfix(jj,1).EQ.1.AND.nf(iabkfix(jj,0),1).ne.0) then
	   loads(nf(iabkfix(jj,0),1))=loads0(nf(iabkfix(jj,0),1))*1.d200
	  end if
cMa.Z	  if(nf(jj,2).ne.0) then
	  if(iabkfix(jj,1).EQ.2.AND.nf(iabkfix(jj,0),2).ne.0) then
	   loads(nf(iabkfix(jj,0),2))=loads0(nf(iabkfix(jj,0),2))*1.d200
	  end if
	 end do
	end if

c***********************************

	return
	end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine transform1(sx,sy,sxy,sr1,st1,ag1)
c
c***** Transform stresses or strains in x-y direction with an angle ****
c      **** "angle" to the maximum principal stress direction  ****
c
       Implicit none
	 real*8 sx,sy,sxy,sr1,st1,ag1
	 real*8 pi,aa
	 pi=3.1415926d0
c-------------this is for calculating primety stress and strain
       if(dabs(sx-sy).le.1.d-15) then 
		ag1=0.5d0*pi/2.d0
	 else
		ag1=0.5d0*datan(2.d0*sxy/(sx-sy))
	 end if
	 if(ag1.lt.0.d0) ag1=pi/2.0+ag1
		  sr1 = sx*dcos(ag1)**2.D0 + 
	1	       sy*dsin(ag1)**2.D0 + dsin(2.0D0*ag1)*sxy
	      st1 = sx*dsin(ag1)**2.D0 + 
	1	       sy*dcos(ag1)**2.D0 - dsin(2.0D0*ag1)*sxy
	 if(sr1.gt.st1) then
	  aa=sr1
	  sr1=st1
	  st1=aa
	  ag1=-pi/2.d0+ag1
	 end if
	
c-------------------------------------------------------
       return 
	 end

!============Li==========
	subroutine transform11(sx,sy,sxy,sr1,st1,ag1,imesh,nxe,p)

       Implicit none
	 integer imesh,nxe,p
	 real*8 sx,sy,sxy,sr1,st1,ag1
	 real*8 pi,aa
	 pi=3.1415926d0

       if(imesh.eq.1) then
	    ag1=(p-1/2.0)*pi/2.0/nxe
	 elseif(imesh.gt.3) then
	    ag1=(p-1/2.0)*pi/nxe-pi/2.0
	  endif
		  sr1 = sx*dcos(ag1)+sy*dsin(ag1)
	      st1 = -sx*dsin(ag1)+sy*dcos(ag1)
        
!	  sr1 = sx*dcos(ag1*pi/180.d0)
!     1        +sy*dsin(ag1*pi/180.d0)
!	  st1= -sx*dsin(ag1*pi/180.d0)
!     1        +sy*dcos(ag1*pi/180.d0)
	
       return 
	 end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

       subroutine tranback1(sx,sy,sxy,sr1,st1,ag1)
c
c***** Transform stresses or strains in principal direction  ****
c         **** into x-y direction with an angle "ag1"  ****
c
       Implicit none
	 real*8 sx,sy,sxy,sr1,st1,ag1
		 sx = sr1*dcos(-ag1)**2.D0 + st1*dsin(-ag1)**2.D0 
	     sy = sr1*dsin(-ag1)**2.D0 + st1*dcos(-ag1)**2.D0 
           sxy = 0.5D0*(st1-sr1)*dsin(-2.0D0*ag1)
       return 
	 end

c
c********************************************************************
c  This sub is used to ceperate extra fixed imformation. Fixed imformation is
c  stored in matrix mcc(1000,4)
c  the nomal fixed condition is done like before, this part is used
c  for user to define extra fixed node
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc 

      SUBROUTINE RESIST(mcc)          
       Implicit real*8 (a-h, o-z)
C	COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)
c	COMMON/LOAD1/radius(101),pofix(1000),point(1000,2),gtheta,
	COMMON/LOAD1/radius(82),rad2(42),pofix(1000),point(1000,2),gtheta,
	1gravy,pressa(1000,6),spoke(41),dofix(1000,3)
	COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)
	1,lodpt(1000),nbound,ndrill,ndirect
	integer mcc(1000,4),nc(1000,3)
         
	if(nvfix.eq.0) return

      DO 2 I=1,nvfix
      DO 2 J=1,3
 2    MCC(I,J)=1

      DO 4 IP=1,nvfix
      M=pofix(IP)
      S=pofix(IP)-FLOAT(M)
      R=S*10.+1.0E-10
      DO 4 J=1,3
	MCC(IP,1)=M
      MCC(IP,J+1)=R
      I1=R
      R=(R-FLOAT(I1))*10.
 4    CONTINUE

      N=0
      DO 10 I=1,nvfix
      DO 10 J=1,3
      N=N+1
      NC(I,J)=N
 10   CONTINUE

      do iin=1,nvfix
      end do

      RETURN
      END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE stressi(sz0,sh1,sh3,biot,p0,szg,sx,sy,sxy,sz,sr,st,ex,
	1  ey,exy,ez,er,et,dee,fun,coord,nh,icoord,ngrav,nye,imesh,nxe,p,
     1  radius,geometrytype,e,v)          
c       Implicit real*8 (a-h, o-z)
	Implicit NONE
	REAL*8 sz0,sh1,sh3,biot,p0,szg,sx,sy,sxy,sz,sr,st,ex
	1   ,ey,exy,ez,er,et
	INTEGER nh,icoord,ngrav,nye,imesh,nxe,p
	real*8 dee(nh,nh),coord(8,2),fun(8),radius(82)

	REAL*8 yposi,yposi1,YW,ANGLE,xposi
	INTEGER IN
	integer geometrytype
	real*8 e,v
			
	if (NGRAV.eq.0) then
		sx = sh3 - biot*p0
	    sy = sh1 - biot*p0
          sz = sz0 - biot*p0
	    sxy = 0.0D0
	    ey = (dee(2,1)*sx-dee(1,1)*sy)/
     1         (dee(1,2)*dee(2,1)-dee(1,1)*dee(2,2))
	    ex = (sx-dee(1,2)*ey)/dee(1,1)
          ez = 0.d0
 		exy = sxy/dee(3,3)
	else
c*****if it is horizental well, gravy has to be tought in intinal stress
c  yposi is the y-coordiration of this gause point**********
		yposi=0.d0
	    xposi=0.0d0
          do in=1,icoord
			yposi=yposi+coord(in,2)*fun(in)
	        xposi=xposi+coord(in,1)*fun(in)
		end do
	    if (imesh.eq.1.or.imesh.eq.3) then
!		if (imesh.eq.1) then
			yposi1=radius(nye+1)-yposi
		else if (imesh.eq.5) then
			yposi1=3.0d0*radius(nye+1)-yposi
		endif
c	     yposi1=radius(nye+1)-yposi
c		yposi1=-yposi
c    sz also should change with depth
		if (geometrytype.eq.1) then
	    sx = sh3 - biot*p0  !+szg*yposi1
	    sy = sh1 - biot*p0  !+szg*yposi1
          sz = sz0 - biot*p0  !+szg*yposi1
	    sxy = 0.0D0	
	    ey = (dee(2,1)*sx-dee(1,1)*sy)/
     1             (dee(1,2)*dee(2,1)-dee(1,1)*dee(2,2))
	    ex = (sx-dee(1,2)*ey)/dee(1,1)
          ez = 0.d0
 	    exy = sxy/dee(3,3)
 	    else if (geometrytype.eq.2) then
 		sx = sh3 - biot*p0 +szg*yposi1
	    sy = sz0 - biot*p0 +szg*yposi1
          sz = sh1 - biot*p0 +szg*yposi1
	    sxy = 0.0D0
	    ex = sx/e-v/e*(sy+sz)
	    ey = sy/e-v/e*(sx+sz)
          ez = sz/e-v/e*(sy+sx)
 		exy = sxy/dee(3,3)		
		endif	    
      end if
	yw =0.5d0*exy
	call transform1(ex,ey,yw,er,et,angle)
	call transform1(sx,sy,sxy,sr,st,angle)
!	call transform11(sx,sy,sxy,sr,st,angle,imesh,nxe,p)
	RETURN
	END
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c          Erosion Model
c

	SUBROUTINE WORM_CAL(PW,PD,XL,DT0,TMAX,T,RW,CON,CONQ,CONQF,dxl)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/DATAIT/nx,IT
      COMMON/DATA/PHI0,PHIMAX,SK0,SKC,YS,YF,UF,C0,SNMD,
     &            Err,YA0,WORM_PHI(1000),
     &      WORM_SK(1000),WORM_C(1000),
     &      WORM_YA(1000),WORM_X(1000),WORM_T_PHI(1000),
     &      WORM_T_SK(1000),WORM_T_C(1000),WORM_Q(1000),
     &      WORM_TMPC(1000)

c	WRITE(T_OUT,*) 'pw,pd',pw,pd,pd-pw,xl
	Dx = XL / nx
	DO I = 1,NX
		WORM_X(I)    = I * Dx
		WORM_PHI(I)  = PHI0
		WORM_SK(I)   = SK0
		WORM_C(I)    = C0
		WORM_TMPC(I) = C0
		WORM_YA(I)   = YA0
	END DO
	    WORM_C(nx + 1) = C0
C***
          WORM_TMPC(NX+1)=C0
C***
c	T = 0
        if( T .lt. 50.D0 ) then
	     EPL = T*SNMD/50.D0
	  ELSE
	     EPL = SNMD
	  END IF
	IT = 0
	DT = DT0
100   IT = IT + 1 !THE NUMBER OF TIME STEP TO BE CALCULATED
C	WRITE(T_OUT,11) T/TMAX*100.D0,IT,T
C11	FORMAT('FINISHED',F6.2,' STEP AT',I5,'  TIME AT',F9.4,'(day)')
	 
C	WRITE(3,1) IT 
C1	FORMAT('CALCULATION STEP AT',I5)
C	WRITE(3,2) T
C2	FORMAT('SIMULATION TIME IS',F9.4,'(days)')
C	WRITE(3,3)WORM_Q(I) * 1000000.D0
C3	FORMAT('SIMULATION FLOW RATE IS',F7.2,'(10e-6 m/s)')
c       temp=rw
	 temp=dxl
	Call INTE_Q(Temp, XL, SUM) !CALCULATE THE NOMALIZED Q
C****
	IF(dabs(sum).le.1.d-10) THEN
	conq=0.d0
	conqf=0.d0
	return
	REAL_Q = (PD - PW) / UF  !CALCULATE THE REAL Q at node where r=rw
	ELSE
	REAL_Q = (PD - PW) / UF / SUM !CALCULATE THE REAL Q
	END IF
C****
C	WRITE(3,4) 
C4	FORMAT('   No. X(m)  Porosity  Permeability,Darcy  Concentration')
       
	DO I = 1,nx
C     CALCULATE PHI ON NEXT TIME
		PHI2 = WORM_PHI(I) + DT * EPL * (1 - WORM_PHI(I)) * 
     &           WORM_TMPC(I) * REAL_Q * (nx - I + 1) / nx 

		TMP1 = (WORM_TMPC(I + 1) - WORM_TMPC(I)) / Dx * REAL_Q + 
     &           (PHI2 - WORM_PHI(I)) / DT
C     CALCULATE CONCENTRATION ON NEXT TIME

		WORM_TMPC(I) = (TMP1 * DT + WORM_C(I) * WORM_PHI(I)) / 
     &           PHI2 

		If (WORM_TMPC(I).LT.C0)     WORM_TMPC(I) = C0
		If (WORM_TMPC(I).GE.0.95D0) WORM_TMPC(I) = 0.95D0
          WORM_PHI(I) = PHI2
C     CALCULATE MIXTURE DENSITY ON NEXT TIME
		WORM_YA(I) = YF + WORM_C(I) * (YS - YF) 
C     CALCULATE PERMEABILITY ON NEXT TIME

		If (WORM_PHI(I).GE.PHIMAX)  
     &   WORM_SK(I) = SKC * PHIMAX ** 3.D0 / (1.D0 - PHIMAX) ** 2.D0
		If (WORM_PHI(I).LT.PHIMAX)  
     &   WORM_SK(I) = SKC * WORM_PHI(I) ** 3.D0 / (1.D0 - WORM_PHI(I)) 
     1                ** 2.D0
	  Con  = worm_tmpc(1)*100.D0
	  conq = dabs(real_q)*86400.D0*(1.D0-worm_phi(1))*worm_tmpc(1)*EPL
	  conqf= EPL*dabs(real_q)*86400.D0
c	WRITE(T_OUT,*) I
c	WRITE(T_OUT,*) '***',CONQ,REAL_Q,WORM_PHI(1),WORM_TMPC(1),EPL
c	WRITE(T_OUT,*) '***',WORM_PHI(I),WORM_TMPC(I)

C     OUTPUT
C	WRITE(3,5) I,WORM_X(I),WORM_PHI(I),
C     &           WORM_SK(I)/0.987D0/0.000000000000001D0,WORM_C(I)
C5     FORMAT(I5,F6.2,F6.2,F11.2,F23.6)
	END DO
	DO I = 1,nx
		WORM_C(I) = WORM_TMPC(I)
	END DO
C*** WORM_T_PHI, WORM_T_SK, WORM_T_C AND WORM_Q ARE NOT USED IN ANYWHERE, THEREFORE,
C    COMMOM THEM AT FIRST. OTHRWISE, IT WILL BE LARGER THAN 40000 AND CAUSE ARRAY
C    BOUNDARY ERROR
C	WORM_T_PHI(IT) = WORM_PHI(1)
C	WORM_T_SK(IT)  = WORM_SK(1)
C	WORM_T_C(IT)   = WORM_C(1)
C	WORM_Q(IT)     = REAL_Q
C***
C     AUTOMATIC TIME STEP SELECTION
	If (REAL_Q * DT / Dx .GT. Err) DT = Err * Dx / REAL_Q 
	If (REAL_Q * DT / Dx .LE. Err) DT = DT * 1.25D0
C     CALCULATE ELASPED TIME
       
c	T = T + DT / 86400.D0
	T = T + DT
	wormp=worm_phi(1)
	wormtm=worm_tmpc(1)
      IF (IT.GE.10000) RETURN
	If (T .LE. TMAX) GoTo 100

	RETURN
	END
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE INTE_Q(A, B, SUM)
C'*****************************************************
C'      Integral Evaluation of Q between 0 and L      *
C'*****************************************************
C'      SUM  - Final result of the integral           *
C'      A    - INITIAL POINT                          *
C'      B    - LAST  POINT                            *
C'*****************************************************
      IMPLICIT REAL*8(A-H,O-Z)
	DIMENSION WORM_T(5), WORM_W(5)
      COMMON/DATAIT/nx,IT
      COMMON/DATA/PHI0,PHIMAX,SK0,SKC,YS,YF,UF,C0,SNMD,
     &            Err,YA0,WORM_PHI(1000),
     &      WORM_SK(1000),WORM_C(1000),
     &      WORM_YA(1000),WORM_X(1000),WORM_T_PHI(1000),
     &      WORM_T_SK(1000),WORM_T_C(1000),WORM_Q(1000),
     &      WORM_TMPC(1000)
	WORM_T(1) = 0.1488743989D0
	WORM_T(2) = 0.4333953941D0
	WORM_T(3) = 0.6794095682D0
	WORM_T(4) = 0.8650633666D0
	WORM_T(5) = 0.9739065285D0
	WORM_W(1) = 0.2955242247D0
	WORM_W(2) = 0.2692667193D0
	WORM_W(3) = 0.2190863625D0
	WORM_W(4) = 0.1494513491D0
	WORM_W(5) = 0.0666713443D0
	TM  = 0.5D0 * (B + a)
	TR  = 0.5D0 * (B - a)
	SUM = 0.D0
	DO  J = 1, 5
		DT = TR * WORM_T(J)
		t1 = TM + DT
		Call LINEINTEP(WORM_X, WORM_YA, nx, t1, YA1)
		Call LINEINTEP(WORM_X, WORM_SK, nx, t1, SK1)
		F1 = YA1 / SK1
		T2 = TM - DT
		Call LINEINTEP(WORM_X, WORM_YA, nx, T2, YA2)
		Call LINEINTEP(WORM_X, WORM_SK, nx, T2, SK2)
		F2 = YA2 / SK2
		SUM = SUM + WORM_W(J) * (F1 * t1 + F2 * T2)
	END DO
	SUM = TR * SUM
	RETURN
	End

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE LINEINTEP(X,Y,N,XO,YO)
C=================================================================INTERP
C   LINEAR INTERPOLATION
C=======================================================================
      IMPLICIT REAL*8(A-H,O-Z)
      PARAMETER(NTE=1000)
      DIMENSION X(NTE),Y(NTE)
      IF(XO.GE.X(N)) YO=Y(N)
      IF(XO.GE.X(N)) RETURN
      DO 10 I=2,N
      IF(XO.GE.X(I)) GO TO 10
      YO=Y(I-1) +(XO-X(I-1))*(Y(I)-Y(I-1))/(X(I)-X(I-1))
      RETURN
  10  CONTINUE
      RETURN
      END
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE GETIW(NW,NXE,NYE,NODEP4)
	IMPLICIT NONE
	INTEGER NW,NXE,NYE,NODEP4(40,80,4)
	INTEGER II,JJ,KK1,KK2
	INTEGER IUNOD1,IUNOD2
	
	NW=0
	DO II=1,NXE
	DO JJ=1,NYE
		DO KK1=1,4
			IUNOD1=nodep4(II,JJ,KK1)
			DO KK2=1,4
				IUNOD2=nodep4(II,JJ,KK2)
				IF(ABS(IUNOD1-IUNOD2).GT.NW) NW=ABS(IUNOD1-IUNOD2)
			ENDDO
		ENDDO
	ENDDO
	ENDDO
	END


c*******************************************************************
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine tungeosym(p,q,nxe,nye,spoke,radius,coord,icoord,
     *              coordf, icordf,g,x,y,angl,angm,nodep,nodeg)
c
c for symmetric
c***** This subroutine create a mesh of 8-node quadrilateral        ****
c***** plane elements in which coordinate for fluid is included too ****
c
c       PARAMETER(INF=309)
	 Implicit real*8 (a-h, o-z)
       integer p,q,nxe,nye,icoord,icordf,g(20)
     *             ,ao,al,am,an,ap,aq,ar,as
       real*8 sl,cl,sr,cr,ang,pi
       real*8 ifacl,ifacr,ofacr,ofacl
       real*8 spoke(41),radius(82)
       real*8 coord(8,2),coordf(4,2)
       real*8 x(5000),Y(5000)
	 integer nodep(40,80,8),nodeg(40,80,20)
C	COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)

       i1= 6
       ao=(q-1)*(3*nxe+2)+2*p-1
       al=ao+1
       am=al+1
       an=q*(3*nxe+2)+p-nxe-1
       ap=an+1
       aq=q*(3*nxe+2)+2*p-1
       ar=aq+1
       as=ar+1
       g(1)=nf(aq,1)
       g(2)=nf(aq,2)
       g(3)=nf(an,1)
       g(4)=nf(an,2)
       g(5)=nf(ao,1)
       g(6)=nf(ao,2)
       g(7)=nf(al,1)
       g(8)=nf(al,2)
       g(9)=nf(am,1)
       g(10)=nf(am,2)
       g(11)=nf(ap,1)
       g(12)=nf(ap,2)
       g(13)=nf(as,1)
       g(14)=nf(as,2)
       g(15)=nf(ar,1)
       g(16)=nf(ar,2)
       g(17)=nf(aq,3)
       g(18)=nf(ao,3)
       g(19)=nf(am,3)
       g(20)=nf(as,3)

       depth=3.d0*radius(nye+1)   
       depthd=3.d0*radius(nye+1)/nxe

       ifacl=q*((1.D0-spoke(p))/nye)+spoke(p)
       ifacr=q*((1.D0-spoke(p+1))/nye)+spoke(p+1)
       ofacr=(q-1)*((1.D0-spoke(p+1))/nye)+spoke(p+1)
       ofacl=(q-1)*((1.D0-spoke(p))/nye)+spoke(p)

	tempd=0.3d0   ! demension of depth
c
       coord(1,1)=radius(q+1)
       coordf(1,1)=coord(1,1)
c
       coord(1,2)=depth-depthd*(p-1)
       coordf(1,2)=coord(1,2)
c
       coord(3,1)=radius(q)
       coordf(2,1)=coord(3,1)
c
       coord(3,2)=depth-depthd*(p-1)
       coordf(2,2)=coord(3,2)
c
       coord(5,1)=radius(q)
       coordf(3,1)=coord(5,1)
c
       coord(5,2)=depth-depthd*p
       coordf(3,2)=coord(5,2)
c
       coord(7,1)=radius(q+1)
       coordf(4,1)=coord(7,1)
c
       coord(7,2)=depth-depthd*p
       coordf(4,2)=coord(7,2)
c
       coord(2,1)=(coord(1,1)+coord(3,1))*0.5D0
       coord(2,2)=(coord(1,2)+coord(3,2))*0.5D0
       coord(4,1)=(coord(3,1)+coord(5,1))*0.5D0
       coord(4,2)=(coord(3,2)+coord(5,2))*0.5D0
       coord(6,1)=(coord(5,1)+coord(7,1))*0.5D0
       coord(6,2)=(coord(5,2)+coord(7,2))*0.5D0
       coord(8,1)=(coord(7,1)+coord(1,1))*0.5D0
       coord(8,2)=(coord(7,2)+coord(1,2))*0.5D0

       x(aq)=coord(1,1)
       y(aq)=coord(1,2)
       x(an)=coord(2,1)
       y(an)=coord(2,2)
       x(ao)=coord(3,1)
       y(ao)=coord(3,2)
       x(al)=coord(4,1)
       y(al)=coord(4,2)
       x(am)=coord(5,1)
       y(am)=coord(5,2)
       x(ap)=coord(6,1)
       y(ap)=coord(6,2)
       x(as)=coord(7,1)
       y(as)=coord(7,2)
       x(ar)=coord(8,1)
       y(ar)=coord(8,2)
       angl=datan(coord(8,2)/coord(8,1))
       angm=datan(coord(8,1)/coord(8,2))

	nodep(p,q,1)=aq
	nodep(p,q,2)=an
	nodep(p,q,3)=ao
	nodep(p,q,4)=al
	nodep(p,q,5)=am
	nodep(p,q,6)=ap
	nodep(p,q,7)=as
	nodep(p,q,8)=ar
      
	do ii=1,20
	nodeg(p,q,ii)=g(ii)
	end do

       return
       end

C***********************************************************************
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine tungeosqe(p,q,nxe,nye,spoke,radius,coord,icoord,
     *              coordf, icordf,g,x,y,angl,angm,nodep,nodeg)
c
c***** This subroutine create a mesh of 8-node quadrilateral        ****
c***** plane elements in which coordinate for fluid is included too ****
c***** square outside

c       PARAMETER(INF=309)
	 Implicit real*8 (a-h, o-z)
       integer p,q,nxe,nye,icoord,icordf,g(20)
     *             ,ao,al,am,an,ap,aq,ar,as
       real*8 sl,cl,sr,cr,ang,pi
       real*8 ifacl,ifacr,ofacr,ofacl
       real*8 spoke(41),radius(82)
       real*8 coord(8,2),coordf(4,2)
       real*8 x(5000),Y(5000)
C
       REAL*8 DLENGTH(51),DX(51),DY(51)
	 integer nodep(40,80,8),nodeg(40,80,20)
C	COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)

       i1= 6
       ao=(q-1)*(3*nxe+2)+2*p-1
       al=ao+1
       am=al+1
       an=q*(3*nxe+2)+p-nxe-1
       ap=an+1
       aq=q*(3*nxe+2)+2*p-1
       ar=aq+1
       as=ar+1
       g(1)=nf(aq,1)
       g(2)=nf(aq,2)
       g(3)=nf(an,1)
       g(4)=nf(an,2)
       g(5)=nf(ao,1)
       g(6)=nf(ao,2)
       g(7)=nf(al,1)
       g(8)=nf(al,2)
       g(9)=nf(am,1)
       g(10)=nf(am,2)
       g(11)=nf(ap,1)
       g(12)=nf(ap,2)
       g(13)=nf(as,1)
       g(14)=nf(as,2)
       g(15)=nf(ar,1)
       g(16)=nf(ar,2)
       g(17)=nf(aq,3)
       g(18)=nf(ao,3)
       g(19)=nf(am,3)
       g(20)=nf(as,3)

       pi=4.0D0*datan(1.D00)
       ang=0.5D0*pi/nxe
C
      DO I=1,NXE+1

	DANG=ANG*(I-1)
      	DX(I)=RADIUS(NYE+1)
	    DY(I)=DTAN(DANG)*DX(I)

        IF (DY(I).GT.RADIUS(NYE+1)) THEN
	    DY(I)=RADIUS(NYE+1)
	    DX(I)=DY(I)/DTAN(DANG)
	  END IF 
	             
          DLENGTH(I)=DSQRT(DX(I)**2.D0+DY(I)**2.D0)-radius(1)

	END DO

       dtemp=radius(nye+1)-radius(1)

       cl=dcos((p-1)*ang)
       cr=dcos(p*ang)
       sl=dsin((p-1)*ang)
       sr=dsin(p*ang)
c       ifacl=q*((1.D0-spoke(p))/nye)+spoke(p)
c       ifacr=q*((1.D0-spoke(p+1))/nye)+spoke(p+1)
c       ofacr=(q-1)*((1.D0-spoke(p+1))/nye)+spoke(p+1)
c       ofacl=(q-1)*((1.D0-spoke(p))/nye)+spoke(p)

	 r1=radius(1)+(radius(q)-radius(1))/dtemp*dlength(p)
	 r2=radius(1)+(radius(q)-radius(1))/dtemp*dlength(p+1)
	 r3=radius(1)+(radius(q+1)-radius(1))/dtemp*dlength(p)
	 r4=radius(1)+(radius(q+1)-radius(1))/dtemp*dlength(p+1)
c
       coord(1,1)=r3*cl
       coordf(1,1)=coord(1,1)
c
       coord(1,2)=r3*sl
       coordf(1,2)=coord(1,2)
c
       coord(3,1)=r1*cl
       coordf(2,1)=coord(3,1)
c
       coord(3,2)=r1*sl
       coordf(2,2)=coord(3,2)
c
       coord(5,1)=r2*cr
       coordf(3,1)=coord(5,1)
c
       coord(5,2)=r2*sr
       coordf(3,2)=coord(5,2)
c
       coord(7,1)=r4*cr
       coordf(4,1)=coord(7,1)
c
       coord(7,2)=r4*sr
       coordf(4,2)=coord(7,2)
c
       coord(2,1)=(coord(1,1)+coord(3,1))*0.5D0
       coord(2,2)=(coord(1,2)+coord(3,2))*0.5D0
       coord(4,1)=(coord(3,1)+coord(5,1))*0.5D0
       coord(4,2)=(coord(3,2)+coord(5,2))*0.5D0
       coord(6,1)=(coord(5,1)+coord(7,1))*0.5D0
       coord(6,2)=(coord(5,2)+coord(7,2))*0.5D0
       coord(8,1)=(coord(7,1)+coord(1,1))*0.5D0
       coord(8,2)=(coord(7,2)+coord(1,2))*0.5D0

       x(aq)=coord(1,1)
       y(aq)=coord(1,2)
       x(an)=coord(2,1)
       y(an)=coord(2,2)
       x(ao)=coord(3,1)
       y(ao)=coord(3,2)
       x(al)=coord(4,1)
       y(al)=coord(4,2)
       x(am)=coord(5,1)
       y(am)=coord(5,2)
       x(ap)=coord(6,1)
       y(ap)=coord(6,2)
       x(as)=coord(7,1)
       y(as)=coord(7,2)
       x(ar)=coord(8,1)
       y(ar)=coord(8,2)
       angl=datan(coord(8,2)/coord(8,1))
       angm=datan(coord(8,1)/coord(8,2))

	nodep(p,q,1)=aq
	nodep(p,q,2)=an
	nodep(p,q,3)=ao
	nodep(p,q,4)=al
	nodep(p,q,5)=am
	nodep(p,q,6)=ap
	nodep(p,q,7)=as
	nodep(p,q,8)=ar
      
	do ii=1,20
	nodeg(p,q,ii)=g(ii)
	end do

       return
       end


C***********************************************************************
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine tungeosqes(p,q,nxe,nye,spoke,radius,coord,icoord,
     *              coordf, icordf,g,x,y,angl,angm,nodep,nodeg)
c
c***** This subroutine create a mesh of 8-node quadrilateral        ****
c***** plane elements in which coordinate for fluid is included too ****
c***** square sharp outside

c       PARAMETER(INF=309)
	 Implicit real*8 (a-h, o-z)
       integer p,q,nxe,nye,icoord,icordf,g(20)
     *             ,ao,al,am,an,ap,aq,ar,as
       real*8 sl,cl,sr,cr,ang,pi
       real*8 ifacl,ifacr,ofacr,ofacl
       real*8 spoke(41),radius(82)
       real*8 coord(8,2),coordf(4,2)
       real*8 x(5000),Y(5000)
C
       REAL*8 DLENGTH(51),DX(51),DY(51)
	 integer nodep(40,80,8),nodeg(40,80,20)
C	COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)
       i1= 6
       ao=(q-1)*(3*nxe+2)+2*p-1
       al=ao+1
       am=al+1
       an=q*(3*nxe+2)+p-nxe-1
       ap=an+1
       aq=q*(3*nxe+2)+2*p-1
       ar=aq+1
       as=ar+1
       g(1)=nf(aq,1)
       g(2)=nf(aq,2)
       g(3)=nf(an,1)
       g(4)=nf(an,2)
       g(5)=nf(ao,1)
       g(6)=nf(ao,2)
       g(7)=nf(al,1)
       g(8)=nf(al,2)
       g(9)=nf(am,1)
       g(10)=nf(am,2)
       g(11)=nf(ap,1)
       g(12)=nf(ap,2)
       g(13)=nf(as,1)
       g(14)=nf(as,2)
       g(15)=nf(ar,1)
       g(16)=nf(ar,2)
       g(17)=nf(aq,3)
       g(18)=nf(ao,3)
       g(19)=nf(am,3)
       g(20)=nf(as,3)

       pi=4.0D0*datan(1.D00)
       ang=0.5D0*pi/nxe
C
      DO I=1,NXE+1

	DANG=ANG*(I-1)
      	DX(I)=RADIUS(NYE+1)
	    DY(I)=DTAN(DANG)*DX(I)

        IF (DY(I).GT.RADIUS(NYE+1)) THEN
	    DY(I)=RADIUS(NYE+1)
	    DX(I)=DY(I)/DTAN(DANG)
	  END IF 
	             
          DLENGTH(I)=DSQRT(DX(I)**2.D0+DY(I)**2.D0)-radius(1)

	END DO

       dtemp=radius(nye+1)-radius(1)

       cl=dcos((p-1)*ang)
       cr=dcos(p*ang)
       sl=dsin((p-1)*ang)
       sr=dsin(p*ang)
c       ifacl=q*((1.D0-spoke(p))/nye)+spoke(p)
c       ifacr=q*((1.D0-spoke(p+1))/nye)+spoke(p+1)
c       ofacr=(q-1)*((1.D0-spoke(p+1))/nye)+spoke(p+1)
c       ofacl=(q-1)*((1.D0-spoke(p))/nye)+spoke(p)

	 r1=radius(1)+(radius(q)-radius(1))/dtemp*dlength(p)
	 r2=radius(1)+(radius(q)-radius(1))/dtemp*dlength(p+1)
	 r3=radius(1)+(radius(q+1)-radius(1))/dtemp*dlength(p)
	 r4=radius(1)+(radius(q+1)-radius(1))/dtemp*dlength(p+1)
c
       coord(1,1)=r3*cl
       coordf(1,1)=coord(1,1)
c
       coord(1,2)=r3*sl
       coordf(1,2)=coord(1,2)
c
       coord(3,1)=r1*cl
       coordf(2,1)=coord(3,1)
c
       coord(3,2)=r1*sl
       coordf(2,2)=coord(3,2)
c
       coord(5,1)=r2*cr
       coordf(3,1)=coord(5,1)
c
       coord(5,2)=r2*sr
       coordf(3,2)=coord(5,2)
c
       coord(7,1)=r4*cr
       coordf(4,1)=coord(7,1)
c
       coord(7,2)=r4*sr
       coordf(4,2)=coord(7,2)
c
       coord(2,1)=(coord(1,1)+coord(3,1))*0.5D0
       coord(2,2)=(coord(1,2)+coord(3,2))*0.5D0
       coord(4,1)=(coord(3,1)+coord(5,1))*0.5D0
       coord(4,2)=(coord(3,2)+coord(5,2))*0.5D0
       coord(6,1)=(coord(5,1)+coord(7,1))*0.5D0
       coord(6,2)=(coord(5,2)+coord(7,2))*0.5D0
       coord(8,1)=(coord(7,1)+coord(1,1))*0.5D0
       coord(8,2)=(coord(7,2)+coord(1,2))*0.5D0

       x(aq)=coord(1,1)
       y(aq)=coord(1,2)
       x(an)=coord(2,1)
       y(an)=coord(2,2)
       x(ao)=coord(3,1)
       y(ao)=coord(3,2)
       x(al)=coord(4,1)
       y(al)=coord(4,2)
       x(am)=coord(5,1)
       y(am)=coord(5,2)
       x(ap)=coord(6,1)
       y(ap)=coord(6,2)
       x(as)=coord(7,1)
       y(as)=coord(7,2)
       x(ar)=coord(8,1)
       y(ar)=coord(8,2)
       angl=datan(coord(8,2)/coord(8,1))
       angm=datan(coord(8,1)/coord(8,2))

	nodep(p,q,1)=aq
	nodep(p,q,2)=an
	nodep(p,q,3)=ao
	nodep(p,q,4)=al
	nodep(p,q,5)=am
	nodep(p,q,6)=ap
	nodep(p,q,7)=as
	nodep(p,q,8)=ar
      
	do ii=1,20
	nodeg(p,q,ii)=g(ii)
	end do

       return
       end
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE DRUCKERPRAGER(PHI,C,SIGM,DSBAR,FNEW)
	IMPLICIT NONE
	REAL*8 PHI,C,SIGM,DSBAR,FNEW
	REAL*8 ALPHA,K,PHIR,PI


	PI=4.0D00*DATAN(1.0D0)
c	PHIR=PHI*PI/180.0D0
	PHIR=PHI
 	ALPHA = DSIN(PHIR)/DSQRT(9.0D0+3.0D0*DSIN(PHIR)*DSIN(PHIR))
 	K     = DSQRT(3.0D0)*C*DCOS(PHIR)/DSQRT(3.0D0+DSIN(PHIR)*DSIN(PHIR))
c	ALPHA = 2.0d0*DSQRT(3.0D0)*DSIN(PHIR)/(9.0D0-DSIN(PHIR)*DSIN(PHIR))
c	K     = 6*DSQRT(3.0D0)*C*DCOS(PHIR)/(9.0D0-DSIN(PHIR)*DSIN(PHIR))
	FNEW=DSBAR/DSQRT(3.0D0)-ALPHA*SIGM*3.0d0-K
	FNEW=-FNEW
	END SUBROUTINE
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE MOCOPLDP(PHI,E,V,SX,SY,SZ,TXY,PL)
	IMPLICIT NONE
	REAL*8 PHI,E,V,SIGM,DSBAR,SX,SY,SZ,TXY,PL(4,4)
	REAL*8 PI,PHIR,K,G,ALPHA
	REAL*8 HX,HY,HZ,HXY,COEF
	REAL*8 COL(4)
	INTEGER II,JJ

!	SIGM=(SX+SY+SZ)/3.0D0
	SIGM=(SX+SY+SZ)/3.0D0
	DSBAR=dsqrt(0.5D0*((sX-sY)**2.D0+(sY-sZ)**2.D0+(sZ-sX)**2.D0
     1	+6.D0*TXY**2.D0))

	PI=4.0D00*DATAN(1.0D0)
c	PHIR=PHI*PI/180.0D0
	PHIR=PHI 
 	ALPHA = DSIN(PHIR)/DSQRT(9.0D0+3.0D0*DSIN(PHIR)*DSIN(PHIR))
c	ALPHA = 2.0d0*DSQRT(3.0D0)*DSIN(PHIR)/(9.0D0-DSIN(PHIR)*DSIN(PHIR))
	K=E/3.0D0/(1.0D0-2.0D0*V)
	G=E/2.0D0/(1.0D0+V)
	COEF=1.0D0/(9.0D0*K*ALPHA*ALPHA+G)

	HX=-3.0D0*K*ALPHA+DSQRT(3.0D0)*G/DSBAR*(SX-SIGM)
	HY=-3.0D0*K*ALPHA+DSQRT(3.0D0)*G/DSBAR*(SY-SIGM)
	HZ=-3.0D0*K*ALPHA+DSQRT(3.0D0)*G/DSBAR*(SZ-SIGM)
	HXY=DSQRT(3.0D0)*G/DSBAR*TXY

	COL(1)=HX
	COL(2)=HY
	COL(3)=HXY
	COL(4)=HZ

	DO II=1,4
	DO JJ=1,4
		PL(II,JJ)=COEF*COL(II)*COL(JJ)
	ENDDO
	ENDDO
	continue
	END SUBROUTINE
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	subroutine Bashar_Test1

	real*8 x1,y1,x3,y3,x5,y5,x7,y7,eta_x,eta_y,xi_x,xi_y,x_eta,x_xi,
     *	y_eta,y_xi,etx,ety,xi,eta,det
	real*8 samp(3,2),coord(8,2),jac(2,2),jac1(2,2),fun(8),der(2,8)
     *		,deriv(2,8)
	integer nxi,ider,gp,ig,i,j,icoord,ijac,ijac1,nod,cd

	gp=2
	ider=2
	icoord=8
	ijac=2
	ijac1=2
	nod=8

	x1=2.0
	y1=1.0
	x3=1.0
	y3=3.0
	x5=3.0
	y5=5.0
	x7=4.0
	y7=1.0
	
	coord(1,1)=x1;			coord(1,2)=y1
	coord(2,1)=(x1+x3)/2.;	coord(2,2)=(y1+y3)/2.
	coord(3,1)=x3;			coord(3,2)=y3
	coord(4,1)=(x3+x5)/2.;	coord(4,2)=(y3+y5)/2.
	coord(5,1)=x5;			coord(5,2)=y5
	coord(6,1)=(x5+x7)/2.;	coord(6,2)=(y5+y7)/2.
	coord(7,1)=x7;			coord(7,2)=y7
	coord(8,1)=(x7+x1)/2.;	coord(8,2)=(y7+y1)/2.

	do ig=1,2
		call gauss(samp,gp)
          
		i=ig
		j=1
		nxi=2
		eta=samp(i,1)
		xi=samp(j,1)
		if (nxi.eq.2) xi=-1.d0
		
		call fmquad(der,ider,fun,samp,gp,ig,1,2) !...,i,j,nxi)
		call matmul(der,ider,coord,icoord,jac,ijac)
		call twoby2(jac,ijac,jac1,ijac1,det)
		call matmul(jac1,ijac1,der,ider,deriv,nod)
		
		
		x_eta=jac(2,1)
		y_eta=jac(2,2)
		etx=x_eta/(x_eta**2.0+y_eta**2.0)**0.5
		ety=y_eta/(x_eta**2.0+y_eta**2.0)**0.5
	
		
	enddo
	do i=-10,10	
	cd=i
	if(cd-1)   5,4,4
4	print*,"line 4"
5	print*,"line 5"
	enddo
	end subroutine Bashar_Test1

! >>>> Dr. Fotios Karaoulanis >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! >>>> subroutine fmdeps(dee,idee,e,v)
      subroutine fmdeps1(dee, idee, E, nu, Et, Gt, npt, a)
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
c
c************ This suboutine forms the elastic plane strain  ***********
c************ stress/strain matrix                           ***********
c
      !Dr. Fotios Karaoulanis >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      Implicit real*8 (a-h, o-z)
      integer idee
      real*8 dee(idee,idee),e,v,v1, nu
	v = nu
      v1=1.D0-v
      c=E/((1.D0+v)*(1.D0-2.D0*v))
      dee(1,1)=v1*c
      dee(2,2)=v1*c
      dee(3,3)=.5D0*c*(1.D0-2.D0*v)
      dee(1,2)=v*c
      dee(2,1)=v*c
      dee(1,3)=0.D0
      dee(3,1)=0.D0
      dee(2,3)=0.D0
      dee(3,2)=0.D0
	return
      !implicit none
	!integer idee
	!real*8 dee(idee,idee), E, nu, Et, Gt, npt, a
	!real*8 Ep, np, ntp, D, pi, c, s, C11, C12, C22, C33
      Ep  = E
	np  = nu
      pi  = 4.d0 * atan(1.d0)
      a   = a / 180.d0 * pi
      s   = sin(a)
      c   = cos(a)
      ntp = npt * Et / Ep
      D   = (1. + np) * (1. - np - 2. * npt * ntp) / (Ep * Ep * Et)
      C11 = (1. - npt * ntp) / (Ep * Et * D)
      C12 = npt * (ntp + 1.) / (Ep * Ep * D)
      C22 = (1. - np * np)   / (Ep * Ep * D)
      C33 = Gt
      dee(1, 1) =    C11 * c**4 - 2.d0 * C12 * c**4 + 2.d0 * C12 * c**2
     &             + C22 * s**4 - 4.d0 * C33 * c**4 + 4.d0 * C33 * c**2
      dee(1, 2) =  - C11 * c**4 + C11 * c**2 + C12 * s**4 + C12 * c**4
     &             - C22 * c**4 + C22 * c**2
     &             + 4.d0 * C33 * c**4 - 4.d0 * C33*c**2
      dee(1, 3) = (- C11 * c**2 - 2.d0 * C12 * s**2 + C12 +
     &               C22 * s**2 - 4.d0 * C33 * s**2 + 2.d0 * C33
     &            ) * s * c
      dee(2, 1) =  - C11 * c**4 + C11 * c**2 + C12 * s**4 + C12 * c**4
     &             - C22 * c**4 + C22 * c**2
     &             + 4.d0 * C33 * c**4 - 4.d0 * C33 * c**2
      dee(2, 2) =    C11 * s**4 - 2.d0 * C12 * c**4 + 2.d0 * C12 * c**2
     &             + C22 * c**4 - 4.d0 * C33 * c**4 + 4.d0 * C33 * c**2
      dee(2, 3) = (  C11 * c**2 - C11 - 2.d0 * C12 * c**2 + C12 +
     &               C22 * c**2 - 4.d0 * C33 * c**2 + 2.d0 * C33
     &            ) * s * c
      dee(3, 1) = (- C11 * c**2 - 2.d0 * C12 * s**2 + C12 + C22 * s**2
     &             - 4.d0 * C33 * s**2 + 2.d0 * C33
     &            ) * s * c

      dee(3, 2) = (  C11 * c**2 - C11 - 2.d0 * C12 * c**2 + C12
     &             + C22 * c**2 - 4.d0 * C33 *c**2 + 2.d0 * C33
     &            ) * s * c

      dee(3, 3) = - C11 * c**4 + C11 + 2.d0 * C12 * c**4 - C12 * c**2
     &            - C12 - C22 * c**4 + C22 * c**2
     &            + C33 * (2.d0 * s**2 - 1.d0)**2 + (- C11 + C12) * s**2
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
       return
       end

!!=========================================================			
!!============================LI===========================
	 subroutine fmkdke_M(c,ic,ke,ike,kd,ikd,dof,nodf,
     1                     tot,M,theta,biotm)  
       Implicit NONE  
       Integer ic,ike,ikd,dof,nodf,tot,i,j,ii  
       real*8 c(16,4),ke(20,20),kd(20,20),theta  
       real*8 M(4,4),dumpm(5000),biotm

       do 12 i=1,tot 
		do 12 j=1,tot  
			kd(i,j)=0.0d0 
 12    ke(i,j)=0.0d0 
       do 13 i=1,dof                                                                      
		do 13 j=dof+1,tot                                                                       
13			ke(i,j)=-c(i,j-dof)*biotm
c13			ke(i,j)=0.0d0                                                                                                                                                                                                                                      
       do 19 i=dof+1,tot 
		do 19 j=dof+1,tot  
			ke(i,j)=M(i-dof,j-dof)  
19     kd(i,j)=-M(i-dof,j-dof) 
       return  
       end

      subroutine fmdeps6(dee, idee, p, q)
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!************ This suboutine forms the elastic plane strain  ***********
!************ stress/strain matrix                           ***********

      implicit none
      common /materials/ elemmats(40, 80), matprops(2000, 6)
      integer p, q, idee, elemmats
      real*8 dee(idee,idee), E, nu, c, matprops
      E  = matprops(elemmats(p, q), 1)
      nu = matprops(elemmats(p, q), 2)
      c  = E / ((1.D0 + nu) * (1.D0 - 2.D0 * nu))
      dee( 1, 1) = (1.D0 - nu) * c
      dee( 2, 2) = (1.D0 - nu) * c
      dee( 3, 3) = 0.5D0 * c * (1.D0 - 2.D0 * nu)
      dee( 1, 2) = nu * c
      dee( 2, 1) = nu * c
      dee( 1, 3) = 0.D0
      dee( 3, 1) = 0.D0
      dee( 2, 3) = 0.D0
      dee( 3, 2) = 0.D0
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
       return
       end

      subroutine fmkdke1(km, ikm, kp, ikp, c, ic, ke, ike, kd, ikd, dof,
     &                  nod, tot, theta, dtkQ, coeff_c, coeff_cT)

! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
!*** This subroutine forms coupled elemental matrices KE and KD from ***
!*** elastic stiffness KM,fluid "stiffness" KP and coupling matrix C ***
       Implicit none
       integer ikm,ikp,ic,ike,ikd,dof,nod,tot,i,j,k,m
       real*8 c(16,4),ke(20,20),kd(20,20)
       real*8 theta, km(16,16), kp(4,4), dtkQ(4,4), coeff_c, coeff_cT
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

       do 11 i=1,dof
		do 12 j=1,dof
 12		ke(i,j)=km(i,j)
		do 13 k=1,nod
      ke(i, dof + k) = coeff_c  * c(i, k)
   13 ke(dof + k, i) = coeff_cT * c(i, k)
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 11    continue
       do 14 i=1,nod
		do 15 j=1,nod
 15		ke(dof+i,dof+j)=kp(i,j)
 14    continue
       do 17 i=1,dof
       do 17 j=1,tot
		kd(i,j)=(theta-1.0D0)*ke(i,j)
 17    ke(i,j)=theta*ke(i,j)
       m=dof+1
       do 18 i=m,tot
       do 18 j=1,dof
		kd(i,j)=ke(i,j)*theta
 18    ke(i,j)=kd(i,j)
       do 19 i=m,tot
       do 19 j=m,tot

		kd(i,j)= -ke(i,j)*(theta-1.0)*theta - dtkQ(i-dof,j-dof)*theta
 19     ke(i,j)=-ke(i,j)*theta*theta -  dtkQ(i-dof,j-dof)*theta

       return
       end

