

C**********************************************************************
C  GAUSS SOLUTION
C**********************************************************************
      SUBROUTINE GASSSO(SK,F,N)           
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION SK(1000,1000),F(1000)

      DO 1 I=1,N
      I1=I+1
      DO 10 J=I1,N
	if( sk(i,i).le.1.d-5) then
C       write(*,*) 'sk(i,i)=',i,sk(i,i)
	end if
 10   SK(I,J)=SK(I,J)/SK(I,I)
      F(I)=F(I)/SK(I,I)
      SK(I,I)=1.0
      DO 20 J=I1,N
      DO 30 M=I1,N
 30   SK(J,M)=SK(J,M)-SK(J,I)*SK(I,M)
 20   F(J)=F(J)-SK(J,I)*F(I)
 1    CONTINUE
      DO 40 I=N-1,1,-1
      DO 50 J=I+1,N
 50   F(I)=F(I)-SK(I,J)*F(J)
 40   CONTINUE
      
      RETURN
      END

	 subroutine QUANTITY(IMESH,nxe,nye,nodep,nodep4,
     $ dump,x,y,kro,krw,qwater,qoil)
	 implicit none
	 integer IMESH,nxe,nye,n_bkfix,iabkfix(500,0:3)
	 INTEGER nodep(40,80,8),nodep4(40,80,4)                                                                                   
       integer n3_deload                        
	 real*8 dump(5000),x(5000),y(5000)
	 real*8 kro(40,80,4,2),krw(40,80,4,2)

	 INTEGER GP,IG,IN,INN,NODEID4(4),NODEID8(4)
	 INTEGER II,JJ,NNP
	 REAL*8 coordf(4,2),coord(8,2),DUMP_G(4),A_KW(2,2)
	 INTEGER ICORDF,icord,nodf
	 DATA ICORDF,icord,nodf/4,8,4/
	 INTEGER ijac,ijac1,ikay,iderf,idervf
	 data ijac,ijac1,ikay,iderf,idervf/5*2/
	 real*8 det,quot,samp(2,2),jac(2,2),jac1(2,2)
	 REAL*8 funf(4),derf(2,4),derivf(2,4)
	 REAL*8 P_GRAD(2),Tderiv(2),KWP_GRAD(2,4),KOP_GRAD(2,4)
	 real*8 qwater(2),qoil(2)
	 INTEGER I,J,KK
	 integer nf, nfT
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)
	!! FOR BOUNDARY INTEGRATION
	 REAL*8 SAMPB(2),N(4),WATER_Grad(2,2),OIL_Grad(2,2),elcod(2,3)
	 REAL*8 der(2,8),FUN(8),DIRECTION(2),NORMAL(2)
	 INTEGER iodeg,I_WELL,IYE,igrav
	REAL*8 GTHETA,GRAVY,GCOMWATER(2),GCOMOIL(2)
	real*8 PW_GRAD(2),Po_GRAD(2),ang
	common /iconsider/igrav
	integer mm,nn


	Gtheta=0.0d0
!	GRAVY=900.0d0*10.0d0
	IF(igrav.EQ.1) THEN
		GRAVY= 10000.0D0
		GCOMWATER(1) = 1.0D-6*GRAVY*DSIN(GTHETA)
		GCOMWATER(2) =1.0D-6*GRAVY*DCOS(GTHETA)
!          GCOMWATER(1) = -1.0D-6*GRAVY*DSIN(GTHETA) !改
!	    GCOMWATER(2) = -1.0D-6*GRAVY*DCOS(GTHETA)  !改
		GRAVY= 9000.0D0
		GCOMOIL(1)   = 1.0D-6*GRAVY*DSIN(GTHETA)
		GCOMOIL(2)   =1.0D-6*GRAVY*DCOS(GTHETA)
!          GCOMOIL(1)   = -1.0D-6*GRAVY*DSIN(GTHETA)  	!改
!		GCOMOIL(2)   = -1.0D-6*GRAVY*DCOS(GTHETA)  	!改
	ELSE
		GCOMWATER(1) = 0.0D0
		GCOMWATER(2) = 0.0D0
		GCOMOIL(1)   = 0.0D0
		GCOMOIL(2)   = 0.0D0
	ENDIF

c Modification by Bashar
c	qwater=0.0d0
c	qoil=0.0d0
	qwater(1)=0.0d0
	qwater(2)=0.0d0
	qoil(1)=0.0d0
	qoil(2)=0.0d0
c end of modification by bashar
!!	FOR THE FIRST WELL
	GP=2
	I_WELL=0
100	CONTINUE
	I_WELL=I_WELL+1
	IF(I_WELL.EQ.1) THEN
		IYE=1
	ELSE
		IYE=NYE 
	ENDIF
	DO II=1,nxe
	DO JJ=IYE,IYE !JJ is eigher 1 or 8
		do in=1,icordf !!get the coordinate information                                                                      
			inn=2*in-1
			nodeid4(in)= nodep4(II,JJ,in)
			nodeid8(in)=nodep(II,JJ,inn)                                                                            
			coordf(in,1)=x(nodep(II,JJ,inn))                                                         
			coordf(in,2)=y(nodep(II,JJ,inn))                                                        
		end do
		do in=1,icord
			coord(in,1)=x(nodep(II,JJ,in))
			coord(in,2)=y(nodep(II,JJ,in)) 
		enddo
		DO IN=1,4
			NNP=NODEP(II,JJ,IN*2-1)
			DUMP_G(IN)= DUMP(NF(NNP,3)) 

		ENDDO 

       ! 改  ccccccccccccccccccccccccccccccc
!	    if(imesh.EQ.5) then
!		  ang=(0.5D0+ii-1)*180.D0/NXE-90.D0
!          elseif (imesh.eq.2) then
!	      ang = 0.0d0
!	    elseif(imesh.EQ.3) then
!		  ang=(0.5D0+ii-1)*180.D0/NXE-90.D0
!	    elseif(imesh.EQ.1) then
!		  ang=(0.5D0+ii-1)*90.D0/NXE
!	    else
!	    endif
!		Gtheta=0.0d0
!	    IF(igrav.EQ.1) THEN
!	      if(ang.ge.0) then
!		    GRAVY= 10000.0D0
!              GCOMWATER(1) = 1.0D-6*GRAVY*DSIN(GTHETA)
!	        GCOMWATER(2) = 1.0D-6*GRAVY*DCOS(GTHETA)
!		    GRAVY= 9000.0D0
!              GCOMOIL(1)   = 1.0D-6*GRAVY*DSIN(GTHETA)	
!		    GCOMOIL(2)   = 1.0D-6*GRAVY*DCOS(GTHETA)
!	      else
!              GCOMWATER(1) = 0.0d0
!	        GCOMWATER(2) = 0.0d0
!              GCOMOIL(1)   = 0.0d0	
!		    GCOMOIL(2)   = 0.0d0
!		  endif	
!	    ELSE
!		GCOMWATER(1) = 0.0D0
!		GCOMWATER(2) = 0.0D0
!		GCOMOIL(1)   = 0.0D0
!		GCOMOIL(2)   = 0.0D0
!	    ENDIF

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

		CALL GAUSS(samp,gp)
		IG=0
		do 4 i = 1, gp                                                                          
		do 4 j = 1, gp
			ig=ig+1
	!! FOR WATER PRODUCTION
		    A_KW(1,1) = krw(II,JJ,ig,1)
			A_KW(2,2) = krw(II,JJ,ig,2)
			A_KW(1,2) = 0.0d0                                                                          
			A_KW(2,1) = 0.0d0
			call nulvec(P_GRAD,2)                                                                                                                                                   
			call formln(derf,iderf,funf,samp,gp,i,j)                                               
			call matmul(derf,iderf,coordf,icordf,jac,ijac)                                                                                                     
			call twoby2(jac,ijac,jac1,ijac1,det)                                                                                                 
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf) 
			call matmul(derivf,idervf,DUMP_G,nodf,P_GRAD,1)

			do KK=1,2
				PW_GRAD(KK)= P_GRAD(KK) + GCOMWATER(KK)
			enddo
			call matmul(A_KW,ikay,PW_GRAD,idervf,Tderiv,1)
			do KK=1,2
				KWP_GRAD(KK,ig)= -Tderiv(KK)
			enddo

	!! FOR OIL PRODUCTION
		    A_KW(1,1) = krO(II,JJ,ig,1)
			A_KW(2,2) = krO(II,JJ,ig,2)                                                                            
			A_KW(1,2) = 0.0d0                                                                          
			A_KW(2,1) = 0.0d0
			do KK=1,2
				Po_GRAD(KK)= P_GRAD(KK) + GCOMOIL(KK)
			enddo
			call matmul(A_KW,ikay,Po_GRAD,idervf,Tderiv,1)
			do KK=1,2
				KOP_GRAD(KK,ig)= -Tderiv(KK) 
			enddo       	

4		CONTINUE

!            if(ii.eq.1.) then 
!	        write(331,*) KwP_GRAD(1,1),KwP_GRAD(1,2),KwP_GRAD(1,3)
!     1			           ,KwP_GRAD(1,4)
!            endif

	!! BOUNDARY INTEGRATION FOR WATER
		SAMPB(1)=dsqrt(3.0d0)/3.0
		SAMPB(2)=-SAMPB(1)
		N(1)=(1.0d0+SAMPB(1))/(2.0d0*SAMPB(1)) !! shape functiom for point 1/sart(3.0)
		N(2)=(1.0d0+SAMPB(2))/(2.0d0*SAMPB(2)) !! shape functiom for point -1/sart(3.0)
		WATER_grad=0.0d0
		OIL_grad=0.0D0	
		do i=1,2 !! for gauss point ii in one dimension
		do j=1,2 !!Direction x, y
		WATER_grad(j,i)=KWP_GRAD(j,i)*n(1)+KWP_GRAD(j,i+2)*n(2)
		OIL_grad(j,i)=KOP_GRAD(j,i)*n(1)+KOP_GRAD(j,i+2)*n(2)
		enddo	
		enddo

!          if(ii.eq.1) then
!	    write(331,*) oil_grad(1,1),oil_grad(1,2)
!	    endif

		do in=1,3                                                                        
			elcod(1,IN)=coord(2+IN,1)                                                         
			elcod(2,IN)=coord(2+IN,2)
		enddo
		do 9105 ig=1,gp                                                                        
			call gauss(samp,gp)
c************** xi=weigh in gauss in Owen                                                 
			call fmquad(der,2,fun,samp,gp,ig,1,2) !! xi can equal to -1.0 only (so gauss quadrature was limited to one dimension)
			do IN=1,2
				direction(IN)=0.0d0	
 				do iodeg=1,3
 			direction(IN)=direction(IN)+elcod(IN,iodeg)*der(2,iodeg) !! [d x/ d eta; d y/ d eta]
 				enddo
 			enddo
	!! NORMAL DIRECTION
			normal(1)=-direction(2) 
			normal(2)=direction(1)
!			normal(1)=direction(2)    
!			normal(2)=-direction(1)

			qwater(i_WELL)=qwater(i_WELL)+
     $			WATER_grad(1,ig)*normal(1)+WATER_grad(2,ig)*normal(2)
			qOIL(i_WELL)=qOIL(i_WELL)+
     $			OIL_grad(1,ig)*normal(1)+OIL_grad(2,ig)*normal(2)		                 
9105		continue
	ENDDO
	ENDDO
!	IF(IMESH.GE.4.AND.I_WELL.EQ.1) GOTO 100 
!	IF(I_WELL.EQ.1) GOTO 100
      IF(IMESH.GE.5.AND.I_WELL.EQ.1) GOTO 100  !! ========Lxd========
	RETURN
	end subroutine

      SUBROUTINE FLUID_BODY_LOAD(NXE,NYE,NODEP,NODEG,IDER,ISAMP,TOT,  
     1ICOORD,IJAC,IJAC1,NGRAV,X,Y,geometrytype,GRAVY,FORCE,KAY,dtim
     1	,rho,if_boussinesq)
C
C********* THIS SUBROUTINE PERFORMS MULTIPLY A MATRIX BY A VECTOR ******
C
      IMPLICIT NONE
	INTEGER NXE,NYE,NODEP(40,80,8),NODEG(40,80,20)
	integer geometrytype
	INTEGER IDER,ISAMP,TOT,ICOORD,NODF,IJAC,IJAC1,NGRAV
	REAL*8  X(*),Y(*),TERM,GRAVY,FORCE(*),kay(40,80,4,2),DTIM

	REAL*8 GTHETA,GXCOM,GYCOM,COORD(4,2),SAMP(ISAMP,2)
	INTEGER II,JJ,IN,I,J,G(20),GP,INP
	REAL*8 DER(2,4),DERIV(2,4),FUN(4),JAC(2,2),JAC1(2,2),DET,QUOT
	INTEGER NF,NFT
	INTEGER igrav,IDEN,IG
	real*8 pi,rho
	COMMON /TMP1/ NF(5000,3), NFT(5000,1)	
	common /iconsider/igrav
	integer if_boussinesq
C***********SETTING INPUT DATA. THESE INPUT DATA SHOULD BE 
C           READEN FROM DATA FILE
C********WE THINK THAT EXTRA FORCE IS LOADED WHEN NS=0 ********
C NOTE: THIS PART IS JUST FOR EXTRA LOAD. IT MEANS THAT THE NORMAL LOAD IS 
C FORCE PRESSURE AND PORE PRESSURE IN INSIDE AND OUTSIDE OF WELL( IT IS DONE AS 
C ABOVE CODE). EXCEPT NORMAL LOAD CONDITION, IF USER WANT TO  
C LOAD ORTHER FORCE, USE FOLLOWING CODE.

C***********GRAVITY LOAD*******************
C********** LOOP OVER EACH ELEMENT********
C  SET THICKNESS=1 AND MATERIAL MASS DENSITY=2
C NOTE:  IN FACT, THEY SHOULD BE INPUT DATA********MODIFY AFTER***JULY,EQ
C        THICK=TERM
c	igrav=0
c      IF(NGRAV.EQ.0) GO TO 21
	IF(igrav.EQ.0) GO TO 21
	pi=4.0d0*datan(1.0d0)
C********AVERAGE DANSITY OF STONE IS ABOUT 2650KG/M^3=2.65*10^-6KG/MM^3
C  DANCY*GRAVY=SZG*10E6  **************** SEPT3,2000 EQ
C****DEFAULT GTHETA=0.D0
	print*,"fluid_body_load is used"
	GTHETA=0.0D0
	GRAVY= 9.8*rho
	GXCOM= 1.0D-6*GRAVY*DSIN(GTHETA)
	GYCOM=-1.0D-6*GRAVY*DCOS(GTHETA)
      DO 20 II=1,NXE
	DO 20 JJ=1,NYE
C******ADVOICE TO CALL TUNGEO AGAIN AND AGAIN*****************************
		DO IN=1,TOT  !TOT=20 for sagd 8 for ux, 8 for uy and 4 for p
			G(IN)=NODEG(II,JJ,IN) !G is a nf number (degree of freedom index)
		END DO
		DO IN=1,ICOORD !ICOORD=4 for sagd for 4 nodes on vertices
			COORD(IN,1)=X(NODEP(II,JJ,2*IN-1))
			COORD(IN,2)=Y(NODEP(II,JJ,2*IN-1))
		END DO
C*************************************************************************
		GP=2
		IG=0
		call null(samp,ISAMP,2)
		DO 30 I=1,GP
		DO 30 J=1,GP
			IG=IG+1
			CALL GAUSS(SAMP,GP)
			CALL FORMLN(DER,IDER,FUN,SAMP,GP,I,J)
C
C************ CONVERT LOCAL COORDINATES TO GLOBAL COORDINATES ***********
			CALL MATMUL(DER,IDER,COORD,ICOORD,JAC,IJAC)
			CALL TWOBY2(JAC,IJAC,JAC1,IJAC1,DET)
			CALL MATMUL(JAC1,IJAC1,DER,IDER,DERIV,4)
!!Shunfu.Z 2010.06.11
			term=0.0d0
			if (geometrytype.eq.1) then
				term=1.0d0
			else if (geometrytype.eq.2) then
				DO IN=1,ICOORD
					term=term+fun(in)*COORD(IN,1)
				END DO
				term=2.0d0*pi*term    
			endif
c			write(*,*) term
!!Shunfu.Z 2010.06.11
			CALL QUOTCA ( SAMP,GP,DET,TERM,QUOT,I,J )
C** IF AXIAL SYMMETRY, IT IS ANOTHER FOMULAR
			DO 40 IN=1,ICOORD
				INP=NODEP(II,JJ,2*IN-1)
				IDEN=NF(INP,3)
				IF (IDEN.NE.0) THEN
	FORCE(IDEN)=FORCE(IDEN)-DERIV(1,IN)*GXCOM*QUOT*KAY(II,JJ,IG,1)*DTIM
	FORCE(IDEN)=FORCE(IDEN)-DERIV(2,IN)*GYCOM*QUOT*KAY(II,JJ,IG,2)*DTIM
				END IF
 40			CONTINUE
 30		CONTINUE
 20   CONTINUE

 21   CONTINUE
	RETURN
	END

      SUBROUTINE SOLID_BODY_LOAD(NXE,NYE,NODEP,NODEG,IDER,ISAMP,TOT,  
     1ICOORD,IJAC,IJAC1,NGRAV,X,Y,geometrytype,GRAVY,FORCE,KAY,dtim
     1	,rho,if_boussinesq)
C
C********* THIS SUBROUTINE PERFORMS MULTIPLY A MATRIX BY A VECTOR ******
C
      IMPLICIT NONE
	INTEGER NXE,NYE,NODEP(40,80,8),NODEG(40,80,20)
	integer geometrytype
	INTEGER IDER,ISAMP,TOT,ICOORD,NODF,IJAC,IJAC1,NGRAV
	REAL*8  X(*),Y(*),TERM,GRAVY,FORCE(*),kay(40,80,4,2),DTIM

	REAL*8 GTHETA,GXCOM,GYCOM,COORD(ICOORD,2),SAMP(ISAMP,2)
	INTEGER II,JJ,IN,I,J,G(20),GP,INP
	REAL*8 DER(2,ICOORD),DERIV(2,ICOORD),FUN(ICOORD)
	real*8 JAC(2,2),JAC1(2,2),DET,QUOT
	INTEGER NF,NFT
	INTEGER igrav,IDEN,IG
	real*8 pi,rho
	COMMON /TMP1/ NF(5000,3), NFT(5000,1)	
	common /iconsider/igrav
	integer if_boussinesq
C***********SETTING INPUT DATA. THESE INPUT DATA SHOULD BE 
C           READEN FROM DATA FILE
C********WE THINK THAT EXTRA FORCE IS LOADED WHEN NS=0 ********
C NOTE: THIS PART IS JUST FOR EXTRA LOAD. IT MEANS THAT THE NORMAL LOAD IS 
C FORCE PRESSURE AND PORE PRESSURE IN INSIDE AND OUTSIDE OF WELL( IT IS DONE AS 
C ABOVE CODE). EXCEPT NORMAL LOAD CONDITION, IF USER WANT TO  
C LOAD ORTHER FORCE, USE FOLLOWING CODE.

C***********GRAVITY LOAD*******************
C********** LOOP OVER EACH ELEMENT********
C  SET THICKNESS=1 AND MATERIAL MASS DENSITY=2
C NOTE:  IN FACT, THEY SHOULD BE INPUT DATA********MODIFY AFTER***JULY,EQ
C        THICK=TERM
c	igrav=0
c      IF(NGRAV.EQ.0) GO TO 21
	IF(igrav.EQ.0) GO TO 21
	pi=4.0d0*datan(1.0d0)
C********AVERAGE DANSITY OF STONE IS ABOUT 2650KG/M^3=2.65*10^-6KG/MM^3
C  DANCY*GRAVY=SZG*10E6  **************** SEPT3,2000 EQ
C****DEFAULT GTHETA=0.D0
	print*,"solid_body_load is used"
	GTHETA=0.0D0
	GRAVY= 9.8*rho
	GXCOM= 1.0D-6*GRAVY*DSIN(GTHETA)
	GYCOM=-1.0D-6*GRAVY*DCOS(GTHETA)
      DO 20 II=1,NXE
	DO 20 JJ=1,NYE
C******ADVOICE TO CALL TUNGEO AGAIN AND AGAIN*****************************
		DO IN=1,TOT
			G(IN)=NODEG(II,JJ,IN)
		END DO
		DO IN=1,ICOORD
			COORD(IN,1)=X(NODEP(II,JJ,IN))
			COORD(IN,2)=Y(NODEP(II,JJ,IN))
		END DO
C*************************************************************************
		GP=2
		IG=0
		call null(samp,ISAMP,2)
		DO 30 I=1,GP
		DO 30 J=1,GP
			IG=IG+1
			CALL GAUSS(SAMP,GP)
			call fmquad(der,ider,fun,samp,gp,i,j,1)
C
C************ CONVERT LOCAL COORDINATES TO GLOBAL COORDINATES ***********
			CALL MATMUL(DER,IDER,COORD,ICOORD,JAC,IJAC)
			CALL TWOBY2(JAC,IJAC,JAC1,IJAC1,DET)
			CALL MATMUL(JAC1,IJAC1,DER,IDER,DERIV,4)
!!Shunfu.Z 2010.06.11
			term=0.0d0
			if (geometrytype.eq.1) then
				term=1.0d0
			else if (geometrytype.eq.2) then
				DO IN=1,ICOORD
					term=term+fun(in)*COORD(IN,1)
				END DO
				term=2.0d0*pi*term    
			endif
c			write(*,*) term
!!Shunfu.Z 2010.06.11
			CALL QUOTCA ( SAMP,GP,DET,TERM,QUOT,I,J )
C** IF AXIAL SYMMETRY, IT IS ANOTHER FOMULAR
			DO 40 IN=1,ICOORD
				INP=NODEP(II,JJ,IN)
				IDEN=NF(INP,1)
				IF (IDEN.NE.0) THEN
					FORCE(IDEN)=FORCE(IDEN)-fun(IN)*GXCOM*QUOT
				END IF
				IDEN=NF(INP,2)
				IF (IDEN.NE.0) THEN
					FORCE(IDEN)=FORCE(IDEN)-fun(IN)*GyCOM*QUOT
				END IF
 40			CONTINUE
 30		CONTINUE
 20   CONTINUE

 21   CONTINUE
	RETURN
	END

C  ! By Shunfu.Z, 2010-07-21 不含毛管力、源汇项，边界积分项,采用单位制 MPa,Day,m  ！Without capillary force, sources and sinks, the boundary integral term system of 
C s MPa, Day, m
      SUBROUTINE SAT_zhangshunfu(NXE,NYE,nn,Num_T_V,A_SAT,krw,dump,
     $                X,Y,nodep4,nodep,poro,dtim,
     $                N_BKFIX,IABKFIX,i_4_8_nodes,i_8_4_nodes,
     $				ww,forcereac,st1)                                                                                                                   
      Implicit NONE
	integer n_bkfix,iabkfix(500,0:3)
	integer i_4_8_nodes(*),i_8_4_nodes(*)
	integer NXE,NYE,nn,nodep4(40,80,4),nodep(40,80,8),Num_T_V
	real*8  A_SAT(*),x(*),Y(*)
	real*8 poro(40,80,4),dtim,ww,krw(40,80,4,2),dump(*)
	real*8 forcereac(*),st1(*)
	real*8  SKROT,SKRWT,SWWC,SWT,SO,SW,SG
	integer NUMOW
	COMMON/PHASE2/SKROT(20),SKRWT(20),SWWC,SWT(20),SO(40,80,4),
     &              SW(40,80,4),SG(40,80,4),NUMOW
	real*8 vis_temp
	COMMON/vistemp/vis_temp(50,3)

	integer ii,jj
	real*8 SKRO,SKRW,fw,temp(5000),vel(2,nn),VIS_O,VIS_w
	real*8 temperature(50),vis_water(50),vis_oil(50)
c	real*8 F_GLOBE(nn),por(nn)
	integer p,q,nodeid4(4),IN,IG,GP,I,J
	real*8 samp(2,2),DUMP_X(4),DUMP_Y(4),coordf(4,2),A_F2(4)
	REAL*8 ELEMENT_F2(4),EM(4,4),dtkd(4,4),es(4,4),KDERIV(2,4)
	REAL*8 derf(2,4),FUNF(4),derivf(2,4)
	real*8 jac(2,2),jac1(2,2),derivt(4,2)
	REAL*8 DET,QUOT,VALUE,EMMAX,EMMIN

	integer ijac,ijac1,ikay,ikderv,iderf,idervf
	integer icordf,idtkd,ikp,nodf

      data ijac,ijac1,ikay,ikderv,iderf,idervf/6*2/                                                                
      data icordf,idtkd,ikp,nodf/4*4/
	real*8 node(2,4),kexi,eta,dsat,dsatmax
	integer nstep
	real*8 value_x,value_y
	REAL*8 emass(50000),loadss(5000),ANs(5000)
	REAL*8 AP(50000),BP(50000),APori(50000)
	integer iw
	real*8 DSMAX,ddtim,A_KW(2,2)
	integer number,ii1,jj1
	real*8 dump4(nn)
	integer nf,nft
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)

c	write(4999,*) NXE,NYE,nn,Num_T_V,dtim,N_BKFIX,IABKFIX,ww
c	write(4999,*) nodep,poro
c	stop
	II=nodep4(1,1,2)
	JJ=nodep4(1,1,4)
	IF(ABS(II-JJ).GT.iW) iW=ABS(II-JJ)
	do ii=1,nn
		dump4(ii)=dump(i_4_8_nodes(ii))
	enddo

	do jj=1,Num_T_V
		temperature(jj) = vis_temp(jj,1)
		vis_water(jj)   = vis_temp(jj,2)
		vis_oil(jj)     = vis_temp(jj,3)
	enddo
	DO II=1,NN
		CALL INTERP(SWT,SKRWT,NUMOW,a_sat(ii),SKRW)
	!! relative permeability for oil
		CALL INTERP(SWT,SKROT,NUMOW,a_sat(ii),SKRO)
	!! water viscosity at nodes
		CALL INTERP(temperature,vis_water,Num_T_V,st1(ii),vis_w)
	!! oil viscosity at nodes
		CALL INTERP(temperature,vis_oil,  Num_T_V,st1(ii),vis_o)
		fw=SKRW/VIS_W/(SKRO/VIS_O+SKRW/VIS_W)
	fw=1.0d0
		forcereac(nf(i_4_8_nodes(ii),3))=FW*forcereac(nf(i_4_8_nodes(ii),3))
	ENDDO
 !!	LEAST SQUARE SOLUTION
c 	call NULVEC(F_GLOBE, NN)
	call NULVEC(EMASS,50000)
	call NULVEC(AP,50000)
	call NULVEC(APori,50000)
	call NULVEC(BP,50000)
 	GP=2
	do p=1,nxe
	do q=1,nye                                                                                               
		do in=1,icordf !!get the coordinate information
			nodeid4(in)= nodep4(p,q,in)                                                                           
			coordf(in,1)=x(nodep(p,q,2*in-1))                                                         
			coordf(in,2)=y(nodep(p,q,2*in-1))                                                       
		end do

 		CALL GAUSS(samp,2)
		ig=0
c		call NULVEC(A_F2,4)
		call NULl(EM,4,4)
		call null(es,4,4)
		do i = 1, gp                                                                          
		do j = 1, gp                                                                       
			ig=ig+1                                                                                                                                                   
			call formln(derf,iderf,funf,samp,gp,i,j)                                        
			call matmul(derf,iderf,coordf,icordf,jac,ijac)                                                                                                     
			call twoby2(jac,ijac,jac1,ijac1,det)                                                                                                 
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf) 
			call quotca ( samp,gp,det,1.0D0,quot,i,j)     			
			!NODE INTEGRATION TO FORM LOAD VECTOR
!! stiffness MATRIX
	        A_KW(1,1) = krw(p,q,ig,1) 
			A_KW(2,2) = krw(p,q,ig,2)                                                                 
			A_KW(1,2) = 0.0d0                                                                          
			A_KW(2,1) = 0.0d0
			CALL NULL(DTKD,4,4)
 			CALL MATMUL(A_KW,2,derivf,2,KDERIV,4)             
 			call matran(derivt,4,derivf,2)                           
			CALL MATMUL(DERIVT,4,KDERIV,2,DTKD,4)
			CALL MSMULT(DTKD,quot,4,4)
c			CALL MSMULT(DTKD,DTIM,4,4)
			call matadd(ES,4,DTKD,4)  								
!! CONSISTENT MASS MATRIX 
			CALL NULL(DTKD,4,4)
			CALL MATMUL(funf,4,funf,1,DTKD,4)
			CALL MSMULT(DTKD, poro(p,q,ig),4, 4)
			CALL MSMULT(DTKD,quot,4,4) 
			call matadd(EM,4,DTKD,4)                                                                					                                                                                                                                             
		enddo
		enddo  !! END OF GAUSS POINT RECYCLE
		CALL FORMKVT(AP,EM,4,NODEID4,NN,4)
		CALL FORMKVT(BP,ES,4,NODEID4,NN,4)
	enddo
	enddo
!!SOLUTION
	call nulvec(loadss,nn)
	CALL linmul(BP,DUMP4,loadss,nN,iw)
	call nulvec(ANS,nn)
	CALL linmul(AP,A_SAT,ANS,nN,iw)
c	loadss=0.0d0
	do in=1,nn
	ans(in)=-loadss(in)*dtim+ANS(in) -forcereac(nf(i_4_8_nodes(in),3)) 
	enddo

C	call nulvec(loadss,nn)
C	CALL linmul(AP,A_SAT,loadss,nN,iw)
C	call matadd(AP,500000,BP,1) 
c	goto 777
	do ii=1,nn
	do jj=1,n_bkfix
		if(iabkfix(jj,2).eq.256) then
			if(i_4_8_nodes(ii).eq.iabkfix(jj,0)) then
c				AP(ii)=1.0d200
c				ans(ii)=WW*1.0d200
c				write(4999,*) ii
			endif	
		endif
	enddo
	enddo
777	continue
	do ii=1,50000
		apori(ii)=ap(ii)
	enddo
	call banredT(AP,nn,iw)
	call bacsubT(AP,ans,nn,iw)
	!!上下限处理  ！（Upper and lower processing）
	do ii=1,nn
c		if(loadss(ii).ge.0.8d0) loadss(ii)=0.8d0
c		if(loadss(ii).le.0.2d0) loadss(ii)=0.0d0
	enddo
	
	dsmax=0.0d0
	do ii=1,nn
C		a_sat(ii)=a_sat(ii)+loadss(ii)*dtim
		dsat=ans(ii)-a_sat(ii)
		if (abs(dsat).gt.dsmax) dsmax=abs(dsat)
	enddo
	dsmax=1.0d-4
	if (dsmax.le.0.002d0) then 
		do ii=1,nn
C		a_sat(ii)=a_sat(ii)+loadss(ii)*dtim
			a_sat(ii)=ans(ii)
		enddo 
	else
		number=dsmax/0.002d0
		ddtim=dtim/number
c	write(*,*) number,dsmax
		do ii=1,number
			call nulvec(ANS,nn)
			CALL linmul(APori,A_SAT,ANS,nN,iw)					
			do in=1,nn
				ans(in)=loadss(in)*ddtim+ANS(in) 
			enddo
			do ii1=1,nn
			do jj1=1,n_bkfix
				if(iabkfix(jj1,2).eq.256) then
					if(i_4_8_nodes(ii1).eq.iabkfix(jj1,0)) then
						ans(ii1)=WW*1.0d200
					endif	
				endif
			enddo
			enddo
			call bacsubT(AP,ans,nn,iw)
			do ii1=1,nn
				a_sat(ii1)=ans(ii1)
			enddo
		enddo	
	endif                                                                            
	END


	subroutine saturation(nxe,nye,nn,i_4_8_nodes,Num_T_V,nodep4,
     $			volstr,volstr0,poro,dump,DUMP_OLD,
     $			st1,A_SAT,COMP_W,COMP_o)
	implicit none
	integer nxe,nye,nn,i_4_8_nodes(*),Num_T_V,nodep4(40,80,4)
	real*8 volstr(48,80,4),poro(40,80,4),volstr0(48,80,4) !!node8
	real*8 dump(*),DUMP_OLD(*)  !!node8
	real*8 st1(*),A_SAT(*),COMP_W,COMP_o !!node4
	
	real*8 vis_temp
	COMMON/vistemp/vis_temp(50,3)
	real*8  SKROT,SKRWT,SWWC,SWT,SO,SW,SG
	integer NUMOW
	COMMON/PHASE2/SKROT(20),SKRWT(20),SWWC,SWT(20),SO(40,80,4),
     &              SW(40,80,4),SG(40,80,4),NUMOW

	integer ii,jj,p,q
	real*8 temperature(50),vis_water(50),vis_oil(50)
	real*8 SKRO,SKRW,fw,VIS_O,VIS_w,dsat,node(2,4)
	real*8 funf(4),KEXI,ETA
	real*8 volst(nn),volst0(nn),por(nn)

	call nulvec(volst,nn)
	!!prosity extrapolation
	do p=1,nxe
	do q=1,nye
		node(1,1)=-dsqrt(3.0d0)
		node(2,1)=-dsqrt(3.0d0)
		node(1,2)=-dsqrt(3.0d0)
		node(2,2)=+dsqrt(3.0d0)
		node(1,3)=+dsqrt(3.0d0)
		node(2,3)=+dsqrt(3.0d0)
		node(1,4)=+dsqrt(3.0d0)
		node(2,4)=-dsqrt(3.0d0)
		do ii=1,4
			kexi=node(1,ii)
			eta=node(2,ii) 
			funf(1)=0.25*(1+kexi)*(1+eta)
			funf(2)=0.25*(1-kexi)*(1+eta)
			funf(3)=0.25*(1+kexi)*(1-eta)
			funf(4)=0.25*(1-kexi)*(1-eta)
			volst(nodep4(p,q,ii))=0.0d0
			por(nodep4(p,q,ii))=0.0d0
			volst0(nodep4(p,q,ii))=0.0d0
			do jj=1,4
	volst(nodep4(p,q,ii))=volst(nodep4(p,q,ii))+funf(jj)*volstr(p,q,jj)
	por(nodep4(p,q,ii))=por(nodep4(p,q,ii))+funf(jj)*poro(p,q,jj)
	volst0(nodep4(p,q,ii))=volst0(nodep4(p,q,ii))+funf(jj)*volstr0(p,q,jj)
			enddo
		enddo
	enddo
	enddo

	do jj=1,Num_T_V
		temperature(jj) = vis_temp(jj,1)
		vis_water(jj)   = vis_temp(jj,2)
		vis_oil(jj)     = vis_temp(jj,3)
	enddo
	do ii=1,nn
		dsat=0.0d0		
		!! relative permeability for water
		CALL INTERP(SWT,SKRWT,NUMOW,a_sat(ii),SKRW)
		!! relative permeability for oil
		CALL INTERP(SWT,SKROT,NUMOW,a_sat(ii),SKRO)
	!! water viscosity at nodes
		CALL INTERP(temperature,vis_water,Num_T_V,st1(ii),vis_w)
	!! oil viscosity at nodes
		CALL INTERP(temperature,vis_oil,  Num_T_V,st1(ii),vis_o)
		fw=SKRW/VIS_W/(SKRO/VIS_O+SKRW/VIS_W)
		dsat=fw*(comp_w*a_sat(ii)+comp_o*(1.0d0-a_sat(ii)))
		dsat=dsat*(dump(i_4_8_nodes(ii))-dump_old(i_4_8_nodes(ii)))
!		dsat=dsat+(fw-a_sat(ii))*volst(ii)
!		dsat=dsat+(a_sat(ii)+fw)*(volst(ii)-volst0(ii))/por(ii)

		dsat=dsat+(a_sat(ii)+fw)*(volst(ii) -volst0(ii))/por(ii)
		a_sat(ii)=a_sat(ii)+dsat
		if(a_sat(ii).le.swwc) then
			a_sat(ii)=swwc
		else
			if(a_sat(ii).gt.1.0d0) a_sat(ii)=1.0d0
		endif
	enddo
	end subroutine




