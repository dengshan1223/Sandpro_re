       subroutine set5_nf(n_fix,iafix,
     *              nxe,nye,nn,nodof,n,nodep,IMESH)
c
c********* This subroutine reads node freedom array for the case *******
c********* of more than one freedom per node                     *******
c
c	 PARAMETER(INF=309)
c       Implicit real*8 (a-h, o-z)
	 IMPLICIT NONE
C	 COMMON /TMP1/ nf(20000,3)
	 integer n_fix,nxe,nye,nn,nodof
	 integer nf,nft,imesh
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)
	 integer nodep(40,80,8),iafix(500,0:1) ! [node_number,dof=1--x, 2--y, 3--p], read from input file

	 integer i,j,iq,ip,i_node,idof

	 integer n
	 
	 integer node_number !added by Bashar

       do 1 i = 1, nn
       do 1 j = 1, nodof
C1		nf(i,j) = 1
		nf(i,j) = 1
1	 continue
c	 nx1=2*nxe+1
c	 nx2=nxe+1
c	 ny1=2*nye+1
c	 ny2=nye+1
c	 nnd=3*nxe+2
ccc  setting middle nodes ccccccccccccccc
	do iq=1,nye
	do ip=1,nxe
		nf(nodep(ip,iq,2),3)=0
		nf(nodep(ip,iq,4),3)=0
		nf(nodep(ip,iq,6),3)=0
		nf(nodep(ip,iq,8),3)=0
	end do
	end do


ccc      setting boundary restrain ccccccccc
      do i=1,n_fix
		i_node=iafix(i,0)
		idof=iafix(i,1)
		nf(i_node,idof)=0
	end do
ccc------------end of setting --------------

c--         indexing
	 n=0
       do 2 i = 1, nn
       do 2 j = 1, nodof
              if(nf(i,j) .ne. 0) then
                 n=n+1
                 nf(i,j) = n
              endif
2      continue
c       do ii=1,nn
c 10   format(1x,4i8)
c       end do
c--------------------------------------------------------------------------
	!added by Bashar to remove the constraint on the Uy of the node located
	! at midway between centre of nodes
	if (IMESH.eq.5) then
!	node_number=nodep(nxe,nye/2,7)    !!=== this is wrong ,i think  ===Li===
!	n=n+1
!	nf(node_number,2) = n
!	write(666,*),"nf(",node_number,",2)=",n,";"
	endif
      


	if (IMESH.eq.3) then
c	node_number=nodep(nxe/2,1,5)
c	n=n+1
c	nf(node_number,2) = n
c	write(666,*),"nf(",node_number,",2)=",n,";"
	endif
      

	 !end of added by Bashar
c--------------------------------------------------------------------------
      
	 return
       end

       subroutine set_g(p,q,g,nodep,nodeg)
c
c***** This subroutine create a mesh of 8-node quadrilateral        ****
c***** plane elements in which coordinate for fluid is included too ****
c
c       PARAMETER(INF=309)
	 Implicit none
       integer p,q,g(20)
	 integer nodep(40,80,8),nodeg(40,80,20)
	 integer nf,nft,ii
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)
	
       g(1) =nf(nodep(p,q,1),1)
       g(2) =nf(nodep(p,q,1),2)
       g(3) =nf(nodep(p,q,2),1)
       g(4) =nf(nodep(p,q,2),2)
       g(5) =nf(nodep(p,q,3),1)
       g(6) =nf(nodep(p,q,3),2)
       g(7) =nf(nodep(p,q,4),1)
       g(8) =nf(nodep(p,q,4),2)
       g(9) =nf(nodep(p,q,5),1)
       g(10)=nf(nodep(p,q,5),2)
       g(11)=nf(nodep(p,q,6),1)
       g(12)=nf(nodep(p,q,6),2)
       g(13)=nf(nodep(p,q,7),1)
       g(14)=nf(nodep(p,q,7),2)
       g(15)=nf(nodep(p,q,8),1)
       g(16)=nf(nodep(p,q,8),2)
       g(17)=nf(nodep(p,q,1),3)
       g(18)=nf(nodep(p,q,3),3)
       g(19)=nf(nodep(p,q,5),3)
       g(20)=nf(nodep(p,q,7),3)
    
	do ii=1,20
	nodeg(p,q,ii)=g(ii)
	end do

       return
       end

       subroutine ma5_load(n1_deload,i1elemx,i1elemy,i1elm_deload,                        
     *   ang1_elm_deload,                                                                 
     *   n3_deload,i3elemx,i3elemy,i3elm_deload, !3 is the upper wellbore                 
     *   ang3_elm_deload,                                                                 
     *   n2_deload,i2elemx,i2elemy,i2elm_deload,                                          
     *   ang2_elm_deload, 
     *   temp1,temp3,shv,pwci,pwci2,pwco,force,isamp,nk,ns,                                                                                                        
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf                                    
     1   ,ijac,ijac1,geometrytype,force1,                                                          
     m kay,DTIM,NT,igrav,if_pass_body_load,rho,if_boussinesq)                                                                       
c                                                                                         
c********* This subroutine is for the formation of Right Hand Side ******                  
c                                                                                         
       Implicit real*8 (a-h, o-z)
	 
	 integer geometrytype                                                                                                                                             
      integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer i3elemx(80),i3elemy(80),i3elm_deload(80,3)                           
	 real*8 ang3_elm_deload(80,3)                                                         
       integer p,q,nxe,nye,icoord,icordf,g(20),ider,gp,isamp                              
     1             ,ao,al,am,an,ap,aq,ar,as,ns,nk(3000),tot                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 noprs(3),press(3,2)                                                         
       real*8 coord(8,2),coordf(4,2),elcod(2,3)                                           
       real*8 x(5000),y(5000),pgash(2),dgash(2)                                         
	 real*8 der(2,8),fun(8)                                                                  
       real*8 samp(isamp,2)                                                               
	 real*8 force(10000),force1(5000)                                                       
	 real*8 jac(2,2),jac1(2,2),det,quot                                                      
	 integer nodep(40,80,8),nodeg(40,80,20)                                                   
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
c	 COMMON/LOAD1/radius(51),rad(41),pofix(1000),point(1000,2),gtheta,
	COMMON/LOAD1/radius(82),rad2(42),pofix(1000),point(1000,2),gtheta,                        
	1 gravy,pressa(1000,6),spoke(41),dofix(1000,3)                                            
	 COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)                         
	1 ,lodpt(1000),nbound,ndrill,ndirect                                                      
	 COMMON/flowc/nflowi,nflowo 
	 real*8 PERMX,PERMY,tsdrill0,tsdrill
	 integer NNSTEP
	 COMMON /STEPINF/ PERMX,PERMY,tsdrill0,tsdrill,NNSTEP                                                              
       integer if_pass_body_load ,if_boussinesq                                                                                  
	 real*8 kay(40,80,4,2) ,rho
	 ang=3.141592627/(2.0*nxe)                                                          
c***********setting input data. These input data should be                                
c           readen from data file
! 	if(ns.eq.NNSTEP+1) then
c 	temp1=0.0d0
c 	temp3=0.0d0
c	pwci=0.0d0
c	pwci2=0.0d0
c	pwco=0.0d0
c	print*,"MA5_LOAD is used. NNSTEP=", NNSTEP
 	
	if (if_pass_body_load.eq.0) then
	if(ns.eq.NNSTEP+1) then
       call fluid_body_load(NXE,NYE,NODEP,NODEG,IDER,ISAMP,TOT,ICORDF  
     1   ,IJAC,IJAC1,iGRAV,X,Y,geometrytype,GRAVY,FORCE,KAY,DTIM
     1	,rho,if_boussinesq)

       call SOLID_body_load(NXE,NYE,NODEP,NODEG,IDER,ISAMP,TOT,ICOORD  
     1   ,IJAC,IJAC1,iGRAV,X,Y,geometrytype,GRAVY,FORCE,KAY,DTIM
     1	,rho,if_boussinesq)   
 	endif
	endif
 

	if(ns.gt.20) then
c 	temp1=0.0d0
c 	temp3=0.0d0
 	endif
c	  write(3999,*) ns,'======',1                                                                                                                                      
	 call deload(i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,  !! bottom hole                              
     1	n1_deload,                                                                         
     1	 temp1,temp3,pwci,force,isamp,nk,ns,                                               
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf                                    
     1   ,ijac,ijac1,geometrytype)
c     1   ,ijac,ijac1,term)
c	stop
	!!goto 152
c	write(3999,*) ns,'======',3
	 if (geometrytype.eq.1) then                                                    
	 call deload(i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,  !! top hole                             
     1	n3_deload,                                                                         
     1	 temp1,temp3,pwci2,force,isamp,nk,ns,                                              
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf
     1   ,ijac,ijac1,geometrytype)
	 else if (geometrytype.eq.2) then
	 if (ns.eq.0) then   
	 call deloadaxis(i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,  !! top hole                             
     1	n3_deload,                                                                         
!     1	 shv,temp3,force,isamp,nk,ns, 
     1	 temp1,temp3,force,isamp,nk,ns,                                              
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf
     1   ,ijac,ijac1,geometrytype)
	 endif
	endif
c		stop                                   
c     1   ,ijac,ijac1,term)
!152	continue                                                                                                                                                              
	 if(ns.eq.0) then
c	    write(3999,*) ns,'======',2                                                                     
c		call deloadout(i2elemx,i2elemy,i2elm_deload,ang2_elm_deload, !! outward boundary
		call deload(i2elemx,i2elemy,i2elm_deload,ang2_elm_deload, !! outward boundary                                
     1	n2_deload,                                                                         
     1	 temp1,temp3,pwco,force,isamp,nk,ns,                                               
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf
     1   ,ijac,ijac1,geometrytype)                                     
c     1   ,ijac,ijac1,term) 
c	stop
C	goto 153
!      call body_load(NXE,NYE,NODEP,NODEG,IDER,ISAMP,TOT,ICOORD,  
!     1   ICORDF,IJAC,IJAC1,NGRAV,X,Y,TERM,GRAVY,FORCE)
!       call SOLID_body_load(NXE,NYE,NODEP,NODEG,IDER,ISAMP,TOT,  
!     1   ICOORD,IJAC,IJAC1,iGRAV,X,Y,geometrytype,GRAVY,FORCE,KAY,DTIM)   
! 153	continue                                                          
	 else 
		call deload(i2elemx,i2elemy,i2elm_deload,ang2_elm_deload, !! outward boundary                                
     1	n2_deload,                                                                         
     1	 0.0d0,0.0d0,pwco,force,isamp,nk,ns,                                               
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf 
     1   ,ijac,ijac1,geometrytype)                                     
c     1   ,ijac,ijac1,term)
c		goto 124                                                                                    
c		call deloadout(i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                                
c     1	n2_deload,                                                                         
c     1	 0.d0,0.d0,pwco,force,isamp,nk,ns,                                                 
c	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf                                    
c     1   ,ijac,ijac1,mterm)  
c 124		continue                                                              
	 endif                                                                                 
                                                                                                                                                      
       if (ns.eq.0) then                                                                                                                                         
		call nodal_load(force,point,nplod,nodpt)                                                                                                                                   
		call edge_load(nedge,force,isamp,ider,x,y,term)                                                                                                                                                                       
       !! fluid_body_load was called at each increment
!	 goto 123
!       call fluid_body_load(NXE,NYE,NODEP,NODEG,IDER,ISAMP,TOT,  
!     1   ICORDF,IJAC,IJAC1,NGRAV,X,Y,TERM,GRAVY,FORCE,KAY) 
                                                                          
!       call fluid_body_loadmaz(temp1,temp3,pwci,pwco,force1,isamp,nk,ns,                      
!	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf                                    
!     1   ,ijac,ijac1,term,kay,DTIM) 
!123	 continue                                                                                                                                
	 end if                                                                                    
	 return                                                                                   
	 end 

       subroutine deload(ielemx,ielemy,                                                   
     1	ielm_deload,ang_elm_deload,n_deload,                                              
     1	temp1,temp3,pwc,force,isamp,nk,ns,                                                
	1	ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf
     1   ,ijac,ijac1,geometrytype)	                                    
c     1	,ijac,ijac1,term)                                                                
c                                                                                         
c********* This subroutine performs multiply A matrix by a vector ******                  
c                                                                                         
       Implicit none                                                      
       integer N_DELOAD,nf,IJAC,IJAC1,geometrytype
	 real*8 temp1,temp3,dr,pwc,term
	 real*8 POFIX                                                                                  
       integer ielemx(80),ielemy(80),ielm_deload(80,3)                              
	 real*8 ang_elm_deload(80,3)                                                            
       integer p,q,nxe,nye,icoord,icordf,g(20),ider,gp,isamp                              
     1             ,ao,al,am,an,ap,aq,ar,as,ns,nk(3000),tot                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 press(3,2)
	 integer noprs(3)                                                         
       real*8 coord(8,2),coordf(4,2),elcod(2,3)                                           
       real*8 x(5000),y(5000),pgash(2),dgash(2)                                         
	 real*8 der(2,8),fun(8)                                                                  
       real*8 samp(isamp,2),angle                                                                                                                                     
	 real*8 jac(2,2),jac1(2,2),det,quot                                                      
	 integer nodep(40,80,8),nodeg(40,80,20),NFT                                                   
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                                                                     
c******************************	 
	 integer IEDGE,kk,ii,jj,in,IODEG,ig,IDOFN,knode
	 real*8 t1,t2,t3,dvolu,PXCOM,PYCOM,dfactor
c******************************	 
	 real*8 force(10000)
	 real*8 DVOLUX,DVOLUY  
c***********setting input data. These input data should be                                
c           readen from data file 	                                                        
       ang=atan(1.0d0)/nxe*2.0d0
	 pi=4.0d0*datan(1.0d0)                                                                               
       do 9201 iedge=1,n_deload                                                            
		ao=ielm_deload(iedge,1) !!first node
		al=ielm_deload(iedge,2) !!second node
		am=ielm_deload(iedge,3) !!third node
c*************noprs--node id to be deloaded
		noprs(1)=ao
		noprs(2)=al
		noprs(3)=am
                                                                         
c*********** pressure in each node, 1---x direction, 2--y direction 
	!! deload initial stress in the well to zero                     
	 	t1=ang_elm_deload(iedge,1)                                                      
	 	t2=ang_elm_deload(iedge,2)                                                        
	 	t3=ang_elm_deload(iedge,3)
	!	t1=(iedge-1.0d0)*angle
	!	t2=t1+angle/2.0d0 
	!	t3=t2+angle/2.0d0
		                                                                        
	press(1,1)=-temp3*dcos(t1)**2.d0/dr-temp1*dsin(t1)**2.d0/dr  !!http://en.wikipedia.org/wiki/Principal_stress#Principal_stresses_and_stress_invariants                          
	press(1,2)=-temp1*dcos(t1)*dsin(t1)/dr+temp3*dsin(t1)*dcos(t1)/dr !!  Stress transformation in plane stress and plane strain                     
	press(2,1)=-temp3*dcos(t2)**2.d0/dr-temp1*dsin(t2)**2.d0/dr                            
	press(2,2)=-temp1*dcos(t2)*dsin(t2)/dr+temp3*dsin(t2)*dcos(t2)/dr   !! convert to r,t direction                   
	press(3,1)=-temp3*dcos(t3)**2.d0/dr-temp1*dsin(t3)**2.d0/dr                            
	press(3,2)=-temp1*dcos(t3)*dsin(t3)/dr+temp3*dsin(t3)*dcos(t3)/dr
	goto 7003
	press(1,1)=-temp1*dcos(t1)**2.d0/dr-temp3*dsin(t1)**2.d0/dr  !!http://en.wikipedia.org/wiki/Principal_stress#Principal_stresses_and_stress_invariants                          
	press(1,2)=-temp3*dcos(t1)*dsin(t1)/dr+temp1*dsin(t1)*dcos(t1)/dr !!  Stress transformation in plane stress and plane strain                     
	press(2,1)=-temp1*dcos(t2)**2.d0/dr-temp3*dsin(t2)**2.d0/dr                            
	press(2,2)=-temp3*dcos(t2)*dsin(t2)/dr+temp1*dsin(t2)*dcos(t2)/dr   !! convert to r,t direction                   
	press(3,1)=-temp1*dcos(t3)**2.d0/dr-temp3*dsin(t3)**2.d0/dr                            
	press(3,2)=-temp3*dcos(t3)*dsin(t3)/dr+temp1*dsin(t3)*dcos(t3)/dr
7003  continue		                                                                                                                                                  
		do iodeg=1,3                                                                      
			elcod(1,iodeg)=x(noprs(iodeg))  !! coordinate information                                                        
			elcod(2,iodeg)=y(noprs(iodeg))
 		enddo
c*************** get B matrix for solid, der here = deriv in Owen                         
		gp=2
		do 9105 ig=1,gp                                                                        
			call gauss(samp,gp)
c************** xi=weigh in gauss in Owen                                                 
			call fmquad(der,ider,fun,samp,gp,ig,1,2) !! xi can equal to -1.0 only (so gauss quadrature was limited to one dimension)                                                           
			do idofn=1,2
				pgash(idofn)=0.d0
				dgash(idofn)=0.d0
				do iodeg=1,3
			pgash(idofn)=pgash(idofn)+press(iodeg,idofn)*fun(iodeg) !! pressure at gauss point
 			dgash(idofn)=dgash(idofn)+elcod(idofn,iodeg)*der(2,iodeg) !! [d x/ d eta; d y/ d eta]
				enddo
			enddo
			dvolu=1.d0
!			pxcom=(dgash(1)*pgash(2)-dgash(2)*pgash(1)) !! outward normal vector (-dgash(2),dgash(1)) should be normalized
Cmine			pxcom=(dgash(1)*pgash(2)+dgash(2)*pgash(1)) !! outward normal vector (-dgash(2),dgash(1)) should be normalized
			pxcom=(dgash(1)*pgash(2)+dgash(2)*pgash(1))
     *			!/(dgash(1)**2.0+dgash(2)**2.0)**0.5
!			pycom=(+dgash(1)*pgash(1)+dgash(2)*pgash(2)) !! Tangential vctor (dgash(1),dgash(2)) 
cmine			pycom=(-dgash(1)*pgash(1)+dgash(2)*pgash(2)) !! Tangential vctor (dgash(1),dgash(2))
			pycom=(-dgash(1)*pgash(1)+dgash(2)*pgash(2)) !! Tangential vctor (dgash(1),dgash(2)) 

!Ma.Z 	    pxcom=dgash(1)*pgash(2)-dgash(2)*pgash(1)
!Ma.Z	    pycom=dgash(1)*pgash(1)+dgash(2)*pgash(2)

			term=0.0d0
			if (geometrytype.eq.1) then
				term=1.0d0
			else if (geometrytype.eq.2) then
				do iodeg=1,3
					term=term+elcod(1,iodeg)*fun(iodeg)
				enddo 
				term=2.0d0*pi*term    
			endif
		                                  
			do 9120 knode=1,3
!	           if(nf(ielm_deload(iedge,knode),1).ne.0) then
				if(nf(noprs(knode),1).ne.0) then                                                                                            
					force(nf(noprs(knode),1))                                           
!     1				=force(nf(noprs(knode),1))+fun(knode)*pxcom*dvolu !MA.Z
     1			=force(nf(noprs(knode),1))-fun(knode)*pxcom*dvolu*term !mine                                               
				end if 
!				if(nf(ielm_deload(iedge,knode),2).ne.0) then                                                                            
				if(nf(noprs(knode),2).ne.0) then                                                                                            
					force(nf(noprs(knode),2))                                          
!     1				=force(nf(noprs(knode),2))+fun(knode)*pycom*dvolu  !MA.Z
     1			=force(nf(noprs(knode),2))-fun(knode)*pycom*dvolu*term  !mine                                                 
				end if                                                                             
9120			continue
			force(nf(ao,3))=pwc
			force(nf(am,3))=pwc
9105		continue

9201   continue

!	do iedge=1,n_deload                                                            
!		ao=ielm_deload(iedge,1) !!first node
!		al=ielm_deload(iedge,2) !!second node
!		am=ielm_deload(iedge,3) !!third node
!	enddo

	 return
	 end

c Ma.Z      if (ns.eq.0) then
c Ma.Z      if (ns.eq.0) then                                                             
       subroutine nodal_load(force,point,nplod,nodpt)                                                                
c                                                                                         
c********* This subroutine performs multiply A matrix by a vector ******                  
c                                                                                         
       Implicit none
	 real*8 force(10000),point(1000,2)
	 integer nplod,nf,nodpt(1000),NFT
	 integer ip,INODE,JNODE                                             
                                                                                                                                              
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)
c	 COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)                         
c	1 ,lodpt(1000),nbound,ndrill,ndirect                                                                                                                                                                                
c**********nodal load*********************                                                                                                                              
       if(nplod.eq.0) go to 11                                                             
	 do 10 ip=1,nplod                                                                  
		inode=nf(nodpt(ip),1)                                                                 
		if (inode.ne.0) then                                                                   
			force(inode)=force(inode)+point(ip,1)                                                 
		end if                                                                                 
		jnode=nf(nodpt(ip),2)                                                                 
		if (jnode.ne.0) then                                                                   
			force(jnode)=force(jnode)+point(ip,2)                                                 
		end if                                                                                 
  10	 continue                                                                          
                                                                                          
  11	 continue                                                                            
	 return                                                                                   
	 end


       subroutine fluid_body_loadmaz(temp1,temp3,pwci,pwco,force,isamp,nk,                   
     1 	 ns,                                                                              
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf                                    
     1   ,ijac,ijac1,term,                                                                
     m   kay,DTIM)                                                                        
c                                                                                         
c********* This subroutine performs multiply A matrix by a vector ******                  
c                                                                                         
       Implicit real*8 (a-h, o-z)                                                         
                                                                                          
       integer p,q,nxe,nye,icoord,icordf,g(20),ider,gp,isamp                              
     1             ,ao,al,am,an,ap,aq,ar,as,ns,nk(3000),tot                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 noprs(3),press(3,2)                                                         
       real*8 coord(8,2),coordf(4,2),elcod(2,3)                                           
       real*8 x(5000),y(5000),pgash(2),dgash(2)                                         
	 real*8 der(2,8),fun(8)                                                                  
       real*8 samp(isamp,2)                                                               
	 real*8 force(5000)                                                                     
	 real*8 jac(2,2),jac1(2,2),det,quot                                                      
	                                                                                         
	 real*8 derivt(4,2),RG(2),FORCE_ELEM(4)                                                   
	 real*8 ADD_FORCE_ELEM(4),deriv(2,4)                                                      
                                                                                          
	 integer nodep(40,80,8),nodeg(40,80,20)                                                   
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
       COMMON/INITIAL/shmaxg,shming,szg,pbg,rw,rw2,pw,z,rb                                 
c	 COMMON/LOAD1/radius(51),rad(41),pofix(1000),point(1000,2),gtheta,
	COMMON/LOAD1/radius(82),rad2(42),pofix(1000),point(1000,2),gtheta,                        
	1 gravy,pressa(1000,6),spoke(41),dofix(1000,3)                                            
	 COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)                         
	1 ,lodpt(1000),nbound,ndrill,ndirect                                                      
	 COMMON/flowc/nflowi,nflowo                                                               
                                                                                          
	 real*8 tmp1(2),z(4)                                                                      
	 real*8 kay(40,80,4,2)
                                                                                 
       ang=3.1415926/(2.0*nxe)                                                             
c***********setting input data. These input data should be                                
c           readen from data file                                                         
                                                                                          
                                                                                          
c********we think that extra force is loaded when ns=0 ********                           
c note: this part is just for extra load. It means that the normal load is                
c force pressure and pore pressure in inside and outside of well( it is done as           
c above code). Except normal load condition, if user want to                              
c load orther force, use following code.                                                  
                                                                                          
c***********gravity load*******************                                               
c********** loop over each element********                                                                     
	                                                                                         
       if(ngrav.eq.0) go to 21                                                             
	                                                                                         
c****default gtheta=0.d0                                                                  
	 gtheta=0.d0                                                                              
!	force=0                                                                                 
                                                                                          
       do 20 ii=1,nxe                                                                      
		do 20 jj=1,nye                                                                          
                                                                                          
c	  gxcom=dense*gravy*dsin(gtheta)                                                        
c	  gycom=-dense*gravy*dcos(gtheta)                                                       
			!!gxcom=szg*1.0d6*dsin(gtheta)                                                           
			!!gycom=-szg*1.0d6*dcos(gtheta)
			gxcom=szg*dsin(gtheta) 
			gycom=-szg*dcos(gtheta)                                                                                             
c******advoice to call tungeo again and again*****************************                
			do in=1,tot                                                                            
				g(in)=nodeg(ii,jj,in)                                                                 
			end do                                                                                 
			do in=1,icoord                                                                         
				coord(in,1)=x(nodep(ii,jj,in))                                                        
				coord(in,2)=y(nodep(ii,jj,in))                                                        
			end do                                                                                 
			do in=1,icordf                                                                         
				coordf(in,1)=x(nodep(ii,jj,(2*in-1)))                                                 
				coordf(in,2)=y(nodep(ii,jj,(2*in-1)))                                                 
				z(in)=coordf(in,2) !!horizontal well                                                          
			end do                                                                                 
c*************************************************************************                
			gp=2                                                                                   
			nodf=4                                                                                 
                                                                                          
			ig=0
			ADD_FORCE_ELEM=0
			do 30 i=1,gp
				do 30 j=1,gp
					ig=ig+1
					call gauss(samp,gp)
					call formln(der,ider,fun,samp,gp,i,j)
c                                                                                         
c************ Convert local coordinates to global coordinates ***********                 
					call matmul(der,ider,coordf,icordf,jac,ijac)                                         
					call twoby2(jac,ijac,jac1,ijac1,det)                                                 
					call matmul(jac1,ijac1,der,ider,deriv,nodf)                                          
					call matran(derivt,nodf,deriv,ider)	                                                 
					call matmul(deriv,2,z,4,tmp1,1) ! 对比tmp1和rg, tmp1应是(0,1)                        
                                                                                          
					RG(1)=0 ! Gravity vector                                                             
					RG(2)=DTIM*PBG*kay(ii,jj,ig,2)                                                                                                                                    
!         call matmul(derivt,4,RG,2,FORCE_ELEM,1)                                                                                                                                           
					call matmul(RG,1,deriv,2,FORCE_ELEM,4)                                               
					call quotca ( samp,gp,det,term,quot,i,j )                                                                                               
					ADD_FORCE_ELEM=ADD_FORCE_ELEM+FORCE_ELEM*quot                                        
c** if axial symmetry, it is another fomular
 30			continue                                                                            
                                                                                  
                                                                                          
! 组装到全局载荷向量  !Assembly to the global load vector                                                                    
			do 40 in0=1,icordf
				in=in0*2-1
				inp=nodep(ii,jj,in)
				if (nf(inp,3).ne.0) then
			force(nf(inp,3))=force(nf(inp,3)) + ADD_FORCE_ELEM(in0)                         
				 end if                                                                               
 40			continue                                                                                                                                                              
 20		continue                                                                             
 21    continue                                                                                                                                                            
	 return                                                                                   
	 end 

	 subroutine CALC_FLOW(Q_W,Q_O,PW_GRAD,PO_GRAD,x,y,nodep,                                  
     1	n_bkfix,iabkfix,nxe,nye)                                                           
       Implicit none                                                     
	 real*8 Q_W(2),Q_O(2) ! HOLE1 & HOLE2                                                     
	 real*8 PW_GRAD(40,80,4,2), PO_GRAD(40,80,4,2) ! 50*50 elems, 4 gaussian points, grad alon
	 real*8 x(5000),y(5000)
	 integer nodep(40,80,8),n_bkfix,iabkfix(500,0:3),nxe,nye 
	 real*8 fNORMAL_VEC(2),FLOW_W(2,2),FLOW_O(2,2),EDGE_COORD(2,2)
	                             
	 integer N_GAUSS2NODE(4),gp,ii,jj,ig,i_NODE,i_FLOW_PT
	 integer i,j,ifix,i_FIX_node,i_EDGE                                                                 
       real*8 FW_NORMAL_FLOW,fO_Normal_FLow 
                                                                                                                                                                                   
	 N_GAUSS2NODE(1)=5                                                                        
	 N_GAUSS2NODE(2)=3                                                                        
	 N_GAUSS2NODE(3)=7                                                                        
	 N_GAUSS2NODE(4)=1                                                                        
	 Q_W=0.0d0                                                                                    
	 Q_O=0.0d0                                                                                 
                                                                                          
 	 gp=2                                                                                
       do ii=1,nxe                                                                         
	 do jj=1,nye                                                                              
		ig=0                                                                                    
		i_FLOW_PT=0                                                                             
		do i=1,gp                                                                               
		do j=1,gp                                                                               
			ig=ig+1                                                                                
			i_NODE=nodep(ii,jj,N_GAUSS2NODE(ig))                                                   
			DO ifix=1,n_bkfix                                                                      
				i_FIX_node=iabkfix(ifix,0)                                                            
!				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                        				                                                                                      
				IF((iabkfix(ifix,2).EQ.1.OR.iabkfix(ifix,2).EQ.2)                                     
     1				.AND.i_FIX_node.EQ.i_NODE)THEN ! hole or hole2                                          
					i_FLOW_PT=i_FLOW_PT+1                                                                
					FLOW_W(i_FLOW_PT,1)=PW_GRAD(ii,jj,ig,1)                                              
					FLOW_W(i_FLOW_PT,2)=PW_GRAD(ii,jj,ig,2)                                              
					FLOW_O(i_FLOW_PT,1)=PO_GRAD(ii,jj,ig,1)                                              
					FLOW_O(i_FLOW_PT,2)=PO_GRAD(ii,jj,ig,2)                                              
					EDGE_COORD(i_FLOW_PT,1)=x(i_NODE)                                                    
					EDGE_COORD(i_FLOW_PT,2)=y(i_NODE)                                                    
					i_EDGE=iabkfix(ifix,2) ! 1--hole1, 2--hole2                                    
					GOTO 1 ! found! jump out. avoid continuing loop                                      
				ENDIF                                                                                 
			ENDDO                                                                                  
1			CONTINUE                                                                              
		ENDDO                                                                                   
		ENDDO                                                                                   
                                                                                          
		IF(i_FLOW_PT.GT.0) THEN                                                                 
			IF(i_FLOW_PT.NE.2) THEN                                                                
c				WRITE(*,*) 'Error: Elem ',ii,' ',jj,' has',i_FLOW_PT,                                 
c     1			'flow edge nodes. should be 2.'                                                  
			ENDIF                                                                                  
			! 因是按照高斯点循环，此处i_FLOW_PT顺序是逆时针。右侧是外向 
			!(Due to the cycle of Gauss points the here i_FLOW_PT order is counterclockwise. The right is the outgoing)                           
			fNORMAL_VEC(1)=  EDGE_COORD(2,2)-EDGE_COORD(1,2)                                       
			fNORMAL_VEC(2)=-(EDGE_COORD(2,1)-EDGE_COORD(1,1))                                      
c			fL=sqrt(fNORMAL_VEC(1)*fNORMAL_VEC(1)                                                  
c     1			+fNORMAL_VEC(2)*fNORMAL_VEC(2))                                                  
	!		fNORMAL_VEC(1)=fNORMAL_VEC(1)/fL                                                      
	!		fNORMAL_VEC(2)=fNORMAL_VEC(2)/fL                                                      
	!		fNormal_FLow_W=(FLOW_W(1,1)+FLOW_W(2,1))/2.0*fNORMAL_VEC(1)*fL                        
	!    1		+(FLOW_W(1,2)+FLOW_W(2,2))/2.0*fNORMAL_VEC(2)*fL ! 乘L是在边界上积分    (Multiplied by L boundary integral)         
	! 改为效率较高的，不用先乘后除L     (Changed to higher efficiency, without first multiply except L)                                                     
			fNORMAL_VEC(1)=fNORMAL_VEC(1)                                                          
			fNORMAL_VEC(2)=fNORMAL_VEC(2)                                                          
			fW_Normal_FLow=(FLOW_W(1,1)+FLOW_W(2,1))/2.0*fNORMAL_VEC(1)                            
     1			+(FLOW_W(1,2)+FLOW_W(2,2))/2.0*fNORMAL_VEC(2)                                    
			fO_Normal_FLow=(FLOW_O(1,1)+FLOW_O(2,1))/2.0*fNORMAL_VEC(1)                            
     1			+(FLOW_O(1,2)+FLOW_O(2,2))/2.0*fNORMAL_VEC(2)                                    
			Q_W(i_EDGE)=Q_W(i_EDGE)+fW_Normal_FLow                                                 
			Q_O(i_EDGE)=Q_O(i_EDGE)+fO_Normal_FLow                                                 
		ENDIF                                                                                   
	 ENDDO                                                                                    
	 ENDDO 
	 continue                                                                                 
       END 

	 subroutine CALC_P_GRAD(force,isamp, ! force here is solution, Ux, Uy, p, ...
	1   ider,nxe,nye,x,y,nodep,nodeg,tot,icordf                                    
     1   ,ijac,ijac1,PW_GRAD, PO_GRAD,KRW,KRO,psand,szg,imesh)                                         
c                                                                                         
c********* This subroutine performs multiply A matrix by a vector ******                  
c                                                                                         
       Implicit none 
	 integer nxe,nye,tot,nodeg(40,80,20),ICORDF,nodep(40,80,8),isamp
	 integer ider,ijac,ijac1,imesh
	 real*8 x(5000),y(5000),force(5000),samp(isamp,2)
	 integer nf, nfT
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)
	 real*8 krw(40,80,4,2),kro(40,80,4,2),psand,szg
	 
	 real*8 gtheta,gxcom,gycom,coordf(4,2),z(4),porepress(4)
	 integer ii,jj,g(20),in,nn,gp,nodf,ig,i,j
	 real*8 der(2,8),fun(8),jac(2,2),jac1(2,2),det,psand1
	 real*8 deriv(2,4),derivt(4,2),tmp1(2),flow(2)
	 real*8 PW_GRAD(40,80,4,2), PO_GRAD(40,80,4,2)                                                                   	                                                                                                 
c***********setting input data. These input data should be                                
c           readen from data file                                                                                                                                                   
                                                                                          
c********we think that extra force is loaded when ns=0 ********                           
c note: this part is just for extra load. It means that the normal load is                
c force pressure and pore pressure in inside and outside of well( it is done as           
c above code). Except normal load condition, if user want to                              
c load orther force, use following code.                                                  
                                                                                          
c***********gravity load*******************                                               
c********** loop over each element********                                                
c****default gtheta=0.d0                                                                  
	 gtheta=0.d0                                                                                                                                                                          
       do 20 ii=1,nxe                                                                    
	 do 20 jj=1,nye                                                                                                                                                                   
		gxcom=szg*dsin(gtheta)                                                         
		gycom=-szg*dcos(gtheta)                                                                                                               
c******advoice to call tungeo again and again*****************************                
		do in=1,tot                                                                        
			g(in)=nodeg(ii,jj,in)                                                                   
		end do                                                                                                                                                                   
		do in=1,icordf                                                                          
			coordf(in,1)=x(nodep(ii,jj,(2*in-1)))                                                   
			coordf(in,2)=y(nodep(ii,jj,(2*in-1)))                                                   
			z(in)=coordf(in,2)                                                                       
                                                                                          
			nn=nodep(ii,jj,(2*in-1))                                                                 
			porepress(in)=force(nf(nn,3))                                                            
		end do                                                                                  
c*************************************************************************                
		gp=2                                                                                    
		nodf=4                                                                                   
                                                                                          
		ig=0                                                                                     
		do 30 i=1,gp                                                                            
		do 30 j=1,gp                                                                            
			ig=ig+1                                                                                  
			call gauss(samp,gp)                                                                
			call formln(der,ider,fun,samp,gp,i,j)                                              
c                                                                                         
c************ Convert local coordinates to global coordinates ***********                 
			call matmul(der,ider,coordf,icordf,jac,ijac)                                     
			call twoby2(jac,ijac,jac1,ijac1,det)                                             
			call matmul(jac1,ijac1,der,ider,deriv,nodf)                                      
			call matran(derivt,nodf,deriv,ider)                                                    
                                                                                          
			call matmul(deriv,2,z,4,tmp1,1) ! 对比tmp1和rg, tmp1应是(0,1)   !Comparison tmp1, rg, tmp1 should the (0,1)                         
			call matmul(deriv,2,porepress,4,flow,1)
              !改
		    IF (IMESH.GE.3) THEN   
			   psand1=psand/3.14d0*2.0d0
			ELSE
                 psand1=psand/3.14d0*4.0d0			
			ENDIF                                                  
			PW_GRAD(ii,jj,ig,1)=-flow(1)*psand1*krw(ii,jj,ig,1)  !Negative because of reverse flows along the pressure gradient   
			PW_GRAD(ii,jj,ig,2)=-flow(2)*psand1*krw(ii,jj,ig,2)                                       
			PO_GRAD(ii,jj,ig,1)=-flow(1)*psand1*kro(ii,jj,ig,1)                                       
			PO_GRAD(ii,jj,ig,2)=-flow(2)*psand1*kro(ii,jj,ig,2)
 30		continue                                                                                                                                                                   
 20    continue
	 continue                                                                     
	 return                                                                                   
	 end  

	  subroutine edge_load(nedge,force,isamp,ider,x,y,term)                                                                
c                                                                                         
c********* This subroutine performs multiply A matrix by a vector ******                  
c                                                                                         
       Implicit none
	 integer NEDGE,IN,nf,nopress(1000,3),isamp,ider,NFT 
	 real*8 pressa(1000,6),term                                                                                                        
                                                                                                       
       real*8 x(5000),y(5000),pgash(2),dgash(2)                                         
	 real*8 der(2,8),fun(8)                                                                  
       real*8 samp(isamp,2)                                                                                                                                                                                                                                             
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)

	 integer IODEG,IG,IDOFN,KNODE,gp
	 real*8 DVOLU,DFACTOR,PXCOM,PyCOM
	 real*8 noprs(3),press(3,2),elcod(2,3)  

	 real*8 force(5000) 
                                                                                        
c************distributed edge loads**********************************8                    
c************for solid contribution                                                                                   
       if(nedge.eq.0) go to 51                                                             
       do 50 in=1,nedge                                                                    
     		noprs(1)=nopress(in,1)                                                             
		noprs(2)=nopress(in,2)                                                                  
		noprs(3)=nopress(in,3)                                                                  
c*********** pressure in each node, 1---Tangent direction, 2--normal direction                       
		press(1,1)=pressa(in,1)                                                                 
		press(1,2)=pressa(in,2)                                                                 
		press(2,1)=pressa(in,3)                                                                 
		press(2,2)=pressa(in,4)                                                                 
		press(3,1)=pressa(in,5)                                                                 
		press(3,2)=pressa(in,6)                                                                 	                                                                                         
c*********** avoid to call tungeo again and again*****************************                  
		do 902 iodeg=1,3                                                                        
			elcod(1,iodeg)=x(noprs(iodeg))                                                         
			elcod(2,iodeg)=y(noprs(iodeg))
902		continue                                                       
c*************** get B matrix for solid, der here = deriv in Owen                         
		gp=2 
		call gauss(samp,gp)                                                                                     
		do 905 ig=1,gp                                                                                                                                        
c************** xi=weigh in gauss in Owen                                                 
			call fmquad(der,ider,fun,samp,gp,ig,1,2)                                                                                                                      
			do idofn=1,2                                                                       
				pgash(idofn)=0.d0                                                                     
				dgash(idofn)=0.d0                                                                     
				do iodeg=1,3                                                                      
			pgash(idofn)=pgash(idofn)+press(iodeg,idofn)*fun(iodeg)                   
 			dgash(idofn)=dgash(idofn)+elcod(idofn,iodeg)*der(2,iodeg)
				enddo
			enddo                        
                                                                                          
			dvolu=dsqrt((elcod(1,1)-elcod(1,3))**2+
     $			  (elcod(2,1)-elcod(2,3))**2)/2.0d0
			dfactor=dsqrt(dgash(1)**2+dgash(2)**2)                                                                             
!			pxcom=(dgash(1)*pgash(2)-dgash(2)*pgash(1))/dfactor !! normal vector (-dgash(2),dgash(1)) should be normalized
!			pycom=(dgash(1)*pgash(1)+dgash(2)*pgash(2))/dfactor !! Tangential vctor (dgash(1),dgash(2)) 
			pxcom=(dgash(1)*pgash(2)+dgash(2)*pgash(1))/dfactor !! normal vector (-dgash(2),dgash(1)) should be normalized
			pycom=(-dgash(1)*pgash(1)+dgash(2)*pgash(2))/dfactor !! Tangential vctor (dgash(1),dgash(2))  
			
!			dvolu=1.d0
!	        pxcom=dgash(1)*pgash(2)-dgash(2)*pgash(1)
!	        pycom=dgash(1)*pgash(1)+dgash(2)*pgash(2)
			                                                                       
			do 920 knode=1,3                                                                     
				if(nf(noprs(knode),1).ne.0) then                                                       
				force(nf(noprs(knode),1))=force(nf(noprs(knode),1))                                   
!	1										+fun(knode)*pxcom*dvolu   !zhang
     1                                      -fun(knode)*pxcom*dvolu*term                                                       
	  !! pxcom--x方向分量;  pycom--y方向分量  !pxcom - x direction component; pycom - y direction component                                               
				end if                                                                                 
				if(nf(noprs(knode),2).ne.0) then                                                       
				force(nf(noprs(knode),2))=force(nf(noprs(knode),2))                                   
!	1										+fun(knode)*pycom*dvolu   !zhang
     1                                      -fun(knode)*pycom*dvolu*term                                                              
				end if                                                                                 
920			continue                                                                             
905      continue                                                                         
50     continue                                                                         
51     continue                                                                            
	                                                                                         
       return                                                                             
       end 
c Ma.Z      end if

	subroutine meshing(OPTMESH1)
      IMPLICIT NONE
	CHARACTER*256 OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5,INPUT1,INPUT2
	CHARACTER*256 OPTMESH1
      COMMON/OUTPUT/OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5,INPUT1,INPUT2
C---------------------------------------
C    INPUT DATA BASED KEYWORD
C---------------------------------------
	INTEGER(2) tmpday, tmpmonth, tmpyear
	INTEGER(2) tmphour, tmpminute, tmpsecond, tmphund
	INTEGER N_EXP


	CALL GETDAT(tmpyear, tmpmonth, tmpday)
	CALL GETTIM(tmphour, tmpminute, tmpsecond, tmphund)
      N_EXP=0
	IF (TMPYEAR.GT.2010) N_EXP=1
	IF (TMPYEAR.EQ.2010.AND.TMPMONTH.GT.7) N_EXP=1
	IF (TMPYEAR.EQ.2010.AND.TMPMONTH.EQ.7.AND.TMPDAY.GT.1) N_EXP=1

 	CALL Unpack
      OPEN(unit=12,file=OUTPUT1,status='unknown')
c      OPEN(unit=14,file="output.g04",status='unknown') !.G04 by Ma.Z
      OPEN(unit=13,file=OUTPUT2,status='unknown')
      OPEN(unit=10,file=OUTPUT3,status='unknown')
      OPEN(unit=2,file=OUTPUT4,status='unknown')
c      OPEN(unit=3,file="OUTPUT.DAT",status='unknown')  !taken off by Carl, reused by Ma.Z

143	FORMAT(A100)
C END OF 8-31 CHANGE
C---------------------------------------
C             CALCULATION
C---------------------------------------
      CALL fem(1,OPTMESH1)

      CLOSE(2)
c      CLOSE(3)
c      CLOSE(14) !Added by Ma.Z
	CLOSE(13)
      CLOSE(12)
c	close(11)
	close(10)
      END

	function get_ang(ixe,imesh,nxe)
      IMPLICIT NONE
	integer ixe,imesh,nxe
	real*8 get_ang
	!!Get the angle of LOWEST(FIRST) point
	if(imesh.EQ.5) then
		get_ang=-90.D0+(ixe-1)*180.D0/NXE
	elseif(imesh.EQ.3) then
		get_ang=-90.D0+(ixe-1)*180.D0/NXE
	elseif(imesh.EQ.1) then
		get_ang=(ixe-1)*90.D0/NXE
      elseif (imesh.eq.2) then
		get_ang=0.0d0
	else
c		call FASSERT(0)
	endif

	return
	end

	function get2_ang(ixe,imesh,nxe)
      IMPLICIT none
	integer ixe,imesh,nxe
	real*8 get2_ang
	!!Get the angle of midddle point
	if(imesh.EQ.5) then
		get2_ang=(0.5D0+ixe-1)*180.D0/NXE-90.D0
      elseif (imesh.eq.2) then
	    get2_ang = 0.0d0
	elseif(imesh.EQ.3) then
		get2_ang=(0.5D0+ixe-1)*180.D0/NXE-90.D0
	elseif(imesh.EQ.1) then
		get2_ang=(0.5D0+ixe-1)*90.D0/NXE
	else
c		call FASSERT(0)
	endif

	return
	end

       subroutine body_load(temp1,temp3,pwci,pwco,force,isamp,nk,ns,
	1   ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf
     1   ,ijac,ijac1,term)
c
c********* This subroutine performs multiply A matrix by a vector ******
c
       Implicit real*8 (a-h, o-z)

       integer p,q,nxe,nye,icoord,icordf,g(20),ider,gp,isamp
     1             ,ao,al,am,an,ap,aq,ar,as,ns,nk(3000),tot
       real*8 sl,cl,sr,cr,ang,pi
       real*8 ifacl,ifacr,ofacr,ofacl
       real*8 noprs(3),press(3,2)
       real*8 coord(8,2),coordf(4,2),elcod(2,3)
       real*8 x(5000),y(5000),pgash(2),dgash(2)
	 real*8 der(2,8),fun(8)
       real*8 samp(isamp,2)
	 real*8 force(18000)
	 real*8 jac(2,2),jac1(2,2),det,quot
	integer nodep(50,50,8),nodeg(50,50,20)
	COMMON /TMP1/ nf(5000,3)
      COMMON/INITIAL/shmaxg,shming,szg,pbg,rw,rw2,pw,z,rb
	COMMON/LOAD1/radius(51),rad(51),pofix(1000),point(1000,2),gtheta,
	1gravy,pressa(1000,6),spoke(51),dofix(1000,3)
	COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)
	1,lodpt(1000),nbound,ndrill,ndirect
	COMMON/flowc/nflowi,nflowo

      ang=3.1415926/(2.0*nxe)
c***********setting input data. These input data should be 
c           readen from data file


c********we think that extra force is loaded when ns=0 ********
c note: this part is just for extra load. It means that the normal load is 
c force pressure and pore pressure in inside and outside of well( it is done as 
c above code). Except normal load condition, if user want to  
c load orther force, use following code.

c***********gravity load*******************
c********** loop over each element********
c  set thickness=1 and material mass density=2
c note:  in fact, they should be input data********modify after***July,EQ
c        thick=term
      if(ngrav.eq.0) go to 21
c********average dansity of stone is about 2650kg/m^3=2.65*10^-6kg/mm^3
c  Dancy*gravy=szg*10e6  **************** Sept3,2000 EQ
c****default gtheta=0.d0
	gtheta=0.d0

        do 20 ii=1,nxe
	  do 20 jj=1,nye

c	  gxcom=dense*gravy*dsin(gtheta)
c	  gycom=-dense*gravy*dcos(gtheta)
	  gxcom=szg*1.0d6*dsin(gtheta)
	  gycom=-szg*1.0d6*dcos(gtheta)
c******advoice to call tungeo again and again*****************************
       do in=1,tot
	 g(in)=nodeg(ii,jj,in)
	 end do
	 do in=1,icoord
	 coord(in,1)=x(nodep(ii,jj,in))
	 coord(in,2)=y(nodep(ii,jj,in))
	 end do
	 do in=1,icordf
	 coordf(in,1)=x(nodep(ii,jj,(2*in-1)))
	 coordf(in,2)=y(nodep(ii,jj,(2*in-1)))
	 end do
c*************************************************************************
	 gp=2

	 do 30 i=1,gp
	 do 30 j=1,gp
       call gauss(samp,gp)
       call fmquad(der,ider,fun,samp,gp,i,j,1)
c
c************ Convert local coordinates to global coordinates ***********
       call matmul(der,ider,coord,icoord,jac,ijac)
       call twoby2(jac,ijac,jac1,ijac1,det)
       call quotca ( samp,gp,det,term,quot,i,j )
c** if axial symmetry, it is another fomular
	 do 40 in=1,icoord
	 inp=nodep(ii,jj,in)
	 if (nf(inp,1).ne.0) then
	  force(nf(inp,1))=force(nf(inp,1))+gxcom*fun(in)*quot
	 end if
	 if (nf(inp,2).ne.0) then
	  force(nf(inp,2))=force(nf(inp,2))+gycom*fun(in)*quot
	 end if
 40    continue
 30    continue

 20    continue
 21    continue
	return
	end
