! iangbkfix------ 井眼 （Borehole）                                                                   
! iabkfix(i, 1)==3  ------ 井眼和外边界，只有角节点 （The borehole and the outer boundary, only corner nodes）                                      
! iabkfix(i, 1)==1,2  ------ 外边界  ！Outer boundary                                                     
                                                                                          
!	ielemx(iedge)=iedge                                                                     
!	ielemy(iedge)=1                                                                         
!	ielm_deload(iedge,1)=ao                                                                 
!	ielm_deload(iedge,2)=al                                                                 
!	ielm_deload(iedge,3)=am                                                                 
 !     ang_elm_deload(iedge,1)=t3                                                         
       subroutine tun5geo(n_angbkfix,iangbkfix,angbkfix,                                  
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *              nn,nye,NYL,nntemp,                                                    
     *              nxe,nye0,spoke,radius,rad2,coord,icoord,                              
     *          coordf, icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                    
                                                                                     
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82),rad2(42)                                               
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	 real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
	 real*8 x_in2(0:100),y_in2(0:100)                                                         
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                  
	 integer iangbkfix(500)                                                                   
	 real*8 angbkfix(500)                                                                     
                                                                                          
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer i3elemx(80),i3elemy(80),i3elm_deload(80,3)                           
	 real*8 ang3_elm_deload(80,3)                                                          
  
      common/materials/elemmats(40,80)
	integer elemmats  
                                                                                          
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	 COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     * i_v4nodes(82,42),i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
c       added by BASHAR to write the geometry information and review
	 real*8 aspect_ratio
		
c        end of added by BASHAR
	 call null(ang1_elm_deload,80,3) 
	 call null(ang2_elm_deload,80,3)
	 call null(ang3_elm_deload,80,3)                
	 
	 nye=nye0*2  !nye0=12, nye=24   
c	 write(101,*),"NXE=",NXE,";"
c	 write(101,*),"NYE=",NYE,";"
c	 write(101,*),"NYE0=",NYE0,";"                                                                       
	 NYL=(nye0+1)*2 !NYL=26                                                                                                                                             
  	 nn_1=nye/2*(3*nxe+2)+(2*nxe+1) ! Total nodes in one area                           
	 nn=nn_1*2-(nxe/4*2+1) ! two areas minus common nodes                                    
 	 NELEMENT=nxe*nye                                                                                                                                                 
  	 nn4_1=(nye/2+1)*(nxe+1) ! Total nodes in one area                                  
	 nn4=nn4_1*2-(nxe/4+1) ! two areas minus common nodes                                                                                                                       
	 nntemp=(nxe+1)*NYL-(nxe/4+1) ! vertex nodes

! Make vertex nodes index                                                                                                                      
	do i2=1,nxe+1                                                                            
		do i1=1,nye+2                                                                           
			if(i1.LE.nye/2+1) then					! for bottom area:                                          
				i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                           
			elseif(i1.LE.nye/2+2) then                                                             
				if(i2.LE.nxe*1/4+1) then			! for top    area extra edges:                             
					i_vnodes(i1,i2)=0                                                                    
				else								! for top    area common edges:                                           
					i_vnodes(i1,i2)=nn_1+(i2-(nxe*1/4+1))*2                                              
				endif                                                                                 
			else										! for top    area:                                                       
				i_vnodes(i1,i2)=nn_1-(nxe*1/2+1)                                                      
     *			+ 2*i2-1+(2*nxe+2+nxe)*(i1-(nye/2+2))                                            
				continue                                                                              
			end if
c		write(101,*), "i_vnodes(",i1,",",i2,")=",i_vnodes(i1,i2),";"	                                                                                                                                   
	    end do                                                                               
	end do
                                                                         
! Splitting angles                                                                        
       pi=4.0D0*datan(1.D00)                                                              
       ang=0.5D0*pi/nxe !Ma.Z 0.5D0*pi/nxe                                                
	n_node=0                                                                                 
      do p=0,nxe*2                                                                        
		p_ang=-0.5*pi+p*ang                                                                     
		x_in(p)=radius(1)*dcos(p_ang) !!@下部井眼边界节点坐标(含中间结点) （Lower wellbore boundary node coordinates (including intermediate node)）                      
		y_in(p)=radius(1)*dsin(p_ang)                                                           
		x_in2(p)=rad2(1)*dcos(p_ang) !!@上部井眼边界节点坐标     （The upper borehole border node coordinates）                               
		y_in2(p)=rad2(1)*dsin(p_ang)                                                            
c          write(101,*),"x_in(",p+1,")=",x_in(p),";"
c		write(101,*),"y_in(",p+1,")=",y_in(p),";"
c		write(101,*),"x_in2(",p+1,")=",x_in2(p),";"
c		write(101,*),"y_in2(",p+1,")=",y_in2(p),";"                                                                                
		if(p_ang.LT.-pi/4.D0) then                                                              
			y_out(p)=-radius(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                          
		elseif(p_ang.LT.pi/4.D0) then                                                           
			x_out(p)=radius(nye0+1)                                                                
			y_out(p)=x_out(p)*dtan(p_ang)                                                          
		else                                                                                    
			y_out(p)=radius(nye0+1)                                                                
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                           
		endif
c		write(101,*),"x_out(",p+1,")=",x_out(p),";"
c		write(101,*),"y_out(",p+1,")=",y_out(p),";"                                                                                   
	end do                                                                                   
                                                                                          
	rm=radius(nye0+1)                                                                        
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0                                                                             
! bottom area coord 
                                      
	do q=1,nye/2+1                                                                           
	f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	f_out_weight=1.D0-f_in_weight                                                            
	f2_in_weight=(radius(nye0+1)-(radius(q+1)+radius(q))/2.D0)                               
     *	/(radius(nye0+1)-radius(1))                                                        
	f2_out_weight=1.D0-f2_in_weight
c	write(101,*),"f_in_weight(",q,")=",f_in_weight,";"
c	write(101,*),"f_out_weight(",q,")=",f_out_weight,";"
c	write(101,*),"f2_in_weight(",q,")=",f2_in_weight,";"
c	write(101,*),"f2_out_weight(",q,")=",f2_out_weight,";"
	                                                          
        do p=0,nxe ! lines that include vertex nodes & Middle points                      
		! Vertex points                                                                         
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                 
	!!	y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight-rm 
		y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight  !! Modified By Shunfu.Z 2009.07.08                           
999       format(i10,2f20.12)
                                                                                                                                                                                                
		if(p.EQ.0.OR.p.EQ.nxe) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=1 ! x disp is fixed                                                                                                              
		endif

!!==================Li================
		if(q.EQ.nye0+1.AND.p.le.nxe/4.0) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=2 ! y disp is fixed                                                                                                         
		endif

!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.le.(3*nxe/4)) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================

		!! Added By Shunfu.Z 2009.07.08 ,to constrain the bottom node's displacement in direction y
			                                                                                       
		if(q.EQ.1) then                                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                          
			iabkfix(n_bkfix,2)=1 ! hole1                                                           
		else if(q.EQ.nye0+1.AND.p.LE.nxe*3/4) then                                         
		!!else if(q.EQ.nye0+1.AND.p.LE.nxe*3/4.and.p.ge.nxe*1/4) then                                              
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=3 ! pressure disp is fixed
			iabkfix(n_bkfix,2)=256                                                                                            
		endif                                                                                   
		if(q.EQ.nye0+1.AND.p.LE.nxe*3/4) then	                                                                                       
		!!if(q.EQ.nye0+1.AND.p.LE.nxe*3/4.and.p.ge.nxe*1/4) then                                                   
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=1 ! Ux                                                              
			iabkfix(n_bkfix,2)=256 ! outer                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=2 ! Uy                                                              
			iabkfix(n_bkfix,2)=256 ! outer                                                                                                                        
		endif
	                                                                                       
		if(q.EQ.1) then                                                                         
			n_angbkfix=n_angbkfix+1                                                                
			iangbkfix(n_angbkfix)=n_node                                                           
c			angbkfix(n_angbkfix)=-3.1414926d0/2.d0+p*3.1414926d0/nxe  ! 包括边节点，故有2*nxe等分  （Edge nodes, so 2 * nxe decile）                                                        
			angbkfix(n_angbkfix)=-pi/2.d0+p*pi/nxe  ! 包括边节点，故有2*nxe等分     （Edge nodes, so 2 * nxe decile）                                                        
		endif                                                                                   
			                                                                                       
		if(p.LT.nxe) then ! 这一圈全都是Middle points      （This circle all Middle points ）                                     
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
		!!y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight-rm 
		y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight !! Modified by Shunfu.Z 2009.07.07                                                                  
!	if(q.EQ.nye0+1) then ! 好像应为 .AND.p.LE.nxe*3/4) then                                 
		!!if(q.EQ.nye0+1.AND.p.LE.nxe*3/4) then 
	!! Added By Shunfu.Z 2009.07.08 ,to constrain the bottom nodes' displacement in direction y
	!!	if(q.EQ.nye0+1.AND.p.Lt.nxe*1/4) then                                                             
	!!		n_fix=n_fix+1                                                                          
	!!		iafix(n_fix,0)=n_node                                                                  
	!!		iafix(n_fix,1)=2 ! y disp is fixed                                                                                                           
	!!	endif 
	!!  if(q.EQ.nye0+1.AND.p.Le.nxe*3/4) then   
		if(q.EQ.nye0+1.AND.p.Lt.nxe*3/4) then !!以免跟上部区域有重复节点 （To avoid duplicate nodes to keep up with the Ministry of Regional）
		!!if(q.EQ.nye0+1.AND.p.Lt.nxe*3/4.and.p.ge.nxe*1/4) then    （Including the edge nodes, so 2 * nxe, etc.）                                              
		n_bkfix=n_bkfix+1                                                                       
		iabkfix(n_bkfix,0)=n_node                                                               
		iabkfix(n_bkfix,1)=1 ! Ux                                                               
		iabkfix(n_bkfix,2)=256 ! outer                                                                                          
		n_bkfix=n_bkfix+1                                                                       
		iabkfix(n_bkfix,0)=n_node                                                               
		iabkfix(n_bkfix,1)=2 ! Uy                                                               
		iabkfix(n_bkfix,2)=256 ! outer                                                          
		endif  
		
!!==================Li================
		if(q.EQ.nye0+1.AND.p.lt.nxe/4.0) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=2 ! y disp is fixed                                                                                                         
		endif                                                                                   

!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.le.(3*nxe/4)) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================
			                                                                                       
		if(q.EQ.1) then                                                                         
		n_angbkfix=n_angbkfix+1                                                                 
		iangbkfix(n_angbkfix)=n_node                                                            
c	angbkfix(n_angbkfix)=-3.1414926d0/2.d0+(p+0.5d0)*3.1414926d0/nxe !包括边节点，故有2*nxe等=Including the edge nodes, so 2 * nxe, etc. （Including the edge nodes, so 2 * nxe, etc.）                                                    
	angbkfix(n_angbkfix)=-pi/2.d0+(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等 =Including the edge nodes, so 2 * nxe, etc.                                                    
		endif                                                                                   
		endif                                                                                   
	  end do                                                                                 
                                                                                          
          if(q.LT.nye0+1) then                                                            
		do p=0,nxe ! Middle edges                                                               
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                               
		!!y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight-rm 
		y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight                           
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
		n_fix=n_fix+1                                                                            
		iafix(n_fix,0)=n_node                                                                    
		iafix(n_fix,1)=1 ! x disp is fixed 
	endif                                                                                    
		end do                                                                                  
	endif                                                                                    
	end do 
                                                                                 
!! Removed by Shunfu.Z 2009.07.08                                                                                         
!	n_fix=n_fix+1                                                                            
!	iafix(n_fix,0)=n_node                                                                    
!	iafix(n_fix,1)=2 ! y disp is fixed                                                                

! top area coord                                                                          
c	do q=nye/2+2,nye+2                                                                      
	do q=nye/2+1,1,-1                                                                        
	f_in_weight=(rad2(nye0+1)-rad2(q))/(rad2(nye0+1)-rad2(1))                                
	f_out_weight=1.D0-f_in_weight
c	write(101,*),"f_in_weight(",q,")=",f_in_weight,";"
c	write(101,*),"f_out_weight(",q,")=",f_out_weight,";"                                                            
       do p=0,nxe ! lines that include vertex nodes                                       
		if((q.LT.nye/2+1).OR.(p.GE.nxe*1/4)) then                                               
			if((q.LT.nye/2+1).OR.(p.GT.nxe*1/4)) then ! Vertex points                               
				n_node=n_node+1                                                                         
				x(n_node)=x_in2(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                
			!!	y(n_node)=y_in2(p*2)*f_in_weight+y_out(p*2)*f_out_weight+rm 
	y(n_node)=y_in2(p*2)*f_in_weight+y_out(p*2)*f_out_weight+2.0d0*rm                                                                      
				if(p.EQ.0.OR.p.EQ.nxe) then                                                              
					n_fix=n_fix+1                                                                            
					iafix(n_fix,0)=n_node                                                                    
					iafix(n_fix,1)=1 ! x disp is fixed  
				endif                                                                                    

!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.ge.nxe/4) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================
			                                                                                       
				if(q.EQ.1) then ! Hole2                                                                  
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
					iabkfix(n_bkfix,2)=2 ! hole2                                                                                              
				else if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                               
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                                                                     
					iabkfix(n_bkfix,2)=256 ! outer	                                                          
				endif                                                                                    
			                                                                                       
				if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                                    
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=1 ! Ux                                                                
					iabkfix(n_bkfix,2)=256 ! outer                                                                                                    
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=2 ! Ux                                                                
					iabkfix(n_bkfix,2)=256 ! outer                                                          
				endif                                                                                    
			                                                                                       
				if(q.EQ.1) then                                                                          
					n_angbkfix=n_angbkfix+1                                                                  
					iangbkfix(n_angbkfix)=n_node                                                             
c			angbkfix(n_angbkfix)=-3.1414926d0/2.d0+p*3.1414926d0/nxe  ! 包括边节点，故有2*nxe等分 = Edge nodes, so 2 * nxe decile
			angbkfix(n_angbkfix)=-pi/2.d0+p*pi/nxe  ! 包括边节点，故有2*nxe等分 =Edge nodes, so 2 * nxe decile
				endif                                                                                    
			endif
	                                                                                 
			                                                                                       
	if(p.LT.nxe) then !Middle points                                                         
	n_node=n_node+1                                                                          
	x(n_node)=x_in2(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
	!!y(n_node)=y_in2(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight+rm
	y(n_node)=y_in2(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight+2.0d0*rm                          		                                              
!	if(q.EQ.nye0+1) then ! 好像应为 =Seems should be  .AND.p.GE.nxe*1/4) then                                 

!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.ge.nxe/4) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================

	if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                                    
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=1 ! Ux                                                                
		iabkfix(n_bkfix,2)=256 ! outer                                                          
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=2 ! Uy                                                                
		iabkfix(n_bkfix,2)=256 ! outer                                                          
	endif                                                                                    
			                                                                                       
	if(q.EQ.1) then                                                                          
	n_angbkfix=n_angbkfix+1                                                                  
	iangbkfix(n_angbkfix)=n_node                                                             
c	angbkfix(n_angbkfix)=-3.1414926d0/2.d0+(p+0.5d0)*3.1414926d0/nxe !包括边节点，故有2*nxe等=Including the edge nodes, so 2 * nxe, etc.
	angbkfix(n_angbkfix)=-pi/2.d0+(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等=Including the edge nodes, so 2 * nxe, etc.
	endif                                                                                    
		endif                                                                                   
		endif                                                                                   
	  end do                                                                                 
                                                                                          
          if(q.GT.1) then                                                                 
	f2_in_weight=(rad2(nye0+1)-(rad2(q-1)+rad2(q))/2.D0)                                     
     *	/(rad2(nye0+1)-rad2(1))                                                            
	f2_out_weight=1.D0-f2_in_weight
c	write(101,*),"f2_in_weight(",q,")=",f2_in_weight,";" 
c	write(101,*),"f2_out_weight(",q,")=",f_out_weight,";"                                                         
 		do p=0,nxe ! Middle edges                                                              
		n_node=n_node+1                                                                         
		x(n_node)=x_in2(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                              
		!!y(n_node)=y_in2(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight+rm 
	y(n_node)=y_in2(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight
     *	+2.0d0*rm                           
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
		n_fix=n_fix+1                                                                            
		iafix(n_fix,0)=n_node                                                                    
		iafix(n_fix,1)=1 ! x disp is fixed                                                       
	endif                                                                                    
		end do                                                                                  
	endif                                                                                    
	end do                                                                                                                                                                   
	                                                                                         
	n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0                                                                              
! elem nodes                                           
	do q=1,nye                                                                               
      do p=1,nxe                                                                          
	if(q.LE.nye0) then                                                                       
	      i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
101    format(8i10)                                                                       
		! 4节点网格系统   =   4-node grid system                                                                      
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
		                                                                                        
	elseif((q.EQ.nye0+1).AND.(p.LE.nxe*1/4))then                                             
	    aq=nn_1-(p-1)*2                                                                      
	    ar=aq-1                                                                              
	    as=ar-1                                                                              
		                                                                                        
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
	                                                                                         
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统    = 4-node grid system                                                                     
		Iq = NN4_1 - (P - 1)                                                                    
		Is = Iq - 1                                                                             
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
	elseif((q.EQ.nye0+1).AND.(p.EQ.nxe*1/4+1)) then                                          
	    aq=nn_1-(p-1)*2                                                                      
	    ar=(q-1)*(3*nxe+2)+2*p-1+nxe*3/2+1                                                   
	    as=ar+1                                                                              
		                                                                                        
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
	                                                                                         
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统      = Node grid system                                                                   
		Iq = NN4_1 - (P - 1)                                                                    
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
		Is = Im - (NXE + 1)                                                                     
	else                                                                                     
		aq=(q-1)*(3*nxe+2)+2*p-1+nxe*3/2                                                        
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
                                                                                          
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
                                                                                          
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统     =  Node grid system                                                                    
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
		Is = Im - (NXE + 1)                                                                     
		Iq = Is - 1                                                                             
	endif                                                                                                                                                  
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is  
		
          elemmats(p,q)=(q-1)*nxe+p
				                                                                      
c          write(101,*),"nodep(",p,",",q,",1)=",aq,";"
c		write(101,*),"nodep(",p,",",q,",2)=",an,";"
c		write(101,*),"nodep(",p,",",q,",3)=",ao,";"
c		write(101,*),"nodep(",p,",",q,",4)=",al,";"
c		write(101,*),"nodep(",p,",",q,",5)=",am,";"
c		write(101,*),"nodep(",p,",",q,",6)=",ap,";"
c		write(101,*),"nodep(",p,",",q,",7)=",as,";"
c		write(101,*),"nodep(",p,",",q,",8)=",ar,";"
c          write(101,*),"nodep4(",p,",",q,",1)=",iq,";"
c		write(101,*),"nodep4(",p,",",q,",2)=",io,";"
c		write(101,*),"nodep4(",p,",",q,",3)=",im,";"
c		write(101,*),"nodep4(",p,",",q,",4)=",is,";"
	if(q.EQ.1) then !hole 1                                                                                                                         
		n1_deload=n1_deload+1                                                                   
		i1elemx(n1_deload)=p
          i1elemy(n1_deload)=q                                                                    
		i1elm_deload(n1_deload,1)=ao                                                            
		i1elm_deload(n1_deload,2)=al                                                            
		i1elm_deload(n1_deload,3)=am
c		write(101,*),"i1elemx(",n1_deload,")=",p,";"
c		write(101,*),"i1elemy(",n1_deload,")=",q,";"
c		write(101,*),"i1elm_deload(",n1_deload,",1)=",ao,";"
c		write(101,*),"i1elm_deload(",n1_deload,",2)=",al,";"
c		write(101,*),"i1elm_deload(",n1_deload,",3)=",am,";"                                                            
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang1_elm_deload(n1_deload,1)=angbkfix(ij)
c	write(101,*),"ang1_elm_deload(",n1_deload,",1)=",angbkfix(ij),";"
			else if(iangbkfix(ij).eq.al) then
				ang1_elm_deload(n1_deload,2)=angbkfix(ij)
c	write(101,*),"ang1_elm_deload(",n1_deload,",2)=",angbkfix(ij),";"
			else if(iangbkfix(ij).eq.am) then                                                         
				ang1_elm_deload(n1_deload,3)=angbkfix(ij)
c	write(101,*),"ang1_elm_deload(",n1_deload,",3)=",angbkfix(ij),";"
			else
			endif
		enddo	                                                                                              
	endif
	continue                                                                                    
	if(q.eq.nye) then !hole 2                                                                                                                       
		n3_deload=n3_deload+1                                                                   
		i3elemx(n3_deload)=p                                                                    
		i3elemy(n3_deload)=q                                                                    
		i3elm_deload(n3_deload,1)=ao                                                            
		i3elm_deload(n3_deload,2)=al                                                            
		i3elm_deload(n3_deload,3)=am
c	    write(101,*),"i3elemx(",n3_deload,")=",p,";"
c		write(101,*),"i3elemy(",n3_deload,")=",q,";"
c		write(101,*),"i3elm_deload(",n3_deload,",1)=",ao,";"
c		write(101,*),"i3elm_deload(",n3_deload,",2)=",al,";"
c		write(101,*),"i3elm_deload(",n3_deload,",3)=",am,";"
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang3_elm_deload(n3_deload,1)=angbkfix(ij)
c     write(101,*),"ang3_elm_deload(",n3_deload,",1)=",angbkfix(ij),";"
			else if(iangbkfix(ij).eq.al) then                                                         
				ang3_elm_deload(n3_deload,2)=angbkfix(ij)
c      write(101,*),"ang3_elm_deload(",n3_deload,",2)=",angbkfix(ij),";"
			else if(iangbkfix(ij).eq.am) then                                                         
				ang3_elm_deload(n3_deload,3)=angbkfix(ij)
c      write(101,*),"ang3_elm_deload(",n3_deload,",3)=",angbkfix(ij),";"
			else
			endif
		enddo                                                                                                                                                                     
	endif                                                                                    
	if(q.eq.nye0.AND.p.LE.nxe*3/4.OR.q.eq.nye0+1.AND.p.GT.nxe/4) then ! out                  
c	if(q.eq.nye0) then ! out                                                                
		n2_deload=n2_deload+1                                                                   
		i2elemx(n2_deload)=p                                                                    
		i2elemy(n2_deload)=q                                                                    
		i2elm_deload(n2_deload,1)=as                                                            
		i2elm_deload(n2_deload,2)=ar                                                            
		i2elm_deload(n2_deload,3)=aq                                                            
c		ang2_elm_deload(n2_deload,1)=0
c		ang2_elm_deload(n2_deload,2)=0
c		ang2_elm_deload(n2_deload,3)=0
		!!Shunfu.Z
c	    write(101,*),"i2elemx(",n2_deload,")=",p,";"
c		write(101,*),"i2elemy(",n2_deload,")=",q,";"
c		write(101,*),"i2elm_deload(",n2_deload,",1)=",as,";"
c		write(101,*),"i2elm_deload(",n2_deload,",2)=",ar,";"
c		write(101,*),"i2elm_deload(",n2_deload,",3)=",aq,";"
		ang2_elm_deload(n2_deload,1)=0.0d0
		ang2_elm_deload(n2_deload,2)=0.0d0
		ang2_elm_deload(n2_deload,3)=0.0d0
c          write(101,*),"ang2_elm_deload(",n2_deload,",1)=",0,";"
c		write(101,*),"ang2_elm_deload(",n2_deload,",2)=",0,";"
c		write(101,*),"ang2_elm_deload(",n2_deload,",3)=",0,";"
		if (q.eq.nye0.AND.p.LE.nxe*1/4) then
 			ang2_elm_deload(n2_deload,1)=-pi/2.0d0
 			ang2_elm_deload(n2_deload,2)=-pi/2.0d0
 			ang2_elm_deload(n2_deload,3)=-pi/2.0d0
c	    write(101,*),"ang2_elm_deload(",n2_deload,",1)=",-pi/2,";"
c		write(101,*),"ang2_elm_deload(",n2_deload,",2)=",-pi/2,";"
c		write(101,*),"ang2_elm_deload(",n2_deload,",3)=",-pi/2,";"
				
		endif
		if (q.eq.nye0+1.AND.p.GT.nxe*3/4) then
 			ang2_elm_deload(n2_deload,1)=pi/2.0d0
 			ang2_elm_deload(n2_deload,2)=pi/2.0d0
 			ang2_elm_deload(n2_deload,3)=pi/2.0d0
c	    write(101,*),"ang2_elm_deload(",n2_deload,",1)=",pi/2,";"
c		write(101,*),"ang2_elm_deload(",n2_deload,",2)=",pi/2,";"
c		write(101,*),"ang2_elm_deload(",n2_deload,",3)=",pi/2,";"				
		endif                                                 
	endif                                                                                    
                                                                                       
	end do                                                                                   
	end do 
	!added by BASHAR to write the geometry information and review
      do ii=1,nye0+1
c      write(101,*) "radius(",ii,") = ",radius(ii),";"
	enddo
	do ii=1,nye0+1
c       write(101,*) "rad2(",ii,") = ",rad2(ii),";"

	enddo
	
	
	    do ii=1,n_angbkfix
c		write(101,*),"iangbkfix(",ii,")=",iangbkfix(ii),";"
c	    write(101,*),"angbkfix(",ii,")=",angbkfix(ii),";"
          enddo
		do ii=1,n_fix
c		write(101,*),"iafix(",ii,",1)=",iafix(ii,0),";"
c	    write(101,*),"iafix(",ii,",2)=",iafix(ii,1),";"
          enddo
		do ii=1,n_bkfix
c		write(101,*),"iabkfix(",ii,",1)=",iabkfix(ii,0),";"
c	    write(101,*),"iabkfix(",ii,",2)=",iabkfix(ii,1),";"
c	    write(101,*),"iabkfix(",ii,",3)=",iabkfix(ii,2),";"
c	    write(101,*),"iabkfix(",ii,",4)=",iabkfix(ii,3),";"
          enddo
c		write(101,*),"n1_deload=",n1_deload,";"
c	    write(101,*),"n2_deload=",n2_deload,";"
c	    write(101,*),"n3_deload=",n3_deload,";"
c		write(101,*),"n_fix=",n_fix,";"
c		write(101,*),"n_bkfix=",n_bkfix,";"
c		write(101,*),"n_angbkfix=",n_angbkfix,";" 
c       added by Bashar to change x locations
		aspect_ratio=0.25
c	write(101,*),"aspect_ratio=",aspect_ratio,";"
		call aspect_ratio_changer_sagd(x,y,nxe,nye,nn,nodep,
     *        aspect_ratio)
	do ii=1,nn
c			write(101,*) "x(",ii,") = ",x(ii),";"
c              write(101,*) "y(",ii,") = ",y(ii),";"
	enddo
	!end of added by BASHAR                                                                                                                                                                     
853   format(2f15.8)                                                                                                                  
       return                                                                             
       end 
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	subroutine aspect_ratio_changer_sagd(x,y,nxe,nye,nn,nodep,
     * aspect_ratio) !added by Bashar
	!output: changed x and y vectors.
	!aspect ratio: (max(x)-min(x))/(max(y)-min(y))
	
	integer nn,nxe,nye,nodep(40,80,8),nye0
	real*8 x(5000),y(5000),aspect_ratio,x_new(5000),y_new(5000)
      real*8 H !distance between well centers
	real*8 H0, H1 !old and new maxium x, previously max(x)=H/2
	real*8 xw1,yw1,xw2,yw2 !(xw1,yw1) and (xw2,yw2) are the locations of the centers of the 
	!lower and upper wells, repectively.
      real*8 x1,y1,x2,y2,x3,y3,x4,y4,x5,y5 !dummy coordinates
	integer n1,n2,n3 !dummy node numbers
	integer p1,q1,p2,q2 !dummy element numbers
	real*8 r1,r2,r0 !radius of wells
	real*8 xmax0,xmax !maximum x before and after changes
	real*8 x0,y0,ym,y00
	real*8 theta,theta1
	nye0=nye/2
	
	xw1=x(1)
	xw2=x(1)
	
	n1=nodep(nxe/2,1,5)
	yw1=y(n1)
	r1=x(n1)-xw1
	
	n2=nodep(nxe/2,nye,5)
	yw2=y(n2)
	r2=x(n2)-xw2

	H=yw2-yw1
      	
	n1=nodep(nxe/2,nye0,7)
	H0=x(n1)
	H1=H*2.0d0*aspect_ratio
      ym=(yw1+yw2)/2.0
	
	do 1287 n1=1,nn
		x0=x(n1)
		y0=y(n1)
		y00=y(n1)
		r0=r1
		if(y0.ge.ym) then
			y0=y0-H
			r0=r2
		endif
		
		if (abs(y0).le.x0) then
			
			x2=H/2.0
			y2=x2*y0/x0
			theta=atan(y0/x0)
			x3=r0*x0/(y0**2.0+x0**2.0)**0.5 !x3=r0*cos(theta) !
			y3=r0*y0/(y0**2.0+x0**2.0)**0.5 !y3=r0*sin(theta) !
			y4=y2
			x4=H1
			theta1=atan(y4/x4)
			x5=r0*x4/(y4**2.0+x4**2.0)**0.5 !x5=r0*cos(theta1) !
			y5=r0*y4/(y4**2.0+x4**2.0)**0.5 !y5=r0*sin(theta1) !
			if (abs(x2-x3).ge.1.0e-10) then
				x_new(n1)=x5+(x0-x3)*(x4-x5)/(x2-x3)
			else 
				x_new(n1)=x5
			endif
			if (abs(y2-y3).ge.1.0e-10) then
				y_new(n1)=y5+(y0-y3)*(y4-y5)/(y2-y3)
			else
				y_new(n1)=y5
			endif
		else
			theta=atan(y0/x0)
			x3=r0*x0/(y0**2.0+x0**2.0)**0.5 !x3=r0*cos(theta) !
			y3=r0*y0/(y0**2.0+x0**2.0)**0.5 !y3=r0*sin(theta) !
			y2=H/2.0*y0/abs(y0)
			x2=y2/(y0/x0)
			y4=y2
			x4=x2*H1*2.0/H
			theta1=atan(y4/x4)
			x5=r0*x4/(y4**2.0+x4**2.0)**0.5 !x5=r0*cos(theta1) !
			y5=r0*y4/(y4**2.0+x4**2.0)**0.5 !y5=r0*sin(theta1) !
			if (abs(x2-x3).ge.1.0e-10) then
				x_new(n1)=x5+(x0-x3)*(x4-x5)/(x2-x3)
			else 
				x_new(n1)=x5
			endif
			if (abs(y2-y3).ge.1.0e-10) then
				y_new(n1)=y5+(y0-y3)*(y4-y5)/(y2-y3)
			else
				y_new(n1)=y5
			endif
		endif
		
		
		if(y00.ge.ym) then
			y_new(n1)=y_new(n1)+H
		endif
		
1287	continue

	do n1=1,nn
		x(n1)=x_new(n1)
		y(n1)=y_new(n1)
c		write(101,*),"x_new(",n1,")=",x_new(n1),";"
c		write(101,*),"y_new(",n1,")=",y_new(n1),";"
	enddo
	return
	end subroutine aspect_ratio_changer_sagd

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine tun31geo(is_arc_out,n_angbkfix,iangbkfix,angbkfix,                      
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *              nn,nye,NYL,nntemp,                                                    
     *              nxe,nye0,spoke,radius,coord,icoord,                                   
     *           coordf, icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                           
c                                                                                         
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82)                                                        
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
                                                                                          
	 integer nodep(40,80,8),nodeg(40,80,20)                                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                 
	integer iangbkfix(500),iangbkfixout(500)                                                                   
	real*8 angbkfix(500)                                                                     

       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer nodep4(40,80,4),nn4
	                                                         
      common/materials/elemmats(40,80)
	integer elemmats 
                                                                                                                                                                                   
C	COMMON /TMP1/ nf(20000,3)  
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                               
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     *i_v4nodes(82,42),                                                                 
     *	i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
      
	   nye=nye0                                                                              
	NYL=nye0+1                                                                               
  	    nn=nye0*(3*nxe+2)+(2*nxe+1) ! Total nodes                                          
 	    NELEMENT=nxe*nye                                                                    
	nntemp=(nxe+1)*NYL !vertex nodes
	nn4=nntemp                                                        
! Make vertex nodes index                                                                 
	   do i2=1,nxe+1                                                                         
	    do i1=1,nye+1                                                                        
	         i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                     
	    end do                                                                               
	   end do                                                                                
                                                                                       
       pi=4.0D0*datan(1.D00)                                                              
       ang=0.5D0*pi/nxe !Ma.Z 0.5D0*pi/nxe                                                
	n_node=0                                                                                 
      do p=0,nxe*2                                                                        
		p_ang=-0.5*pi+p*ang                                                                     
		x_in(p)=radius(1)*dcos(p_ang)                                                           
		y_in(p)=radius(1)*dsin(p_ang)                                                           
                                                                                          
		if(is_arc_out.EQ.1) then                                                                
			x_out(p)=radius(nye0+1)*dcos(p_ang)                                                     
			y_out(p)=radius(nye0+1)*dsin(p_ang)                                                     
		else                                                                                    
			if(p_ang.LT.-pi/4.D0) then                                                              
				y_out(p)=-radius(nye0+1)                                                               
				x_out(p)=y_out(p)/dtan(p_ang)                                                          
			elseif(p_ang.LT.pi/4.D0) then                                                           
				x_out(p)=radius(nye0+1)                                                                
				y_out(p)=x_out(p)*dtan(p_ang)                                                          
			else                                                                                    
				y_out(p)=radius(nye0+1)                                                                
				x_out(p)=y_out(p)/dtan(p_ang)                                                          
			endif                                                                                   
		endif                                                                                   
	end do                                                                                   
                                                                                          
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0
	!!Shunfu.Z 2009.12.31
	n_angbkfixout=0
	!!Shunfu.Z 2009.12.31                                                                             
	do q=1,nye0+1                                                                            
	f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	f_out_weight=1.D0-f_in_weight                                                            
	f2_in_weight=(radius(nye0+1)-(radius(q+1)+radius(q))/2.D0)                               
     *	/(radius(nye0+1)-radius(1))                                                        
	f2_out_weight=1.D0-f2_in_weight                                                          
        do p=0,nxe ! lines that include vertex nodes                                      
		! Vertex points                                                                         
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                 
		y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight                                 
                                                                                          
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
	n_fix=n_fix+1                                                                            
	iafix(n_fix,0)=n_node                                                                    
	iafix(n_fix,1)=1 ! x disp is fixed                                                       
	endif                                                                                    
                                                                                          
!	if(q.EQ.1.AND.p.EQ.nxe/2) then                                                           
!	n_fix=n_fix+1                                                                            
!	iafix(n_fix,0)=n_node                                                                    
!	iafix(n_fix,1)=2 ! y disp is fixed                                                       
!	endif  

!!==========Li=========
      if(is_arc_out.EQ.1) then 
        if(q.EQ.1.AND.p.EQ.nxe/2) then                                                           
	     n_fix=n_fix+1                                                                            
	     iafix(n_fix,0)=n_node                                                                    
	     iafix(n_fix,1)=2 ! y disp is fixed                                                       
	  endif
	else
	  if(q.EQ.nye0+1.AND.p.le.nxe/4) then                                                           
	    n_fix=n_fix+1                                                                            
	    iafix(n_fix,0)=n_node                                                                    
	    iafix(n_fix,1)=2 ! y disp is fixed                                                       
	  endif
      endif 
	
!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================	                                                                                 
			                                                                                       
	if(q.EQ.1.OR.q.EQ.nye0+1) then                                                           
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=3 ! pressure disp is fixed  
		if(q.EQ.1) then
			iabkfix(n_bkfix,2)=1
		elseif (q.EQ.nye0+1) then
			iabkfix(n_bkfix,2)=256
		endif                                            
	endif                                                                                    
			                                                                                       
	if(q.EQ.nye0+1) then                                                                     
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=1
	iabkfix(n_bkfix,2)=256
	                                                                     
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=2 
	iabkfix(n_bkfix,2)=256                                                                    
	endif                                                                                    
			                                                                                       
	if(q.EQ.1) then                                                                          
	n_angbkfix=n_angbkfix+1                                                                  
	iangbkfix(n_angbkfix)=n_node                                                             
c	angbkfix(n_angbkfix)=-3.1414926d0/2.d0+p*3.1414926d0/nxe  ! 包括边节点，故有2*nxe等分    =  Edge nodes, so 2 * nxe decile
	angbkfix(n_angbkfix)=-pi/2.d0+p*pi/nxe  ! 包括边节点，故有2*nxe等分   =  Edge nodes, so 2 * nxe decile 
	endif 
	!!Shunfu.Z 2009.12.31
	if(q.eq.nye0+1) then
	n_angbkfixout=n_angbkfixout+1
	iangbkfixout(n_angbkfixout)=n_node
	endif                                                                                   
	!!Shunfu.Z 2009.12.31		                                                                                       			                                                                                       
		if(p.LT.nxe) then !Middle points                                                        
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
		y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight                             
			                                                                                       
	if(q.EQ.nye0+1) then                                                                     
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=1  
	iabkfix(n_bkfix,2)=256  !!                                                                  
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=2 
	iabkfix(n_bkfix,2)=256  !!                                                                  
	endif  
	
!!==========Li=========
      if(is_arc_out.EQ.0) then
	  if(q.EQ.nye0+1.AND.p.lt.nxe/4) then                                                           
	    n_fix=n_fix+1                                                                            
	    iafix(n_fix,0)=n_node                                                                    
	    iafix(n_fix,1)=2 ! y disp is fixed
        endif                                                       
	endif 
!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================	
	                                                                                  
			                                                                                       
	if(q.EQ.1) then                                                                          
	n_angbkfix=n_angbkfix+1                                                                  
	iangbkfix(n_angbkfix)=n_node                                                             
c	angbkfix(n_angbkfix)=-3.1414926d0/2.d0+(p+0.5d0)*3.1414926d0/nxe !包括边节点，故有2*nxe等 =  Including the edge nodes, so 2 * nxe, etc.
	angbkfix(n_angbkfix)=-pi/2.d0+(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等 = Including the edge nodes, so 2 * nxe, etc.
	endif
	!!Shunfu.Z 2009.12.31
	if(q.eq.nye0+1) then
	n_angbkfixout=n_angbkfixout+1
	iangbkfixout(n_angbkfixout)=n_node
	endif                                                                                   
	!!Shunfu.Z 2009.12.31                                                                                    
		endif                                                                                   
	  end do                                                                                 
                                                                                          
          if(q.LT.nye0+1) then                                                            
		do p=0,nxe ! Middle edges                                                               
			n_node=n_node+1                                                                        
			x(n_node)=x_in(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                              
			y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight                              
                                                                                          
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
	n_fix=n_fix+1                                                                            
	iafix(n_fix,0)=n_node                                                                    
	iafix(n_fix,1)=1 ! x disp is fixed                                                       
	endif 
	                                                                                   
			                                                                                       
		end do                                                                                  
	endif                                                                                    
	end do
	!!Shunfu.Z 2009.12.21                                                                                  
      n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0 
	!!Shunfu.Z 2009.12.21                                                                                     
      do p=1,nxe                                                                          
	do q=1,nye0                                                                              
	      i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
c		nodep(p,q,1)=aq                                                                         
c		nodep(p,q,2)=an                                                                         
c		nodep(p,q,3)=ao                                                                         
c		nodep(p,q,4)=al                                                                         
c		nodep(p,q,5)=am                                                                         
c		nodep(p,q,6)=ap                                                                         
c		nodep(p,q,7)=as                                                                         
c		nodep(p,q,8)=ar
 		! 4节点网格系统                                                                         
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
                                                                                          
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is 
		
          elemmats(p,q)=(q-1)*nxe+p		                                                                        
                                                                                          
	if(q.EQ.1) then !hole 1 or hole 2                                                        
		n1_deload=n1_deload+1                                                                   
		i1elemx(n1_deload)=p                                                                    
		i1elemy(n1_deload)=q                                                                    
		i1elm_deload(n1_deload,1)=ao                                                            
		i1elm_deload(n1_deload,2)=al                                                            
		i1elm_deload(n1_deload,3)=am 
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang1_elm_deload(n1_deload,1)=angbkfix(ij)
			else if(iangbkfix(ij).eq.al) then                                                         
				ang1_elm_deload(n1_deload,2)=angbkfix(ij)
			else if(iangbkfix(ij).eq.am) then                                                         
				ang1_elm_deload(n1_deload,3)=angbkfix(ij)
			else
			endif
		enddo                                                           
c		ang1_elm_deload(n1_deload,1)=0                                                          
c		ang1_elm_deload(n1_deload,2)=0                                                          
c		ang1_elm_deload(n1_deload,3)=0                                                          
	endif                                                                                    
	if(q.eq.nye0) then ! out                                                                 
		n2_deload=n2_deload+1                                                                   
		i2elemx(n2_deload)=p                                                                    
		i2elemy(n2_deload)=q                                                                    
		i2elm_deload(n2_deload,1)=as                                                            
		i2elm_deload(n2_deload,2)=ar                                                            
		i2elm_deload(n2_deload,3)=aq
 	!!Shunfu.Z 2009.12.31
		do ij=1,n_angbkfixout
			if(is_arc_out.eq.1) then
				if (iangbkfixout(ij).eq.as) then                                                            
					ang2_elm_deload(n2_deload,1)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.ar) then                                                         
					ang2_elm_deload(n2_deload,2)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.aq) then                                                         
					ang2_elm_deload(n2_deload,3)=angbkfix(ij)
				else
				endif
			else
				if (iangbkfixout(ij).eq.as) then
					if(p.le.nxe/4) then                                                           
						ang2_elm_deload(n2_deload,1)=-pi/2.0d0
					else if(p.le.3*nxe/4) then
						ang2_elm_deload(n2_deload,1)=0.0d0
					else
						ang2_elm_deload(n2_deload,1)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.ar) then
					if(p.le.nxe/4) then                                                           
						ang2_elm_deload(n2_deload,2)=-pi/2.0d0
					else if(p.le.3*nxe/4) then
						ang2_elm_deload(n2_deload,2)=0.0d0
					else
						ang2_elm_deload(n2_deload,2)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.aq) then                                                         
					if(p.le.nxe/4) then                                                           
						ang2_elm_deload(n2_deload,3)=-pi/2.0d0
					else if(p.le.3*nxe/4) then
						ang2_elm_deload(n2_deload,3)=0.0d0
					else
						ang2_elm_deload(n2_deload,3)=pi/2.0d0
					endif
				else
				endif					
			endif
		enddo
	!!Shunfu.Z 2009.12.31                                                            
c		ang2_elm_deload(n2_deload,1)=0                                                          
c		ang2_elm_deload(n2_deload,2)=0                                                          
c		ang2_elm_deload(n2_deload,3)=0                                                          
	endif                                                                                    
                                                                                          
	end do                                                                                   
	end do                                                                                   
                                                                                          
       return                                                                             
       end

       subroutine tun11geo(is_arc_out,n_angbkfix,iangbkfix,angbkfix,                      
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *              nn,nye,NYL,nntemp,                                                    
     *              nxe,nye0,spoke,radius,coord,icoord,                                   
     *     coordf, icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                         
c                                                                                         
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82)                                                        
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	 real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
                                                                                          
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                 
	integer iangbkfix(500)                                                                   
	real*8 angbkfix(500)                                                                     
                                                                                          
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)

	integer n_angbkfixout,iangbkfixout(500)                                                            

      common/materials/elemmats(40,80)
	integer elemmats                                                                                          
                                                                                          
C	COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     *i_v4nodes(82,42),                                                                 
     *	i_8_4_nodes(5000),i_4_8_nodes(5000),N_N 
 
	nye=nye0                                                                              
	NYL=nye0+1                                                                               
  	nn=nye0*(3*nxe+2)+(2*nxe+1) ! Total nodes                                          
 	NELEMENT=nxe*nye                                                                    
	nntemp=(nxe+1)*NYL !vertex nodes                                                         
  	nn4=(nye0+1)*(nxe+1) ! Total nodes in one area                                     
! Make vertex nodes index                                                                 
	do i2=1,nxe+1                                                                         
	do i1=1,nye+1                                                                        
		i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                     
	end do                                                                               
	end do                                                                                
                                                                                          
      pi=4.0D0*datan(1.D00)                                                              
      ang=0.5D0*pi/(nxe*2)                                                               
	n_node=0                                                                                 
      do p=0,nxe*2                                                                        
		p_ang=p*ang                                                                             
		x_in(p)=radius(1)*dcos(p_ang)                                                           
		y_in(p)=radius(1)*dsin(p_ang)                                                           
                                                                               
		if(is_arc_out.EQ.1) then                                                                
			x_out(p)=radius(nye0+1)*dcos(p_ang)                                                     
			y_out(p)=radius(nye0+1)*dsin(p_ang)                                                     
		else   
		!!Shunfu.Z 2009.12.31                                                                                 
c			if(p_ang.LT.-pi/4.D0) then                                                              
c				y_out(p)=-radius(nye0+1)                                                               
c				x_out(p)=y_out(p)/dtan(p_ang)                                                          
c			elseif(p_ang.LT.pi/4.D0) then                                                           
c				x_out(p)=radius(nye0+1)                                                                
c				y_out(p)=x_out(p)*dtan(p_ang)
			if(p_ang.LT.pi/4.D0) then                                                                                                                        
				x_out(p)=radius(nye0+1)                                                                
				y_out(p)=x_out(p)*dtan(p_ang)
				aa=dtan(p_ang)
				continue                                                          
			else                                                                                    
				y_out(p)=radius(nye0+1)                                                                
				x_out(p)=y_out(p)/dtan(p_ang)
				!!Shunfu.Z 2009.12.31 tan(pi/2)可能会带来问题  =  May cause problems                                                      
			endif                                                                                   
		endif                                                                                   
	end do                                                                                   
                                                                                          
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0 
	n_angbkfixout=0                                                                            
	do q=1,nye0+1                                                                            
	 f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	 f_out_weight=1.D0-f_in_weight                                                            
	 f2_in_weight=(radius(nye0+1)-(radius(q+1)+radius(q))/2.D0)                               
     *	/(radius(nye0+1)-radius(1))                                                        
	 f2_out_weight=1.D0-f2_in_weight                                                          
		do p=0,nxe ! lines that include vertex nodes                                      
		! Vertex points                                                                         
			n_node=n_node+1                                                                         
			x(n_node)=x_in(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                 
			y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight                                 
                                                                                          
			if(p.EQ.nxe) then                                                                        
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed                                                       
			endif                                                                                    
                                                                                          
			if(p.EQ.0) then                                                                          
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed                                                       
			endif
			
!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================                                                                                  
			                                                                                       
			if(q.EQ.1) then                                                                          
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=1 ! hole1                                                             
			endif                                                                                    
			                                                                                       
			if(q.EQ.nye0+1) then                                                                     
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=256 ! outer                                                           
                                                                                          
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=1                                                                     
				iabkfix(n_bkfix,2)=256 ! outer                                                           
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=2                                                                     
				iabkfix(n_bkfix,2)=256 ! outer                                                           
			endif                                                                                    
			                                                                                       
			if(q.EQ.1) then                                                                          
				n_angbkfix=n_angbkfix+1                                                                  
				iangbkfix(n_angbkfix)=n_node                                                             
				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  !包括边节点，故有2*nxe等分         =  Edge nodes, so 2 * nxe decile                 
			endif 
			!!Shunfu.Z 2009.11.11
			if(q.EQ.nye0+1) then                                                                          
				n_angbkfixout=n_angbkfixout+1                                                                  
				iangbkfixout(n_angbkfixout)=n_node                                                             
c				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  ! 包括边节点，故有2*nxe等分   = Edge nodes, so 2 * nxe decile                      
			endif                                                                                   
			                                                                                       
			                                                                                       
			if(p.LT.nxe) then !Middle points                                                        
				n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
		y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight                             
			                                                                                       
				if(q.EQ.nye0+1) then                                                                     
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=1                                                                     
					iabkfix(n_bkfix,2)=256 ! outer                                                           
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=2                                                                     
					iabkfix(n_bkfix,2)=256 ! outer                                                           
				endif  

!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================                                                                                   
			                                                                                       
				if(q.EQ.1) then                                                                          
					n_angbkfix=n_angbkfix+1                                                                  
					iangbkfix(n_angbkfix)=n_node                                                             
					angbkfix(n_angbkfix)=(p+0.5d0)*pi/2.D0/nxe  !包括边节点，故有2*nxe等分   = Edge nodes, so 2 * nxe decile                
				endif
				!!Shunfu.Z 2009.11.11
				if(q.EQ.nye0+1) then                                                                          
					n_angbkfixout=n_angbkfixout+1                                                                  
					iangbkfixout(n_angbkfixout)=n_node                                                             
c				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  ! 包括边节点，故有2*nxe等分 = Edge nodes, so 2 * nxe decile                         
				endif                                                                                     
			endif                                                                                   
		end do                                                                                 
                                                                                          
          if(q.LT.nye0+1) then                                                            
		do p=0,nxe ! Middle edges                                                               
			n_node=n_node+1                                                                        
			x(n_node)=x_in(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                              
			y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight                              
                                                                                          
			if(p.EQ.nxe) then                                                                        
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed                                                       
			endif                                                                                    
			                                                                                       
			if(p.EQ.0) then                                                                          
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed                                                       
			endif                                                                                   	                                                                                       
		end do                                                                                  
		endif                                                                                    
	end do                                                                                   
	continue
	!!Shunfu.Z 2009.12.21  	                                                                                         
	n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0
	!!Shunfu.Z 2009.12.21                                                                                            
      do p=1,nxe                                                                          
	do q=1,nye                                                                             
c	    i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
                                                                                          
		! 4节点网格系统 = Node grid system                                                                        
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
                                                                                          
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is  
		
          elemmats(p,q)=(q-1)*nxe+p		
		                                                                      
                                                                                          
		if(q.EQ.1) then !inner boundary                                                      
			n1_deload=n1_deload+1                                                                   
			i1elemx(n1_deload)=p                                                                    
			i1elemy(n1_deload)=q                                                                    
			i1elm_deload(n1_deload,1)=ao                                                            
			i1elm_deload(n1_deload,2)=al                                                            
			i1elm_deload(n1_deload,3)=am
	!!Shunfu.Z
			do ij=1,n_angbkfix
				if (iangbkfix(ij).eq.ao) then                                                            
					ang1_elm_deload(n1_deload,1)=angbkfix(ij)
				else if(iangbkfix(ij).eq.al) then                                                         
					ang1_elm_deload(n1_deload,2)=angbkfix(ij)
				else if(iangbkfix(ij).eq.am) then                                                         
					ang1_elm_deload(n1_deload,3)=angbkfix(ij)
				else
				endif
			enddo                                                            
c			ang1_elm_deload(n1_deload,1)=0                                                          
c			ang1_elm_deload(n1_deload,2)=0                                                          
c			ang1_elm_deload(n1_deload,3)=0                                                          
		endif                                                                                    
		if(q.eq.nye0) then ! out                                                                 
			n2_deload=n2_deload+1                                                                   
			i2elemx(n2_deload)=p                                                                    
			i2elemy(n2_deload)=q                                                                    
			i2elm_deload(n2_deload,1)=as                                                            
			i2elm_deload(n2_deload,2)=ar                                                            
			i2elm_deload(n2_deload,3)=aq
	!!Shunfu.Z 2009.11.11
			do ij=1,n_angbkfixout
				if(is_arc_out.eq.1) then
				if (iangbkfixout(ij).eq.as) then                                                            
					ang2_elm_deload(n2_deload,1)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.ar) then                                                         
					ang2_elm_deload(n2_deload,2)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.aq) then                                                         
					ang2_elm_deload(n2_deload,3)=angbkfix(ij)
				else
				endif
				else
				if (iangbkfixout(ij).eq.as) then
					if(p.le.nxe/2) then                                                           
						ang2_elm_deload(n2_deload,1)=0.0d0
					else
						ang2_elm_deload(n2_deload,1)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.ar) then
					if(p.le.nxe/2) then                                                         
						ang2_elm_deload(n2_deload,2)=0.0d0
					else
						ang2_elm_deload(n2_deload,2)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.aq) then                                                         
					if(p.le.nxe/2) then                                                         
						ang2_elm_deload(n2_deload,3)=0.0d0
					else
						ang2_elm_deload(n2_deload,3)=pi/2.0d0
					endif
				else
				endif					
				endif
			enddo
	!!Shunfu.Z 2009.11.11                                                            
c			ang2_elm_deload(n2_deload,1)=0                                                          
c			ang2_elm_deload(n2_deload,2)=0                                                          
c			ang2_elm_deload(n2_deload,3)=0                                                          
		endif                                                                                                                                                                       
	end do                                                                                   
	end do                                                                                   
	continue                                                                                      
       return                                                                             
       end 

c============================================================================================
! 下面做4节点的温度场边界信息     =  Do 4 nodes below the temperature field boundary information                                                       
       subroutine Bound4Node(is_arc_out,n_angbkfix,iangbkfix,angbkfix,                    
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *              nn,nye,NYL,nntemp,                                                    
     *              nxe,nye0,spoke,radius,coord,icoord,                                   
     *     coordf, icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                         
c                                                                                         
c***** This subroutine create boundary conditions for 8-node mesh   ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82)                                                        
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
                                                                                          
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                 
	integer iangbkfix(500)                                                                   
	real*8 angbkfix(500)                                                                     
                                                                                          
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)                                                                                                                                              
                                                                                          
C	COMMON /TMP1/ nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     *i_v4nodes(82,42),                                                                 
     *	i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
                          
	now_bkfix=n_bkfix  !只用现有的压力边界作参考，不用对新加的温度边界作判断 = For reference only existing pressure boundary judgment without new plus temperature boundary                
      do p=1,nxe                                                                          
	do q=1,nye                                                                               
	do i=1,4                                                                                 
	i1=i*2-1                                                                                 
	do j=1,now_bkfix                                                                         
		if(nodep(p,q,i1).EQ.iabkfix(j,0)                                                        
     *	.AND.iabkfix(j,1).EQ.3) THEN  ! 如果已存在压力边界，则也赋予温度边界   = If there is a pressure boundary, given the temperature boundary            
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=nodep4(p,q,i)                                                       
			iabkfix(n_bkfix,1)=4 ! temperature is fixed                                            
			iabkfix(n_bkfix,2)=iabkfix(j,2) ! hole1 or hole2 or outer                              
		ENDIF                                                                                   
	end do                                                                                   
	end do                                                                                   
	end do                                                                                   
	end do                                                                                   
       return                                                                             
       end 

! 下面做4节点的温度场边界信息, 另一套方案，采用单独的温度场边界数组 = 4 nodes below the temperature field boundary, and another set of options, with separate temperature field boundary array ia_t_fix(n_t_fix)     
       subroutine Bound4Node1(n_t_fix,ia_t_fix,n_bkfix,iabkfix,                                                                   
     *              nxe,nye,nodep,nodep4)                         
c                                                                                         
c***** This subroutine create boundary conditions for 8-node mesh   ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                                                                                        
	 Implicit NONE                                                           
       integer p,q,nxe,nye,I,I1,J                                                                                                                                   
	 integer nodep(40,80,8),nodep4(40,80,4)                                                                                                                           
	 integer n_bkfix,iabkfix(500,0:3)                                                                                                                                                                                                                           
	 integer ia_t_fix(500,0:3),n_t_fix  
                                                                                                                                            
	 n_t_fix=0                                                                                                                                                                 
	 do j=1,n_bkfix                                                                           
		IF(iabkfix(j,1).NE.3) GOTO 10                                                           
		do p=1,nxe
		do q=1,nye
			do i=1,4
				i1=i*2-1
				if(nodep(p,q,i1).EQ.iabkfix(j,0)) THEN  ! 如果已存在压力边界，则也赋予温度边界  =If there is a pressure boundary, given the temperature boundary     
					n_t_fix=n_t_fix+1                                                                   
					ia_t_fix(n_t_fix,0)=nodep4(p,q,i) 
					ia_t_fix(n_t_fix,2)=iabkfix(j,2) ! hole1 or hole2 or outer
				goto 10                                               
				ENDIF                                                                                
			end do                                                                                
		end do                                                                                 
		end do                                                                                  
10		CONTINUE                                                                              
	 end do
	do i=1,n_t_fix
c	write(101,*),"ia_t_fix(",i,",1)=",ia_t_fix(i,0),";"
c	write(101,*),"ia_t_fix(",i,",2)=",ia_t_fix(i,1),";"
c	write(101,*),"ia_t_fix(",i,",3)=",ia_t_fix(i,2),";"
	enddo
c	write(101,*),"n_t_fix=",n_t_fix,";"
c	print*,"n_t_fix=",n_t_fix                                                                               
       return                                                                             
       end 

c=====================================================================================
	SUBROUTINE SET_4_8_VNODES(NXE,NYE,NODEP,NODEP4)                                          
	Implicit none
	integer nxe,nye,nodep(40,80,8),nodep4(40,80,4)
	integer IMESH,n_node,NELEMENT,n_4node,i_vnodes,                            
     *i_v4nodes,i_8_4_nodes,i_4_8_nodes,N_N                                                               
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     *i_v4nodes(82,42),                                                                 
     *	i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
	integer nodeg(40,80,20) 	
	integer i1,i2,i3,i4,n4,n8                          
                                              
	do i2=1,nxe                                                                              
	do i1=1,nye                                                                             
		do i3=1,4                                                                              
			I4=I3*2-1                                                                             
			N4=NODEP4(I2,I1,I3)                                                                   
			N8=NODEP(I2,I1,I4)                                                                    
			i_8_4_nodes(N8)=N4                                                                    
			i_4_8_nodes(N4)=N8                                                                                                                                        
		ENDDO                                                                                  
	ENDDO                                                                                   
	ENDDO
	continue                                                                                 
	RETURN                                                                                   
	END 

!!	Boundary integral part for Saturation Caculation --Add by Shunfu.Z 2009.07.20
	 subroutine flux(n_bkfix,iabkfix,nodeid,nodeid8,nn4,
     $	            ns,p,q,term,P_grad,coordf,F_GLOBE,coord,dt,cflag)
c                                                                                                                                                    
c********* This subroutine caculate the flux on the boundary ******                  
c                                                                                         
       Implicit none
	 integer n_bkfix,iabkfix(500,0:3),nodeid(4),nodeid8(4),nn4,ns,p,q
	 real*8 term,P_Grad(2,4),coordf(4,2),F_GLOBE(2000),coord(8,2),dt
c	 real*8 term,P_Grad(2,4),coordf(4,2),F_GLOBE(nn4),coord(8,2),dt
	 integer cflag

	 integer ig,ii,jj,kk,iflag(4),istart,iend,ikind
	 real*8 A_kw(2,2,2)
	 real*8 Grad(2,2),element_f3(2),normal(2)
	 real*8 funf(2),n(2),samp(2),tempr,fun(8)

	 integer gp,iodeg,IDOFN
	 real*8 samp8(2,2),der8(2,8),elcod(2,3)
	 real*8 jac(2,2),jac1(2,2),det,direction(2)
	 !!  what is used are the second and third node in four-node system
	 iflag=0                                                                                                                                               
       do ii=1,n_bkfix
		do jj=1,4
			if(iabkfix(ii,0).eq.nodeid8(jj)) iflag(jj)=1
		enddo
	 enddo
	 if (iflag(2).eq.1.and.iflag(3).eq.1) then
		istart=2
		iend=3
		ikind=1
 		if (cflag.eq.2)	goto 100
	 elseif (iflag(1).eq.1.and.iflag(4).eq.1) then
		istart=1
		iend=4
		ikind=256
c		if (cflag.eq.2)	goto 100
c 		goto 100
	 else
		goto 100
	 endif
	!! normal direction for the boundary
	!! permeability extrapolation from four gauss point
	 samp(1)=dsqrt(3.0d0)/3.0
	 samp(2)=-samp(1)
	 a_kw=0.0d0
	 if (ikind.eq.1) then
		n(1)=(1.0d0+samp(1))/(2.0d0*samp(1)) !! shape functiom for point 1/sart(3.0)
		n(2)=(1.0d0+samp(2))/(2.0d0*samp(2)) !! shape functiom for point -1/sart(3.0)
	 else
		n(1)=(-1.0d0+samp(1))/(2.0d0*samp(1))
		n(2)=(-1.0d0+samp(2))/(2.0d0*samp(2))
	 endif
	 grad=0.0d0	
	 do ii=1,2 !! for gauss point ii in one dimension
		do jj=1,2 !!Direction x, y
			grad(jj,ii)=P_grad(jj,ii)*n(1)+P_grad(jj,ii+2)*n(2)
		enddo	
	 enddo
1456   format(2f20.16,a16,i6,i6)
c	ikind=256
	 if (ikind.eq.1) then
		do idofn=1,3                                                                        
			elcod(1,idofn)=coord(2+idofn,1)                                                         
			elcod(2,idofn)=coord(2+idofn,2)
		enddo
	 else
		do idofn=1,3
			if (10-idofn.le.8) then                                                                        
				elcod(1,idofn)=coord(10-idofn,1)                                                         
				elcod(2,idofn)=coord(10-idofn,2)
			else
				elcod(1,idofn)=coord(10-idofn-8,1)                                                         
				elcod(2,idofn)=coord(10-idofn-8,2)			
			endif
		enddo
	 endif
c	 elcod(1,2)=(elcod(1,1)+elcod(1,3))/2.0d0
c	 elcod(2,2)=(elcod(2,1)+elcod(2,3))/2.0d0
	 gp=2
	 element_f3=0.0d0
	 do 9105 ig=1,gp                                                                        
		call gauss(samp8,gp)
c************** xi=weigh in gauss in Owen                                                 
		call fmquad(der8,2,fun,samp8,gp,ig,1,2) !! xi can equal to -1.0 only (so gauss quadrature was limited to one dimension)
		do idofn=1,2
			direction(idofn)=0.0d0	
 			do iodeg=1,3
 	direction(idofn)=direction(idofn)+elcod(idofn,iodeg)*der8(2,iodeg) !! [d x/ d eta; d y/ d eta]
 			enddo
 		enddo
		if (ikind.eq.1) then
			normal(1)=-direction(2)
			normal(2)=direction(1)
		else
			normal(1)=direction(2)
			normal(2)=-direction(1)
		endif
		tempr=grad(1,ig)*normal(1)+grad(2,ig)*normal(2)
c		call matmul(der8,2,coord,8,jac,2)
c		call twoby2(jac,2,jac1,2,det)
	
		funf(1)=(1.0d0-samp(ig))/2.0d0
		funf(2)=(1.0d0+samp(ig))/2.0d0
		do jj=1,2  !!Node 1 and Node 2 
			element_f3(jj)=element_f3(jj)+funf(jj)*tempr*term
		enddo		                 
9105	 continue
	 F_GLOBE(nodeid(istart))=F_GLOBE(nodeid(istart))+element_f3(1) !*dt 
	 F_GLOBE(nodeid(iend))=F_GLOBE(nodeid(iend))+element_f3(2) !*dt 
100    continue
	                                                                                         
       return                                                                              
	 END


       subroutine formkv4(bk,km,g,n,dof)
c
c*****This subroutine assembles element matrices into symmetrical*****
c***** band global matrix(stored as a vector)                    *****
c
       Implicit none
	 integer dof
       real*8 bk(*),km(dof,dof)
       integer g(dof),n,i,j,cd,val
       do 1 i=1,dof
       if(g(i).eq.0)go to 1
       do 5 j=1,dof
       if(g(j).eq.0)go to 5
       cd=g(j)-g(i)+1
       if(cd-1)5,4,4
    4  val=n*(cd-1)+g(i)
       bk(val)=bk(val)+km(i,j)
    5  continue
    1  continue
       return
       end

	subroutine velocitynew(nxe,nye,nn4,nn,nodep4,nodep,
     $					TERM,krw,dump,x,y,vel,rhou,ngrav)
!!	velocity in m / day
	implicit none
	integer nxe,nye,nn4,nn,nodep4(40,80,4)
	integer nodep(40,80,8),i_4_8_nodes(5000)
	real*8  TERM,krw(40,80,4,2),dump(nn),x(nn),y(nn),vel(2,5000),rhou
	integer ngrav

	integer ijac,ijac1,ikay,ikderv,iderf,idervf
	integer icordf,idtkd,ikp,nodf

      data ijac,ijac1,ikay,ikderv,iderf,idervf/6*2/                                                                
      data icordf,idtkd,ikp,nodf/4*4/

	integer p,q,nodeid4(4),IN,IG,GP,I,J
	real*8 samp(2,2),DUMP_G(4),coordf(4,2),A_F2(8)
	REAL*8 derf(2,4),FUNF(4),derivf(2,4),jac(2,2),jac1(2,2)
	REAL*8 DET,derivt(4,2),QUOT,A_KW(2,2)
	real*8 F_GLOBE(2*nn4),f2body(2),Tderiv(2)
	REAL*8 ELEMENT_F2(8),EM(8,8),SHAP(2,8),SHAPT(8,2),DTKD(8,8)
	INTEGER II,JJ
c	REAL*8 emmax,EMMIN
	REAL*8 K_GLOBE(50000)
	INTEGER IW,g(8),IUNOD1,IUNOD2 
 
	call NULVEC(F_GLOBE,2*nn4)
	CALL NULVEC(K_GLOBE,50000)
	GP=2

	IW=0
	II=2*nodep4(1,1,2)-1
	JJ=2*nodep4(1,1,4)
	IF(ABS(II-JJ).GT.iW) iW=ABS(II-JJ)
	do p=1,nxe
	do q=1,nye
		call NULVEC(DUMP_G , 4)                                                                                                  
		do in=1,icordf !!get the coordinate information
			nodeid4(in)= nodep4(p,q,in)                                                                           
			coordf(in,1)=x(nodep(p,q,2*in-1))                                                         
			coordf(in,2)=y(nodep(p,q,2*in-1))
			DUMP_G(In)= DUMP(nodep(p,q,2*in-1))                                                        
		end do
		do in=1,nodf
			g(2*in-1) = 2*nodeid4(in)-1
			g(2*in  ) = 2*nodeid4(in)
		enddo
 		CALL GAUSS(samp,2)
		ig=0
		call NULVEC(A_F2,8)
		call NULL(EM,8,8)
		CALL NULL(SHAP,2,8)
		CALL NULL(SHAPt,8,2)
		do i = 1, gp                                                                          
		do j = 1, gp                                                                       
			ig=ig+1                                                                                                                                                   
			call formln(derf,iderf,funf,samp,gp,i,j)                                       
			call matmul(derf,iderf,coordf,icordf,jac,ijac)                                                                                                     
			call twoby2(jac,ijac,jac1,ijac1,det)                                                                                                 
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf) 
			call matran(derivt,nodf,derivf,idervf)
			call quotca ( samp,gp,det,term,quot,i,j)     			
			!NODE INTEGRATION TO FORM LOAD VECTOR 
!	        A_KW(1,1) = krw(p,q,ig,1)*0.08527D0*1.0d3 !*1.0d-12
!			A_KW(2,2) = krw(p,q,ig,2)*0.08527D0*1.0d3 !*1.0d-12
	        A_KW(1,1) = krw(p,q,ig,1)
			A_KW(2,2) = krw(p,q,ig,2)                                                                      
			A_KW(1,2) = 0.0d0                                                                          
			A_KW(2,1) = 0.0d0
			call NULVEC(f2body,2)                                               
			call matmul(derivf,2,DUMP_G,4,f2body,1)
	!! gravity influence
			if (ngrav.eq.1) f2body(2)=f2body(2)-rhou*9.8d0*1.0d-6
			call NULVEC(Tderiv,2)
			call matmul(A_KW,2,f2body,2,Tderiv,1)
c			call matmul(derivt,4,Tderiv,2,ELEMENT_F2,1)				                                                                                                                                                                                                                                            
			!NODE INTEGRATION TO FORM LOAD VECTOR
			call NULVEC(ELEMENT_F2,8)
			do ii=1,nodf
			do jj=1,2
				ELEMENT_F2(2*ii+jj-2)=funf(ii)*Tderiv(jj)*quot
			enddo
			enddo
			call matadd(A_F2,8,ELEMENT_F2,1)
	!! CONSISTENT MASS MATRIX 
			DO II=1,NODF
				SHAP(1,2*II-1)=FUNF(II)
				SHAP(2,2*II)=FUNF(II)
				SHAPT(2*II-1,1)=FUNF(II)
				SHAPT(2*II,2)=FUNF(II)
			ENDDO
			CALL NULL(DTKD,8,8)
			CALL MATMUL(SHAPT,8,SHAP,2,DTKD,8)
			CALL MSMULT(DTKD,quot,8,8) 
			call matadd(EM,8,DTKD,8) 
			                                                                					                                                                                                                                             
		enddo
		enddo  !! END OF GAUSS POINT RECYCLE
		call formkv4(K_GLOBE,em,g,2*nn4,8)
		do ii=1,nodf
		do jj=1,2
			F_GLOBE(2*NODEID4(II)+jj-2)=
     $				F_GLOBE(2*NODEID4(II)+jj-2)-A_F2(2*II+jj-2)
		enddo
		enddo  
	enddo
	enddo


	call banred(K_GLOBE,2*nn4,iw)
	call bacsub(K_GLOBE,F_GLOBE,2*nn4,iw)
	call null(vel,2,10000)
	do ii=1,2
	do jj=1,nn4
		vel(ii,jj)=F_GLOBE(2*JJ+II-2)
	enddo
	enddo
	CONTINUE
	end subroutine


!!========================Li===========================
!!================模型区域不受限制=====================
!!=====================================================
       subroutine tun55geo(n_angbkfix,iangbkfix,angbkfix,                                  
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *             nn,nye,NYL,nntemp,nxe,nye0,spoke,                                                    
     *             radius,rad2,radius1,rad21,radius2,rad22,coord,icoord,                               
     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                   
                                                                                     
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,ang,pi                                                        
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82),rad2(42),radius1(82),radius2(82)
     *        ,rad21(42),rad22(42)                                               
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	 real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
	 real*8 x_in2(0:100),y_in2(0:100)                                                         
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                  
	 integer iangbkfix(500)                                                                   
	 real*8 angbkfix(500)                                                                     
                                                                                          
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer i3elemx(80),i3elemy(80),i3elm_deload(80,3)                           
	 real*8 ang3_elm_deload(80,3)                                                          

      common/materials/elemmats(40,80)
	integer elemmats  
                                                                                          
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	 COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     * i_v4nodes(82,42),i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
c       added by BASHAR to write the geometry information and review
	 real*8 aspect_ratio,mm1,mm2
		
c        end of added by BASHAR
	 call null(ang1_elm_deload,80,3) 
	 call null(ang2_elm_deload,80,3)
	 call null(ang3_elm_deload,80,3)                
	 
	 nye=nye0*2  !nye0=12, nye=24                                                                         
	 NYL=(nye0+1)*2 !NYL=26                                                                                                                                             
  	 nn_1=nye/2*(3*nxe+2)+(2*nxe+1) ! Total nodes in one area                           
	 nn=nn_1*2-(nxe/4*2+1) ! two areas minus common nodes                                    
 	 NELEMENT=nxe*nye                                                                                                                                                 
  	 nn4_1=(nye/2+1)*(nxe+1) ! Total nodes in one area                                  
	 nn4=nn4_1*2-(nxe/4+1) ! two areas minus common nodes                                                                                                                       
	 nntemp=(nxe+1)*NYL-(nxe/4+1) ! vertex nodes

! Make vertex nodes index                                                                                                                      
	do i2=1,nxe+1                                                                            
		do i1=1,nye+2                                                                           
			if(i1.LE.nye/2+1) then					! for bottom area:                                          
				i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                           
			elseif(i1.LE.nye/2+2) then                                                             
				if(i2.LE.nxe*1/4+1) then			! for top    area extra edges:                             
					i_vnodes(i1,i2)=0                                                                    
				else								! for top    area common edges:                                           
					i_vnodes(i1,i2)=nn_1+(i2-(nxe*1/4+1))*2                                              
				endif                                                                                 
			else										! for top    area:                                                       
				i_vnodes(i1,i2)=nn_1-(nxe*1/2+1)                                                      
     *			+ 2*i2-1+(2*nxe+2+nxe)*(i1-(nye/2+2))                                            
				continue                                                                              
			end if	                                                                                                                                   
	    end do                                                                               
	end do
                                                                         
! Splitting angles                                                                        
      pi=4.0D0*datan(1.D00)                                                                                                             
	n_node=0 

!!==================Li================
	                                                                                
      do p=0,nxe*2                                                                        
          if(p.lt.nxe/2) then
		p_ang=-0.5*pi+p*datan(radius1(nye0+1)/radius2(nye0+1))*2.0/nxe
		elseif(p.lt.nxe) then
		   p_ang=-0.5*pi+datan(radius1(nye0+1)/radius2(nye0+1))
     *       +(p-nxe/2.0)*datan(radius2(nye0+1)/radius1(nye0+1))*2.0/nxe
		elseif(p.lt.3*nxe/2) then
		   p_ang=(p-nxe)*datan(radius(nye0+1)/radius1(nye0+1))*2.0/nxe
		else
		   p_ang=datan(radius(nye0+1)/radius1(nye0+1))
     *      +(p-3*nxe/2.0)*datan(radius1(nye0+1)/radius(nye0+1))*2.0/nxe
		endif                                                                    
		x_in(p)=radius(1)*dcos(p_ang) !!@下部井眼边界节点坐标(含中间结点) （Lower wellbore boundary node coordinates (including intermediate node)）                      
		y_in(p)=radius(1)*dsin(p_ang)                                                                                                                                                                                                      
		if(p_ang.Lt.-datan(radius2(nye0+1)/radius1(nye0+1))) then                                                              
			y_out(p)=-radius2(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                          
		elseif(p_ang.Lt.datan(radius(nye0+1)/radius1(nye0+1))) then                                                           
			x_out(p)=radius1(nye0+1)                                                                
			y_out(p)=x_out(p)*dtan(p_ang)                                                          
		else                                                                                    
			y_out(p)=radius(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                           
		endif                                                                                  
	end do                                                                                   
                                                                                          
	rm=radius(nye0+1)                                                                        
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0                                                                             
! bottom area coord 
                                      
	do q=1,nye/2+1                                                                           	  
        do p=0,nxe ! lines that include vertex nodes & Middle points

!!==================Li================	  
	  if(p.le.nxe/4) then
	   f_in_weight=(radius2(nye0+1)-radius2(q))
     *               /(radius2(nye0+1)-radius2(1))                        
	   f_out_weight=1.D0-f_in_weight                                                            
	   elseif(p.le.3*nxe/4) then
	   	f_in_weight=(radius1(nye0+1)-radius1(q))
     *                /(radius1(nye0+1)-radius1(1))                        
	    f_out_weight=1.D0-f_in_weight                                                             
	   else
		f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	   endif 	   
	                      
		! Vertex points                                                                         
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                 
		y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight  !! Modified By Shunfu.Z 2009.07.08                           
999       format(i10,2f20.12)
                                                                                                                                                                                                
		if(p.EQ.0.OR.p.EQ.nxe) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=1 ! x disp is fixed                                                                                                              
		endif

!!==============Li================
!c          if(p.eq.0.and.q.eq.nye0+1) then
!c			n_fix=n_fix+1                                                                          
!c			iafix(n_fix,0)=n_node                                                                  
!c			iafix(n_fix,1)=1 ! x disp is fixed，左下端节点固定
!c          endif

!!==================Li================
		if(q.EQ.nye0+1.AND.p.le.nxe/4.0) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=2 ! y disp is fixed                                                                                                         
		endif

!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.le.(3*nxe/4)) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================

		!! Added By Shunfu.Z 2009.07.08 ,to constrain the bottom node's displacement in direction y
			                                                                                       
		if(q.EQ.1) then                                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                          
			iabkfix(n_bkfix,2)=1 ! hole1                                                           
		else if(q.EQ.nye0+1.AND.p.LE.nxe*3/4) then                                                                                      
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=3 ! pressure disp is fixed
			iabkfix(n_bkfix,2)=256                                                                                            
		endif                                                                                   
		if(q.EQ.nye0+1.AND.p.LE.nxe*3/4) then	                                                                                                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=1 ! Ux                                                              
			iabkfix(n_bkfix,2)=256 ! outer                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=2 ! Uy                                                              
			iabkfix(n_bkfix,2)=256 ! outer                                                                                                                        
		endif
	                                                                                       
		if(q.EQ.1) then                                                                         
			n_angbkfix=n_angbkfix+1                                                                
			iangbkfix(n_angbkfix)=n_node                                                           
			angbkfix(n_angbkfix)=-pi/2.d0+p*pi/nxe  ! 包括边节点，故有2*nxe等分     （Edge nodes, so 2 * nxe decile）                                                        
		endif                                                                                   
			                                                                                       
		if(p.LT.nxe) then ! 这一圈全都是Middle points      （This circle all Middle points ）                                     
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
		y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight !! Modified by Shunfu.Z 2009.07.07                                                                    
		if(q.EQ.nye0+1.AND.p.Lt.nxe*3/4) then !!以免跟上部区域有重复节点 （To avoid duplicate nodes to keep up with the Ministry of Regional）
		n_bkfix=n_bkfix+1                                                                       
		iabkfix(n_bkfix,0)=n_node                                                               
		iabkfix(n_bkfix,1)=1 ! Ux                                                               
		iabkfix(n_bkfix,2)=256 ! outer                                                                                          
		n_bkfix=n_bkfix+1                                                                       
		iabkfix(n_bkfix,0)=n_node                                                               
		iabkfix(n_bkfix,1)=2 ! Uy                                                               
		iabkfix(n_bkfix,2)=256 ! outer                                                          
		endif

!!==================Li================
		if(q.EQ.nye0+1.AND.p.lt.nxe/4.0) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=2 ! y disp is fixed                                                                                                         
		endif                                                                                   

!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.le.(3*nxe/4)) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================
			                                                                                       
		if(q.EQ.1) then                                                                         
		n_angbkfix=n_angbkfix+1                                                                 
		iangbkfix(n_angbkfix)=n_node                                                            
	angbkfix(n_angbkfix)=-pi/2.d0+(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等 =Including the edge nodes, so 2 * nxe, etc.                                                    
		endif                                                                                   
		endif                                                                                   
	  end do                                                                                 
                                                                                          
          if(q.LT.nye0+1) then 	   				                                                          
		do p=0,nxe ! Middle edges 
!!==================Li================				 
	  if(p.le.nxe/4) then                                                           
	   f2_in_weight=(radius2(nye0+1)-(radius2(q+1)+radius2(q))/2.D0)                               
     *	 /(radius2(nye0+1)-radius2(1))                                                        
	   f2_out_weight=1.D0-f2_in_weight 
	   elseif(p.le.3*nxe/4) then                                                          
	    f2_in_weight=(radius1(nye0+1)-(radius1(q+1)+radius1(q))/2.D0)                               
     *	  /(radius1(nye0+1)-radius1(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight 
	   else                                                            
	    f2_in_weight=(radius(nye0+1)-(radius(q+1)+radius(q))/2.D0)                               
     *	  /(radius(nye0+1)-radius(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight
	   endif 
	   		                                                              
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                               
		y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight                           
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
		n_fix=n_fix+1                                                                            
		iafix(n_fix,0)=n_node                                                                    
		iafix(n_fix,1)=1 ! x disp is fixed 
	endif 
				                                                                                   
		end do                                                                                  
	endif                                                                                    
	end do 

!	n_fix=n_fix+1                                                                            
!	iafix(n_fix,0)=n_node                                                                    
!	iafix(n_fix,1)=2 ! y disp is fixed                                                                 

!!==================Li================
 
      do p=0,nxe*2                                                                       
          if(p.lt.nxe/2) then
		 p_ang=-0.5*pi+p*datan(rad21(nye0+1)/rad2(nye0+1))*2.0/nxe
		elseif(p.lt.nxe) then
		  p_ang=-0.5*pi+datan(rad21(nye0+1)/rad2(nye0+1))
     1        +(p-nxe/2.0)*datan(rad2(nye0+1)/rad21(nye0+1))*2.0/nxe
		elseif(p.lt.3*nxe/2) then
		  p_ang=(p-nxe)*datan(rad22(nye0+1)/rad21(nye0+1))*2.0/nxe
		else
		  p_ang=datan(rad22(nye0+1)/rad21(nye0+1))
     1     +(p-3*nxe/2.0)*datan(rad21(nye0+1)/rad22(nye0+1))*2.0/nxe
		endif                                                                    
		x_in2(p)=rad2(1)*dcos(p_ang) !!@上部井眼边界节点坐标     （The upper borehole border node coordinates）                               
		y_in2(p)=rad2(1)*dsin(p_ang)                                                                                                                                            
		if(p_ang.LT.-datan(rad2(nye0+1)/rad21(nye0+1))) then                                                              
			y_out(p)=-rad2(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                          
		elseif(p_ang.LT.datan(rad22(nye0+1)/rad21(nye0+1))) then                                                           
			x_out(p)=rad21(nye0+1)                                                                
			y_out(p)=x_out(p)*dtan(p_ang)                                                          
		else                                                                                    
			y_out(p)=rad22(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                           
		endif                                                                                   
	end do 
	                                                                                                              
	do q=nye/2+1,1,-1                                                                                                                                  
       do p=0,nxe ! lines that include vertex nodes

!!==================Li================ 	 
	 if(p.lt.nxe/4) then
	 f_in_weight=(rad2(nye0+1)-rad2(q))/(rad2(nye0+1)-rad2(1))                        
	   f_out_weight=1.D0-f_in_weight                                                            
	   elseif(p.lt.3*nxe/4) then
	    f_in_weight=(rad21(nye0+1)-rad21(q))
     *               /(rad21(nye0+1)-rad21(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	   else
          f_in_weight=(rad22(nye0+1)-rad22(q))
     *                /(rad22(nye0+1)-rad22(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	 endif 
	                                        
		if((q.LT.nye/2+1).OR.(p.GE.nxe*1/4)) then                                               
			if((q.LT.nye/2+1).OR.(p.GT.nxe*1/4)) then ! Vertex points                               
				n_node=n_node+1                                                                         
			x(n_node)=x_in2(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                
	y(n_node)=y_in2(p*2)*f_in_weight+y_out(p*2)*f_out_weight+2.0d0*rm                                                                      
				if(p.EQ.0.OR.p.EQ.nxe) then                                                              
					n_fix=n_fix+1                                                                            
					iafix(n_fix,0)=n_node                                                                    
					iafix(n_fix,1)=1 ! x disp is fixed  
				endif     
				                                                                               
!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.ge.nxe/4) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================
			                                                                                       
				if(q.EQ.1) then ! Hole2                                                                  
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
					iabkfix(n_bkfix,2)=2 ! hole2                                                                                              
				else if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                               
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                                                                     
					iabkfix(n_bkfix,2)=256 ! outer	                                                          
				endif                                                                                    
			                                                                                       
				if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                                    
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=1 ! Ux                                                                
					iabkfix(n_bkfix,2)=256 ! outer                                                                                                    
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=2 ! Ux                                                                
					iabkfix(n_bkfix,2)=256 ! outer                                                          
				endif                                                                                    
			                                                                                       
				if(q.EQ.1) then                                                                          
					n_angbkfix=n_angbkfix+1                                                                  
					iangbkfix(n_angbkfix)=n_node                                                             
			angbkfix(n_angbkfix)=-pi/2.d0+p*pi/nxe  ! 包括边节点，故有2*nxe等分 =Edge nodes, so 2 * nxe decile
				endif                                                                                    
			endif
	                                                                                 
			                                                                                       
	if(p.LT.nxe) then !Middle points                                                         
	n_node=n_node+1                                                                          
	x(n_node)=x_in2(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
	y(n_node)=y_in2(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight+2.0d0*rm   
	
!!===========================li===================
!!外边界固定，位移为0
!         		if(q.EQ.nye0+1.and.p.ge.nxe/4) then  
!			  	n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=1 ! x disp is fixed 
!				n_fix=n_fix+1                                                                            
!				iafix(n_fix,0)=n_node                                                                    
!				iafix(n_fix,1)=2 ! y disp is fixed
!			endif
!!=================================================	
	                       		                                              
	if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                                    
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=1 ! Ux                                                                
		iabkfix(n_bkfix,2)=256 ! outer                                                          
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=2 ! Uy                                                                
		iabkfix(n_bkfix,2)=256 ! outer                                                          
	endif                                                                                    
			                                                                                       
	if(q.EQ.1) then                                                                          
	n_angbkfix=n_angbkfix+1                                                                  
	iangbkfix(n_angbkfix)=n_node                                                             
	angbkfix(n_angbkfix)=-pi/2.d0+(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等=Including the edge nodes, so 2 * nxe, etc.
	endif                                                                                    
		endif                                                                                   
		endif                                                                                   
	  end do                                                                                 
                                                                                          
          if(q.GT.1) then                                                                 
!	f2_in_weight=(rad2(nye0+1)-(rad2(q-1)+rad2(q))/2.D0)                                     
!     *	/(rad2(nye0+1)-rad2(1))                                                            
!	f2_out_weight=1.D0-f2_in_weight                                              
 		do p=0,nxe ! Middle edges 
!!==================Li================ 		
	if(p.lt.nxe/4) then                                                            
	   f2_in_weight=(rad2(nye0+1)-(rad2(q-1)+rad2(q))/2.D0)                               
     *	 /(rad2(nye0+1)-rad2(1))                                                        
	   f2_out_weight=1.D0-f2_in_weight 
	   elseif(p.lt.3*nxe/4) then                                                           
	    f2_in_weight=(rad21(nye0+1)-(rad21(q-1)+rad21(q))/2.D0)                               
     *	  /(rad21(nye0+1)-rad21(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight 
	   else                                                        
	    f2_in_weight=(rad22(nye0+1)-(rad22(q-1)+rad22(q))/2.D0)                               
     *	  /(rad22(nye0+1)-rad22(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight
	   endif
	   					                                                             
		n_node=n_node+1                                                                         
		x(n_node)=x_in2(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                              
	y(n_node)=y_in2(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight
     *	+2.0d0*rm                           
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
		n_fix=n_fix+1                                                                            
		iafix(n_fix,0)=n_node                                                                    
		iafix(n_fix,1)=1 ! x disp is fixed                                                       
	endif                                                                                    
		end do                                                                                  
	endif                                                                                    
	end do                                                                                                                                                                   
	                                                                                         
	n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0                                                                              
! elem nodes                                           
	do q=1,nye                                                                               
      do p=1,nxe                                                                          
	if(q.LE.nye0) then                                                                       
	      i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
101    format(8i10)                                                                       
		! 4节点网格系统   =   4-node grid system                                                                      
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
		                                                                                        
	elseif((q.EQ.nye0+1).AND.(p.LE.nxe*1/4))then                                             
	    aq=nn_1-(p-1)*2                                                                      
	    ar=aq-1                                                                              
	    as=ar-1                                                                              
		                                                                                        
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
	                                                                                         
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统    = 4-node grid system                                                                     
		Iq = NN4_1 - (P - 1)                                                                    
		Is = Iq - 1                                                                             
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
	elseif((q.EQ.nye0+1).AND.(p.EQ.nxe*1/4+1)) then                                          
	    aq=nn_1-(p-1)*2                                                                      
	    ar=(q-1)*(3*nxe+2)+2*p-1+nxe*3/2+1                                                   
	    as=ar+1                                                                              
		                                                                                        
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
	                                                                                         
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统      = Node grid system                                                                   
		Iq = NN4_1 - (P - 1)                                                                    
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
		Is = Im - (NXE + 1)                                                                     
	else                                                                                     
		aq=(q-1)*(3*nxe+2)+2*p-1+nxe*3/2                                                        
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
                                                                                          
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
                                                                                          
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统     =  Node grid system                                                                    
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
		Is = Im - (NXE + 1)                                                                     
		Iq = Is - 1                                                                             
	endif                                                                                                                                                  
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is                                                                        

          elemmats(p,q)=(q-1)*nxe+p

	if(q.EQ.1) then !hole 1                                                                                                                         
		n1_deload=n1_deload+1                                                                   
		i1elemx(n1_deload)=p
          i1elemy(n1_deload)=q                                                                    
		i1elm_deload(n1_deload,1)=ao                                                            
		i1elm_deload(n1_deload,2)=al                                                            
		i1elm_deload(n1_deload,3)=am                                                   
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang1_elm_deload(n1_deload,1)=angbkfix(ij)
			else if(iangbkfix(ij).eq.al) then
				ang1_elm_deload(n1_deload,2)=angbkfix(ij)
			else if(iangbkfix(ij).eq.am) then                                                         
				ang1_elm_deload(n1_deload,3)=angbkfix(ij)
			else
			endif
		enddo	                                                                                              
	endif
	continue                                                                                    
	if(q.eq.nye) then !hole 2                                                                                                                       
		n3_deload=n3_deload+1                                                                   
		i3elemx(n3_deload)=p                                                                    
		i3elemy(n3_deload)=q                                                                    
		i3elm_deload(n3_deload,1)=ao                                                            
		i3elm_deload(n3_deload,2)=al                                                            
		i3elm_deload(n3_deload,3)=am
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang3_elm_deload(n3_deload,1)=angbkfix(ij)
			else if(iangbkfix(ij).eq.al) then                                                         
				ang3_elm_deload(n3_deload,2)=angbkfix(ij)
			else if(iangbkfix(ij).eq.am) then                                                         
				ang3_elm_deload(n3_deload,3)=angbkfix(ij)
			else
			endif
		enddo                                                                                                                                                                     
	endif                                                                                    
	if(q.eq.nye0.AND.p.LE.nxe*3/4.OR.q.eq.nye0+1.AND.p.GT.nxe/4) then ! out                  
		n2_deload=n2_deload+1                                                                   
		i2elemx(n2_deload)=p                                                                    
		i2elemy(n2_deload)=q                                                                    
		i2elm_deload(n2_deload,1)=as                                                            
		i2elm_deload(n2_deload,2)=ar                                                            
		i2elm_deload(n2_deload,3)=aq                                                            
		!!Shunfu.Z
		ang2_elm_deload(n2_deload,1)=0.0d0
		ang2_elm_deload(n2_deload,2)=0.0d0
		ang2_elm_deload(n2_deload,3)=0.0d0
		if (q.eq.nye0.AND.p.LE.nxe*1/4) then
 			ang2_elm_deload(n2_deload,1)=-pi/2.0d0
 			ang2_elm_deload(n2_deload,2)=-pi/2.0d0
 			ang2_elm_deload(n2_deload,3)=-pi/2.0d0
		endif
		if (q.eq.nye0+1.AND.p.GT.nxe*3/4) then
 			ang2_elm_deload(n2_deload,1)=pi/2.0d0
 			ang2_elm_deload(n2_deload,2)=pi/2.0d0
 			ang2_elm_deload(n2_deload,3)=pi/2.0d0		
		endif                                                 
	endif                                                                                    
                                                                                       
	end do                                                                                   
	end do 	
	
c       added by Bashar to change x locations
		aspect_ratio=0.25
		call aspect_ratio_changer_sagd(x,y,nxe,nye,nn,nodep,
     *        aspect_ratio)
	!end of added by BASHAR                                                                                                                                                                     
853   format(2f15.8)                                                                                                                  
       return                                                                             
       end 


!!==============Li=================
!!模型区域不受限制，边界中点去节点坐标中间值，某个中点坐标错误
       subroutine tun56geo(n_angbkfix,iangbkfix,angbkfix,                                  
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *             nn,nye,NYL,nntemp,nxe,nye0,spoke,                                                    
     *             radius,rad2,radius1,rad21,radius2,rad22,coord,icoord,                               
     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                   
                                                                                     
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,ang,pi                                                        
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82),rad2(42),radius1(82),radius2(82)
     *        ,rad21(42),rad22(42)                                               
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	 real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
	 real*8 x_in2(0:100),y_in2(0:100)                                                         
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                  
	 integer iangbkfix(500)                                                                   
	 real*8 angbkfix(500)                                                                     
                                                                                          
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer i3elemx(80),i3elemy(80),i3elm_deload(80,3)                           
	 real*8 ang3_elm_deload(80,3)                                                          
                                                                                          
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	 COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     * i_v4nodes(82,42),i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
c       added by BASHAR to write the geometry information and review
	 real*8 aspect_ratio,mm1,mm2,x1,x2,y1,y2
		
c        end of added by BASHAR
	 call null(ang1_elm_deload,80,3) 
	 call null(ang2_elm_deload,80,3)
	 call null(ang3_elm_deload,80,3)                
	 
	 nye=nye0*2  !nye0=12, nye=24                                                                         
	 NYL=(nye0+1)*2 !NYL=26                                                                                                                                             
  	 nn_1=nye/2*(3*nxe+2)+(2*nxe+1) ! Total nodes in one area                           
	 nn=nn_1*2-(nxe/4*2+1) ! two areas minus common nodes                                    
 	 NELEMENT=nxe*nye                                                                                                                                                 
  	 nn4_1=(nye/2+1)*(nxe+1) ! Total nodes in one area                                  
	 nn4=nn4_1*2-(nxe/4+1) ! two areas minus common nodes                                                                                                                       
	 nntemp=(nxe+1)*NYL-(nxe/4+1) ! vertex nodes

! Make vertex nodes index                                                                                                                      
	do i2=1,nxe+1                                                                            
		do i1=1,nye+2                                                                           
			if(i1.LE.nye/2+1) then					! for bottom area:                                          
				i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                           
			elseif(i1.LE.nye/2+2) then                                                             
				if(i2.LE.nxe*1/4+1) then			! for top    area extra edges:                             
					i_vnodes(i1,i2)=0                                                                    
				else								! for top    area common edges:                                           
					i_vnodes(i1,i2)=nn_1+(i2-(nxe*1/4+1))*2                                              
				endif                                                                                 
			else										! for top    area:                                                       
				i_vnodes(i1,i2)=nn_1-(nxe*1/2+1)                                                      
     *			+ 2*i2-1+(2*nxe+2+nxe)*(i1-(nye/2+2))                                            
				continue                                                                              
			end if	                                                                                                                                   
	    end do                                                                               
	end do
                                                                         
! Splitting angles                                                                        
      pi=4.0D0*datan(1.D00)                                                                                                             
	n_node=0 

!!==================Li================
	                                                                                
      do p=0,nxe                                                                        
          if(p.lt.nxe/4) then
		p_ang=-0.5*pi+p*datan(radius1(nye0+1)/radius2(nye0+1))*4.0/nxe
		elseif(p.lt.nxe/2) then
		   p_ang=-0.5*pi+datan(radius1(nye0+1)/radius2(nye0+1))
     *       +(p-nxe/4.0)*datan(radius2(nye0+1)/radius1(nye0+1))*4.0/nxe
		elseif(p.lt.3*nxe/4) then
		   p_ang=(p-nxe/2.0)*datan(radius(nye0+1)/radius1(nye0+1))*4.0/nxe
		else
		   p_ang=datan(radius(nye0+1)/radius1(nye0+1))
     *      +(p-3*nxe/4.0)*datan(radius1(nye0+1)/radius(nye0+1))*4.0/nxe
		endif                                                                    
		x_in(p)=radius(1)*dcos(p_ang) !!@下部井眼边界节点坐标(含中间结点) （Lower wellbore boundary node coordinates (including intermediate node)）                      
		y_in(p)=radius(1)*dsin(p_ang)                                                                                                                                                                                                      
		if(p_ang.Lt.-datan(radius2(nye0+1)/radius1(nye0+1))) then                                                              
			y_out(p)=-radius2(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                          
		elseif(p_ang.Lt.datan(radius(nye0+1)/radius1(nye0+1))) then                                                           
			x_out(p)=radius1(nye0+1)                                                                
			y_out(p)=x_out(p)*dtan(p_ang)                                                          
		else                                                                                    
			y_out(p)=radius(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                           
		endif                                                                                  
	end do                                                                                   
                                                                                          
	rm=radius(nye0+1)                                                                        
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0                                                                             
! bottom area coord 
                                      
	do q=1,nye/2+1                                                                           	  
        do p=0,nxe ! lines that include vertex nodes & Middle points

!!==================Li================	  
	  if(p.lt.nxe/4) then
	   f_in_weight=(radius2(nye0+1)-radius2(q))
     *               /(radius2(nye0+1)-radius2(1))                        
	   f_out_weight=1.D0-f_in_weight                                                            
	   elseif(p.lt.3*nxe/4) then
	   	f_in_weight=(radius1(nye0+1)-radius1(q))
     *                /(radius1(nye0+1)-radius1(1))                        
	    f_out_weight=1.D0-f_in_weight                                                             
	   else
		f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	   endif 	   
	                      
		! Vertex points                                                                         
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p)*f_in_weight+x_out(p)*f_out_weight                                 
		y(n_node)=y_in(p)*f_in_weight+y_out(p)*f_out_weight  !! Modified By Shunfu.Z 2009.07.08                           
999       format(i10,2f20.12)
                                                                                                                                                                                                
		if(p.EQ.0.OR.p.EQ.nxe) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=1 ! x disp is fixed                                                                                                              
		endif

!!==================Li================
		if(q.EQ.nye0+1.AND.p.le.nxe/4.0) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=2 ! y disp is fixed                                                                                                         
		endif

		!! Added By Shunfu.Z 2009.07.08 ,to constrain the bottom node's displacement in direction y
	goto 177
		if(q.EQ.nye0+1.AND.p.eq.0) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=2 ! y disp is fixed                                                                                                         
		endif	                                                                                 
177		continue
			                                                                                       
		if(q.EQ.1) then                                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                          
			iabkfix(n_bkfix,2)=1 ! hole1                                                           
		else if(q.EQ.nye0+1.AND.p.LE.nxe*3/4) then                                                                                      
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=3 ! pressure disp is fixed
			iabkfix(n_bkfix,2)=256                                                                                            
		endif                                                                                   
		if(q.EQ.nye0+1.AND.p.LE.nxe*3/4) then	                                                                                                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=1 ! Ux                                                              
			iabkfix(n_bkfix,2)=256 ! outer                                                         
			n_bkfix=n_bkfix+1                                                                      
			iabkfix(n_bkfix,0)=n_node                                                              
			iabkfix(n_bkfix,1)=2 ! Uy                                                              
			iabkfix(n_bkfix,2)=256 ! outer                                                                                                                        
		endif
	                                                                                       
		if(q.EQ.1) then                                                                         
			n_angbkfix=n_angbkfix+1                                                                
			iangbkfix(n_angbkfix)=n_node                                                           
			angbkfix(n_angbkfix)=-pi/2.d0+p*pi/nxe  ! 包括边节点，故有2*nxe等分     （Edge nodes, so 2 * nxe decile）                                                        
		endif                                                                                   
			                                                                                       
		if(p.LT.nxe) then ! 这一圈全都是Middle points      （This circle all Middle points ）                                     
	
!!==================Li================ 
          x1=x(n_node)
	    y1=y(n_node)
          n_node=n_node+1		                                                                         
!		x1=x_in(p)*f_in_weight+x_out(p)*f_out_weight                                 
!		y1=y_in(p)*f_in_weight+y_out(p)*f_out_weight
	  if((p+1).lt.nxe/4) then
	   f_in_weight=(radius2(nye0+1)-radius2(q))
     *               /(radius2(nye0+1)-radius2(1))                        
	   f_out_weight=1.D0-f_in_weight                                                            
	   elseif((p+1).lt.3*nxe/4) then
	   	f_in_weight=(radius1(nye0+1)-radius1(q))
     *                /(radius1(nye0+1)-radius1(1))                        
	    f_out_weight=1.D0-f_in_weight                                                             
	   else
		f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	   endif 
		x2=x_in(p+1)*f_in_weight+x_out(p+1)*f_out_weight                                 
		y2=y_in(p+1)*f_in_weight+y_out(p+1)*f_out_weight
	    x(n_node)=(x1+x2)/2.0d0
	    y(n_node)=(y1+y2)/2.0d0
		                                                                    
		if(q.EQ.nye0+1.AND.p.Lt.nxe*3/4) then !!以免跟上部区域有重复节点 （To avoid duplicate nodes to keep up with the Ministry of Regional）
		n_bkfix=n_bkfix+1                                                                       
		iabkfix(n_bkfix,0)=n_node                                                               
		iabkfix(n_bkfix,1)=1 ! Ux                                                               
		iabkfix(n_bkfix,2)=256 ! outer                                                                                          
		n_bkfix=n_bkfix+1                                                                       
		iabkfix(n_bkfix,0)=n_node                                                               
		iabkfix(n_bkfix,1)=2 ! Uy                                                               
		iabkfix(n_bkfix,2)=256 ! outer                                                          
		endif

!!==================Li================
		if(q.EQ.nye0+1.AND.p.lt.nxe/4.0) then                                                             
			n_fix=n_fix+1                                                                          
			iafix(n_fix,0)=n_node                                                                  
			iafix(n_fix,1)=2 ! y disp is fixed                                                                                                         
		endif                                                                                   
			                                                                                       
		if(q.EQ.1) then                                                                         
		n_angbkfix=n_angbkfix+1                                                                 
		iangbkfix(n_angbkfix)=n_node                                                            
	angbkfix(n_angbkfix)=-pi/2.d0+(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等 =Including the edge nodes, so 2 * nxe, etc.                                                    
		endif                                                                                   
		endif                                                                                   
	  end do                                                                                 
                                                                                          
          if(q.LT.nye0+1) then 	   				                                                          
		do p=0,nxe ! Middle edges 
!!==================Li================				 
	  if(p.lt.nxe/4) then                                                           
	   f2_in_weight=(radius2(nye0+1)-(radius2(q+1)+radius2(q))/2.D0)                               
     *	 /(radius2(nye0+1)-radius2(1))                                                        
	   f2_out_weight=1.D0-f2_in_weight 
	   elseif(p.lt.3*nxe/4) then                                                          
	    f2_in_weight=(radius1(nye0+1)-(radius1(q+1)+radius1(q))/2.D0)                               
     *	  /(radius1(nye0+1)-radius1(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight 
	   else                                                            
	    f2_in_weight=(radius(nye0+1)-(radius(q+1)+radius(q))/2.D0)                               
     *	  /(radius(nye0+1)-radius(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight
	   endif 
	   		                                                              
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p)*f2_in_weight+x_out(p)*f2_out_weight                               
		y(n_node)=y_in(p)*f2_in_weight+y_out(p)*f2_out_weight                           
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
		n_fix=n_fix+1                                                                            
		iafix(n_fix,0)=n_node                                                                    
		iafix(n_fix,1)=1 ! x disp is fixed 
	endif 
				                                                                                   
		end do                                                                                  
	endif                                                                                    
	end do 

!	n_fix=n_fix+1                                                                            
!	iafix(n_fix,0)=n_node                                                                    
!	iafix(n_fix,1)=2 ! y disp is fixed                                                                 

!!==================Li================

      do p=0,nxe                                                                      
          if(p.lt.nxe/4) then
		 p_ang=-0.5*pi+p*datan(rad21(nye0+1)/rad2(nye0+1))*4.0/nxe
		elseif(p.lt.nxe/2) then
		  p_ang=-0.5*pi+datan(rad21(nye0+1)/rad2(nye0+1))
     1        +(p-nxe/4.0)*datan(rad2(nye0+1)/rad21(nye0+1))*4.0/nxe
		elseif(p.lt.3*nxe/4) then
		  p_ang=(p-nxe/2.0)*datan(rad22(nye0+1)/rad21(nye0+1))*4.0/nxe
		else
		  p_ang=datan(rad22(nye0+1)/rad21(nye0+1))
     1     +(p-3*nxe/4.0)*datan(rad21(nye0+1)/rad22(nye0+1))*4.0/nxe
		endif                                                                    
		x_in2(p)=rad2(1)*dcos(p_ang) !!@上部井眼边界节点坐标     （The upper borehole border node coordinates）                               
		y_in2(p)=rad2(1)*dsin(p_ang)                                                                                                                                            
		if(p_ang.LT.-datan(rad2(nye0+1)/rad21(nye0+1))) then                                                              
			y_out(p)=-rad2(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                          
		elseif(p_ang.LT.datan(rad22(nye0+1)/rad21(nye0+1))) then                                                           
			x_out(p)=rad21(nye0+1)                                                                
			y_out(p)=x_out(p)*dtan(p_ang)                                                          
		else                                                                                    
			y_out(p)=rad22(nye0+1)                                                               
			x_out(p)=y_out(p)/dtan(p_ang)
			if(x_out(p).lt.1.0d-8) x_out(p)=0.0d0                                                           
		endif                                                                                   
	end do 
	                                                                                                              
	do q=nye/2+1,1,-1                                                                                                                                  
       do p=0,nxe ! lines that include vertex nodes

!!==================Li================ 	 
	 if(p.lt.nxe/4) then
	 f_in_weight=(rad2(nye0+1)-rad2(q))/(rad2(nye0+1)-rad2(1))                        
	   f_out_weight=1.D0-f_in_weight                                                            
	   elseif(p.lt.3*nxe/4) then
	    f_in_weight=(rad21(nye0+1)-rad21(q))
     *               /(rad21(nye0+1)-rad21(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	   else
          f_in_weight=(rad22(nye0+1)-rad22(q))
     *                /(rad22(nye0+1)-rad22(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	 endif 
	                                        
		if((q.LT.nye/2+1).OR.(p.GE.nxe*1/4)) then                                               
			if((q.LT.nye/2+1).OR.(p.GT.nxe*1/4)) then ! Vertex points                               
				n_node=n_node+1                                                                         
			x(n_node)=x_in2(p)*f_in_weight+x_out(p)*f_out_weight                                
	y(n_node)=y_in2(p)*f_in_weight+y_out(p)*f_out_weight+2.0d0*rm                                                                      
				if(p.EQ.0.OR.p.EQ.nxe) then                                                              
					n_fix=n_fix+1                                                                            
					iafix(n_fix,0)=n_node                                                                    
					iafix(n_fix,1)=1 ! x disp is fixed  
				endif                                                                                    
			                                                                                       
				if(q.EQ.1) then ! Hole2                                                                  
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
					iabkfix(n_bkfix,2)=2 ! hole2                                                                                              
				else if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                               
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                                                                     
					iabkfix(n_bkfix,2)=256 ! outer	                                                          
				endif                                                                                    
			                                                                                       
				if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                                    
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=1 ! Ux                                                                
					iabkfix(n_bkfix,2)=256 ! outer                                                                                                    
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=2 ! Ux                                                                
					iabkfix(n_bkfix,2)=256 ! outer                                                          
				endif                                                                                    
			                                                                                       
				if(q.EQ.1) then                                                                          
					n_angbkfix=n_angbkfix+1                                                                  
					iangbkfix(n_angbkfix)=n_node                                                             
			angbkfix(n_angbkfix)=-pi/2.d0+p*pi/nxe  ! 包括边节点，故有2*nxe等分 =Edge nodes, so 2 * nxe decile
				endif                                                                                    
			endif
	                                                                                 
			                                                                                       
	if(p.LT.nxe) then !Middle points                                                         
	                                                                         
!!==================Li================ 
      if(q.eq.nye/2+1.and.p.eq.nxe/4) then
	x1=x(n_node-nxe/2)
	y1=y(n_node-nxe/2)
	else
	x1=x(n_node)
	y1=y(n_node)
!	x1=x_in(p)*f_in_weight+x_out(p)*f_out_weight                                 
!	y1=y_in(p)*f_in_weight+y_out(p)*f_out_weight+2.0d0*rm
	endif
	n_node=n_node+1 

	if((p+1).le.nxe/4) then
	 f_in_weight=(rad2(nye0+1)-rad2(q))/(rad2(nye0+1)-rad2(1))                        
	   f_out_weight=1.D0-f_in_weight                                                            
	elseif((p+1).le.3*nxe/4) then
	    f_in_weight=(rad21(nye0+1)-rad21(q))
     *               /(rad21(nye0+1)-rad21(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	else
          f_in_weight=(rad22(nye0+1)-rad22(q))
     *                /(rad22(nye0+1)-rad22(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	endif
	x2=x_in(p+1)*f_in_weight+x_out(p+1)*f_out_weight                                 
      y2=y_in(p+1)*f_in_weight+y_out(p+1)*f_out_weight+2.0d0*rm
	x(n_node)=(x1+x2)/2.0d0
	y(n_node)=(y1+y2)/2.0d0 
		                         		                                              
	if(q.EQ.nye0+1.AND.p.GE.nxe*1/4) then                                                    
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=1 ! Ux                                                                
		iabkfix(n_bkfix,2)=256 ! outer                                                          
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=2 ! Uy                                                                
		iabkfix(n_bkfix,2)=256 ! outer                                                          
	endif                                                                                    
			                                                                                       
	if(q.EQ.1) then                                                                          
	n_angbkfix=n_angbkfix+1                                                                  
	iangbkfix(n_angbkfix)=n_node                                                             
	angbkfix(n_angbkfix)=-pi/2.d0+(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等=Including the edge nodes, so 2 * nxe, etc.
	endif                                                                                    
		endif                                                                                   
		endif                                                                                   
	  end do                                                                                 
                                                                                          
          if(q.GT.1) then                                                                                                              
 		do p=0,nxe ! Middle edges 
!!==================Li================ 		
	if(p.le.nxe/4) then                                                            
	   f2_in_weight=(rad2(nye0+1)-(rad2(q-1)+rad2(q))/2.D0)                               
     *	 /(rad2(nye0+1)-rad2(1))                                                        
	   f2_out_weight=1.D0-f2_in_weight 
	   elseif(p.le.3*nxe/4) then                                                           
	    f2_in_weight=(rad21(nye0+1)-(rad21(q-1)+rad21(q))/2.D0)                               
     *	  /(rad21(nye0+1)-rad21(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight 
	   else                                                        
	    f2_in_weight=(rad22(nye0+1)-(rad22(q-1)+rad22(q))/2.D0)                               
     *	  /(rad22(nye0+1)-rad22(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight
	   endif
	   					                                                             
		n_node=n_node+1                                                                         
		x(n_node)=x_in2(p)*f2_in_weight+x_out(p)*f2_out_weight                              
	y(n_node)=y_in2(p)*f2_in_weight+y_out(p)*f2_out_weight
     *	+2.0d0*rm                           
	if(p.EQ.0.OR.p.EQ.nxe) then                                                              
		n_fix=n_fix+1                                                                            
		iafix(n_fix,0)=n_node                                                                    
		iafix(n_fix,1)=1 ! x disp is fixed                                                       
	endif                                                                                    
		end do                                                                                  
	endif                                                                                    
	end do   
	                                                                                         
	n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0                                                                              
! elem nodes                                           
	do q=1,nye                                                                               
      do p=1,nxe                                                                          
	if(q.LE.nye0) then                                                                       
	      i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
101    format(8i10)                                                                       
		! 4节点网格系统   =   4-node grid system                                                                      
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
		                                                                                        
	elseif((q.EQ.nye0+1).AND.(p.LE.nxe*1/4))then                                             
	    aq=nn_1-(p-1)*2                                                                      
	    ar=aq-1                                                                              
	    as=ar-1                                                                              
		                                                                                        
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
	                                                                                         
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统    = 4-node grid system                                                                     
		Iq = NN4_1 - (P - 1)                                                                    
		Is = Iq - 1                                                                             
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
	elseif((q.EQ.nye0+1).AND.(p.EQ.nxe*1/4+1)) then                                          
	    aq=nn_1-(p-1)*2                                                                      
	    ar=(q-1)*(3*nxe+2)+2*p-1+nxe*3/2+1                                                   
	    as=ar+1                                                                              
		                                                                                        
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
	                                                                                         
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统      = Node grid system                                                                   
		Iq = NN4_1 - (P - 1)                                                                    
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
		Is = Im - (NXE + 1)                                                                     
	else                                                                                     
		aq=(q-1)*(3*nxe+2)+2*p-1+nxe*3/2                                                        
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
                                                                                          
		an=q*(3*nxe+2)+p-nxe-1+nxe*3/2                                                          
		ap=an+1                                                                                 
                                                                                          
		ao=q*(3*nxe+2)+2*p-1+nxe*3/2                                                            
		al=ao+1                                                                                 
		am=al+1                                                                                 
                                                                                          
		! 4节点网格系统     =  Node grid system                                                                    
		Io = Q * (NXE + 1) + NXE * 3 / 4 + P                                                    
		Im = Io + 1                                                                             
		Is = Im - (NXE + 1)                                                                     
		Iq = Is - 1                                                                             
	endif                                                                                                                                                  
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is                                                                        

	if(q.EQ.1) then !hole 1                                                                                                                         
		n1_deload=n1_deload+1                                                                   
		i1elemx(n1_deload)=p
          i1elemy(n1_deload)=q                                                                    
		i1elm_deload(n1_deload,1)=ao                                                            
		i1elm_deload(n1_deload,2)=al                                                            
		i1elm_deload(n1_deload,3)=am                                                   
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang1_elm_deload(n1_deload,1)=angbkfix(ij)
			else if(iangbkfix(ij).eq.al) then
				ang1_elm_deload(n1_deload,2)=angbkfix(ij)
			else if(iangbkfix(ij).eq.am) then                                                         
				ang1_elm_deload(n1_deload,3)=angbkfix(ij)
			else
			endif
		enddo	                                                                                              
	endif
	continue                                                                                    
	if(q.eq.nye) then !hole 2                                                                                                                       
		n3_deload=n3_deload+1                                                                   
		i3elemx(n3_deload)=p                                                                    
		i3elemy(n3_deload)=q                                                                    
		i3elm_deload(n3_deload,1)=ao                                                            
		i3elm_deload(n3_deload,2)=al                                                            
		i3elm_deload(n3_deload,3)=am
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang3_elm_deload(n3_deload,1)=angbkfix(ij)
			else if(iangbkfix(ij).eq.al) then                                                         
				ang3_elm_deload(n3_deload,2)=angbkfix(ij)
			else if(iangbkfix(ij).eq.am) then                                                         
				ang3_elm_deload(n3_deload,3)=angbkfix(ij)
			else
			endif
		enddo                                                                                                                                                                     
	endif                                                                                    
	if(q.eq.nye0.AND.p.LE.nxe*3/4.OR.q.eq.nye0+1.AND.p.GT.nxe/4) then ! out                  
		n2_deload=n2_deload+1                                                                   
		i2elemx(n2_deload)=p                                                                    
		i2elemy(n2_deload)=q                                                                    
		i2elm_deload(n2_deload,1)=as                                                            
		i2elm_deload(n2_deload,2)=ar                                                            
		i2elm_deload(n2_deload,3)=aq                                                            
		!!Shunfu.Z
		ang2_elm_deload(n2_deload,1)=0.0d0
		ang2_elm_deload(n2_deload,2)=0.0d0
		ang2_elm_deload(n2_deload,3)=0.0d0
		if (q.eq.nye0.AND.p.LE.nxe*1/4) then
 			ang2_elm_deload(n2_deload,1)=-pi/2.0d0
 			ang2_elm_deload(n2_deload,2)=-pi/2.0d0
 			ang2_elm_deload(n2_deload,3)=-pi/2.0d0
		endif
		if (q.eq.nye0+1.AND.p.GT.nxe*3/4) then
 			ang2_elm_deload(n2_deload,1)=pi/2.0d0
 			ang2_elm_deload(n2_deload,2)=pi/2.0d0
 			ang2_elm_deload(n2_deload,3)=pi/2.0d0		
		endif                                                 
	endif                                                                                    
                                                                                       
	end do                                                                                   
	end do 	
	
c       added by Bashar to change x locations
		aspect_ratio=0.25
		call aspect_ratio_changer_sagd(x,y,nxe,nye,nn,nodep,
     *        aspect_ratio)
	!end of added by BASHAR                                                                                                                                                                     
853   format(2f15.8)                                                                                                                  
       return                                                                             
       end 

! >>>> Dr. Fotios Karaoulanis >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      subroutine crack61geo(n_angbkfix, iangbkfix, angbkfix, n_fix,
     &                    iafix, n_bkfix, iabkfix, n1_deload, i1elemx,
     &                    i1elemy, i1elm_deload, ang1_elm_deload,
     &                    n2_deload, i2elemx, i2elemy, i2elm_deload,
     &                    ang2_elm_deload, n3_deload, i3elemx, i3elemy,
     &                    i3elm_deload, ang3_elm_deload, nn, nye, nyl,
     &                    nntemp, nxe, nye0, radius, spoke, x, y, nodep,
     &                    nodep4, nn4)
c     =================================================================
      implicit none
      integer, intent(in) :: nxe, nye0
      integer, intent(out) :: i1elemx(80), i1elemy(80),
     &                        i1elm_deload(80, 3), i2elemx(80),
     &                        i2elemy(80), i2elm_deload(80, 3),
     &                        i3elemx(80), i3elemy(80),
     &                        i3elm_deload(80, 3), iabkfix(500, 0:3),
     &                        iafix(500, 0:1), iangbkfix(500),
     &                        n1_deload, n2_deload, n3_deload,
     &                        n_angbkfix, n_bkfix, n_fix, nn, nn4,
     &                        nntemp, nodep(40, 80, 8),
     &                        nodep4(40, 80, 4), nye, nyl
      real*8, intent(in) :: radius(82), spoke(41)
      real*8, intent(out) :: ang1_elm_deload(80, 3),
     &                       ang2_elm_deload(80, 3),
     &                       ang3_elm_deload(80, 3),
     &                       angbkfix(500), x(5000),
     &                       y(5000)
      integer :: i1, i2, m1, m2, m3, m4, n1, n2, n3, n4, n5, n6, n7,
     &           n8, p, q
      real*8 :: pi
	common /mesh/ imesh, n_node, nelement, n_4node, i_vnodes(82,42),
     &              i_v4nodes(82,42), i_8_4_nodes(5000),
     &              i_4_8_nodes(5000), n_n
      integer :: imesh, n_node, nelement, n_4node, i_vnodes, i_v4nodes,
     &           i_8_4_nodes, i_4_8_nodes, n_n
      COMMON /mesh2/ crack_tip, crack_analysis_type
      COMMON /mesh3/ crack_width
      integer :: crack_tip, crack_analysis_type
      real*8 :: crack_width
c     ==================================================================
      ! Definitions
      nye = nye0
      nyl = nye0 + 1
      pi = 4.0D0 * datan(1.0d0)
      ! Total number of nodes
      nn = nye0 * (3 * nxe + 2) + (2 * nxe + 1)
      ! Total number of elements
      nelement = nxe * nye
      ! Total number of vertex nodes
      nntemp = (nxe + 1) * nyl
      ! Total number of vertex nodes (redundant?)
	!!@todo: Check difference between nntemp and nn4.
      nn4 = (nye0 + 1) * (nxe + 1)
      ! Vertex node ids
      do i2 = 1, nxe + 1
        do i1 = 1, nye + 1
          i_vnodes(i1, i2) = 2 * i2 - 1 + (2 * nxe + 2 + nxe) * (i1 - 1)
        end do
      end do
	
      ! ----------------------------------------------------------------
      ! Nodes
      ! ----------------------------------------------------------------
      n_node = 0
      do q = 1, nye + 1
        do p = 1, nxe + 1
          ! Vertex point id
          n_node = n_node + 1
          ! Edge nodes
          x(n_node) = radius(q)
          y(n_node) = spoke(p)
          ! Crack width
	    if ((q .eq. 1) .and. (p .lt.crack_tip)) then
            x( n_node ) = x( n_node ) + crack_width
	    end if
          ! Middle nodes in vertical direction
          if (p .lt. (nxe + 1)) then
            n_node = n_node + 1
            x(n_node) = radius(q)
            y(n_node) = 0.5*(spoke(p + 1) + spoke(p))
            if ((q .eq. 1) .and. (p .lt. crack_tip)) then
              x(n_node) = x(n_node) + crack_width
            end if
          end if
        end do
        if(q .lt. (nye + 1)) then
          ! Middle nodes in horizontal direction
          do p = 1, nxe + 1
            n_node = n_node + 1
            x(n_node) = 0.5 * (radius(q + 1) + radius(q))
            y(n_node) = spoke(p)
          end do
        end if
      end do
      
      ! ----------------------------------------------------------------
      ! iafix array
      ! ----------------------------------------------------------------
      n_fix = 0
      ! North west
      q = 1
      do p = 1, nxe
        n1 = (q - 1) * (3 * nxe + 2) + 2 * p - 1
        n2 = n1 + 1
        n3 = n2 + 1
        if (p .eq. crack_tip) then
          ! Fix x displacement in n1
          n_fix = n_fix + 1
          iafix(n_fix, 0) = n1
          iafix(n_fix, 1) = 1
        end if
        if (p .ge. crack_tip) then
          ! Fix x displacement in n2
          n_fix = n_fix + 1
          iafix(n_fix, 0) = n2
          iafix(n_fix, 1) = 1
          ! Fix x displacement in n3
          n_fix = n_fix + 1
          iafix(n_fix, 0) = n3
          iafix(n_fix, 1) = 1
        end if
      end do
      ! South
      p = 1
      ! Fix y displacement in corner
      q = 1
      n1 = (q - 1) * (3 * nxe + 2) + 2 * p - 1
      n_fix = n_fix + 1
      iafix(n_fix, 0) = n1
      iafix(n_fix, 1) = 2
      do q = 1, nye
        ! Fix y displacement in n8
        n8 = q * (3*nxe + 2) + p - nxe - 1
        n_fix = n_fix + 1
        iafix(n_fix, 0) = n8
        iafix(n_fix, 1) = 2
        ! Fix y displacement in n7
        n7 = q * (3 * nxe + 2) + 2 * p - 1
        n_fix = n_fix + 1
        iafix(n_fix, 0) = n7
        iafix(n_fix, 1) = 2
      end do
 
      ! ----------------------------------------------------------------
      ! iangbkfix and angbkfix arrays
      ! ----------------------------------------------------------------
      !!@todo Check about iangbkfixout and angbkfixout.
      !!@todo Check about critical displacements, perhaps there
      !!      should defined two of them.
      ! South west
      q = 1
      p = 1
      n_angbkfix = 0
      do p = 1, nxe
        if (p .le. (crack_tip - 1)) then
	    ! Set n1
          n1 = (q - 1) * (3 * nxe + 2) + 2 * p - 1
	    n_angbkfix = n_angbkfix + 1
	    iangbkfix(n_angbkfix) = n1
	    angbkfix(n_angbkfix)  = 0.0d0
	    ! Set n2
          n2 = (q - 1) * (3 * nxe + 2) + 2 * p
	    n_angbkfix = n_angbkfix + 1
	    iangbkfix(n_angbkfix) = n2
	    angbkfix(n_angbkfix)  = 0.0d0
	  end if
      end do


      ! ----------------------------------------------------------------
      ! iabkfix array
      ! ----------------------------------------------------------------
      n_bkfix = 0

      ! South west corner node (uy, p)
      q = 1
      p = 1
      n_bkfix = n_bkfix + 1
      n1 = (q - 1) * (3 * nxe + 2) + 2 * p - 1
      iabkfix(n_bkfix, 0) = n1
      iabkfix(n_bkfix, 1) = 3
      iabkfix(n_bkfix, 2) = 1
      
      ! South west (p)
      q = 1
      do p = 1, nxe
        if (p .le. (crack_tip - 1)) then
          ! Set n3
          n3 = (q - 1) * (3 * nxe + 2) + 2 * p + 1
          n_bkfix = n_bkfix + 1
          iabkfix(n_bkfix, 0) = n3
          iabkfix(n_bkfix, 1) = 3
          iabkfix(n_bkfix, 2) = 1
        end if
      end do

      ! North
      p = nxe
      ! Fix ux, uy, p in n3, n4
      do q = 1, nye
        n3 = (q - 1) * (3 * nxe + 2) + 2 * p + 1
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n3
        iabkfix(n_bkfix, 1) = 1
        iabkfix(n_bkfix, 2) = 256
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n3
        iabkfix(n_bkfix, 1) = 2
        iabkfix(n_bkfix, 2) = 256
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n3
        iabkfix(n_bkfix, 1) = 3
        iabkfix(n_bkfix, 2) = 256
        n4 = q * (3*nxe + 2) + p - nxe
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n4
        iabkfix(n_bkfix, 1) = 1
        iabkfix(n_bkfix, 2) = 256
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n4
        iabkfix(n_bkfix, 1) = 2
        iabkfix(n_bkfix, 2) = 256
      end do

      ! East
      q = nye
      ! Fix ux, uy, p in n5
      p = nxe
      n5 = q * (3 * nxe + 2) + 2 * p + 1
      n_bkfix = n_bkfix + 1
      iabkfix(n_bkfix, 0) = n5
      iabkfix(n_bkfix, 1) = 1
      iabkfix(n_bkfix, 2) = 256
      n_bkfix = n_bkfix + 1
      iabkfix(n_bkfix, 0) = n5
      iabkfix(n_bkfix, 1) = 2
      iabkfix(n_bkfix, 2) = 256
      n_bkfix = n_bkfix + 1
      iabkfix(n_bkfix, 0) = n5
      iabkfix(n_bkfix, 1) = 3
      iabkfix(n_bkfix, 2) = 256
      ! Fix ux, uy, p in n6, n7
      do p = nxe, 1, -1
        n6 = q * (3 * nxe + 2) + 2 * p
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n6
        iabkfix(n_bkfix, 1) = 1
        iabkfix(n_bkfix, 2) = 256
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n6
        iabkfix(n_bkfix, 1) = 2
        iabkfix(n_bkfix, 2) = 256
        n7 = q * (3 * nxe + 2) + 2 * p - 1
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n7
        iabkfix(n_bkfix, 1) = 1
        iabkfix(n_bkfix, 2) = 256
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n7
        iabkfix(n_bkfix, 1) = 2
        iabkfix(n_bkfix, 2) = 256
        n_bkfix = n_bkfix + 1
        iabkfix(n_bkfix, 0) = n7
        iabkfix(n_bkfix, 1) = 3
        iabkfix(n_bkfix, 2) = 256
      end do

  
      ! ----------------------------------------------------------------
      ! Elements
      ! ----------------------------------------------------------------
      do p = 1, nxe
        do q = 1, nye
          n1 = (q - 1) * (3 * nxe + 2) + 2 * p - 1
          n2 = n1 + 1
          n3 = n2 + 1
          n8 = q * (3 * nxe + 2) + p - nxe - 1
          n4 = n8 + 1
          n7 = q * (3 * nxe + 2) + 2 * p - 1
          n6 = n7 + 1
          n5 = n6 + 1
          m1 = (q - 1) * (nxe + 1) + p
          m2 = m1 + 1
          m4 = m1 + (nxe + 1)
          m3 = m4 + 1
          nodep( p, q, 1) = n7
          nodep( p, q, 2) = n8
          nodep( p, q, 3) = n1
          nodep( p, q, 4) = n2
          nodep( p, q, 5) = n3
          nodep( p, q, 6) = n4
          nodep( p, q, 7) = n5
          nodep( p, q, 8) = n6
          nodep4(p, q, 1) = m4
          nodep4(p, q, 2) = m1
          nodep4(p, q, 3) = m2
          nodep4(p, q, 4) = m3
        end do
      end do

      ! Deload
      n1_deload = 0
      ! West boundary
      q = 1
      do p = 1, crack_tip - 1
        n1_deload = n1_deload + 1
        i1elemx(n1_deload) = p
        i1elemy(n1_deload) = q
        n1 = (q - 1) * (3 * nxe + 2) + 2 * p - 1
        n2 = n1 + 1
        n3 = n2 + 1
        i1elm_deload(n1_deload, 1) = n1
        i1elm_deload(n1_deload, 2) = n2
        i1elm_deload(n1_deload, 3) = n3
        ang1_elm_deload(n1_deload, 1) = 0.0d0
        ang1_elm_deload(n1_deload, 2) = 0.0d0
        ang1_elm_deload(n1_deload, 3) = 0.0d0
      end do

      n2_deload = 0
      ! East boundary
      q = nye
      do p = 1, nxe
        n2_deload = n2_deload + 1
        i2elemx(n2_deload) = p
        i2elemy(n2_deload) = q
        n7 = q * (3 * nxe + 2) + 2 * p - 1
        n6 = q * (3 * nxe + 2) + 2 * p
        n5 = q * (3 * nxe + 2) + 2 * p + 1
        i2elm_deload(n2_deload, 1) = n5
        i2elm_deload(n2_deload, 2) = n6
        i2elm_deload(n2_deload, 3) = n7
        ang2_elm_deload(n2_deload, 1) = pi
        ang2_elm_deload(n2_deload, 2) = pi
        ang2_elm_deload(n2_deload, 3) = pi
      end do
      ! North boundary
      p = nxe
      do q = nye, 1, -1
        n2_deload = n2_deload + 1
        i2elemx(n2_deload) = p
        i2elemy(n2_deload) = q
        n3 = (q - 1) * (3 * nxe + 2) + 2 * p + 1
        n4 = q * (3*nxe + 2) + p - nxe
        n5 = q * (3 * nxe + 2) + 2 * p + 1
        i2elm_deload(n2_deload, 1) = n3
        i2elm_deload(n2_deload, 2) = n4
        i2elm_deload(n2_deload, 3) = n5
        ang2_elm_deload(n2_deload, 1) = 0.5d0 * pi
        ang2_elm_deload(n2_deload, 2) = 0.5d0 * pi
        ang2_elm_deload(n2_deload, 3) = 0.5d0 * pi
      end do

      n3_deload = 0

      !stop
      return
      end subroutine


!!=========================Li==============================
!!=============外边界为井口2、可改变压力===================
!!=========================================================
       subroutine tun51geo(n_angbkfix,iangbkfix,angbkfix,                                  
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *              nn,nye,NYL,nntemp,                                                    
     *              nxe,nye0,spoke,radius,rad2,coord,icoord,                              
     *          coordf, icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                    
                                                                                     
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82),rad2(42)                                               
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	 real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
	 real*8 x_in2(0:100),y_in2(0:100)                                                         
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                  
	 integer iangbkfix(500)                                                                   
	 real*8 angbkfix(500)                                                                     
       integer n_angbkfixout,iangbkfixout(500) 
	                                                                                    
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer i3elemx(80),i3elemy(80),i3elm_deload(80,3)                           
	 real*8 ang3_elm_deload(80,3)                                                          
                                                                                          
C	 COMMON /TMP1/ nf(20000,3)
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	 COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     * i_v4nodes(82,42),i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
c       added by BASHAR to write the geometry information and review
	 real*8 aspect_ratio 
 
	nye=nye0                                                                              
	NYL=nye0+1                                                                               
  	nn=nye0*(3*nxe+2)+(2*nxe+1) ! Total nodes                                          
 	NELEMENT=nxe*nye                                                                    
	nntemp=(nxe+1)*NYL !vertex nodes                                                         
  	nn4=(nye0+1)*(nxe+1) ! Total nodes in one area                                     
! Make vertex nodes index                                                                 
	do i2=1,nxe+1                                                                         
	do i1=1,nye+1                                                                        
		i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                     
	end do                                                                               
	end do                                                                                
                                                                                          
      pi=4.0D0*datan(1.D00)                                                              
      ang=0.5D0*pi/(nxe*2)                                                               
	n_node=0                                                                                 
      do p=0,nxe*2                                                                        
		p_ang=p*ang                                                                             
		x_in(p)=radius(1)*dcos(p_ang)                                                           
		y_in(p)=radius(1)*dsin(p_ang)                                                           
          x_out(p)=radius(nye0+1)*dcos(p_ang)                                                     
	    y_out(p)=radius(nye0+1)*dsin(p_ang)                                                                                                        
	end do                                                                                   
                                                                                          
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0 
	n_angbkfixout=0                                                                            
	do q=1,nye0+1                                                                            
	 f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	 f_out_weight=1.D0-f_in_weight                                                            
	 f2_in_weight=(radius(nye0+1)-(radius(q+1)+radius(q))/2.D0)                               
     *	/(radius(nye0+1)-radius(1))                                                        
	 f2_out_weight=1.D0-f2_in_weight                                                          
		do p=0,nxe ! lines that include vertex nodes                                      
		! Vertex points                                                                         
			n_node=n_node+1                                                                         
			x(n_node)=x_in(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                 
			y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight                                 
                                                                                          
			if(p.EQ.nxe) then                                                                        
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed                                                       
			endif                                                                                    
                                                                                          
			if(p.EQ.0) then                                                                          
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed                                                       
			endif
			
!!===========================li===================
!!外边界固定，位移为0
         		if(q.EQ.nye0+1) then  
			  	n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed 
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed
			endif
!!=================================================                                                                                  
			                                                                                       
			if(q.EQ.1) then                                                                          
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=1 ! hole1                                                             
			endif                                                                                    
			                                                                                       
			if(q.EQ.nye0+1) then                                                                     
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=2 ! outer                                                                                                                                                                                                               
			endif                                                                                    
			                                                                                       
			if(q.EQ.1) then                                                                          
				n_angbkfix=n_angbkfix+1                                                                  
				iangbkfix(n_angbkfix)=n_node                                                             
				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  !包括边节点，故有2*nxe等分         =  Edge nodes, so 2 * nxe decile                 
			endif 
			!!Shunfu.Z 2009.11.11
			if(q.EQ.nye0+1) then                                                                          
				n_angbkfixout=n_angbkfixout+1                                                                  
				iangbkfixout(n_angbkfixout)=n_node                                                             
c				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  ! 包括边节点，故有2*nxe等分   = Edge nodes, so 2 * nxe decile                      
			endif                                                                                   
			                                                                                       
			                                                                                       
			if(p.LT.nxe) then !Middle points                                                        
				n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
		y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight                             
			                                                                                         

!!===========================li===================
!!外边界固定，位移为0
         		if(q.EQ.nye0+1) then  
			  	n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed 
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed
			endif
!!=================================================                                                                                   
			                                                                                       
				if(q.EQ.1) then                                                                          
					n_angbkfix=n_angbkfix+1                                                                  
					iangbkfix(n_angbkfix)=n_node                                                             
					angbkfix(n_angbkfix)=(p+0.5d0)*pi/2.D0/nxe  !包括边节点，故有2*nxe等分   = Edge nodes, so 2 * nxe decile                
				endif
				!!Shunfu.Z 2009.11.11
				if(q.EQ.nye0+1) then                                                                          
					n_angbkfixout=n_angbkfixout+1                                                                  
					iangbkfixout(n_angbkfixout)=n_node                                                             
c				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  ! 包括边节点，故有2*nxe等分 = Edge nodes, so 2 * nxe decile                         
				endif                                                                                     
			endif                                                                                   
		end do                                                                                 
                                                                                          
          if(q.LT.nye0+1) then                                                            
		do p=0,nxe ! Middle edges                                                               
			n_node=n_node+1                                                                        
			x(n_node)=x_in(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                              
			y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight                              
                                                                                          
			if(p.EQ.nxe) then                                                                        
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed                                                       
			endif                                                                                    
			                                                                                       
			if(p.EQ.0) then                                                                          
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed                                                       
			endif                                                                                   	                                                                                       
		end do                                                                                  
		endif                                                                                    
	end do                                                                                   
	continue
	!!Shunfu.Z 2009.12.21  	                                                                                         
	n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0
	!!Shunfu.Z 2009.12.21                                                                                            
      do p=1,nxe                                                                          
	do q=1,nye                                                                             
c	    i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
                                                                                          
		! 4节点网格系统 = Node grid system                                                                        
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
                                                                                          
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is                                                                        
                                                                                          
		if(q.EQ.1) then !inner boundary                                                      
			n1_deload=n1_deload+1                                                                   
			i1elemx(n1_deload)=p                                                                    
			i1elemy(n1_deload)=q                                                                    
			i1elm_deload(n1_deload,1)=ao                                                            
			i1elm_deload(n1_deload,2)=al                                                            
			i1elm_deload(n1_deload,3)=am
	!!Shunfu.Z
			do ij=1,n_angbkfix
				if (iangbkfix(ij).eq.ao) then                                                            
					ang1_elm_deload(n1_deload,1)=angbkfix(ij)
				else if(iangbkfix(ij).eq.al) then                                                         
					ang1_elm_deload(n1_deload,2)=angbkfix(ij)
				else if(iangbkfix(ij).eq.am) then                                                         
					ang1_elm_deload(n1_deload,3)=angbkfix(ij)
				else
				endif
			enddo                                                            
c			ang1_elm_deload(n1_deload,1)=0                                                          
c			ang1_elm_deload(n1_deload,2)=0                                                          
c			ang1_elm_deload(n1_deload,3)=0                                                          
		endif                                                                                    
		if(q.eq.nye0) then ! out                                                                 
			n3_deload=n3_deload+1                                                                   
			i3elemx(n3_deload)=p                                                                    
			i3elemy(n3_deload)=q                                                                    
			i3elm_deload(n3_deload,1)=as                                                            
			i3elm_deload(n3_deload,2)=ar                                                            
			i3elm_deload(n3_deload,3)=aq
	!!Shunfu.Z 2009.11.11
			do ij=1,n_angbkfixout
				if (iangbkfixout(ij).eq.as) then                                                            
					ang3_elm_deload(n3_deload,1)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.ar) then                                                         
					ang3_elm_deload(n3_deload,2)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.aq) then                                                         
					ang3_elm_deload(n3_deload,3)=angbkfix(ij)
				else
				endif	
			enddo
	!!Shunfu.Z 2009.11.11                                                            
c			ang2_elm_deload(n2_deload,1)=0                                                          
c			ang2_elm_deload(n2_deload,2)=0                                                          
c			ang2_elm_deload(n2_deload,3)=0                                                          
		endif                                                                                                                                                                       
	end do                                                                                   
	end do                                                                                   
	continue                                                                                      
       return                                                                             
       end

!!=========================================================
!!=========================================================
      !!单井眼、裂缝
       subroutine tun12geo(is_arc_out,n_angbkfix,iangbkfix,angbkfix,                      
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *              nn,nye,NYL,nntemp,                                                    
     *              nxe,nye0,spoke,radius,coord,icoord,                                   
     *     coordf, icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                         
c                                                                                         
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as,f_node                                               
       real*8 sl,cl,sr,cr,ang,pi                                                          
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(41),radius(82)                                                        
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	 real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
                                                                                          
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                 
	integer iangbkfix(500)                                                                   
	real*8 angbkfix(500)                                                                     
                                                                                          
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)

	integer n_angbkfixout,iangbkfixout(500)                                                            
                                                                                          
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     *i_v4nodes(82,42),                                                                 
     *	i_8_4_nodes(5000),i_4_8_nodes(5000),N_N 

      f_node=11
	nye=nye0                                                                              
	NYL=nye0+1                                                                               
  	nn=nye0*(3*nxe+2)+(2*nxe+1) ! Total nodes                                          
 	NELEMENT=nxe*nye                                                                    
	nntemp=(nxe+1)*NYL !vertex nodes                                                         
  	nn4=(nye0+1)*(nxe+1) ! Total nodes in one area                                     
! Make vertex nodes index                                                                 
	do i2=1,nxe+1                                                                         
	do i1=1,nye+1                                                                        
		i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                     
	end do                                                                               
	end do                                                                                
                                                                                          
      pi=4.0D0*datan(1.D00)                                                              
      ang=0.5D0*pi/(nxe*2)                                                               
	n_node=0                                                                                 
      do p=0,nxe*2                                                                        
		p_ang=p*ang                                                                             
		x_in(p)=radius(1)*dcos(p_ang)                                                           
		y_in(p)=radius(1)*dsin(p_ang)                                                           
                                                                               
		if(is_arc_out.EQ.1) then                                                                
			x_out(p)=radius(nye0+1)*dcos(p_ang)                                                     
			y_out(p)=radius(nye0+1)*dsin(p_ang)                                                     
		else   
			if(p_ang.LT.pi/4.D0) then                                                                                                                        
				x_out(p)=radius(nye0+1)                                                                
				y_out(p)=x_out(p)*dtan(p_ang)
				aa=dtan(p_ang)
				continue                                                          
			else                                                                                    
				y_out(p)=radius(nye0+1)                                                                
				x_out(p)=y_out(p)/dtan(p_ang)
				!!Shunfu.Z 2009.12.31 tan(pi/2)可能会带来问题  =  May cause problems                                                      
			endif                                                                                   
		endif                                                                                   
	end do                                                                                   
                                                                                          
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0 
	n_angbkfixout=0                                                                            
	do q=1,nye0+1                                                                            
	 f_in_weight=(radius(nye0+1)-radius(q))/(radius(nye0+1)-radius(1))                        
	 f_out_weight=1.D0-f_in_weight                                                            
	 f2_in_weight=(radius(nye0+1)-(radius(q+1)+radius(q))/2.D0)                               
     *	/(radius(nye0+1)-radius(1))                                                        
	 f2_out_weight=1.D0-f2_in_weight                                                          
		do p=0,nxe ! lines that include vertex nodes                                      
		! Vertex points                                                                         
			n_node=n_node+1                                                                         
			x(n_node)=x_in(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                 
			y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight

ccccccccccccccccccccccccccccccccccccccccccccccccccc			
cc			if(p.EQ.nxe.and.q.lt.f_node) then
cc		      x(n_node)=x(n_node)+0.03
cc			endif	
ccccccccccccccccccccccccccccccccccccccccccccccccccc                               
!!================================================                                                                                          
			if(p.EQ.nxe.and.q.ge.f_node) then                                                                        
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed                                                       
			endif                                                                                    
!!================================================                                                                                          
			if(p.EQ.0) then                                                                          
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed                                                       
			endif                                                                               
!!===========================li===================
!!外边界固定，位移为0
c         		if(q.EQ.nye0+1) then  
c			  	n_fix=n_fix+1                                                                            
c				iafix(n_fix,0)=n_node                                                                    
c				iafix(n_fix,1)=1 ! x disp is fixed 
c				n_fix=n_fix+1                                                                            
c				iafix(n_fix,0)=n_node                                                                    
c				iafix(n_fix,1)=2 ! y disp is fixed
c			endif
!!=================================================			                                                                                       
			if(q.EQ.1) then                                                                          
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=1 ! hole1                                                             
			endif
!!================================================			
			if(p.EQ.nxe.and.q.le.f_node.and.q.gt.1) then                                                                        
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=1 ! hole1                                                        
			endif			                                                                                    
!!================================================			                                                                                       
			if(q.EQ.nye0+1) then                                                                     
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=256 ! outer                                                                                                                                                     
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=1                                                                     
				iabkfix(n_bkfix,2)=256 ! outer                                                           
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=2                                                                     
				iabkfix(n_bkfix,2)=256 ! outer                                                           
			endif                                                                                    
			                                                                                       
			if(q.EQ.1) then                                                                          
				n_angbkfix=n_angbkfix+1                                                                  
				iangbkfix(n_angbkfix)=n_node                                                             
				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  !包括边节点，故有2*nxe等分  =  Edge nodes, so 2 * nxe decile                 
			endif 
!!================================================			
			if(p.EQ.nxe.and.q.le.f_node) then                                                                        
				n_angbkfix=n_angbkfix+1                                                                  
				iangbkfix(n_angbkfix)=n_node                                                             
				angbkfix(n_angbkfix)=0.0d0                                                      
			endif			                                                                                    
!!================================================
			!!Shunfu.Z 2009.11.11
			if(q.EQ.nye0+1) then                                                                          
				n_angbkfixout=n_angbkfixout+1                                                                  
				iangbkfixout(n_angbkfixout)=n_node                                                             
c				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  ! 包括边节点，故有2*nxe等分   = Edge nodes, so 2 * nxe decile                      
			endif                                                                                   
			                                                                                       
			                                                                                       
			if(p.LT.nxe) then !Middle points                                                        
				n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight                             
		y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight                             
			                                                                                       
				if(q.EQ.nye0+1) then                                                                     
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=1                                                                     
					iabkfix(n_bkfix,2)=256 ! outer                                                           
					n_bkfix=n_bkfix+1                                                                        
					iabkfix(n_bkfix,0)=n_node                                                                
					iabkfix(n_bkfix,1)=2                                                                     
					iabkfix(n_bkfix,2)=256 ! outer                                                           
				endif                                                                                    
!!===========================li===================
!!外边界固定，位移为0
c         		if(q.EQ.nye0+1) then  
c			  	n_fix=n_fix+1                                                                            
c				iafix(n_fix,0)=n_node                                                                    
c				iafix(n_fix,1)=1 ! x disp is fixed 
c				n_fix=n_fix+1                                                                            
c				iafix(n_fix,0)=n_node                                                                    
c				iafix(n_fix,1)=2 ! y disp is fixed
c			endif
!!=================================================  			                                                                                       
				if(q.EQ.1) then                                                                          
					n_angbkfix=n_angbkfix+1                                                                  
					iangbkfix(n_angbkfix)=n_node                                                             
					angbkfix(n_angbkfix)=(p+0.5d0)*pi/2.D0/nxe  !包括边节点，故有2*nxe等分   = Edge nodes, so 2 * nxe decile                
				endif
				!!Shunfu.Z 2009.11.11
				if(q.EQ.nye0+1) then                                                                          
					n_angbkfixout=n_angbkfixout+1                                                                  
					iangbkfixout(n_angbkfixout)=n_node                                                             
c				angbkfix(n_angbkfix)=p*pi/2.D0/nxe  ! 包括边节点，故有2*nxe等分 = Edge nodes, so 2 * nxe decile                         
				endif                                                                                     
			endif                                                                                   
		end do                                                                                 
                                                                                          
          if(q.LT.nye0+1) then                                                            
		do p=0,nxe ! Middle edges                                                               
			n_node=n_node+1                                                                        
			x(n_node)=x_in(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                              
			y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight 
			
ccccccccccccccccccccccccccccccccccccccccccccccccccc			
cc			if(p.EQ.nxe.and.q.le.f_node) then
cc		      x(n_node)=x(n_node)+0.03
cc			endif	
ccccccccccccccccccccccccccccccccccccccccccccccccccc 			                             
!!================================================                                                                                           
			if(p.EQ.nxe.and.q.ge.f_node) then                                                                        
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed                                                       
			endif                                                                                    
!!================================================ 			                                                                                       
			if(p.EQ.0) then                                                                          
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed                                                       
			endif  
!!================================================			
			if(p.EQ.nxe.and.q.le.f_node) then                                                                        
				n_angbkfix=n_angbkfix+1                                                                  
				iangbkfix(n_angbkfix)=n_node                                                             
				angbkfix(n_angbkfix)=0.0d0                                                      
			endif			                                                                                    
!!================================================			                                                                                 	                                                                                       
		end do                                                                                  
		endif                                                                                    
	end do                                                                                   
	continue
	!!Shunfu.Z 2009.12.21  	                                                                                         
	n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0
	!!Shunfu.Z 2009.12.21                                                                                            
      do p=1,nxe                                                                          
	do q=1,nye                                                                             
c	    i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
                                                                                          
		! 4节点网格系统 = Node grid system                                                                        
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
                                                                                          
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is                                                                        
                                                                                          
		if(q.EQ.1) then !inner boundary                                                      
			n1_deload=n1_deload+1                                                                   
			i1elemx(n1_deload)=p                                                                    
			i1elemy(n1_deload)=q                                                                    
			i1elm_deload(n1_deload,1)=ao                                                            
			i1elm_deload(n1_deload,2)=al                                                            
			i1elm_deload(n1_deload,3)=am
	!!Shunfu.Z
			do ij=1,n_angbkfix
				if (iangbkfix(ij).eq.ao) then                                                            
					ang1_elm_deload(n1_deload,1)=angbkfix(ij)
				else if(iangbkfix(ij).eq.al) then                                                         
					ang1_elm_deload(n1_deload,2)=angbkfix(ij)
				else if(iangbkfix(ij).eq.am) then                                                         
					ang1_elm_deload(n1_deload,3)=angbkfix(ij)
				else
				endif
			enddo                                                                                                                     
		endif
!!================================================			
			if(p.EQ.nxe.and.q.le.f_node) then                                                                        
			n1_deload=n1_deload+1                                                                   
			i1elemx(n1_deload)=p                                                                    
			i1elemy(n1_deload)=q                                                                    
			i1elm_deload(n1_deload,1)=am                                                            
			i1elm_deload(n1_deload,2)=ap                                                            
			i1elm_deload(n1_deload,3)=as
              ang1_elm_deload(n1_deload,1)=0.0d0
              ang1_elm_deload(n1_deload,2)=0.0d0
              ang1_elm_deload(n1_deload,3)=0.0d0									                                                     
			endif			                                                                                    
!!================================================		                                                                                    
		if(q.eq.nye0) then ! out                                                                 
			n2_deload=n2_deload+1                                                                   
			i2elemx(n2_deload)=p                                                                    
			i2elemy(n2_deload)=q                                                                    
			i2elm_deload(n2_deload,1)=as                                                            
			i2elm_deload(n2_deload,2)=ar                                                            
			i2elm_deload(n2_deload,3)=aq
	!!Shunfu.Z 2009.11.11
			do ij=1,n_angbkfixout
				if(is_arc_out.eq.1) then
				if (iangbkfixout(ij).eq.as) then                                                            
					ang2_elm_deload(n2_deload,1)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.ar) then                                                         
					ang2_elm_deload(n2_deload,2)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.aq) then                                                         
					ang2_elm_deload(n2_deload,3)=angbkfix(ij)
				else
				endif
				else
				if (iangbkfixout(ij).eq.as) then
					if(p.le.nxe/2) then                                                           
						ang2_elm_deload(n2_deload,1)=0.0d0
					else
						ang2_elm_deload(n2_deload,1)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.ar) then
					if(p.le.nxe/2) then                                                         
						ang2_elm_deload(n2_deload,2)=0.0d0
					else
						ang2_elm_deload(n2_deload,2)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.aq) then                                                         
					if(p.le.nxe/2) then                                                         
						ang2_elm_deload(n2_deload,3)=0.0d0
					else
						ang2_elm_deload(n2_deload,3)=pi/2.0d0
					endif
				else
				endif					
				endif
			enddo                                                          
		endif                                                                                                                                                                       
	end do                                                                                   
	end do                                                                                   
	continue                                                                                      
       return                                                                             
       end 


cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!!==========================Li==========================================!!
!!单井眼、椭圆形井眼                           
       subroutine tun15geo(n_angbkfix,iangbkfix,angbkfix,                                  
     *	 n_fix,iafix,n_bkfix,iabkfix,                                                      
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *             nn,nye,NYL,nntemp,nxe,nye0,spoke,                                                    
     *             radius,radius1,radius2,coord,icoord,                               
     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                   
                                                                                     
c***** This subroutine create a mesh of 8-node quadrilateral        ****                  
c***** plane elements in which coordinate for fluid is included too ****                  
c                                                                                         
c       PARAMETER(INF=309)                                                                
	 Implicit real*8 (a-h, o-z)                                                              
       integer p,q,nxe,nye0,icoord,icordf,g(20)                                     
     *             ,ao,al,am,an,ap,aq,ar,as                                               
       real*8 sl,cl,sr,cr,p_ang,pi                                                        
       real*8 ifacl,ifacr,ofacr,ofacl                                                     
       real*8 spoke(82),radius(82),radius1(82),radius2(82)                                               
       real*8 coord(8,2),coordf(4,2)                                                      
       real*8 x(5000),y(5000)                                                           
	 real*8 x_in(0:100),y_in(0:100),x_out(0:100),y_out(0:100)                                 
	 real*8 x_in2(0:100),y_in2(0:100)                                                         
	 integer nodep(40,80,8),nodeg(40,80,20),nodep4(40,80,4)                                  
                                                                                          
	 integer iafix(500,0:1),iabkfix(500,0:3)                                                  
	 integer iangbkfix(500),iangbkfixout(500)                                                                   
	 real*8 angbkfix(500)                                                                     
                                                                                          
       integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer i3elemx(80),i3elemy(80),i3elm_deload(80,3)                           
	 real*8 ang3_elm_deload(80,3)                                                          
                                                                                          
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)                                                                 
	 COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),                            
     * i_v4nodes(82,42),i_8_4_nodes(5000),i_4_8_nodes(5000),N_N                                            
	 real*8 aspect_ratio,mm1,mm2
      integer :: crack_tip, crack_analysis_type, elemmats,payzone_num,
     &           payzone_node_lower, payzone_node_upper,payzone_node
      common/materials/elemmats(40,80),matprops(2000,6),payzone_node(40)
		
c        end of added by BASHAR
	 call null(ang1_elm_deload,80,3) 
	 call null(ang2_elm_deload,80,3)
	 call null(ang3_elm_deload,80,3)                                         
      
	nye=nye0                                                                              
	NYL=nye0+1                                                                               
  	nn=nye0*(3*nxe+2)+(2*nxe+1) ! Total nodes                                          
 	NELEMENT=nxe*nye                                                                    
	nntemp=(nxe+1)*NYL !vertex nodes
	nn4=nntemp                                                        
! Make vertex nodes index                                                                 
	   do i2=1,nxe+1                                                                         
	    do i1=1,nye+1                                                                        
	         i_vnodes(i1,i2)=2*i2-1+(2*nxe+2+nxe)*(i1-1)                                     
	    end do                                                                               
	   end do                                                                                
                                                                                       
      pi=4.0D0*datan(1.D00)                                                                                                              
	n_node=0                                                                                 
      do p=0,nxe*2
	  if(p.lt.nxe) then                                                                        
		p_ang=p*datan(radius2(nye0+1)/radius1(nye0+1))/nxe
        elseif(p.lt.nxe*2) then
		p_ang=datan(radius2(nye0+1)/radius1(nye0+1))+
     *		(p-nxe)*datan(radius1(nye0+1)/radius2(nye0+1))/nxe
	  else
		p_ang=pi/2.0
	  endif

        if(p_ang.lt.pi/2.0) then				  		  		  		  		  		                                                                      
		x_in(p)=radius1(1)*radius2(1)
     *	      /dsqrt(radius1(1)**2*dtan(p_ang)**2+radius2(1)**2)                                                           
		y_in(p)=x_in(p)*dtan(p_ang)
	  else
	    x_in(p)=0.0
		y_in(p)=radius2(1)
        endif			  	  					  	 
		                                                                                                                                                                                                                                      
	  if(p_ang.LT.datan(radius2(nye0+1)/radius1(nye0+1))) then                                                              
		x_out(p)=radius1(nye0+1)                                                               
		y_out(p)=x_out(p)*dtan(p_ang)                                                          
	  elseif(p_ang.LT.pi/2.0) then                                                           
		y_out(p)=radius2(nye0+1)                                                               
		x_out(p)=y_out(p)*dtan(pi/2.0-p_ang)                                                          
 	  else                                                           
		y_out(p)= radius2(nye0+1)                                                              
		x_out(p)= 0.0d0 			 			 						 			 			 			                                                        
	  endif                                                                                                                                                                    
	end do                                                                                   
                                                                                          
	n_fix=0                                                                                  
	n_bkfix=0                                                                                
	n_angbkfix=0
	n_angbkfixout=0                                                                          
	do q=1,nye0+1 	                                                        
        do p=0,nxe  
	  if(p.le.nxe/2) then
	   f_in_weight=(radius1(nye0+1)-radius1(q))
     *               /(radius1(nye0+1)-radius1(1))                        
	   f_out_weight=1.D0-f_in_weight                                                            
	   else
	   	f_in_weight=(radius2(nye0+1)-radius2(q))
     *                /(radius2(nye0+1)-radius2(1))                        
	    f_out_weight=1.D0-f_in_weight                                                            
	   endif
	   	                                        
		! Vertex points                                                                         
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2)*f_in_weight+x_out(p*2)*f_out_weight                                 
		y(n_node)=y_in(p*2)*f_in_weight+y_out(p*2)*f_out_weight                                 
                                                                                          
!!==========Li=========
        if(p.eq.nxe) then
           n_fix=n_fix+1                                                                            
	     iafix(n_fix,0)=n_node                                                                    
	     iafix(n_fix,1)=1 ! x disp is fixed
	  endif
        if(p.eq.0) then
           n_fix=n_fix+1                                                                            
	     iafix(n_fix,0)=n_node                                                                    
	     iafix(n_fix,1)=2 ! y disp is fixed
	  endif	                                                                                  
			                                                                                       
			if(q.EQ.1) then                                                                          
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=1 ! hole1                                                             
			endif                                                                                    
			                                                                                       
			if(q.EQ.nye0+1) then                                                                     
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=3 ! pressure disp is fixed                                            
				iabkfix(n_bkfix,2)=256 ! outer                                                           
                                                                                          
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=1                                                                     
				iabkfix(n_bkfix,2)=256 ! outer                                                           
				n_bkfix=n_bkfix+1                                                                        
				iabkfix(n_bkfix,0)=n_node                                                                
				iabkfix(n_bkfix,1)=2                                                                     
				iabkfix(n_bkfix,2)=256 ! outer                                                           
			endif                                                                                     
			                                                                                       
	if(q.EQ.1) then                                                                          
	n_angbkfix=n_angbkfix+1                                                                  
	iangbkfix(n_angbkfix)=n_node                                                             
	angbkfix(n_angbkfix)=2.0*p*pi/nxe  ! 包括边节点，故有2*nxe等分   =  Edge nodes, so 2 * nxe decile 
	endif 
	if(q.eq.nye0+1) then
	n_angbkfixout=n_angbkfixout+1
	iangbkfixout(n_angbkfixout)=n_node
	endif                                                                                   		                                                                                       			                                                                                       
		if(p.LT.nxe) then !Middle points                                                        
		n_node=n_node+1                                                                         
		x(n_node)=x_in(p*2+1)*f_in_weight+x_out(p*2+1)*f_out_weight		                                                                                                      
		y(n_node)=y_in(p*2+1)*f_in_weight+y_out(p*2+1)*f_out_weight                             
			                                                                                       
	if(q.EQ.nye0+1) then                                                                     
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=1  
	iabkfix(n_bkfix,2)=256  !!                                                                  
	n_bkfix=n_bkfix+1                                                                        
	iabkfix(n_bkfix,0)=n_node                                                                
	iabkfix(n_bkfix,1)=2 
	iabkfix(n_bkfix,2)=256  !!                                                                  
	endif  
	                                                                                                                                      
			                                                                                       
	if(q.EQ.1) then                                                                          
	n_angbkfix=n_angbkfix+1                                                                  
	iangbkfix(n_angbkfix)=n_node                                                             
	angbkfix(n_angbkfix)=2.0*(p+0.5d0)*pi/nxe !包括边节点，故有2*nxe等 = Including the edge nodes, so 2 * nxe, etc.
	endif
	if(q.eq.nye0+1) then
	n_angbkfixout=n_angbkfixout+1
	iangbkfixout(n_angbkfixout)=n_node
	endif                                                                                                                                                                       
		endif                                                                                   
	  end do                                                                                 
                                                                                          
        if(q.LT.nye0+1) then                                                            
        do p=0,nxe ! Middle edges
	  if(p.le.nxe/2) then                                                           
	   f2_in_weight=(radius1(nye0+1)-(radius1(q+1)+radius1(q))/2.D0)                               
     *	 /(radius1(nye0+1)-radius1(1))                                                        
	   f2_out_weight=1.D0-f2_in_weight 
	   else                                                          
	    f2_in_weight=(radius2(nye0+1)-(radius2(q+1)+radius2(q))/2.D0)                               
     *	  /(radius2(nye0+1)-radius2(1))                                                        
	    f2_out_weight=1.D0-f2_in_weight
	   endif 		
	   		                                                               
			n_node=n_node+1                                                                        
			x(n_node)=x_in(p*2)*f2_in_weight+x_out(p*2)*f2_out_weight                             
			y(n_node)=y_in(p*2)*f2_in_weight+y_out(p*2)*f2_out_weight                              
    
			if(p.EQ.nxe) then                                                                        
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=1 ! x disp is fixed                                                       
			endif                                                                                    
			                                                                                       
			if(p.EQ.0) then                                                                          
				n_fix=n_fix+1                                                                            
				iafix(n_fix,0)=n_node                                                                    
				iafix(n_fix,1)=2 ! y disp is fixed                                                       
			endif 
	                                                                                            	                                                                                  			                                                                                       
		end do                                                                                  
	endif                                                                                    
	end do
	!!Shunfu.Z 2009.12.21                                                                                  
      n1_deload=0                                                                              
	n3_deload=0                                                                              
	n2_deload=0 
	!!Shunfu.Z 2009.12.21                                                                                     
      do p=1,nxe                                                                          
	do q=1,nye0                                                                              
	      i1= 6                                                                              
		ao=(q-1)*(3*nxe+2)+2*p-1                                                                
		al=ao+1                                                                                 
		am=al+1                                                                                 
		an=q*(3*nxe+2)+p-nxe-1                                                                  
		ap=an+1                                                                                 
		aq=q*(3*nxe+2)+2*p-1                                                                    
		ar=aq+1                                                                                 
		as=ar+1                                                                                 
 		! 4节点网格系统                                                                         
		Io = (Q - 1) * (NXE + 1) + P                                                            
		Im = Io + 1                                                                             
		Iq = Io + (NXE + 1)                                                                     
		Is = Iq + 1                                                                             
                                                                                          
		nodep(p,q,1)=aq                                                                         
		nodep(p,q,2)=an                                                                         
		nodep(p,q,3)=ao                                                                         
		nodep(p,q,4)=al                                                                         
		nodep(p,q,5)=am                                                                         
		nodep(p,q,6)=ap                                                                         
		nodep(p,q,7)=as                                                                         
		nodep(p,q,8)=ar                                                                         
                                                                                          
		nodep4(p,q,1)=Iq                                                                        
		nodep4(p,q,2)=Io                                                                        
		nodep4(p,q,3)=Im                                                                        
		nodep4(p,q,4)=Is                                                                         
 
         elemmats(p,q)=1 
c         if(p.eq.8) then
c           if(q.ge.2.and.q.le.12) then
c		   elemmats(p,q)=2 
c		 endif
c	   endif             
                                                                                                 
	if(q.EQ.1) then !hole 1 or hole 2                                                       
		n1_deload=n1_deload+1                                                                   
		i1elemx(n1_deload)=p                                                                    
		i1elemy(n1_deload)=q                                                                    
		i1elm_deload(n1_deload,1)=ao                                                            
		i1elm_deload(n1_deload,2)=al                                                            
		i1elm_deload(n1_deload,3)=am 
		do ij=1,n_angbkfix
			if (iangbkfix(ij).eq.ao) then                                                            
				ang1_elm_deload(n1_deload,1)=angbkfix(ij)
			else if(iangbkfix(ij).eq.al) then                                                         
				ang1_elm_deload(n1_deload,2)=angbkfix(ij)
			else if(iangbkfix(ij).eq.am) then                                                         
				ang1_elm_deload(n1_deload,3)=angbkfix(ij)
			else
			endif
		enddo			                                                                                                                     
	endif                                                                                    
	if(q.eq.nye0) then ! out                                                                 
		n2_deload=n2_deload+1                                                                   
		i2elemx(n2_deload)=p                                                                    
		i2elemy(n2_deload)=q                                                                    
		i2elm_deload(n2_deload,1)=as                                                            
		i2elm_deload(n2_deload,2)=ar                                                            
		i2elm_deload(n2_deload,3)=aq
 			do ij=1,n_angbkfixout
				if(is_arc_out.eq.1) then
				if (iangbkfixout(ij).eq.as) then                                                            
					ang2_elm_deload(n2_deload,1)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.ar) then                                                         
					ang2_elm_deload(n2_deload,2)=angbkfix(ij)
				else if(iangbkfixout(ij).eq.aq) then                                                         
					ang2_elm_deload(n2_deload,3)=angbkfix(ij)
				else
				endif
				else
				if (iangbkfixout(ij).eq.as) then
					if(p.le.nxe/2) then                                                           
						ang2_elm_deload(n2_deload,1)=0.0d0
					else
						ang2_elm_deload(n2_deload,1)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.ar) then
					if(p.le.nxe/2) then                                                         
						ang2_elm_deload(n2_deload,2)=0.0d0
					else
						ang2_elm_deload(n2_deload,2)=pi/2.0d0
					endif
				else if(iangbkfixout(ij).eq.aq) then                                                         
					if(p.le.nxe/2) then                                                         
						ang2_elm_deload(n2_deload,3)=0.0d0
					else
						ang2_elm_deload(n2_deload,3)=pi/2.0d0
					endif
				else
				endif					
				endif
			enddo                                                          
		endif                                                                                                                                                                       
	end do                                                                                   
	end do                                                                                   
	continue                                                                                      
       return                                                                             
       end


