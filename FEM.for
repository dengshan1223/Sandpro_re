	SUBROUTINE FEM(is_meshing,OPTMESH1)
      Implicit real*8 (a-h, o-z)
	CHARACTER*256 OPTMESH, OPTMESH1
      REAL*8 RKF(5000,20)
      integer p,q,r,cdmax,dof,steps,tot,band,h,t,gp,g(20),nk(3000),
     &        dt(200),sandg(40,80,4),nelem(40,80,4),g2(20),number
      real*8 det,kxp,E,v,dtim,quot,theta,hards,ou,dee(4,4)
     & ,samp(3,2),coord(8,2),jac(2,2),jac1(2,2),timenow
c     & ,derivt(4,2),poro(40,80,4),kay(40,80,4,2),der(2,8),deriv(2,8)
     & ,derivt(4,2),der(2,8),deriv(2,8),derivt2(4,2)
     & ,kay1(2,2),qcom1(2,2),kderiv(2,4),bee(4,16),dbee(4,16),bt(16,4)
     & ,btdb(16,16),dtkd(4,4),km(16,16),kp(4,4),ke(20,20),kd(20,20)
     & ,fun(8),funf(4),coordf(4,2),derf(2,4),derivf(2,4),tforce(5000)
     & ,c(16,4),volf(16,4),eldf,eldfp,mc(3,2),fmderiv(3,4),eld(20)
     & ,vol(16),eps(4),sigma(4),tbdyld(5000),dpl(4,4),eload(16)
     & ,bload(16),tload(5000),dloads(5000),bdylds(5000),oldlds(5000)
c     &,sx(40,80,4),EN(40,80,4),sy(40,80,4),sz(40,80,4),sxy(40,80,4)
     &,EN(40,80,4),diff(900),derf2(2,4),derivf2(2,4),funf2(4)
     & ,pl(4,4),elso(4),delso(4),volf2(16,4),c2(16,4)
c     & ,ex(40,80,4),ey(40,80,4),ez(40,80,4),exy(40,80,4),diff(900)
c     & ,er(40,80,4),sr(40,80,4),pl(4,4),elso(4),delso(4),et(40,80,4)
c     & ,st(40,80,4),SOI,SWI,SWWC,SKRO,SKRW,volstr(40,80,4)
c     & ,st(40,80,4),SOI,SWI,SWWC,SKRO,SKRW
     & ,SOI,SWI,SWWC,SKRO,SKRW
     & ,OUTOIL,OUTWAT,bhp,sh3,mud,np,coh,COHI,COEFR,BETA,COEFP,PS0,KR2
     & ,SM,S0MAX,KR1,P0,SHV,loads(40000),ans(10000),pb(5000,5000)
     & ,bk(800000),dump(5000),dumu(5000),loads0(10000)
c     & ,dump(20000),dumu(20000),loads0(40000)
     & ,dumux(5000),dumuY(5000),dumut(5000),dumsteam(5000)
     & ,force(10000),x(5000),Y(5000),prelds(10000),qcom(40,80,4)
     & ,qderiv(2,8),dtkq(4,4),PNOW,RSO0,kxy(40,80,4,2)
c	1 ,seld(4),RSOO(40,80),krw(40,80,4,2),kro(40,80,4,2)
	1 ,seld(4),RSOO(40,80),volstr0(40,80,4)
	 real*8 poron(40,80,4)
cc	 character*4096 T_OUT

	real*8 dumpm(5000),dumpm0(5000),dumpf0(5000),kxym(40,80,4,2)
     1       ,K_M(40,80,4,2),a_km(2,2),dump_1(20),dump_0(20)
	1       ,porom(40,80,4),force_t1(10000),force_t0(10000)
	1       ,tran0(20,20),tran1(20,20),dtkm(4,4),kderivm(2,4)
	1       ,load_t1(20),load_t0(20),dumpm1(5000),dumpm11(5000)
	1       ,kaym(40,80,4,2),krom(40,80,4,2),krwm(40,80,4,2)

c	integer lq(50),Rpold(50),iq(50),nodep(40,80,8),nodeg(40,80,20)
c	integer lq(50),Rpold(50),iq(50),nodeg(40,80,20)
	integer lq(100),Rpold(100),iq(100),nodeg(40,80,20)
c******* nsandi for rescording the time of sanding for each cohesion
c******* npelasti for rescording the time of yielding for each cohesion
	integer mcc(1000,4),nsandi(10001),npelasti(10001)
      integer problemtype,modeltype,damagetype,geometrytype,ctype
     & ,completiontype,reservoirtype,failuretype,stresstype,filtercake
c
      data ijac,ijac1,ikay,ider,ideriv,ikderv,T,iderf,idervf/9*2/
      data isamp,nodof/2*3/
c********* idee,ibee,idbee should be 3 in plan stress and plan strain *************
      data idee,ibee,idbee/3*3/
      data icordf,idtkd,ikp,nodf/4*4/
      data icoord,idervt,nod/3*8/
      data ike,ikd,tot/3*20/
      data ibt,ikm,ic,ivolf,dof,ibtdb/6*16/
      data ipl,idpl/2*4/
C	COMMON/TMP1/nf(20000,3)
	COMMON /TMP1/ nf(5000,3), nfT(5000,1)
c      COMMON/INITIAL/shmaxg,shming,szg,pbg,rw,pw,z,rb
	COMMON/INITIAL/shmaxg,shming,szg,pbg,rw,rw2,pw,z,rb,pbgm,alpha_t
      COMMON/GEOMECH/E,v,cou,col,phi,csco,psi,Biot,Biotp,cs,Ts,Biotm
      COMMON/RESERVIOR/PO(20),RSO(20),RKE_X,RKE_Y,rkp,VIS_O,VIS_W,qf,bm,
     &             COMP_O,COMP_W,porosity,porey,BUBP,NUMO,RKEm_X,RKEm_Y
      COMMON/SUBNONL/Sc,Sv,CM,Qc,AP,AB,AL,am,ai
      COMMON/SUBFIELD/TIMEF,vs,pores,wks,CT,WD,Frac,S0,
     &                C_DISPLACEMENT,C_DSP2
      COMMON/SUBINITIAL/sigmah,pa,af,az,amhs,mud,pd,pay,pradius,packerl
      COMMON/SUBRESER/w0,ww,ak,BB,vu
      COMMON/OPTNUMERI1/NT,NX,NLOAD
      COMMON/OPTNUMERI2/dr,T1,Dtim
c      COMMON/OPTFEM1/NXE,NYE,NN,N,NW,GP,NL,Its
	COMMON/OPTFEM1/NXE,NYE,NYE0,nyl,NN,NN4,N,NW,GP,NL,Its,nntemp,ntemp
      COMMON/OPTFEM2/THETA,TOL,length,wideth
      COMMON/DATAIT/nxN, It
      COMMON/DATA/PHI0,PHIMAX,SK0,SKC,YS,YF,UF,C0,SNMD,
     &            Err,YA0,WORM_PHI(1000),
     &      WORM_SK(1000),WORM_C(1000),
     &      WORM_YA(1000),WORM_X(1000),WORM_T_PHI(1000),
     &      WORM_T_SK(1000),WORM_T_C(1000),WORM_Q(1000),
     &      WORM_TMPC(1000)
      COMMON/OPTIONS/problemtype,modeltype,damagetype,geometrytype,
     *                completiontype,reservoirtype,failuretype,ctype,
     *                stresstype,filtercake,imethod,iflowtype,iproblem
      COMMON/PHASE2/SKROT(20),SKRWT(20),SWWC,SWT(20),SO(40,80,4),
     &           SW(40,80,4),SG(40,80,4),NUMOW,tt(40,80,4),dd(40,80,4)
      COMMON/VAR_ADD1/COEFR,BETA,COEFP,PS0,SM,S0MAX,P0,SHV,pm0
c*******added data******************************************************
c	COMMON/LOAD1/radius(101),pofix(1000),point(1000,2),gtheta,
	COMMON/LOAD1/radius(82),rad2(42),pofix(1000),point(1000,2),gtheta,
	1gravy,pressa(1000,6),spoke(41),dofix(1000,3),radius1(82)
     1,radius2(82),rad21(42),rad22(42)
	COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)
	1,lodpt(1000),nbound,ndrill,ndirect
	COMMON/flow/flowqi,flowqo
	COMMON/flowc/nflowi,nflowo
c	COMMON/loads1/pdate(1000),pdelta(1000)
	COMMON/loads1/pdate(1000),pdelta(1000),PDLTA2(1000)
	COMMON/loads2/npoint,npr(1000),NPR2(1000),nstep(1000)
c	COMMON/loads2/npoint,npr(1000),nstep(1000)
	!!Added
c	integer nodep4(40,80,4)
	COMMON/BC/n_bkfix,iabkfix(500,0:3)
	!added by Shunfu.Z
c	real*8 DUMP_OLD(20000),volst0(40,80,4)
	real*8 DUMP_OLD(20000) 
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),
     *i_v4nodes(82,42),
     *	i_8_4_nodes(5000),i_4_8_nodes(5000),N_N
      real*8 kaynow,kaynowo
c	integer i_8_4_nodes(1000),i_4_8_nodes(1000)
	real*8 a_sat(10000),A_SAT0(10000),a_sat8(10000),a_sat_new(10000) 
	 integer iafix(500,0:1),ia_t_fix(500,0:3)
	real*8 force1(10000),a_sat_f2(10000)
c	COMMON/BC/n_bkfix,iabkfix(500,0:3)
	integer iangbkfix(500)
	real*8 angbkfix(500)
	integer lq2(50),Rpold2(50),iq2(50)

      integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	 real*8 ang1_elm_deload(80,3)                                                           
       integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	 real*8 ang2_elm_deload(80,3)
	 integer i3elemx(80),i3elemy(80),i3elm_deload(80,3)                           
	 real*8 ang3_elm_deload(80,3) 

	dimension yielding_P(50),sanding_P(50),plastic_P(50),t_P(50),                       
	1          coh_P(50)
	real*8 st0(2000),st1(2000),st11(2000)
	real*8 PW_GRAD(40,80,4,2),PO_GRAD(40,80,4,2)
	real*8 Q_W(2),Q_O(2) ! HOLE1 & HOLE2 

	real*8 ddm(4,16),shap8(16,2),ddm1(4,16)
c	real*8 srzsf(40,80,4),stzsf(40,80,4),taozsf(40,80,4)
	real*8 srzsf(40,80,4),stzsf(40,80,4)
	integer IP_gauss(4)
	real*8 velocit(2,10000),v_water(2,10000),v_oil(2,10000)
	 REAL*8 SWZSF(82,42)
	!Shundu.Z
	real*8 bkstore(800000)
	real*8 bkreac(800000),forcereac(5000),NEWLO(5000)
c	real*8 reaction(40000),forceall(40000)
c	real*8 ansreac(40000),loadsreac(40000)
c	COMMON /TMPreac/ nfreac(8000,3)
c	integer nodegreac(40,80,20),greac(20)
c	real*8 permeabilitY(20000,3),waterflux(20000)
	integer nodep,nodep4
	real*8 kay,kro,krw,poro,volstr,volst0
C	real*8 sz(40,80,4)
 	common /elementinf/ nodep(40,80,8),nodep4(40,80,4),
     $kay(40,80,4,2),kro(40,80,4,2),krw(40,80,4,2),
     $poro(40,80,4),volstr(40,80,4),volst0(40,80,4)
	common /stresscal/sx(40,80,4),sy(40,80,4),sxy(40,80,4),
     $sz(40,80,4),sr(40,80,4),st(40,80,4),
C     $taozsf(40,80,4),sr(40,80,4),st(40,80,4),
     $ex(40,80,4),ey(40,80,4),exy(40,80,4),
     $ez(40,80,4),er(40,80,4),et(40,80,4)
C======================================================================                   
C Temperature from kevin                                                                  
	COMMON/temperature/COMP_B,COMP_S,alfaT,COMP_WT,COMP_OT,                                  
     &      VAL0,Tiner,Tiner2,Tout,Num_T_V                                                
      COMMON/vistemp/vis_temp(50,3)
C	REAL*8 FORCEt1(40000), FORCEt0(40000),ELDT(20)                                                       
C====================================================================== 
C============================================================================             
c      following variables added by Kevin for coupled temproal effect 2006-05-08          
	REAL*8 KdT0(20,20),KdT1(20,20),                                                          
     &Tloads0(10000),Tloads1(10000),qcomt(40,80,4),dtkqT(4,4),                           
     &FORCEt1(10000), FORCEt0(10000),dloadst(10000),eldt(20),eldft,                       
     &temp_1(20), temp_0(20),eload1(20),eload0(20)
      real*8 dtkqa(4,4),dtkqta(4,4)                                       
      Real*8 Tin,Tin2,Tou,temp_element 
	REAL*8 A_KT(2,2) 
	COMMON/GAS/GASP0 
	real*8  pay,pradius,packerl,tderiv(2,4)
	real*8 qwater(2),qoil(2)
	integer nmesh(0:1,0:1) 
	integer*1 IS_CHAMBER(20000)
	integer  NNSTEP
	real*8 PERMX,PERMY, tsdrill0,tsdrill
	COMMON /STEPINF/ PERMX,PERMY,tsdrill0,tsdrill,NNSTEP
	common /iconsider/igrav
	real*8 fracwater(20000)
c	 real*8 forcezsf(40000)                                                  
!	INTEGER nodep4(40,80,4),nodep4(40,80,4)                                                 
C      end definition kevin                                                               
C============================================================================ 
c	real*8 bk(40000,851),work(40000,851)
c	real*8 bk(40000,851),work(851,40000)
c*****setting ntotal to record total load step for porepressure or rate
!! IP_gauss为相应gauss点对应的节点号  (The corresponding node number for the corresponding gauss point)
c=======================================================================================================================
c=======================================================================================================================
C   VARIABLES BY BASHAR
	CHARACTER*256 fname, fnumber
	integer i4,i8,maxpnode
	REAL*8 C_PRIME(40,80,4),ALFA_F,SKEMPTON, V_U
	!REMOVE
	real*8 maxt,mint,meant,dumt(5000),dumsw(5000)
	real*8 maxp,minp,meanp,maxperm,minperm,meanperm
	integer current_time(3),Tcase_number
	character*256 Tcase_text,Tcase_fname,f106,f105,f107,f108
	integer iii,smn !smn=stress_mat_number
c	real*8 VEL_st2(40,80,4,2),VEL_st3(40,80,9,2)
	real*8 rho_o,rho_w,rho_st,rho,rho1     
	real*8 PHI_GP2(40,80,4,2) !PHI_GP3(40,80,9,2),
	real*8 PSI_GP2(40,80,4,2),vscale !PSI_GP3(40,80,9,2)
	real*8 Kr1_GP2(40,80,4) !,Kr1_GP3(40,80,9)
	real*8 Kr2_GP2(40,80,4) !,Kr2_GP3(40,80,9)
c	real*8 Kr1_nodal(2000),vis1_nodal(2000)
c	real*8 Kr2_nodal(2000),vis2_nodal(2000)
	real*8 gxcom,gyxom
	real*8 VEL_w2(40,80,4,2) !,VEL_w3(40,80,9,2)
	real*8 h_coeff,gamma
c	real*8 st1r(2000),st1s(2000),SRT(2000)
	real*8 VEL_o2(40,80,4,2) !,VEL_o3(40,80,9,2)
	real*8 maxu,minu,meanu,maxv,minv,meanv
	real*8 maxeps,minesp,meaneps,maxqcom,minqcom,meanqcom
	real*8 maxqcomt,minqcomt,meanqcomt
	real*8 maxkay11,minkay11,meankay11,maxkay22,minkay22,meankay22
	integer ipp,iqq
	real*8 pmax_node_sw,pmax_pressure
	integer pmax_node
	real*8 vis_w_mat(40,80),vis_o_mat(40,80),sw0
	real*8 dkw_dsT(20),d2kw_ds2T(20),dkw_ds,dko_ds
	real*8 dkO_dsT(20),d2kO_ds2T(20)
	integer iterator,n_iterator
	real*8 dump_mat(200,10000),dumsw_mat(200,10000),dumt_mat(200,10000)
	REAL*8 TIME_MAT(2000)
	integer v_inodes(10000,2),if_qcom,if_qcomt,if_kr_wells
	integer if_sat45_orig,if_qcomt_new,if_const_kr
	integer radial_nodes(100),n_radial_nodes,radial_ns,radial_GP(200,3)
	integer tangential_nodes(100),n_tangential_nodes,tangential_ns
	real*8 sw1,SKRW1,SKRO1,dkw_ds1,dko_ds1,pcpie1
	real*8 uwra1,uwra2,uwra,DUMP_G(4)
	integer if_ups_krw,amm,ann,aii,a1,a2,nodeid4(4),if_extrapolate2
	real*8 A_KW3(2,2),A_KW4(2,2),kay2(2,2),lambdaw,lambdao
	real*8 lambda_wm,lambda_wn,lambda_om,lambda_on,alpha,abs_alpha
	real*8 A_SATP(20000),A_SATPP(20000),dtmp,A_SATPF2(20000)
	integer nns,if_simple_ups_per2,it_total,nsub,if_auto_iter
	integer if_finish2,iterator2,if_sat_pass
	real*8 rnns,dt_min,A_C_GLOBE2(400,400)
	real*8 A_SAT_newim1(20000),dumpim1(20000),errp,errsw,errpmax,errswmax
	real*8 kQCOM,kQCOMT,W_t,sat_pass_value,radius_GP(200),SWA,SWB
	REAL*8 FW,DFW_DS,krw1,kro1,kay11,fff1,fff2,fff3,rad_i,time_sagd_prod
	integer radial_nodes2(100),radial_nodes3(100),radial_nodes4(100)
	real*8 KAY_T_SW(200,200),SLWO_T_SW(200,200),p11,p1a,p1b
	character*256 txt11
	real*8 r1A,r1B,r11,T1A,T1B,T11,n11,T11cond,T11test,T_pass_value
	integer if_T_pass,if_pass_body_load,if_GRAV_BC_pass
	real*8 qw_mat(40,80),qo_mat(40,80),qt_mat(40,80),p_element(4)
	real*8 qwx,qwy,qox,qoy,qtx,qty,qwr,qor,qtr,p_grad(2),x_gp,y_gp
	real*8 radial_qw(100),radial_qo(100),radial_qt(100)
	real*8 a_sat_dummy(1000),aaa,steam_vis,ktheta
	integer if_record_dumt,if_record_dump,if_record_dumsw,if_steam_mu
	integer if_record_dum,if_record_vel,if_record_SR,if_record_SX
	integer if_grav_wellbore,if_boussinesq,if_jump_ns_1_20,if_p_pass
	integer if_Q1,if_Q2,if_Qout
	real*8 Q1,Q2,Qout,srr

      real*8 e_L, p_L, V_L,fra_den,coff1,coff2,matprops
	integer k_s,fra_num,itype,elemmats
      COMMON /FOTIS/ e_L, p_L, V_L,fra_num,fra_den,coff1,coff2,k_s,itype
      common/materials/elemmats(40,80),matprops(2000,6)
	real*8 coeff_c, coeff_cT

	if_auto_iter=0
	n_iterator=1
	errpmax=1.0e-5
	errswmax=1.0e-4

	if_qcom=1
	if_qcomt=1
	if_qcomt_new=0
	kQCOM=1.0
	kQCOMT=1.0

	if_kr_wells=2
	if_sat45_orig=2 !0 for SAT_NEW45_v8, 2 for SAT_NEW5, 3 for SAT_NEW45_ZMA and 4 for SAT_OIL_ZMA, 5 for SAT_NEW45_2013A, 6 for SAT_NEW56

	if_const_kr=0   !*****************
	if_simple_ups_per2=1   !*****************

	if_sat_pass=1 !if 1, the saturation solvers are bypassed and saturation would everywhere equal to sat_pass_value below
	sat_pass_value=1.0

	uwra1=0.0 !do not use uwra1=uwra2 non-zero.
	uwra2=0.0
	uwra=abs(uwra1)+abs(uwra2)
	if_ups_krw=0
	abs_alpha=0.0

c	BIOT=0.0
c	alfaT=0.0
c	SZG=0.0
c	ngrav=0.0
c	igrav=0.0
c	P0=5.0
	
	
	if_extrapolate2=0
	time_sagd_prod=1000.0
	
	if_T_pass=1 !if 1, the temperature_c is bypassed and temperature would everywhere equal to T_pass_value below 
	T_pass_value=10

	if_pass_body_load=1 !if 1, the solid_body_load and fluid_body_load are bypassed
	if_GRAV_BC_pass=1 !if 1, the garvity boundary conditions are not applied for the pressure
	rho=1000.0 !density
	if_grav_wellbore=1 !makes the pressure around well bore changing with depth but the average is the same as input
	if_boussinesq=1 !applying the Boussinesq approximation, rho=eho1*(1-alfaF*(T-T0))
	if (if_boussinesq.eq.1) if_auto_iter=1

	ALFA_F=1E-4 !the effective expansion coefficient of all fluid phases.
	SKEMPTON=0.89 !Skempton's constant, "B" in Biot's theory
	V_U=0.33 !nu_u, the undrained poisson ratio
	
	if_p_pass=0 !always keep it 0
	if_steam_mu=1 !consider different vis for steam, if T>265 the water is steam
	steam_vis=0.1

	if_q1=0 !apply type 2 bounadry condition (energy equation) around the production well, the heat flux is q1*rho*C (See YArlong 2003 paper)
	q1=250.0

	if_q2=0 !for injection well, type BC 
	q2=0.0

	if_Qout=0
	Qout=0.0

	if_jump_ns_1_20=0 !if 1 ns1 to ns=20 are bypassed
	if_record_dumsw=0 !if 1 means recording saturation 
	if_record_dumt=0
	if_record_dump=0
	if_record_dum=0 !recording other parameters such as permeability, viscosity and etc.
	if_record_vel=0
	if_record_SR=0
	if_record_SX=0
	vscale=1.0 !the scale for velocity recorder. Only the avergae velocity is recorded
	number=0 
c==============================================================
	if (if_auto_iter.eq.0) 	n_iterator=1
	if (if_sat_pass.gt.1) if_kr_wells=0
	if (if_sat_pass.gt.1) WW=sat_pass_value
	if (if_sat_pass.gt.1) if_const_kr=0
c==============================================================

	nns=0
	dt_min=10000.0

	call dy_dxT_maker(SWT,SKRWT,NUMOW,dkw_dsT)	
	call dy_dxT_maker(SWT,dkw_dsT,NUMOW,d2kw_ds2T)
	call dy_dxT_maker(SWT,SKROT,NUMOW,dkO_dsT)	
	call dy_dxT_maker(SWT,dkO_dsT,NUMOW,d2kO_ds2T)
	

	W_t=0.0
	smn=0;
	call itime(current_time)
	Tcase_number=current_time(1)*10000+current_time(2)*100
     1	+current_time(3)

C   END OF VARIABLES BY BASHAR
      time_cal=0.0d0
	IP_gauss(1)=3
	IP_gauss(2)=2
	IP_gauss(3)=4
	IP_gauss(4)=1
	nmesh(0,0)=1
	nmesh(1,0)=2
	nmesh(1,1)=3
	nmesh(0,1)=4
*****************************************************************************
	!modified by Bashar, the original:
c	if (ngrav.eq.0) then
c		Igrav=0
c	endif
	!the new version:
	if (ngrav.eq.0.or.ngrav.eq.0) then
		Igrav=0
		ngrav=0
	endif
c end of modification by Bashar
**************************************************************************
	if(geometrytype.eq.2) then
		igrav=1
!		ngrav=1
	endif	

	if(ndrill.eq.0) then
		TIMEF=TIMEF+NNSTEP*tsdrill
	else
		TIMEF=TIMEF+tsdrill
	end if
c
	d0 = C_DISPLACEMENT
	if (IMESH.ge.4) d02=C_DSP2  !second well
	sz0 = SHV
c******when horizental well and first principle stress in x-direction
c       sh1=ps0-s0; sh3=ps0+s0
c       if(ndirect.ne.1) then
      sh1 = PS0 + S0
      sh3 = PS0 - S0
      PI  = 4.D0*DATAN(1.D0)
      sq3 = dsqrt(3.0D0)
	IDERROR = 0

	dangle = 1.0D0
	ID = problemtype
      phi = phi*pi/180.D0
      psi = psi*pi/180.D0
c
c   Convert read in permeability and viscosity in units of MD and cP into m^2/MPa Day
c   As stress and time are MPa and Day, respectively.
c
c--------------------------------------------------------------------------------------
c***********************************************************************
c                         Geomtrical Input
c***********************************************************************
      open(unit=102,file='test_window.txt')
	nye0=nye
	rad2(1)=radius(1)*rw2
	rad21(1)=radius(1)*rw2
	rad22(1)=radius(1)*rw2
      do ii=1,nye+1
         radius(ii) = radius(ii)*rw
	   radius1(ii) = radius1(ii)*rw
	   radius2(ii) = radius2(ii)*rw
	end do
 	if ((imesh.eq.4).or.(imesh.eq.5)) then                                                                                                               
		do ii=1,nye+1                                                                       
		rad2(ii)=(rad2(1)-radius(nye+1))*(radius(ii)-radius(nye+1))                             
     *	 /(radius(1)-radius(nye+1))+radius(nye+1)
		rad21(ii)=(rad21(1)-radius1(nye+1))*(radius1(ii)-radius1(nye+1))                             
     *	 /(radius1(1)-radius1(nye+1))+radius1(nye+1)
		rad22(ii)=(rad22(1)-radius2(nye+1))*(radius2(ii)-radius2(nye+1))                             
     *	 /(radius2(1)-radius2(nye+1))+radius2(nye+1)                                          
		end do                                                                                                                                                                     
		ii1=nye+1 
	    do ii=nye+1,1,-1                                                                    
			ii1=ii1+1                                                                               
			radius(ii1)  = radius(nye+1)+rad2(nye+1)-rad2(ii)  
			radius1(ii1)  = radius1(nye+1)+rad21(nye+1)-rad21(ii)
			radius2(ii1)  = radius2(nye+1)+rad22(nye+1)-rad22(ii)                                     
		end do                                                                               
 	endif !(imesh.ge.4)
	write(102,*),"radius="
	write(102,*),radius
	write(102,*),"radius1="
	write(102,*),radius1
	write(102,*),"radius2="
	write(102,*),radius2
	write(102,*),"rad2="
	write(102,*),rad2 
	write(102,*),"rad21="
	write(102,*),rad21
	write(102,*),"rad22="
	write(102,*),rad22
	
	m = nxe+1   
	if(GEOMETRYTYPE.eq.1) then !! Plain strain  
	IF (IMESH.EQ.5) THEN 
	      call tun5geo(n_angbkfix,iangbkfix,angbkfix,                                     
     * n_fix,iafix,n_bkfix,iabkfix,                                                       
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *             nn,nye,NYL,nntemp,                                                   
     *             nxe,nye0,spoke,radius,rad2,coord,icoord,                               
     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)                                                               
c          call tun55geo(n_angbkfix,iangbkfix,angbkfix,                                     
c     * n_fix,iafix,n_bkfix,iabkfix,                                                       
c     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
c     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
c     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
c     *             nn,nye,NYL,nntemp,nxe,nye0,spoke,                                                    
c     *             radius,rad2,radius1,rad21,radius2,rad22,coord,icoord,                               
c     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4) 
		 
	else if(imesh.EQ.3) then                                                                
          call tun31geo(0,n_angbkfix,iangbkfix,angbkfix,! 0 means out bound is rect.      
     * n_fix,iafix,n_bkfix,iabkfix,                                                       
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *             nn,nye,NYL,nntemp,                                                     
     *             nxe,nye0,spoke,radius,coord,icoord,                                    
     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4) 
	else if(imesh.EQ.1) then                                                              
          call tun11geo(1,n_angbkfix,iangbkfix,angbkfix,! 1 means out bound is arc.       
     * n_fix,iafix,n_bkfix,iabkfix,                                                       
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,                   
     *             nn,nye,NYL,nntemp,                                                     
     *             nxe,nye0,spoke,radius,coord,icoord,                                    
     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4) 
                          
	endif
	elseif (GEOMETRYTYPE.eq.2) then
		call tunaxigeo(1,n_angbkfix,iangbkfix,angbkfix,! 1 means out bound is arc.       
     * n_fix,iafix,n_bkfix,iabkfix,                                                       
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                       
     *             nn,nye,NYL,nntemp,                                                     
     *             nxe,nye0,spoke,radius,coord,icoord,                                    
     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg,nodep4,NN4)

	endif
	


4523  format(i10,f20.8)  
	continue
	
	NN8=NN
	CALL SET_4_8_VNODES(NXE,NYE,NODEP,NODEP4)
!!       	call mscmarcin4(x,y,nn4,nn,nxe,nye,nodep4,i_4_8_nodes)
c  	stop
!        call mscmarcin(x,y,nn,nxe,nye,nodep,n_fix,n_bkfix,iafix,iabkfix)
        
	call Bound4Node1(n_t_fix,ia_t_fix,n_bkfix,iabkfix,                                                                 
     *              nxe,nye,nodep,nodep4)
	

	call set5_nf(n_fix,iafix,
     *              nxe,nye,nn,nodof,iload,nodep,IMESH)
	
	!recording node locations
c	call setreac_nf(nxe,nye,nn,nodof,nreac,nodep)
	I_NODE=0
	i_v4nodes=0
	do i1=1,NYL
		do i2=1,nxe+1
			nn=i_vnodes(i1,i2)
			if(nn.GT.0) then
				I_NODE=I_NODE+1
				i_v4nodes(I1,I2)=I_NODE
			END IF
		END DO
	END DO 

	DO I=1,NN4
		A_SAT (I)=WW
		A_SAT0(I)=WW
		A_SAT8(I)=WW
		A_SAT_new(I)=WW
		a_satp(I)=ww
		a_satpp(I)=ww
		II=i_4_8_nodes(I)
		DUMSW(I)=ww
		a_sat_dummy(I)=0.5
	END DO
	iload1=iload
	SW=0.0D0
	DO P=1,NXE
	DO Q=1,NYE
		DO IN=1,4
			SW(P,Q,IN)=WW
		ENDDO
	ENDDO
	ENDDO


C=======================================================================
C Temperature, kevin added
c*****calculate number of node temproal field Kevin May 10/2006***********
c	nntemp=nye*(3*nxe+2)+(2*nxe+1) ! 8 nodes
c      nntemp=(nxe+1)*(nye+1) ! 4 nodes
C      call READNFT(nxe,nye,nntemp,1,iload)
	call READNFT(nxe,nye,nntemp,1,iload,ia_t_fix,n_t_fix)   
c********get total freedom degree number of temperature***********
      ntemp=iload
c initial temperature READ(5,*)VAL0
	CALL NULVEC(st0,ntemp)
	CALL NULVEC(st1,ntemp)
	!!
	val0=Tout

      DO 50 I=1,ntemp  ! Kevin
		st0(I)=VAL0           
    		st1(I)=VAL0
   50 CONTINUE 

      CALL NULVEC(dumpm,nn8)
	CALL NULVEC(dumpm1,ntemp)
	CALL NULVEC(dumpm0,ntemp)
	do i =1, ntemp
	  dumpm1(i) = pm0
	  dumpm0(i) = pm0
	end do
	do i =1, nn8
	  dump(i) = p0
	end do
      DO I = 1, NN
		dumpm(I) = pm0
      ENDDO	 
!      do i=1,nxe
!	   st0(i)=tiner
!	   st1(i)=tiner
!	enddo
C end of Kevin added 
C=======================================================================
c         for temperature kevin May 12/2006
	iload=iload1
      do ix=1,nxe
	do iy=1,nye
          call set_g(ix,iy,g,nodep,nodeg)
c	    call setreac_g(ix,iy,greac,nodep,nodegreac)
!          call TunGEO4T(ix,iy,nxe,nye,nodep4,nodep4)
	end do
	end do
 	if(is_meshing.EQ.0) then
C*************By Ma.Z********OUTPUT Nodes & Elems Geom Info ***********************************
C Multimesh modes
c       if ((imesh.eq.4).or.(imesh.eq.5)) then
c	   nye00=2*nye
c	 else
c	   nye00=nye
c	 end if

!	CALL GETARG(4,OPTMESH)
		OPTMESH=OPTMESH1
		OPEN(11,FILE=OPTMESH,STATUS='UNKNOWN')

c	NPOIN=8 + nxe*5 + nye*5 + (nxe)*(nye)*3
		NPOIN=nn
c	NELEMENT=nye00 * nxe
		NNODE=8
		NMATS=0
		NGAUS=0
	    WRITE(11,*) NPOIN,NELEMENT,NNODE,NMATS,NGAUS    !
c	WRITE(11,*) NPOIN,NELEMENT,NNODE,NMATS,NGAUS    !

		DO I=1,NPOIN
			WRITE(11,*) I,X(I),Y(I)                  ! NODAL COORDINATERS
		END DO

		DO iP=1,NXE
		DO iQ1=1,nye
			WRITE(11,*) iP,iQ1,(NODEP(iP,iQ1,J),J=1,8)    ! ELEMENT NODAL NUMBERS
		END DO
		END DO
      
		DO iP=1,NXE
		DO iQ1=1,nye
			WRITE(11,*) I, (NODEP(iP,iQ1,J),J=1,4)!(PROPS(p,q,J),J=1,4)       ! ELEMENT MATERIAL INFO.
		END DO
		END DO
  30  format (1x,i3,4(1x,f7.2))

		DO i=1,NPOIN
			write(11,*) i,(nf(i,j),j=1,3)
		END do

C      NEVAB=NDOFN*      

c Output vertec node numer, in order to identify the nodes in the later output(*.R08).
c	nVertexNodes=0
c	do i2=1,nxe+1
c	  do i1=1,nye+1
c	   nVertexNodes=nVertexNodes+1
c	  end do
c      end do

		nVertexNodes=nntemp

c	write(11,*)nVertexNodes

c Multimesh mode
c      if (imesh.eq.4) then
c	  nye00=2*nye+1
c	elseif (imesh.eq.5) then
c	  nye00=2*nye+2
c	else
c	  nye00=nye+1
c	end if


c      if (imesh.eq.5) then
c      else
		write(11,*) nntemp
		do i2=1,nxe+1
	    do i1=1,NYL
			nn=i_vnodes(i1,i2)
			if(nn.GT.0) then
				write(11,*)nn
			endif
	    end do
		end do
c	endif
		CLOSE(11)

C*****************************End of OUTPUT Nodes & Elems Geom Info ***********************************
!		return
	endif  !(is_meshing.EQ.1)                                                                        
c********get total freedom degree number***********
	n=iload
c******** get area inside of well. This part is moved here from bottom
c     psand is used in calculate delta propressure vis rate in table
	IF ( completiontype .eq. 1 ) then
		psand   = pi*packerl
      ELSE
 	    psand   = pi*packerl*pd*pi*pradius**2.D0
c			psand   = pd*(pi*packerl*2*pi*pradius+pi*pradius**2.D0)
	END IF
C****NEGATIVAL DRILLING,NT=20, NON-NEGATIVAL DRILLING,NT=1
	NT=1
!      IF (NDRILL.EQ.0) NT=20
      IF (NDRILL.EQ.0) NT=nnstep
C*** FIX NW=71	 
c*******set nw according to nxe and nye, get it from element p=2,q=2
c   npo1=2*nxe+1+nxe+1+3; npo2=2*(2*nxe+1+nxe+1)+5
c		npo1=3*nxe+5
c	    npo2=6*nxe+9
c	    nw=nf(npo2,3)-nf(npo1,1)

c		npo1=3*nxe+5
c	    npo2=6*nxe+9
c	    nw1=nf(npo2,3)-nf(npo1,1)
	nw=0
	do imq=1,nye
	do imp=1,nxe
	do itmp1=1,8
		im_pt1=nodep(imp,imq,itmp1)
		do itmp2=1,3
			imdof1=nf(im_pt1,itmp2)

			if(imdof1.NE.0) then
				do itmp3=1,8
					im_pt2=nodep(imp,imq,itmp3)
					do itmp4=1,3
						imdof2=nf(im_pt2,itmp4)

						if(imdof2.NE.0) then
							if(ABS(imdof1-imdof2).GT.nw) then
								nw=ABS(imdof1-imdof2)
							endif
						endif

					enddo
				enddo
			endif
		enddo
	enddo
	enddo
	enddo
c*****************************Sept4,2000 EQ
      cdmax = nw+1
      tot = dof+nodf
      band = 2*cdmax-1
      r = n*cdmax
      m = nye+1
	ip = 1
	ico = 0

c	CALL GETIW(IW4,NXE,NYE,NODEP4)
C****ULOAD IS USED TO GIVE A LITTLE BIT REDUCING ABOUT PRESSURE AND 
C    PORE PRESSURE WHEN IT IS NON-NEGATIVAL DRILLING
cpppppppppppppppp
c      ULOAD = (SH3-P0)/20
	ULOAD = (SH3-P0)/nnstep


C*****IDD_N IS THE TOTAL STEPS GET FROM TABLE ----aGG24 EQ
      NTSTEP=0
	DO KK1=2,NPOINT+1
		NTSTEP=NTSTEP+NSTEP(KK1)
	END DO
      IDD_N=NTSTEP
C
c--------- May 31, 2000 YW add nc to determine if 10 or 1 iterations are -----
c          required for cohesion change  
c
      nc = 20
	NFLAG=0
 	if ( dabs(cou-col) .lt. 0.00001D0 ) nc = 101
c	if ( dabs(cou-col) .lt. 0.00001D0*1.0d-3 ) nc = 101
c********calculate geometry data**********************************************

	IF (IMESH.EQ.1) THEN
cMa.Z          do ix=1,nxe
cMa.Z	    do iy=1,nye
cMa.Z          call tungeo(ix,iy,nxe,nye,spoke,radius,coord,icoord,
cMa.Z     *             coordf,icordf,g,x,y,angl,angm,nodep,nodeg)
c         for temperature kevin May 12/2006
cMa.Z          call TunGEO4T(ix,iy,nxe,nye,nodep4,nodep4)
cMa.Z	    end do
cMa.Z	    end do
	ELSEIF (IMESH.EQ.2) THEN
          do ix=1,nxe
	    do iy=1,nye
	    CALL tungeosqe(ix,iy,nxe,nye,spoke,radius,coord,icoord,
     *              coordf, icordf,g,x,y,angl,angm,nodep,nodeg)
	    end do
	    end do
	ELSEIF (IMESH.EQ.3) THEN

          do ix=1,nxe
	    do iy=1,nye

	    !CALL tungeosqes(ix,iy,nxe,nye,spoke,radius,coord,icoord,
     *              !coordf, icordf,g,x,y,angl,angm,nodep,nodeg)

	    end do
	    end do
c	call geometry_plotter(x,y,nxe,nye,nodep,i_vnodes,nn4
c     * 	,i_4_8_nodes,imesh)


	ELSEIF (IMESH.EQ.4) THEN
          do ix=1,nxe
	    do iy=1,nye
			CALL tungeosym(ix,iy,nxe,nye,spoke,radius,coord,icoord,
     *              coordf, icordf,g,x,y,angl,angm,nodep,nodeg)
	    end do
	    end do
	ELSEIF (IMESH.EQ.5) THEN
c	   WRITE(T_OUT,*) 'Invalid mesh style'
	ELSE
	END IF


c******get imformation of fixed boundary  ********************	
      call resist(mcc)
      call NULVEC(yielding_P,30)                                                         
      call NULVEC(sanding_P,30)                                                          
      call NULVEC(plastic_P,30)                                                          
      call NULVEC(t_P,30)                                                                
      call NULVEC(coh_P,30)                                                                                                                                                       
      nn_S=0
c	!!stop
c*************************************************************
      
	DO 5000  il = 1, 101, nc
c******parameter for sanding time record  sept28 EQ
		reptime=0.d0
		rebhp=0.d0
		recoh=0.d0
		nrec=0
c******paramater for yielding record  sept28 EQ
		reptimep=0.d0
		rebhpp=0.d0
		recohp=0.d0
		nrecp=0
c*************************
		if(ndrill.eq.0) then
cppppppppppppppppp
!!			ntotal=20
			ntotal=nnstep
		else
			ntotal=1
		end if
c*****setting ispoint for table index************************
		ispoint=1
c*****SETTING INTINAL porepressure RECORDER***********
c*When last raw in table is porepressure, recoder this valuse, when this
c raw is porepressure, updated by this values, if this raw is rate, updat 
c pre0=pre0+delta porepressure in this step, so when this raw is finished,
c pre0 equal to the vaue of porepressure at the end step of this raw, so if
c next raw in table is porepressure, the delta porepressure can be get by
c porepressure in this raw - pre0     -------Aug26 EQ

		pre0=p0
		if (IMESH.ge.4) then
       		pre02=p0   !second well
		end if

		NFLAG=NFLAG+1
C
C------------End of nc correction ----------------------------------------
		ico = ico + 1
  	    disp = 0.0D0
		sandcr = 0.0D0
		drawpre = 0.0D0
		pdrop = 0.0D0
	    ip = 1
	    ix = 0
	    coh = 1.0D0*(il-1)*(cou - col)/100.0D0 + col
c------------------sanding_p                                                              
		nn_s=nn_s+1                                                            
		coh_p(nn_s)=coh 
C---------------------------------------------------------------------------
          ucs = 2.D0*coh*dcos(phi)/(1.D0 - dsin(phi))
	    coef = (1.D0 + dsin(phi))/(1.D0 - dsin(phi))
c*** Carl still did not add input of TS, resume ts=0.5*ucs  March 2004
c	ts=0.05d0*ucs
c***
!!Shunfu.Z 2010.01.21
c           call null(pb,n,band)
!!Shunfu.Z 2010.01.21
           call nulvec(loads,n)
	     ptime = 0.0D0
	     temp  = 1.0D0
	     rplas = rw
	     flowm = 0.0D0
	     flowo = 0.0D0
	     flowf = 0.0D0
	     sandp = 0.0D0
c***** set nsandi array for zero   Sept26,2000 EQ
c***** set npelastic array for zero   Sept26,2000 EQ
           do in=1,10001
			nsandi(in)=0
			npelasti(in)=0
	     end do
c
c*************** Form the plane strain stress-strain matrix ************
c   Note the code is modified to null out-of-balance force bdylds
c   before each time loop (loading loop) so that those unbalanced
c   force will be continuously added into the system in the next
c   loading loop. yw-Oct 17, 1999
c
c    ****************************************************
c
		call NULVEC(bdylds,n)
		call nulvec(tbdyld,n)
		call nulvec(ans,n)
		call nulvec(tload,n)
		call nulvec(dloads,n)
		call nulvec(tforce,n)
		call nulvec(loads0,n)
C                                                                                         
C=======================================================================                  
C Tempreture Kevin add                                                                    
		call nulvec(forcet1,n)                                                              
		call nulvec(forcet0,n) 
		call nulvec(force_t1,n)                                                              
		call nulvec(force_t0,n) 		                                                             
C End of Kevin add                                                                        
C======================================================================= 
c
C 20 = TIME STEP FOR LOADING
C NT/3 = UNLOADING CONTINUE CALCULATION 
c
		WRITE(T_OUT,997)COH                                                           
		! CALL WRTF(T_OUT) 
ccc		WRITE(*,997)COH 
997		FORMAT(2X,5('-'),2X,'COH= ',F9.4,' MPa',2X,5('-'))
ccc		WRITE(2,997)COH
		qo = 0.0D0
		qw = 0.0D0
ccc		WRITE(2,*)'---------- DRILLING TIME ----------'
ccc		WRITE(*,*)'---------- DRILLING TIME ----------'
	    WRITE(T_OUT,*)'---------- DRILLING TIME ----------'
		! CALL WRTF(T_OUT)
		L_TMP=0
		!!temperature_add                                                                                	                                                                                         
		COMP_OWT=COMP_WT*WW+COMP_OT*(1.0-WW) !!mixture heat conductivity
		!!temperature_s
c 	CALL INIT_STEAM(rad2(1),rad2(NYE0+1),radius(1),
! 		CALL INIT_STEAM(rad2(1),rad2(NYE0+1),
!     *	COMP_OWT, Tiner, VAL0, 0.9D3, packerl,  ! 油的密度先给0.9E3 (The density of the oil give 0.9E3)
!     *		RKE_X, POROSITY, 
!     *		Num_T_V, vis_temp(1,1), vis_temp(1,2),vis_temp(1,3),WW)  !温，水粘，油粘,水饱 (Temperature and water viscosity, oil viscosity, water saturation)
  
96264 continue

	do p=1,NXE+1
	do q=1,NYE+1
		ii=i_vnodes(p,q)
		if (ii.gt.0) then 
		v_inodes(ii,1)=p
		v_inodes(ii,2)=q
		endif
	enddo
	enddo

c-------------------------PERMEABILITY PLOTTING--------------------------

	do i=1,101
	SWA=0.0
	SWB=1.0
	sw1=(i-1.0)/100.0*(SWB-SWA)+SWA
	if (if_kr_wells.ne.0) then
		call KR_finder(sw1,SKRW1,dkw_ds1,SKRO1,dko_ds1,if_kr_wells)
	else
		CALL INTERP(SWT,SKROT,NUMOW,SW1,SKRO1)
		CALL INTERP(SWT,SKRWT,NUMOW,SW1,SKRW1)
		CALL INTERP(SWT,dkw_dsT,NUMOW,SW1,dkw_ds1)
		CALL INTERP(SWT,dkO_dsT,NUMOW,SW1,dkO_ds1)

	endif
	fw=SKRW1/(SKRW1+SKRO1)
	dfw_ds=(dkw_ds1*SKRO1-dko_ds1*SKRw1)/(SKRW1+SKRO1)**2.0
	pcpie1=8.763D-3*(-1.D0/1.754D0)*Sw1**(-1.0D0/1.754D0-1.0d0)
      SLWO=SKRO1/3.12+SKRW1/1.0
      kay11 = RKE_X*SLWO*0.08527D0 
      krw1 = SKRw1/1.0*RKE_X*0.08527D0 
      kro1 = SKRo1/3.12*RKE_X*0.08527D0 
      fff1=kay11*sw1**3.0
	fff2=kay11*sw1**3.0*0.08527D0 
	fff3=RKE_X*sw1**3.0*0.08527D0

	enddo
C--------------- TEMPERATURE PLOTS---------------------------------
	do i=1,Num_T_V

	do ii=1,21
	SWA=0.0
	SWB=1.0
	sw1=(ii-1.0)/20.0*(SWB-SWA)+SWA
	if (if_kr_wells.ne.0) then
		call KR_finder(sw1,SKRW1,dkw_ds1,SKRO1,dko_ds1,if_kr_wells)
	else
		CALL INTERP(SWT,SKROT,NUMOW,SW1,SKRO1)
		CALL INTERP(SWT,SKRWT,NUMOW,SW1,SKRW1)
		CALL INTERP(SWT,dkw_dsT,NUMOW,SW1,dkw_ds1)
		CALL INTERP(SWT,dkO_dsT,NUMOW,SW1,dkO_ds1)

	endif
	fw=SKRW1/(SKRW11+SKRO1)
	dfw_ds=(dkw_ds1*SKRO1-dko_ds1*SKRw1)/(SKRW1+SKRO1)**2.0
	pcpie1=8.763D-3*(-1.D0/1.754D0)*Sw1**(-1.0D0/1.754D0-1.0d0)
      SLWO=SKRO1/vis_temp(i,3)+SKRW1/vis_temp(i,2)
	SLWO_T_SW(i,ii)=SLWO

	enddo

	
1779	format(f5.3,5f30.15)
!	close(683)
	enddo
C--------------- SG PLOTS---------------------------------
	do i=1,21
	P1A=1.0
	P1B=6.0
	P11=(i-1.0)/20.0*(P1B-P1A)+P1A
	CALL INTERP(PO,RSO,NUMO,P11,RSO0)
	enddo


	timenow=0.0
	do 8 ns = 0, NT+IDD_N !time-loop
	if(if_jump_ns_1_20.eq.1.and.ns.ge.1.and.ns.le.20) goto 32654
	if (ns.eq.21) timenow=0.0
	if (ns.gt.21) then
	endif
	IF (IF_P_PASS.EQ.1) GOTO 14001
	
	dtmp=dtim

c-----------------------Time increment change ---------------------------
		IF(NT.EQ.nnstep) TMP_C=NS*100.0/NT
c******if negatival drilling, it is. Otherwise, it id different*************
		if (ndrill.eq.0) then !if1
			if (ns.LE.nnstep) THEN !if2
 			dtim=tsdrill
 			if (ns.ne.0) then
				dtim=tsdrill
			else
				dtim=tsdrill0
			endif
				 
			DTIM1=1
			DRILLING_TIME=NS
	        IF(NS.GT.0) THEN                                                                
				WRITE(T_OUT,998)TMP_C,DRILLING_TIME,DTIM1,BHP                                   
!				CALL WRTF(T_OUT)                                                                
			ENDIF
ccc			IF(NS.GT.0) WRITE(*,998)TMP_C,DRILLING_TIME,DTIM1,BHP
ccc	        IF(NS.GT.0) WRITE(2,998)TMP_C,DRILLING_TIME,DTIM1,BHP
998		FORMAT(F6.2,'(%) Finished, Current Time =', F7.2, ' DT =',F7.2,
     & ' (Day)  BHP =', F6.2,' (MPa)') 
			END IF !if2
			if (ns.GT.nnstep) THEN !if3 (inside if1) 
				if(ns.gt.ntotal) then !if4
					ispoint=ispoint+1
					ntotal=ntotal+nstep(ispoint)
					if(npr(ispoint-1).ne.0) then
						pdelta(ispoint-1)=pre0
					else
						pre0=pdelta(ispoint-1)  !***************
					end if

					if (IMESH.ge.4) then
						if(npr2(ispoint-1).ne.0) then
							PDLTA2(ispoint-1)=pre02 ! multimesh
						else
							pre02=PDLTA2(ispoint-1) ! Multimesh
						end if
					end if

  				dtim=(pdate(ispoint)-pdate(ispoint-1))/nstep(ispoint)
				end if !if4
			END IF !if3
		else !else1  if (ndrill.eq.0)
c*****This part for un-negatival drilling*****
			if (ns.LE.1) THEN !if5
!! 				dtim=0.10d+5 
! 				dtim=tsdrill
				if (ns.ne.0) then
					dtim=tsdrill
				else
					dtim=tsdrill0
				endif
				DTIM1=1
				IF(NS.GT.0) THEN                                                                
					 WRITE(T_OUT,998)TMP_C,DTIM1,BHP                                                 
!					 CALL WRTF(T_OUT)                                                                
				ENDIF
ccc				IF(NS.GT.0) WRITE(*,998)TMP_C,DTIM1,BHP 
ccc				IF(NS.GT.0) WRITE(2,998)TMP_C,DTIM1,BHP
			END IF !if5
			if (ns.GT.1) THEN !if6 (inside else1)  
c		dtim=DTI  ----be changed on Aug.15 EQ
				if(ns.gt.ntotal) then !if7
					ispoint=ispoint+1
					ntotal=ntotal+nstep(ispoint)
					if(npr(ispoint-1).ne.0) then
						pdelta(ispoint-1)=pre0
					else
						pre0=pdelta(ispoint-1)
					end if

					if (IMESH.GE.4) then  ! Multimesh
						if(npr2(ispoint-1).ne.0) then
							PDLTA2(ispoint-1)=pre02
						else
							pre02=PDLTA2(ispoint-1)
						end if
					end if

  					dtim=(pdate(ispoint)-pdate(ispoint-1))/nstep(ispoint)
				end if !if7
			END IF !if6
		end if !if1
c****************************************************************************
C PTIME BEGIN FROM DRILLING HAS BEEN FINISHED
		timenow=timenow+dtim
		IF(NS.GT.NT) THEN
			IF (PTIME+DTIM.GT.TIMEF) DTIM=TIMEF-PTIME
				ptime = ptime + dtim
		endif
c-------------------   ITERATOR-LOOP---------------------------------------
	if_finish2=0
	iterator=0
	iterator2=0
c	do 10000 iterator=1,n_iterator
	do 10000 while (if_finish2.eq.0) 
	iterator=iterator+1
	iterator2=iterator2+1

c	if(ns.eq.0) then
c	  do i =1, nn
c		dumpm(i) = pm0
c	    dump(i) = p0
c	  end do
c	endif

			IF(NS.GT.NT) THEN !if-A1

!!==================================================
!!========================LI========================
      if(itype.eq.3) then
			if (iterator.eq.1) then
			do p=1,ntemp
				dumpm0(p)=dumpm1(p)
			end do
			endif
		CALL nulvec(dumpm1,ntemp)

      call pressure_m4(nxe,nye,nn,nntemp,ntemp,bhp,nyl,
     *nodep, dtim,dumpm0,dump,dumpm1,term,x,y ,POROm,nodep4
     * ,vis_o,VIS_W,SW, NUMOW ,kxym,ns ,IMESH,n_bkfix,iabkfix,
     * COMP_O,alpha_t,pm0,SWT,SKROT,SKRWT,i_4_8_nodes,i_vnodes)


      DO I = 1, NN
		dumpm(I) = pm0
      ENDDO
	do i4=1,nn4
		i8=i_4_8_nodes(i4)
		dumpm(i8)=dumpm1(i4)
	enddo

          do ii=1,nn4
			dumpm11(ii)=dumpm1(ii)
		enddo
          do ii=1,nxe
		do jj=1,nye
		ig=0
		do i=1,gp
		do j=1,gp
			ig=ig+1
			call formln(derf,iderf,funf,samp,gp,i,j)
			asum=0.0d0
			do kk=1,nodf
				asum=asum+funf(kk)*dumpm11(nodep4(ii,jj,kk))
			enddo
			dd(ii,jj,ig)=asum
		enddo
		enddo
		enddo
		enddo
      endif
!!==================================================
!!==================================================

			if (iterator.eq.1) then
			do p=1,ntemp
				st0(p)=st1(p)
			end do
			endif

		CALL nulvec(st1,ntemp)
			Tin=Tiner
			if (IMESH.GE.4) Tin2=Tiner2  ! Multimesh
			Tou=Tout

		CALL GETIW(IW4,NXE,NYE,NODEP4)
		
		if (if_T_pass.eq.0) then
		call temperature_c(IW4,nxe,nye,nodep4,nn8,nN4                               
     *		,nodep,n_t_fix,ia_t_fix,PERMX,PERMY,dtim
     *		,Tiner,Tout,Tiner2,st0,st1,term,x,y,dump,KAY,PORO,timenow
     *		 ,time_sagd_prod,POROSITY,RKE_X,radius
     *		,vis_temp,Num_T_V,SW,if_kr_wells,if_simple_ups_per2,NUMOW
     *		,a_sat_new,SWT,SKROT,SKRWT,kxy,ns,rho,if_boussinesq
     *		,alfa_f,IMESH,
     *          n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
     *          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     *          n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload,
     *			  ngrav,igrav,if_steam_mu,steam_vis,
     *			if_Q1,if_Q2,if_Qout,Q1,Q2,Qout)

c		call temperature_c_orig(IW4,nxe,nye,nodep4,nn8,nN4,                               
c     *		nodep,n_t_fix,ia_t_fix,PERMX,PERMY,dtim,
c     *		Tiner,Tout,Tiner2,st0,st1,term,x,y,dump,KAY,PORO)		
		else
			do ii=1,nn4
				st1(ii)=T_pass_value
			enddo
		endif

		do ii=1,nn4
			st11(ii)=st1(ii)
		enddo

      	do ii=1,nxe
		do jj=1,nye
		ig=0
		do i=1,gp
		do j=1,gp
			ig=ig+1
			call formln(derf,iderf,funf,samp,gp,i,j)
			asum=0.0d0
			do kk=1,nodf
				asum=asum+funf(kk)*st11(nodep4(ii,jj,kk))
			enddo
			tt(ii,jj,ig)=asum
		enddo
		enddo
		enddo
		enddo
		

		if(ns.eq.nt+1) then
			open(7890,file='chamber.dat')
			do ii=1,nt+1
				write(7890,7891) 0
			enddo
		endif

			write(7890,7891) 9
7891			format(i1)
7892			format(i1,2f12.8)
7893			format(i5,2i2)
7894			format(4i3,3f12.6)
			do ii=0,8
				write(7890,7892) ii,1.0d0*ii,1.0d0*ii
			enddo
			write(7890,7893) nxe*nye,1,1
			do ii1=0,nxe-1
			do ii2=0,nye-1
			do ii3=0,1
			do ii4=0,1
				write(7890,7894) ii1,ii2,ii3,ii4,
     $			x(nodep(ii1+1,ii2+1,2*nmesh(ii3,ii4)-1)),
     $			y(nodep(ii1+1,ii2+1,2*nmesh(ii3,ii4)-1)),
     $			st1(nodep4(ii1+1,ii2+1,nmesh(ii3,ii4)))
			enddo
			enddo
			enddo
			enddo
7899			continue
			call nulvec(forcet1,n)
			call nulvec(forcet0,n)
		    call nulvec(force_t1,n)                                                              
		    call nulvec(force_t0,n) 			 
		else !if-A1

		endif !if-A1 !IF(NS.GT.NT) THEN

		call nulvec(eld,tot)
		call nulvec(eldt,tot) ! kevin Temperature
		call nulvec(bk,r)
		do ii=1,n
		do jj=1,band
			pb(ii,jj)=0.0d0
		enddo
		enddo
c******* It should set an option here that idee,ibee,ibdee,h should be 3
c        in plan stress and plan strain, otherwise, they should be 4
c****************************************************************************
	    h=3
c****************************************************************************
          call null(dee,h,h)
c****************** Form Gaussian integration points ************************
c      Because force array is calculated in 2 point gauss points, gp must be 2 here.
c      After modify the force array subroutine, gp=1 will be avalible
c****************************************************************************
	    gp=2
		call gauss(samp,gp)
c*************** Element Matrix Integration and Assembly********************
		do 3 p=1,nxe !main element-loop
		do 3 q=1,nye
			DO a1=1,4
				a2=NODEP(P,Q,a1*2-1)
				DUMP_G(a1)= DUMP(a2)
				nodeid4(a1)= nodep4(p,q,a1)
			enddo
c	  if ( ns .eq. 0) THEN
			if ( ns .eq. 1 .or. ns .eq. 0) THEN !ifns01
			!!temperature_add
C===========================================================================
C Tempreture from kevin
C Temperature viscosity-temp relationship kevin 2006-06-20

				temp_element=0.0   ! Element temperature
				do in=1,4
				   temp_element=temp_element+st1(nodep4(p,q,in))/4.0
				end do
C         interpolation to get Vis_0 and Vis_w
				if(temp_element.LT.vis_temp(1,1)) then !ifAB1
					Vis_w=vis_temp(1,2)
					Vis_o=vis_temp(1,3)
C	write (*,*)"Warnning temperature is out of viscosity-temperature"
				else if(temp_element.GT.vis_temp(Num_T_V,1)) then
					Vis_w=vis_temp(Num_T_V,2)
					Vis_o=vis_temp(Num_T_V,3)
C	write (*,*)"Warnning temperature is out of viscosity-temperature"
				else !ifAB1
					DO 110 I=1,Num_T_V
						IF(temp_element.GE.vis_temp(I,1)) GO TO 110
				Vis_W=vis_temp(I-1,2)+(temp_element-vis_temp(I-1,1))
     &						*(vis_temp(I,2)-vis_temp(I-1,2))/ 
     &						(vis_temp(I,1)-vis_temp(I-1,1))
				
				Vis_O=vis_temp(I-1,3)+(temp_element-vis_temp(I-1,1))
     &						*(vis_temp(I,3)-vis_temp(I-1,3))/
     &						(vis_temp(I,1)-vis_temp(I-1,1))
					goto 120  
  110					continue
  120					CONTINUE
				end if !ifAB1 inside ifns01
		 if(temp_element.gt.265.and.if_steam_mu.eq.1)
     1		  Vis_W=steam_vis !steam instead of water
C End of Kevin added
C===========================================================================
				ig=0
				do  i=1,gp !Gauss-loop1 inside if ( ns .eq. 1 .or. ns .eq. 0)=ifns01
				do  j=1,gp
					ig=ig+1
					ex(p,q,ig) = 0.0D0
					ey(p,q,ig) = 0.0D0
					ez(p,q,ig) = 0.0D0
					exy(p,q,ig) = 0.0D0
					sx(p,q,ig) = 0.0D0
					sy(p,q,ig) = 0.0D0
					sz(p,q,ig) = 0.0D0
					sxy(p,q,ig) = 0.0D0
					sandg(p,q,ig) = 0
					nelem(p,q,ig) = 0
					poro(p,q,ig) = porosity
					volstr0(p,q,ig)=0.d0
c********set kay in two directions*****Aug24 EQ
!Here is inside Gauss-loop1 inside if ( ns .eq. 1 .or. ns .eq. 0)
					kxy(p,q,ig,1) = RKE_X
					kxy(p,q,ig,2) = RKE_Y
!!============================================
!!=====================LI=====================
                      if(itype.eq.3) then
                      porom(p,q,ig) = porositym
                      kxym(p,q,ig,1) = RKEm_X
					kxym(p,q,ig,2) = RKEm_Y
					endif 
!!============================================
!!============================================
					en(p,q,ig) = E
  					RSOO(p,q)=0.d0
					if (q.le.nye0) then
						lq(p) = 1
						iq(p) = 1
						RPold(p) = 1
					else
						if (imesh.ge.4) then
							lq2(p) = nye     !q=nye+1
							iq2(p) = nye
							RPold2(p) = nye
						end if
					end if
C
c*******************  average compressibility *************@@@@@
C!Here is inside Gauss-loop1 inside if ( ns .eq. 1 .or. ns .eq. 0)
					CALL INTERP(PO,RSO,NUMO,P0,RSO0)
c	     SO(P,Q)=1.D0-SW(P,Q)-RSO0
					!!Shunfu.Z 2010.07.01
					sg(p,q,ig)=RSO0
					!!Shunfu.Z 2010.07.01
					so(p,q,ig)=1.D0-sw(P,Q,ig)-sg(p,q,ig)
					IF (NS.EQ.0) THEN
	!! water+oil
!!=====================================================
!!=====================================================
	                   if(itype.eq.1.or.itype.eq.2) then
						cbar = COMP_W*sw(P,Q,ig)+COMP_O*so(p,q,ig)
     &         +V_L*P_L/(poro(p,q,ig)*(P_L+dump(nodep(p,q,2*ig-1)))**2)
	                   else
						cbar = COMP_W*sw(P,Q,ig)+COMP_O*so(p,q,ig)
	                   endif
!!=====================================================
!!=====================================================
	!!  water + oil + gas (free and dissolved)
						if (P0.le.1.0d-8) then 
							cbar = cbar+bm*sg(p,q,ig)/(P0+1.013d-1)
						else
							cbar = cbar+bm*sg(p,q,ig)/P0
						endif
	!!  water + oil + gas (free and dissolved) + solid matrix
	!Here is inside Gauss-loop1 inside if ( ns .eq. 1 .or. ns .eq. 0)
				qcom(p,q,ig)=cbar*poro(p,q,ig)+comp_s*(biot-poro(p,q,ig))
					ELSE
						NNG=NODEP(P,Q,IG*2-1)
						cbar = COMP_W*sw(P,Q,ig)+COMP_O*so(p,q,ig)
						if (DUMp(NNG).le.1.0d-8) then
						cbar = cbar+bm*sg(p,q,ig)/(DUMp(NNG)+1.013d-1)
						else
						cbar = cbar+bm*sg(p,q,ig)/(DUMp(NNG))
						endif
			qcom(p,q,ig)=cbar*poro(p,q,ig)+comp_s*(biot-poro(p,q,ig))
					END IF
C
C***********************************************************@@@@@@
C
c    Note: 0.08527 is used to convert k/u unit from mD/cp to m^2/MPa Day
		!Here is inside Gauss-loop1 inside if ( ns .eq. 1 .or. ns .eq. 0)
					CALL INTERP(SWT,SKROT,NUMOW,SW(P,Q,ig),SKRO)
					CALL INTERP(SWT,SKRWT,NUMOW,SW(P,Q,ig),SKRW)
					SLWO=SKRO/VIS_o+SKRW/VIS_W
					 kay(p,q,ig,1)= KXY(P,Q,IG,1)*SLWO*0.08527D0 
					 krw(p,q,ig,1) = SKRw/VIS_w*KXY(p,q,ig,1)*0.08527D0 
					 kro(p,q,ig,1) = SKRo/VIS_o*KXY(p,q,ig,1)*0.08527D0
					 kay(p,q,ig,2)= KXY(P,Q,IG,2)*SLWO*0.08527D0 
					 krw(p,q,ig,2) = SKRw/VIS_w*KXY(p,q,ig,2)*0.08527D0 
					 kro(p,q,ig,2) = SKRo/VIS_o*KXY(p,q,ig,2)*0.08527D0
!!=====================================================
!!=====================================================
                  if(itype.eq.3) then
				kaym(p,q,ig,1)= KXYm(P,Q,IG,1)*SLWO*0.08527D0 
				krwm(p,q,ig,1) = SKRw/VIS_w*KXYm(p,q,ig,1)*0.08527D0 
				krom(p,q,ig,1) = SKRo/VIS_o*KXYm(p,q,ig,1)*0.08527D0
				kaym(p,q,ig,2)= KXYm(P,Q,IG,2)*SLWO*0.08527D0 
				krwm(p,q,ig,2) = SKRw/VIS_w*KXYm(p,q,ig,2)*0.08527D0 
				krom(p,q,ig,2) = SKRo/VIS_o*KXYm(p,q,ig,2)*0.08527D0
	            endif
!!====================================================
!!====================================================
c**************
c
				end do
				end do !Gauss-loop1 inside if ( ns .eq. 1 .or. ns .eq. 0)
				if ( ns .eq. 0) THEN
					do i =1, nn8
						dump(i) = p0
						dumu(i) = 0.0D0
					end do
				end if
c
c         radius(iq) - Location for sanding
c         radius(lq) - Location for plastic yielding
c
				drawdown = p0-sh3
				bhp      = sh3
				pdrop    = 0.0D0
			END IF !if ( ns .eq. 1 .or. ns .eq. 0)=ifns01, no else or elseif

c*************************************************************************
c   get data from x,y,nodep and node g, replace sub tungeo
c******avoid to call tungeo again and again*****************************
			do in=1,tot
				g(in)=nodeg(p,q,in)
			end do
			do in=1,icoord
				coord(in,1)=x(nodep(p,q,in))
				coord(in,2)=y(nodep(p,q,in))
			end do
			do in=1,icordf
				inn=2*in-1
				coordf(in,1)=x(nodep(p,q,inn))
				coordf(in,2)=y(nodep(p,q,inn))
			end do
c*************************************************************************
			call null(km,ikm,dof)
			call null(c,dof,nodf)
			call null(c2,dof,nodf)
			call null(kp,nodf,nodf)
			call null(dtkqa,nodf,nodf)
			call null(dtkqta,nodf,nodf)

			ig=0
			do 4 i = 1, gp !main gauss-loop
			do 4 j = 1, gp
				ig=ig+1
c*************** modify so,sw, calculate qcom ***************************
				do 19 m=dof+1,dof+nodf
					if(g(m).eq.0) seld(m-dof)=0.0d0
					if(g(m).ne.0) seld(m-dof)=loads(g(m))
 19				continue		
         
	!!Shunfu.Z 2009.12.21
				if(ns.eq.0) then
					do ii=1,nodf
						seld(ii)=p0
					enddo
				endif
				call formln(derf,iderf,funf,samp,gp,i,j)
				

				pnow=0.0d0
				do kk1=1,nodf
					pnow=pnow+funf(kk1)*seld(kk1)
				end do
				CALL INTERP(PO,RSO,NUMO,PNOW,RSO0)
				!used at all time steps
				sg(p,q,ig)=RSO0
				SO(P,Q,ig)=1.d0-SW(P,Q,ig)-Sg(P,Q,ig)
!!=====================================================
!!=====================================================
	            if(itype.eq.1.or.itype.eq.2) then
				cbar = COMP_W*sw(P,Q,ig)+COMP_O*so(p,q,ig)
     &         +V_L*P_L/(poro(p,q,ig)*(P_L+dump(nodep(p,q,2*ig-1)))**2)
	            else
				cbar = COMP_W*sw(P,Q,ig)+COMP_O*so(p,q,ig)
	            endif
!!=====================================================
!!=====================================================
			if (pnow.le.1.0d-8) then
			cbar = cbar+bm*sg(p,q,ig)/(pnow+1.013d-1)
			else
			cbar = cbar+bm*sg(p,q,ig)/pnow
			endif
			qcom(p,q,ig)=cbar*poro(p,q,ig)+comp_s*(biot-poro(p,q,ig))
			
	if (if_qcom.eq.0) qcom(p,q,ig)=0.0
	qcom(p,q,ig)=kQCOM*qcom(p,q,ig)
C=========================================================================
C Tempreture from kevin              ! COMP_WT 水的热传导系数 (The coefficient of thermal conductivity of the water)                                                                  
! COMP_OT 油的热传导系数 (The thermal conductivity coefficient of the oil)            ! alfaT 热涨系数 (Thermal expansion coefficient )  
!COMMENTED BY BASHAR (just one line below)
	if (if_qcomt_new.eq.0) then
 	  qcomt(p,q,ig)=COMP_WT*SW(P,Q,IG)+COMP_OT*SO(P,Q,IG)                                          
     &	 -(COMP_B*(1-poro(p,q,ig))-COMP_S)*E*alfaT/(1-2*v)/(1+v)*1.0e0
c	  qcomt(p,q,ig)=COMP_WT*SW(P,Q,IG)*poro(P,Q,IG)
c     $     +COMP_OT*SO(P,Q,IG)*poro(P,Q,IG)
c     &	 -(COMP_s*(1-poro(p,q,ig))-COMP_B)*E*alfaT/(1-2*v)/(1+v)*1.0e0                                          
c     &	 -(COMP_B*(1-poro(p,q,ig))-COMP_S)*E*alfaT/(1-2*v)/(1+v)*1.0e0 
C End of Kevin added                                                                      
C=========================================================================  
	else
C BY BASHAR: MODIFICATION OF QCOMT ACCRODING TO YARLONG 2003 PAPER
        C_PRIME(P,Q,IG)=E/(1.+V)*SKEMPTON**2.*(1.+V_U)**2.*(1.-V)/9
     *  /(1.-V_U)/(V_U-V)*(2.*ALFAT*(V_U-V)/SKEMPTON/(1.-V)/(1.+V_U)
     *  +PORO(P,Q,IG)*(ALFA_F-ALFAT))

	  QCOMT(P,Q,IG)=C_PRIME(P,Q,IG)*COMP_S*(BIOT-PORO(P,Q,IG))
	endif
	if (if_qcomt.eq.0) qcomt(p,q,ig)=0.0
	qcomt(p,q,ig)=kQCOMT*qcomt(p,q,ig)
c  END OF ADDED BY BASHAR
c*************************************************************************
c				if (ns.ne.0) then
c	write(101,5600),"so(",ns,",",p,",",q,",",ig,")=",so(p,q,ig),";"
c	write(101,5600),"sg(",ns,",",p,",",q,",",ig,")=",sg(p,q,ig),";"
c	write(101,5600),"sw(",ns,",",p,",",q,",",ig,")=",sw(p,q,ig),";"
c	write(101,5600),"qcom(",ns,",",p,",",q,",",ig,")=",qcom(p,q,ig),";"
c	write(101,5600),"qcomt(",ns,",",p,",",q,",",ig,")=",qcomt(p,q,ig),";"
c
c				endif

c         when gauss point =1 in stress calculation, the En on each gauss 
c         point in same element should be same
c         note: it should be corrected when gp=1 avalible here
c*************************************************************************
				if (geometrytype .eq. 1) Then
cc					call fmdeps(dee,idee,EN(p,q,ig),v)
                      call fmdeps6(dee, idee, p, q)
					h = 3
				Else
					call fmdrad(dee,4,EN(p,q,ig),v)
					h = 4
				end if
c
c************************* solid contribution **************************
c
c**Compute the 2-D quadratic shape functions FUN & their derivatives ****
c
c***********************************************************************
				gp=2
				call gauss(samp,gp)
				call fmquad(der,ider,fun,samp,gp,i,j,1)
c*******calculate intinal stress when ns=0  sept6,2000********
				IF ( ns .eq. 0 ) Then
	  call stressi(sz0,sh1,sh3,biot,p0,szg,sx(p,q,ig),sy(p,q,ig)
	1	  ,sxy(p,q,ig),sz(p,q,ig),sr(p,q,ig),st(p,q,ig),ex(p,q,ig)
     1     ,ey(p,q,ig),exy(p,q,ig),ez(p,q,ig),er(p,q,ig),
	1   et(p,q,ig),dee,fun,coord,h,icoord,ngrav,nye,imesh,nxe,p,
     1      radius,geometrytype,e,v) 
 				ENDIF

c
c************ Convert local coordinates to global coordinates ***********
				call matmul(der,ider,coord,icoord,jac,ijac)
				call twoby2(jac,ijac,jac1,ijac1,det)
				call matmul(jac1,ijac1,der,ider,deriv,nod)
				call null(bee,h,dof)
!!Shunfu.Z 2010.01.22

				if (geometrytype .eq. 1) Then
					call formb(bee,ibee,deriv,ideriv,nod)
					term = 1.0D0
	
				Else
         call fmbrad(bee,4,deriv,ideriv,fun,coord,icoord,vol,sum,nod)
					term = 2.0D0*pi*sum
				End If
				if (geometrytype .eq. 1) Then
					CALL VOL2D(BEE,IBEE,VOL,nod)
					call matmul(dee,idee,bee,ibee,dbee,dof)
					call matran(bt,ibt,bee,ibee)
					call matmul(bt,ibt,dbee,idbee,btdb,ibtdb)
				else
					CALL VOL2D(BEE,4,VOL,nod)
 					call matmul(dee,4,bee,4,dbee,dof)
					call matran(bt,ibt,bee,4)
					call matmul(bt,ibt,dbee,4,btdb,ibtdb)
				endif
c***********************************************************************
c      set subroutine to calculate quat. The reason is the capacity of
c      matrix samp is changed( define 3X2, but we just use 2X2 and 1X2, 
c      so samp can't be used directary in main subroutine of fem
c***********************************************************************		
				call quotca ( samp,gp,det,term,quot,i,j )
				CALL MSMULT(BTDB, QUOT,DOF,DOF)
				call matadd(km,ikm,btdb,ibtdb)
5688  format(100e14.4) 
				call formln(derf,iderf,funf,samp,gp,i,j)
c				if (uwra.lt.1e-2) then
					call formln(derf2,iderf,funf2,samp,gp,i,j)
c				else
c					a1=0
c					call formln_uw(derf2,iderf,funf2,samp,gp,i,j,uwra1
c    1				,uwra2,0,dump_g)
c				endif

c*************** Convert local coordinate to global coordinate *********
c				call matmul(derf2,iderf,coordf,icordf,jac,ijac)
c				call twoby2(jac,ijac,jac1,ijac1,det)
c				call matmul(jac1,ijac1,derf2,iderf,derivf2,nodf)

				call matmul(derf,iderf,coordf,icordf,jac,ijac)
				call twoby2(jac,ijac,jac1,ijac1,det)
				call matmul(jac1,ijac1,derf,iderf,derivf,nodf)
c

c*************** Form element 'stiffness' matrix of fluid **************
c
c************there are two valus of kay in each gauss point, it means that 
c     Kay should be kay(p,q,ig,2), here let kay(p,q,ig,1)=kay(p,q,ig,2)
c     so here set kay1(2,2), let Kay1(1,1)=kay1(2,2)=kay(p,q,ig)
c                                kay1(1,2)=kay1(2,1)=0.d0
c     It is said that EN should be set same in the future
c************************************************************************
				kay1(1,1)=kay(p,q,ig,1)
				kay1(2,2)=kay(p,q,ig,2)
				kay1(1,2)=0.d0
				kay1(2,1)=0.d0

c        ktheta=6*pi/12.d0
c        kay1(1,1)=0.5d0*(kay(p,q,ig,1)+kay(p,q,ig,2))+
c     &            0.5d0*(kay(p,q,ig,1)-kay(p,q,ig,2))*cos(2.0d0*ktheta)
c        kay1(2,2)=0.5d0*(kay(p,q,ig,1)+kay(p,q,ig,2))-
c     &            0.5d0*(kay(p,q,ig,1)-kay(p,q,ig,2))*cos(2.0d0*ktheta)
c        kay1(1,2)=-0.5d0*(kay(p,q,ig,1)-kay(p,q,ig,2))*sin(2.0d0*ktheta)
c        kay1(2,1)=-0.5d0*(kay(p,q,ig,1)-kay(p,q,ig,2))*sin(2.0d0*ktheta)

				call matmul(kay1,ikay,derivf,idervf,kderiv,nodf)
		goto 12395
		!-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*
		if (if_ups_krw.eq.1.and.ns.gt.21) then

			!A_KW3 is K_abs/vis
			A_KW3(1,1)=1.0/vis_w_mat(p,q)*RKE_X*0.08527D0
			A_KW3(2,2)=1.0/vis_w_mat(p,q)*RKE_Y*0.08527D0
			A_KW3(1,2)=0.0
			A_KW3(1,1)=0.0
			A_KW4(1,1)=1.0/vis_o_mat(p,q)*RKE_X*0.08527D0
			A_KW4(2,2)=1.0/vis_o_mat(p,q)*RKE_Y*0.08527D0
			A_KW4(1,2)=0.0
			A_KW4(1,1)=0.0

			do amm=1,2
			do ann=1,4
				kderiv(amm,ann)=0.0
			do aii=1,2
				a1=nodeid4(amm)
				a2=nodeid4(ann)
c				CALL INTERP(SWT,dkw_dsT,NUMOW,a_sat(a1),dkw_ds1)
c				CALL INTERP(SWT,dkw_dsT,NUMOW,a_sat(a2),dkw_ds2)
		call KR_finder(a_sat_new(a1),lambda_wm,dkw_ds,lambda_om,dko_ds,1)
		call KR_finder(a_sat_new(a2),lambda_on,dkw_ds,lambda_on,dko_ds,1)

c			if (if_extrapolate2.eq.1) then
c			call KR_finder(a_sat_f2(a1),lambda_wm,dkw_ds,lambda_om,dko_ds)
c			call KR_finder(a_sat_f2(a2),lambda_on,dkw_ds,lambda_on,dko_ds)
c			endif
c				sw0=0.5
c				call KR_finder(sw0,lambda_m,dkw_ds,SKRO,dko_ds)
c				call KR_finder(sw0,lambda_n,dkw_ds,SKRO,dko_ds)

c				!lambda here is relative permeability and not mobiolity
				
c				
				if (dump(a1).gt.dump(a2)) then
					alpha=abs_alpha
				else
					alpha=-abs_alpha
				endif
				if (abs(dump(a1)-dump(a2)).lt.0.01) alpha=0.0
			lambdaw=0.5*((1.0+alpha*lambda_wm+(1.0-alpha)*lambda_wn))
			lambdao=0.5*((1.0+alpha*lambda_om+(1.0-alpha)*lambda_on))
			kay2(1,1)=A_KW3(1,1)*lambdaw+A_KW4(1,1)*lambdao
			kay2(2,2)=A_KW3(2,2)*lambdaw+A_KW4(2,2)*lambdao
			kay2(1,2)=0.0
			kay2(1,1)=0.0


				if (abs(alpha).lt.0.01) then
				kderiv(amm,ann)=kderiv(amm,ann)+kay1(amm,aii)*derivf(aii,ann)
				else
				kderiv(amm,ann)=kderiv(amm,ann)+kay2(amm,aii)*derivf(aii,ann)
				endif

			enddo
			enddo
			enddo
		endif
12395		continue
		!-*-*-*-*-*-*-*-*-*-*-*-*-*--*-*-*-*-*-*-*-*-*-*-*

				
				call matran(derivt,nodf,derivf,idervf)
				call matran(derivt2,nodf,derivf2,idervf)
				
c				if (uwra.lt.1e-2) then
				call matmul(derivt,nodf,kderiv,ikderv,dtkd,idtkd)
c				else
c				call matmul(derivt2,nodf,kderiv,ikderv,dtkd,idtkd)
c				endif
				call quotca ( samp,gp,det,term,quot,i,j )
				PROD = QUOT*DTIM
				CALL MSMULT(DTKD, PROD,IDTKD, NODF)
				call matadd(kp,ikp,dtkd,idtkd)
c------------------------------------------------------------------------
c
c   ***Add fluid compressibility matrix [s] as a function of qc ***
c
c   qcom is same as kay, so set qcom1(2,2)
c   dtkq is one part of fluid, so it is 4X4, it means that the fluid part in
c   coupled matrix is dtkd+dtkq
c**********************************************************************
				qcom1(1,1)=qcom(p,q,ig)
				qcom1(2,2)=qcom(p,q,ig)
				qcom1(1,2)=0.d0
				qcom1(2,1)=0.d0
c******************************** qcom * funf **************************May6
				do kk1=1,nodf
				do kk2=1,nodf
c					if (uwra.lt.1e-2) then
					dtkq(kk1,kk2)=funf(kk1)*qcom1(1,1)*funf(kk2)
c					else
c					dtkq(kk1,kk2)=funf2(kk1)*qcom1(1,1)*funf(kk2)
c					endif
				end do
				end do
c****************************************************************************
				PRODS = QUOT
				CALL MSMULT(DTKQ, PRODS,IDTKD, NODF)
				call matadd(DTKQA,IDTKD,DTKQ,nodf)
C===========================================================================              
C Tempreture from Kevin ! Ma.Z modified 2008-8-21                                         
c  qcomT * funf          Kevin 05/08/2006                                                                                                                     
				a_kt(1,1)=qcomT(p,q,ig)                                                                
				a_kt(2,2)=qcomT(p,q,ig)                                                                
				a_kt(1,2)=0.D0                                                                         
				a_kt(2,1)=0.D0                                                                         	                                                                                         
!!    Modified by SHunfu.Z 2009.12.08
				do kk1=1,nodf                                                                       
				do kk2=1,nodf  
c				    if (uwra.lt.1e-2) then                                                                     
					dtkqt(kk1,kk2)=funf(kk1)*a_kt(1,1)*funf(kk2)  
c					else
c					dtkqt(kk1,kk2)=funf2(kk1)*a_kt(1,1)*funf(kk2)
c					endif                                                                                               
				end do                                                                                  
				end do
				CALL MSMULT(DTKQT, PRODS,IDTKD, NODF)
				call matadd(DTKQTA,IDTKD,DTKQT,nodf) 
				
!!=========================================================
!!============================LI===========================
             if(itype.eq.3) then				
              K_M(p,q,ig,1)=KXYm(P,Q,IG,1)*alpha_t
     &                      /VIS_o*0.08527D0
              K_M(p,q,ig,2)=KXYm(P,Q,IG,2)*alpha_t
     &                      /VIS_o*0.08527D0
c              K_M(p,q,ig,1)=Krwm(P,Q,IG,1)*alpha_t
c              K_M(p,q,ig,2)=Krwm(P,Q,IG,2)*alpha_t
		    a_km(1,1)=k_M(p,q,ig,1)
			a_km(2,2)=k_M(p,q,ig,2)
			a_km(1,2)=0.d0
			a_km(2,1)=0.d0
			call matmul(a_km,ikay,funf,idervf,kderivm,nodf)
		    call matmul(funf,nodf,kderivm,ikderv,dtkm,idtkd)
c            	do kk1=1,nodf                                                                       
c			do kk2=1,nodf                                                                  
c				dtkm(kk1,kk2)=funf(kk1)*a_km(1,1)*funf(kk2)                                                                                                 
c			end do                                                                                  
c			end do
			PRODm = QUOT*DTIM
			CALL MSMULT(DTKm, PRODm,IDTKD, NODF)
			call matadd(M,ikp,dtkm,idtkd)
	       endif
!!=========================================================				
!!=========================================================				
									  
!!     Removed by SHunfu.Z 2009.12.08                                                                        	                                                                                         
				do 10 k=1,dof
				do 10 l=1,nodf
 				volf(k,l)=vol(k)*funf(l)*quot
c				if (uwra.lt.1e-2) then
					volf2(k,l)=vol(k)*funf(l)*quot
c				else
c					volf2(k,l)=vol(k)*funf2(l)*quot
c				endif
10				continue 
				call matadd(c,ic,volf,nodf)
				call matadd(c2,ic,volf2,nodf)
  4			continue !main gauss-loop
c******** Combine stiffness KM,KP and coupling matrix C to form ********
c********            element matrices KE and KD                 ********
      K_bulk   = e / (3.0d0 * (1.0d0 - 2.0d0 * v))
      coeff_c  = biot 
	coeff_cT = biot

			if (ns.eq.21) then
				continue
			endif
cc			call fmkdke(km,ikm,kp,ikp,c,ic,ke,ike,kd,ikd,dof,nodf,tot,
cc	1			theta,dtkQa,biot,c2) !Shunfu.Z 2010.07.21
          call fmkdke1(km, ikm, kp, ikp, c, ic, ke, ike, kd, ikd, dof,
     &                nodf, tot, theta, dtkQa, coeff_c, coeff_cT)
			call formkv( bk,ke,ike,g,n,tot )
			call formtb( pb,iloads,kd,ikd,g,nw,tot )
			call fmkdkeT(c,ic,kDT1,ike,kdT0,ikd,dof,nodf,                                      
     &						tot,dtkQTa,e,v,alfaT,theta)   !Shunfu.Z 2010.07.21
!!=========================================================			
!!============================LI=========================== 
            if(itype.eq.3) then     
              call fmkdke_M(c,ic,tran1,ike,tran0,ikd,dof,nodf,
     1                      tot,M,theta,biotm)
	      endif
!!=========================================================			
!!=========================================================
			 
5888          format(50e20.8)                                                                                                                      
			IF(NS.GT.nt) THEN !debug                                                            
				do in=1,20                                                                         
					temp_1(in)=0.0d0                                                                          
					temp_0(in)=0.0d0                                                                         
				end do                                                                             
				do in=17,20  
					temp_0(in)=(st0(nodep4(p,q,in-16))-val0)/2.0                                            
					temp_1(in)=(st1(nodep4(p,q,in-16))-val0)/2.0                                                                      
!					temp_0(in)=st0(nodep4(p,q,in-16))-val0                                           
!					temp_1(in)=st1(nodep4(p,q,in-16))-val0                                           
				end do 
                                                                            
				call mvmult(kdt1,ikd,temp_1,20,eload1)                                                  
				call mvmult(kdt0,ikd,temp_0,20,eload0)                                             
c	assemble thermal induced force to to global                                           
				call frmthermal(forcet1,eload1,g)                                                  
				call frmthermal(forcet0,eload0,g)                                                  
			end if 
			
!!=========================================================			
!!============================LI===========================
            if(itype.eq.3) then
            IF(NS.GT.nt) THEN
				do in=1,20 
					dump_1(in)=0.0d0 
					dump_0(in)=0.0d0  
				end do 
				do in=17,20 
                      inn=2*in-33
					dump_0(in)=dump(nodep(p,q,inn))
					dump_1(in)=dumpm(nodep(p,q,inn))
				end do
				call mvmult(tran1,ikd,dump_1,20,load_t1)  
				call mvmult(tran0,ikd,dump_0,20,load_t0) 
				call frmthermal(force_t1,load_t1,g)
				call frmthermal(force_t0,load_t0,g) 
			end if
	        endif
!!=========================================================
!!=========================================================			                                                                                  
  3		continue !main element-loop

8889		format(3000e20.8)
8887		format(i10,40i5)
          mt1 = 2*nxe+1
          mt2 = nye*(mt1+nxe+1)+1
	    mt3 = mt2+mt1-1
		do ii=1,r
			bkreac(ii)=bk(ii)
		enddo
		do i=1,n_bkfix
			if(iabkfix(i,1).EQ.3) then
				bk(nf(iabkfix(i,0),3))=1.D200
			endif
		enddo
	!in the above loop the pressure coefficient of nodes iabkfix(i,1)=3 is set to infinity
	!these nodes are all the boundary nodes except the on the centerlines.
	    call displa1(n_angbkfix,iangbkfix,angbkfix,
     *	n_bkfix,iabkfix,
     * loads,tload,bk,n,nxe,nye,d0,mcc,dofix,nvfix,
 	1              mt2,mt3,loads0,nbound,ns,nw)
c*********************** Reduce left hand side *************************PARAMETRING BEGINS
		do ii=1,n
			bkstore(ii)=bk(ii)
		enddo
		call banred(bk,n,nw)

		if (imesh.eq.2) then
			dr=3.d0*radius(nye+1)/nxe
		else
			dr=rw*pi/(2.d0*nxe)
		end if
		IF (ITERATOR.eq.1) then !LOOP3A
		IF ( pdrop .ge. sh3 ) THEN !LOOP3*****1*1*1**1*1*1*1*1*1*1*1*1*1*1*1*
              !BLOCK1
			bhp = 0.0D0
	        rep = 0.0D0 
			pwco  =0.d0
			pwci  =0.0d0
	        temp1=-pwci*dr
	        temp3=-pwci*dr
	        REP=-pwci
			DO 11 I=1,N
				force(i) = 0.0D0
11			continue
			continue
			temp1=0.0d0
			temp3=0.0d0
		ELSE  !IF ( pdrop .ge. sh3 ) THEN !LOOP3
c-----------------------------------------------------------------------------------------
			IF(ns.eq.0) then !LOOP4
				!BLOCK2
				temp1=-Sh1*dr/temp
				temp3=-Sh3*dr/temp
				if (IMESH.ge.4) pwci2=p0  ! multimesh
				pwci=p0
				pwco=p0
				rep=0.0d0
			ELSE !IF(ns.eq.0) then !LOOP4
				if(ndrill.ne.1) then !LOOP5
c**************negatival drilling
					IF(ns.le.nnstep) THEN !LOOP6
						!BLOCK3
						pwci=0.0D0
						if (IMESH.ge.4) pwci2=0.0D0  ! multimesh
						pwco=0.0d0
						temp1=(Sh1-p0)*dr/nnstep
						temp3=(Sh3-p0)*dr/nnstep
						rep=temp3/dr
					END IF !LOOP6
					IF(ns.GT.nnstep.AND.bhp.gt.pw) THEN !LOOP6.1
						!BLOCK4
						kaynow=0.d0
						do jj1=1,nxe
						do jj2=1,1
							ekay1=0.d0
							ekay2=0.d0
							if (imesh.eq.2) then
								eang=0
							else
								eang=(3.1415926/2.d0/nxe)*(jj1-0.5d0)
							end if
							iig=0
							do jj3=1,gp
							do jj4=1,gp
								iig=iig+1
								ekay1=ekay1+kay(jj1,jj2,iig,1)
								ekay2=ekay2+kay(jj1,jj2,iig,2)
							end do
							end do
							ekay1=ekay1/iig
							ekay2=ekay2/iig
							ekay=dsqrt(kay(jj1,jj2,iig,1)*dcos(eang)**2.d0
	1						+kay(jj1,jj2,iig,2)*dsin(eang)**2.d0)
							kaynow=kaynow+ekay
						end do
						end do
						kaynow=kaynow/nxe
						if(npr(ispoint).eq.1) then
							kaynowi=kaynow*psand
				pwci=-pdelta(ispoint)*(radius(2)-radius(1))/kaynowi
							pre0=pre0-pwci
						else
                pwci=(pdelta(ispoint)-pdelta(ispoint-1))/nstep(ispoint)
						end if

						if (IMESH.ge.4) then ! Multimesh
							if(npr2(ispoint).eq.1) then
					pwci2=-PDLTA2(ispoint)*(rad2(2)-rad2(1))/kaynowi
								pre02=pre02-pwci2
							else
                pwci2=(PDLTA2(ispoint)-PDLTA2(ispoint-1))/nstep(ispoint)
							end if
						end if

						if(nflowo.eq.1) then
	            kaynowo=kaynow*packerl*3.1415926d0*radius(nye+1)/0.5d0
	            pwco=-flowqo*(radius(nye+1)-radius(nye))/kaynowo
						else
							pwco  =0.d0
						end if
						temp1= -pwci*dr
						temp3= -pwci*dr
						rep  = -pwci
						IF(BHP-REP.LT.PW) then
							pwci=pw-bhp
							if (dabs(pwci).le.1.d-10) pwci=0.d0
							if (IMESH.ge.4) then
								pwci2=pw-bhp   ! multimesh
								if (dabs(pwci2).le.1.d-10) pwci2=0.d0
							end if
							temp1=-pwci*dr
							temp3=-pwci*dr
							REP=-pwci
						END IF
						!end of BLOCK4
					else !LOOP6.1
						IF(ns.GT.nnstep.AND.bhp.le.pw) THEN !LOOP7
							!BLOCK5
							pwco  =0.d0
							pwci  =0.0d0
							temp1=-pwci*dr
							temp3=-pwci*dr
							REP=-pwci
						endif !LOOP7
					END IF !LOOP6.1
				else !LOOP5
c******* un-negatival drilling
					IF(ns.eq.1) THEN !LOOP6.2
c****when it is un-negative and temp1.ne.temp3, let temp1 and temp3 
c    be reduced to same in first load step
c  set first step loading for cases sh1=sh3, sh1>sh3 and sh1<sh3,
c   reset pdelta in these cases   Sept26,2000 EQ
						if(sh1.eq.sh3) then !LOOP7.1
							!BLOCK6
							pwci=sh3-p0-uload
							pwco=0.0d0
							temp1=((sh1-sh3)+uload)*dr
							temp3=(uload+0.d0)*dr
							rep=temp3/dr
 							pdelta(1)=sh3-uload
							if (IMESH.ge.4) then
								pwci2=sh3-p0-uload  !multimesh
 								PDLTA2(1)=sh3-uload
							 end if
						else !LOOP7.1
							if (sh1.ge.sh3) then !LOOP8
								!BLOCK7
								pwci=sh3-p0
								pwco=0.0d0
								temp1=(sh1-sh3)*dr
								temp3=0.d0
								rep=temp3/dr
 								pdelta(1)=sh3
								if (IMESH.ge.4) then
									pwci2=sh3-p0  !multimesh
 									PDLTA2(1)=sh3
								end if
							else !LOOP8
								!BLOCK8
								pwci=sh1-p0
								pwco=0.0d0
								temp1=(sh3-sh1)*dr
								temp3=0.d0
								rep=temp3/dr
 								pdelta(1)=sh1
								if (IMESH.ge.4) then
									pwci2=sh1-p0  !multimesh
 									PDLTA2(1)=sh1
								end if
							end if !LOOP8
						endif !LOOP7.1
					ELSE !LOOP6.2
						!BLOCK9 & BLOCK10
						kaynow=0.d0
						do jj1=1,nxe
						do jj2=1,1
							ekay1=0.d0
							ekay2=0.d0
							if (imesh.eq.2) then
								eang=0
							else
								eang=(3.1415926/2.d0/nxe)*(jj1-0.5d0)
							end if
							iig=0
							do jj3=1,gp
							do jj4=1,gp
								iig=iig+1
								ekay1=ekay1+kay(jj1,jj2,iig,1)
								ekay2=ekay2+kay(jj1,jj2,iig,2)
							end do
							end do
							ekay1=ekay1/iig
							ekay2=ekay2/iig
						ekay=dsqrt(kay(jj1,jj2,iig,1)*dcos(eang)**2.d0
	1				             +kay(jj1,jj2,iig,2)*dsin(eang)**2.d0)
							kaynow=kaynow+ekay
						end do
						end do
						kaynow=kaynow/nxe

						if(npr(ispoint).eq.1) then !LOOP7.2
							!continuation of BLOCK9
							kaynowi=kaynow*psand
				pwci=-pdelta(ispoint)*(radius(2)-radius(1))/kaynowi
							pre0=pre0-pwci
							if (IMESH.ge.4) then  ! multimesh
					pwci2=-PDLTA2(ispoint)*(rad2(2)-rad2(1))/kaynowi
								pre02=pre02-pwci2
							end if
						else !LOOP7.2
							!continuation of BLOCK10
                pwci=(pdelta(ispoint)-pdelta(ispoint-1))/nstep(ispoint)
							if (IMESH.ge.4) then
             pwci2=(PDLTA2(ispoint)-PDLTA2(ispoint-1))/nstep(ispoint) !pwci (The original Dr. Qian is pwci)
							end if
						end if !LOOP7.2
					!continuation of BLOCK9 & BLOCK10
						if(nflowo.eq.1) then
	            kaynowo=kaynow*packerl*3.1415926d0*radius(nye+1)/0.5d0
	            pwco=-flowqo*(radius(nye+1)-radius(nye))/kaynowo
						else
							pwco  =0.d0
						end if
						temp1= -pwci*dr
						temp3= -pwci*dr
						rep  = -pwci
C RECALCULATE REP AT THE LAST POINT
						IF(BHP-REP.LT.PW) then
							pwci=pw-bhp
							if (dabs(pwci).le.1.d-10) pwci=0.d0
							temp1=-pwci*dr
							temp3=-pwci*dr
							REP=-pwci
						END IF 
c%
					END IF !LOOP6.2
				end if !LOOP5

				IF(bhp.LE.pw) then 
C UNLOAD !!stop ****** note:  only limit pwci*********************
					pwci=0.0D0
					temp1=0.0D0
					temp3=0.0D0
					rep=0.0D0
				END IF
			End if !LOOP4
		END IF !LOOP3

	    pdrop   = pdrop    + rep
	    bhp     = bhp      - rep
	    drawdown= drawdown + rep
		ENDIF !LOOP3A

          call nulvec(force,n)
		call nulvec(force1,n)
4998		format(i10,5f20.9,a)

      	call ma5_load(n1_deload,i1elemx,i1elemy,i1elm_deload,
     *   ang1_elm_deload,
     *   n3_deload,i3elemx,i3elemy,i3elm_deload,
     *   ang3_elm_deload,
     *   n2_deload,i2elemx,i2elemy,i2elm_deload,
     *   ang2_elm_deload,
     *   temp1,temp3,shv,pwci,pwci2,pwco,force,isamp,nk,ns,
	1           ider,dr,nxe,nye,x,y,nodep,nodeg,tot,icoord,icordf
     1           ,ijac,ijac1,geometrytype,force1,kay,DTIM,NT,igrav
     1	,if_pass_body_load,rho,if_boussinesq)
		
		if (if_GRAV_BC_pass.eq.0) then !if_AA1
		IF(ngrav.NE.0.AND.NS.EQ.0)THEN !IF(ngrav.NE.0)THEN, if_AA2
		
		if (if_grav_wellbore.eq.0) then !if_AA3
			DO i=1,n_bkfix
				if (if_boussinesq.eq.1) then
					rho1=rho*(1.0
     1					-alfa_F*(st1(i_8_4_nodes(iabkfix(i,0)))-Tout))
				else
					rho1=rho
				endif
				!IF(iabkfix(i,1).EQ.3) THEN 
				IF(iabkfix(i,1).EQ.3.AND.iabkfix(i,2).EQ.256) THEN ! pressure boundary for the outer boundary
					if (imesh.eq.1.or.imesh.eq.3) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
     1		+rho1*9.8d0*1.0d-6*(0.0-y(iabkfix(i,0)))
					elseif(imesh.eq.5) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
	1	+rho1*9.8d0*1.0d-6*(0.0-y(iabkfix(i,0)))
					else
					endif
				ENDIF

			ENDDO
		elseif (if_grav_wellbore.eq.1) then !if_AA3
			DO i=1,n_bkfix
				IF(iabkfix(i,1).EQ.3) THEN
				
				if (if_boussinesq.eq.1) then
					rho1=rho*(1.0
     1					-alfa_F*(st1(i_8_4_nodes(iabkfix(i,0)))-Tout))
				else
					rho1=rho
				endif
 
				!IF(iabkfix(i,1).EQ.3.AND.iabkfix(i,2).EQ.256) THEN ! pressure boundary for the outer boundary
				if (imesh.eq.1.or.imesh.eq.3) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
     1		+rho1*9.8d0*1.0d-6*(0.0-y(iabkfix(i,0)))
					elseif(imesh.eq.5) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
	1	+rho1*9.8d0*1.0d-6*(0.0-y(iabkfix(i,0)))
					else
					endif
				ENDIF
			ENDDO
		endif !if_AA3
		ENDIF !if_AA2


		IF(ngrav.NE.0.AND.NS.gt.0.and.if_boussinesq.eq.1 )THEN !if_AA2p
		if (if_grav_wellbore.eq.0) then !if_AA3p
			DO i=1,n_bkfix
			rho1=rho*(-alfa_F*(st1(i_8_4_nodes(iabkfix(i,0)))
     1						  -st0(i_8_4_nodes(iabkfix(i,0)))))
				!IF(iabkfix(i,1).EQ.3) THEN 
				IF(iabkfix(i,1).EQ.3.AND.iabkfix(i,2).EQ.256) THEN ! pressure boundary for the outer boundary
					if (imesh.eq.1.or.imesh.eq.3) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
     1		+rho1*9.8d0*1.0d-6*(radius(nye0+1)-y(iabkfix(i,0)))
					elseif(imesh.eq.5) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
	1	+rho1*9.8d0*1.0d-6*(3.0d0*radius(nye0+1)-y(iabkfix(i,0)))
					else
					endif
				ENDIF

			ENDDO
		else !if_AA3p
			DO i=1,n_bkfix
				IF(iabkfix(i,1).EQ.3) THEN
			rho1=rho*(-alfa_F*(st1(i_8_4_nodes(iabkfix(i,0)))
     1						  -st0(i_8_4_nodes(iabkfix(i,0)))))

				!IF(iabkfix(i,1).EQ.3.AND.iabkfix(i,2).EQ.256) THEN ! pressure boundary for the outer boundary
					if (imesh.eq.1.or.imesh.eq.3) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
     1		+rho1*9.8d0*1.0d-6*(radius(nye0+1)-y(iabkfix(i,0)))
					elseif(imesh.eq.5) then
				force(nf(iabkfix(i,0),3))=force(nf(iabkfix(i,0),3))
	1	+rho1*9.8d0*1.0d-6*(3.0d0*radius(nye0+1)-y(iabkfix(i,0)))
					else
					endif
				ENDIF
			ENDDO
		endif !if_AA3p
		ENDIF !if_AA2p

		ENDIF !if_AA1p	
c**********************************************************************
c    Time Stepping Loop
c-----------------------------------------------------------------------
c    <Loads> = 0 for the first loop step, but = the displacement from 
c    the previous loading for the rest of loading steps, to produce
c    an internal virtual force <ans>. 
c-----------------------------------------------------------------------
c
c*********** last step displacement should affect next step ************
		call bantml(pb,iloads,tload,ans,n,nw)

		call nulvec(Tloads1,nn4)                                                          
		call nulvec(Tloads0,nn4)                                                           
		k=0                                                                                                                                                                   
		do i = 1, nn4 
!			if(nf(i,3) .ne. 0) then                                                     
!				k=k+1                                                                        
!				Tloads1(nf(i,3))=st1(k)-val0 !st0(k)  ! T1 temperature                   
!				Tloads0(nf(i,3))=st0(k)-val0 !st0(k)  ! T0 temperature                    
!			endif                                                                   
			Tloads1(i)=st1(i)-val0 !st0(k)  ! T1 temperature                   
			Tloads0(i)=st0(i)-val0 !st0(k)  ! T0 temperature                                                                        
		end do                                                                                                            
	call nulvec(forcereac,40000)
		do ii=1,n
			forcereac(ii)=ans(ii) !!loads from previous step
		enddo
		do ii=1,nn8
		do jj=1,3
			if(nf(ii,jj).ne.0) then
				if(jj.ne.3) then
			forcereac(nf(ii,jj))=forcereac(nf(ii,jj))+force(nf(ii,jj))
			forcereac(nf(ii,jj))=forcereac(nf(ii,jj))+tforce(nf(ii,jj))
				else		
				endif
			endif
		enddo
		enddo

		do i=1,n_bkfix   !!pressure boundary                                                                        
			if(iabkfix(i,1).EQ.3) then                                                              
				ans(nf(iabkfix(i,0),3))=0.d0 
				forcet1(nf(iabkfix(i,0),3))=0.d0 
				forcet0(nf(iabkfix(i,0),3))=0.d0 
!!=========================================================			
!!============================LI===========================
                 if(itype.eq.3) then
				force_t1(nf(iabkfix(i,0),3))=0.d0
				force_t0(nf(iabkfix(i,0),3))=0.d0
	           endif
!!=========================================================			
!!=========================================================				                                                          
			endif                                                                                   
		enddo
	
		do 39 i = 1,n
			if (iterator.eq.1) then
				tforce(i)= tforce(i)+ force(i)
			endif
			ans(i)   = ans(i)   + tforce(i)
39		continue
			
			call vecadd(ans,tbdyld,ans,n)
			call vecadd(ans,forcet1,ans,n)  ! kevin 2006-06-011                            
			call vecadd(ans,forcet0,ans,n)  ! kevin 2006-06-011
!!=========================================================			
!!============================LI===========================
              if(itype.eq.3) then
			call vecadd(ans,force_t1,ans,n)                             
			call vecadd(ans,force_t0,ans,n)
	        endif
!!=========================================================			
!!=========================================================			  
			
			call vecadd(forcereac,tbdyld,forcereac,n)                                                           
			call vecadd(forcereac,forcet1,forcereac,n)                             
			call vecadd(forcereac,forcet0,forcereac,n)
!!=========================================================			
!!============================LI===========================
              if(itype.eq.3) then
			call vecadd(forcereac,force_t1,forcereac,n)                             
			call vecadd(forcereac,force_t0,forcereac,n)
	        endif
!!=========================================================			
!!=========================================================			 
		do i=1,n_bkfix
			if(iabkfix(i,1).EQ.3) then
				ans(nf(iabkfix(i,0),3))=ans(nf(iabkfix(i,0),3))
     $			*bkstore(nf(iabkfix(i,0),3))
			endif
		enddo

    	    itd = 0
901	    itd = itd + 1
c
          isub = 0 
          iter = 0
200       iter = iter + 1
          iz = 1
207		format('It is running!: Iter->',i3,i4,e9.2,4e9.2/)
		do i=1,n_bkfix                                                                           
			if(iabkfix(i,1).EQ.3) then ! pressure boundary
				bdylds(nf(iabkfix(i,0),3))=bdylds(nf(iabkfix(i,0),3))                                                              
     *		*bkstore(nf(iabkfix(i,0),3)) 
			
			endif                                                                                    
		enddo
c**********************************************************************************
   	    call vecadd(ans, bdylds, loads, n)
c          call displa2ori(loads,tload,bk,n,nxe,nye,d0,mcc,dofix,nvfix,
c	1     mt2,mt3,loads0,nbound,ns)
          call displa2(n_angbkfix,iangbkfix,angbkfix,                                    
     *		n_bkfix,iabkfix,                                                                  
     *		loads,tload,bk,n,nxe,nye,d0,mcc,dofix,nvfix,                                      
 	1		mt2,mt3,loads0,nbound,ns,nw)
		IF(NS.EQ.21) THEN
			CONTINUE
		ENDIF
9998		format(i10,3e20.12)
201       call bacsub(bk,loads,n,nw)    !!!!!=====solution=====
1		format( 1x, 9e10.3)
c---------trimming loads--------------------------------------------------------
		do ii1=1,nn
			if(nf(ii1,1).ne.0) then
		if (dabs(loads(nf(ii1,1))).lt.1.d-100) loads(nf(ii1,1))=0.d0
 			end if
			if(nf(ii1,2).ne.0) then
	    if (dabs(loads(nf(ii1,2))).lt.1.d-100) loads(nf(ii1,2))=0.d0
			end if
		end do
c------output according to r direction at first and t direction at secound---------	     
		IF ( ns .eq. 0 ) then !if-b1
			ntt=1
			if (ndrill.ne.0) ntt=nnstep
			DO iii=1,ntt !do-B1
				do i2=1,nxe+1 !do-B2
				do i1=1,nyl !do-B3
					nn=i_vnodes(i1,i2)                                                                   
					I_NODE=i_v4nodes(I1,I2)
					if(nn.GT.0) then 
						if( nf(nn,1).ne.0) then
						  dumux(nn)=loads(nf(nn,1))
						else
						  dumux(nn)=0.d0
						end if
						if( nf(nn,2).ne.0) then
						  dumuy(nn)=loads(nf(nn,2))
						else
						  dumuy(nn)=0.d0
						end if
						if( nf(nn,3).ne.0) then
						  dump(nn)=loads(nf(nn,3))
						else
						  dump(nn)=0.d0
						end if
 						agn=get_ang(i2,imesh,nxe)
						dumu(nn)  = dumux(nn)*dcos(agn*pi/180.D0)
	1		                + dumuy(nn)*dsin(agn*pi/180.D0)
						dumut(nn) = dumuy(nn)*dcos(agn*pi/180.D0)
	1		                - dumux(nn)*dsin(agn*pi/180.D0)
		  write(13,6013)coh,ptime,radius(i1),agn,dumux(nn)*100.D0, ! by Ma.Z
 	1	 dumuy(nn)*100.d0,dumu(nn)*100.D0,dumut(nn)*100.D0,dump(nn), !P_new(I_NODE)
	1 drawdown,bhp,sandp,con,conq,conqf,ratew,qo,qw,
  	2	 radiusp*100.D0,sradius*100.D0,st1(I_NODE),A_SAT(I_NODE)  !st1 is for temperature
	     
					endif
				end do !do-B3
				end do !do-b2
c***************record initial displacement loads0 for initial displacement 
c     boundary condition
				if (Iterator.eq.n_iterator) then 
				do ii1=1,n
					tload(ii1)=loads(ii1)
					loads0(ii1)=loads(ii1)
				end do
				endif
c*************print stress when ns=0*******8sept6,40000
				do p=1,nxe !do-B4
				do q=1,nye !do-B5
					if(q.LE.nye0) then
						radiusn = radius(q) 
						radiusg = radius(q) + (radius(q+1)-radius(q))/2.D0
					else                                                                                 
						radiusn = radius(nye+1-q)                                                           
						radiusg = radius(nye+1-q)+ (radius(nye+1-q+1)                                       
     *					- radius(nye+1-q))/2.D0                                                     
					endif
 					agg = get2_ang(p,imesh,nxe)
					ng    = gp**2.D0
					srp   = 0.d0
					stp   = 0.D0
					sxg   = 0.D0
					syg   = 0.D0
					sxyp  = 0.D0
					szgg  = 0.D0
					exg   = 0.D0
					eyg   = 0.D0
					exyg  = 0.D0
					erp   = 0.0D0
					etp   = 0.D0
					EG    = 0.D0
					porop = 0.0D0
					kxp   = 0.0D0
c***                                                                                      
					cqcom=0.d0                                                                           
c*** 
					do lg = 1, NG !do-B6
						srp   = srp   + sr(p,q,lg)/4.D0
						stp   = stp   + st(p,q,lg)/4.D0
						sxg   = sxg   + sx(p,q,lg)/4.D0
						syg   = syg   + sy(p,q,lg)/4.D0
						sxyp  = sxyp  + sxy(p,q,lg)/4.D0
						szgg  = szgg  + sz(p,q,lg)/4.D0
						exg   = exg   + ex(p,q,lg)*100.0D0/4.D0
						eyg   = eyg   + ey(p,q,lg)*100.0D0/4.D0
						exyg  = exyg  + exy(p,q,lg)*100.D0/4.D0
						erp   = erp   + er(p,q,lg)*100.0D0/4.D0
						etp   = etp   + et(p,q,lg)*100.D0/4.D0
						EG    = EG    + EN(p,q,lg)/4.D0
						porop = porop + poro(p,q,lg)*1.D0/4.D0
						kxp   = kxp   + kxy(p,q,lg,1)*1.D0/4.D0
						cqcom=cqcom+qcom(p,q,lg)/4.d0                                                       
						RKE=dsqrt(RKE_X*dcos(agg)**2.d0
	1						+RKE_Y*dsin(agg)**2.d0)
           	if (Iterator.eq.n_iterator) then 
		  write(14,6015)coh,TIME_CAL,radiusg,agg,sr(p,q,lg),st(p,q,lg),
	1 sx(p,q,lg),sy(p,q,lg),sxy(p,q,lg),sz(p,q,lg),
	1	 ex(p,q,lg),ey(p,q,lg),volstr(p,q,lg),
	1 er(p,q,lg),et(p,q,lg),qcom(p,q,lg),
	1 poro(p,q,lg)*1.D0/porosity*100.D0,
	1   kxy(p,q,lg,1)*1.D0/RKE*100.D0, nelem(p,q,lg),sandg(p,q,lg),
	1   sw(p,q,lg),
	1 PW_GRAD(p,q,lg,1),PW_GRAD(p,q,lg,2),
	1 PO_GRAD(p,q,lg,1),PO_GRAD(p,q,lg,2)  
						snelem=snelem+nelem(p,q,lg)/4.d0
						ssandg=ssandg+sandg(p,q,lg)/4.d0   
			endif   
					end do !do-B6

					TIME_CAL=iii-1
					write(12,6015)coh,TIME_CAL,radiusg,agg,srp,stp,
	1sxg,syg,sxyp,szgg,
	1	 exg,eyg,volstr(p,q,4),erp,etp,cqcom*100.d0,porop/porosity*100.D0,
	1   kxp/RKE*100.D0, nelem(p,q,4),sandg(p,q,4),sw(p,q,1)
				end do !do-B5
				end do !do-B4
			END DO !do-B1
c*****************************************************
			continue		
			go to 811
		Endif !if-B1, IF ( ns .eq. 0 )
          call NULVEC(bdylds,n)
c************************ Iteration tolerance **************************
c        Check Convergence
 	    call checon1(loads, oldlds, n, tol, iz,ns,iter,il)
          if( iter.eq.1 ) iz=0
c********************** calculate increamence of displacement**********
		do ii1=1,n
	        dloads(ii1)=loads(ii1)-tload(ii1)
		end do
		do ii1=1,nn4
			dloadst(ii1)=(tloads1(ii1)-tloads0(ii1))/1.0d0 ! Kevin
		end do 
c*********************************Plasticity begins*************************************************
c************************** Inspect all the elements *******************
		continue
		do 7  p=1,nxe !plasticity-element-loop
		do 7  q=1,nye
			call NULVEC(bload,dof)
			call Nulvec(seld,nodf)

c******avoid to call tungeo again and again*****************************
			do in=1,tot
				g(in)=nodeg(p,q,in)
			end do
			do in=1,icoord
				coord(in,1)=x(nodep(p,q,in))
				coord(in,2)=y(nodep(p,q,in))
			end do
			do in=1,icordf
				coordf(in,1)=x(nodep(p,q,(2*in-1)))
				coordf(in,2)=y(nodep(p,q,(2*in-1)))
			end do
c*************************************************************************
			do 9 m=1,dof+nodf ! dof+nodf=20
				if(g(m).eq.0) THEN
					eld(m)=0.0
!					eldt(m)=0.0 ! kevin
				ENDIF
		!! temperature_add
		!! temperature_add
				if(g(m).ne.0) THEN
					eld(m)=dloads(g(m))
!					eldt(m)=dloadst(g(m)) !kevin
				ENDIF

9			CONTINUE
!!Shunfu.Z 2010.06.04
			do m=1,nodf
				eldt(m)=dloadst(nodep4(p,q,m))  !kevin
			enddo
!!Shunfu.Z 2010.06.04
c*************** Form Gaussian integration points **********************
			gp=2
			call gauss(samp,gp)

			ig=0
			do 15 i = 1,gp
			do 15 j = 1,gp
				ig=ig+1
				call nulvec(delso,4)
c------------ recalculate elastic stress-strain matrix for e(p,q,ig)
				if (geometrytype .eq. 1) Then
cc					call fmdeps(dee,idee,EN(p,q,ig),v)
                      call fmdeps6(dee, idee, p, q)
					h = 3
				Else
c					call fmdrad(dee,idee,EN(p,q,ig),v)
					call fmdrad(dee,4,EN(p,q,ig),v)
					h = 4
				end if
c-------------------------------
				call fmquad(der,ider,fun,samp,gp,i,j,1)
c            call null(km,dof,dof)
c            call null(kp,nodf,nodf)
				call matmul(der,ider,coord,icoord,jac,ijac)
				call twoby2(jac,ijac,jac1,ijac1,det)
				call matmul(jac1,ijac1,der,ider,deriv,nod)
c	open(3467,file='der.t19')
c	do ii=1,2
c	write(3467,3468) (deriv(ii,jj),jj=1,8)
c	enddo
c 3468 format(8f20.9)
c	close(3467)

c	open(3467,file='uu.t19')
c	do ii=1,8
c	write(3467,3468) eld(2*ii)
c	enddo
c	close(3467)
!!Shunfu.Z 2010.01.22
c       call null(bee,h,dof)
				call null(bee,4,16)
!!Shunfu.Z 2010.01.22
c
c**************************** Axisymmetric strain **********************
c
				IF( geometrytype.eq.2 ) then
!!Shunfu.Z 2010.06.09
					call fmbrad(bee,4,deriv,ideriv,fun,coord,
     +                  icoord,vol,sum,nod)
!!Shunfu.Z 2010.06.09
!					call fmbrad(bee,ibee,deriv,ideriv,fun,coord,
!     +                  icoord,vol,sum,nod)
				ELSE
c************************************* Plane strain *********************
					call formb(bee,ibee,deriv,ideriv,nod)
				ENDIF
c*************get pore pressure at each gauss point  April 24
!!Shunfu.Z 2010.06.04
				call formln(derf,iderf,funf,samp,gp,i,j)
!!Shunfu.Z 2010.06.04
				if (ns.eq.21) then
					continue
				endif
				eldf=0.d0
				eldft=0.0d0
 				do iik=1,nodf
 					eldf=eldf+funf(iik)*eld(dof+iik)
! 					eldf=eldf+funf(iik)*eld(dof+iik)
c	eldft=eldft+funf(iik)*(eldt(iik)-val0)*qcomT(p,q,ig)*e/(1.0d0-2.0d0*v)
	eldft=eldft+funf(iik)*(eldt(iik))*alfaT*e/(1.0d0-2.0d0*v)
!	eldft=0.0d0
!	eldft=eldft-funf(iik)*(eldt(iik))*qcomT(p,q,ig)*e/(1.0d0-2.0d0*v)
 				end do

c************ Incremental strain are EPS and Elastic stresses are sigma **
c
c     Note: both displacement and strain are incremental, but the stresses
c           used are total for plastic yielding check.
c
	if (ns.eq.1) then
		continue
	endif
				call nulvec(eps,h)
				call nulvec(sigma,h)
				if (geometrytype.eq.1) then
					call matran(bt, ibt, bee,ibee)
					call mvmult(bee,ibee,eld,dof,eps)
					eps(4)=0.d0
					call mvmult(dee,idee,eps,h,sigma)	
				else
					call matran(bt, ibt, bee,4)
					call mvmult(bee,4,eld,dof,eps)
					call mvmult(dee,4,eps,h,sigma)
				endif


!	            sigma(1)=sigma(1)-eldf +eldft   ! kevin
!	            sigma(2)=sigma(2)-eldf +eldft   ! kevin
!                 sigma(4)=sigma(4)-eldf +eldft   ! kevin
					sigma(1)=sigma(1) + eldft   ! kevin
					sigma(2)=sigma(2) + eldft   ! kevin
					sigma(4)=sigma(4) + eldft   ! kevin
				call nulvec(elso,4)
				if (geometrytype.eq.1) sigma(4)=v*(sigma(1)+sigma(2))


c*************  if temp1,temp3,pwci = 0, deal with it as elastic process
				if (iter.eq.1.and.temp1.eq.0.d0) go to 101
				if (iter.ne.1.and.temp1.eq.0.d0) then
					iz=1
					go to 101
				end if
c                                                                                         *
c       Defining the initial stresses, but do make sure that the principal stresses at    *
c       each point along the quarter circular are approrpiately defined as the principal  *
c       stresses will not be along the X-Y direction, instead they should be along the    *
c       radial and tangential directions, respectively.                                   *
c       Note: the minimum principal in-situ stress, sh3 is along the X-axis               *
c                                                                                         *
c-----------------------------------------------------------------------------------------*
				IF ( ns .eq. 1 ) Then
			call stressi(sz0,sh1,sh3,biot,p0,szg,sx(p,q,ig),sy(p,q,ig)
	1		,sxy(p,q,ig),sz(p,q,ig),sr(p,q,ig),st(p,q,ig),ex(p,q,ig)
	1		,ey(p,q,ig),exy(p,q,ig),ez(p,q,ig),er(p,q,ig),
	1		et(p,q,ig),dee,fun,coord,h,icoord,ngrav,nye,imesh,nxe,p,
     1        radius,geometrytype,e,v)  
 				ENDIF
c
				s1 = sigma(1) +  sx(p,q,ig) 
				s2 = sigma(2) +  sy(p,q,ig)
				s3 = sigma(4) +  sz(p,q,ig)
				s4 = sigma(3) + sxy(p,q,ig)
	goto 101
!	include 'plasticity.for'

c***********end of stress correct when s1<0*****************************
cc
c******************* Check Mohr-Coulomb Yielding ***********************
c
c     If fnew for total effective stress > 0 no plastic yielding
c
c-----------------------------------------------------------------------
				fac = 0.0
				IF ( failuretype.eq. 2 ) then !! 3-D C-M Yielding
c **** erq (1) ****
					call transform1(s1,s2,s4,sm3,sm1,angle)
c ****
c15	      if ( sm1 .lt. -ucs .or. sm3 .lt. -ucs ) then 
					if ( sm1 .lt. -ts .or. sm3 .lt. -ts ) then 
						fnew = 100000.0D0 
						go to 8001
					end if
					call  invar(s1,s2,s3,s4,amean,dsbar,alode)
					call mocouf(PHI,COH,amean,dsbar,alode,fnew)
c------------------------------------------------
!!Shunfu.Z 2010.02.03
					IF (dabs(fnew).le.0.00001D0.or.fnew.gt.0.00001D0) then
						go to 101
					endif
!!Shunfu.Z 2010.02.03
c              IF (dabs(fnew).le.0.00001D0.or.fnew.gt.0.001D0) go to 101
c      IF (dabs(fnew).le.0.00001D0*1.0d-3.or.fnew.gt.0.001D0*1.0d-3)
c     $ go to 101
					nelem(p,q,ig)=1
c******sept28 EQ
					npelasti(ns)=1
c
c 	          IF ( q .gt. lq(p) ) THEN
c	               lq(p) = q
c	          End IF
c'multimesh                                                                               
 					if (q.le.nye0) then                                                             
						IF ( q .gt. lq(p) ) THEN                                                             
							lq(p) = q                                                                 
						End IF                                                                         
					else                                                                                   
						if (imesh.eq.4) then                                                           
		  					IF ( q .lt. lq2(p) ) THEN                                                       
								lq2(p) = q                                                             
							End IF                                                                      
						elseif (imesh.eq.5) then                                                       
		  					IF ( q .lt. lq2(p) ) THEN                                                       
								lq2(p) = q                                                             
							End IF                                                                      
						end if                                                                         
					end if
				ELSE if ( failuretype .eq. 3) then !! 2-D C-M Yielding
	!! Added By Shunfu.Z 2009.12.01
					call transform1(s1,s2,s4,sm3,sm1,angle)
c15	      if ( sm1 .lt. -ucs .or. sm3 .lt. -ucs ) then 
					if ( sm1 .lt. -ts .or. sm3 .lt. -ts ) then 
						fnew = 100000.0D0 
						go to 8001
					end if
					call MCY(phi,coh,s1,s2,s4,fnew)
c------------------------------------------------
	!!Shunfu.Z 2010.02.03
					IF (dabs(fnew).le.0.00001D0.or.fnew.gt.0.00001D0) then
						go to 101
					endif
	!!Shunfu.Z 2010.02.03
c             IF (dabs(fnew).le.0.00001D0.or.fnew.gt.0.001D0) go to 101
					nelem(p,q,ig)=1
c 	          IF ( q .gt. lq(p) ) THEN
c	               lq(p) = q
c	          End if
					if (q.le.nye0) then                                                              
 						IF ( q .gt. lq(p) ) THEN                                                      
							lq(p) = q   !! bottom area                                                              
						End if                                                                         
					else                                                                             
						if (imesh.eq.4) then                                                           
 							IF ( q .lt. lq2(p) ) THEN                                                  
								lq2(p) = q                                                             
							End if                                                                      
						elseif (imesh.eq.5) then                                                       
 							IF ( q .lt. lq2(p) ) THEN                                                  
								lq2(p) = q                                                             
							End if                                                                      
						end if                                                                         
					end if 
!!Shunfu.Z 2010.04.16
				ELSE if ( failuretype .eq.1) then !! D-P Yielding
					call transform1(s1,s2,s4,sm3,sm1,angle)
					if ( sm1 .lt. -ts .or. sm3 .lt. -ts ) then 
						fnew = 100000.0D0 
						go to 8001
					end if
					call  invar(s1,s2,s3,s4,amean,dsbar,alode)
					CALL DRUCKERPRAGER(PHI,Coh,amean,dsbar,Fnew)
c------------------------------------------------
!!Shunfu.Z 2010.02.03
					IF (dabs(fnew).le.0.00001D0.or.fnew.gt.0.00001D0) then
						go to 101
					endif
!!Shunfu.Z 2010.02.03
c              IF (dabs(fnew).le.0.00001D0.or.fnew.gt.0.001D0) go to 101
c      IF (dabs(fnew).le.0.00001D0*1.0d-3.or.fnew.gt.0.001D0*1.0d-3)
c     $ go to 101
					nelem(p,q,ig)=1
c******sept28 EQ
					npelasti(ns)=1
c
c 	          IF ( q .gt. lq(p) ) THEN
c	               lq(p) = q
c	          End IF
c'multimesh                                                                               
 					if (q.le.nye0) then                                                             
						IF ( q .gt. lq(p) ) THEN                                                             
							lq(p) = q                                                                 
						End IF                                                                         
					else                                                                                   
						if (imesh.eq.4) then                                                           
		  					IF ( q .lt. lq2(p) ) THEN                                                       
								lq2(p) = q                                                             
							End IF                                                                      
						elseif (imesh.eq.5) then                                                       
		  					IF ( q .lt. lq2(p) ) THEN                                                       
								lq2(p) = q                                                             
							End IF                                                                      
						end if                                                                         
					end if
!!Shunfu.Z 2010.04.16
				ELSE
c
c********************** Check von Mises Yielding  **********************
c
					dsbar = dsqrt(0.5D0*((s1-s2)**2+(s2-s3)**2+(s3-s1)**2
	1					+ 6.D0*s4**2))
					IF (dsbar.lt.sbary) go to 101
					nelem(p,q,ig)=1
c         WRITE(T_OUT,*) 'Warning: This is Option which is not available!'
c         WRITE(T_OUT,*) 'failure', failuretype
					!commented by Bashar WRITE(T_OUT,*) 'Warning: This is Option which is 
!commented by Bashar      $not available!'                
					!commented by BasharCALL WRTF(T_OUT)                                                               
					!commented by Bashar WRITE(T_OUT,*) 'failure', failuretype                                           
					!commented by Bashar CALL WRTF(T_OUT) 
					Write(2,*) 'Warning: This is Option which is not available!'
					write(2,*) 'failure', failuretype
				ENDIF
C*********STRESS COMPONENT FROM PREVIOUS ITERATION 
8001				t1 =  sx(p,q,ig)
				t2 =  sy(p,q,ig)
				t3 =  sz(p,q,ig)
				t4 = sxy(p,q,ig)

c
c************** Form plastic dsigma from Mohr-Coulomb ******************
c
c     If f for initial stress < 0, we need to adjust the total stress 
c     (previously total stress)  under the yielding surface
c     as f > 0.0 indicates that the element has previously yielded
c
c-----------------------------------------------------------------------

!			IF ( failuretype .gt. 2) then
!!Shunfu.Z 2010.04.16
 				IF ( failuretype .gt. 3) then
	!!Shunfu.Z 2010.04.16
c**************** Form plastic dsigma from von Mises *******************
					call fmdpl(dpl,idpl,dee,idee,t1,t2,t3,t4,dsbar,
	1  						sbary,EN(p,q,ig),v)
c            WRITE(T_OUT,*) 'Warning: This is Option which is not available!'
				!commented by Bashar WRITE(T_OUT,*) 'Warning: This is Option which is not 
     $			!commented by Bashar available!'                
					!commented by BasharCALL WRTF(T_OUT)
				Write(2,*) 'Warning: This is Option which is not available!'
				ELSE
c******* Form plastic dsigma from Mohr-Coulomb and Drucker-Prager ******
					if ( failuretype .eq. 2) then
						call invar(t1,t2,t3,t4,amean,dsbar,alode)
						call mocouf(PHI,COH,amean,dsbar,alode,f)
					else if ( failuretype .eq. 3) then
						call MCY(phi,coh,t1,t2,t4,f)
				!!Shunfu.Z 2010.04.16
					else if ( failuretype .eq. 1) then
						CALL DRUCKERPRAGER(PHI,COH,amean,dsbar,f)
				!!Shunfu.Z 2010.04.16
					end if
					if (dabs(f).lt.1.0d-10) f = 0.d0
202					fac = fnew/(fnew-f)
!!Shunufu.Z 2010.03.10
					if (f.lt. 0.0D0 .and. fac .gt.  1.D0 ) fac = 1.0D0 
					if (f.lt. 0.0D0 .and. fac .lt. -1.D0 ) fac = 1.0D0
!!Shunufu.Z 2010.03.10
c				if ( f .lt. 0.0D0 .and. fac .gt.  50.D0 ) fac = 1.0D0 
c				if ( f .lt. 0.0D0 .and. fac .lt. -50.D0 ) fac = 1.0D0 

c*************************************************************************************
c   Note: sr is the previous stress before plastic yielding, and sigma are the
c         incremental stresses in addition to sr. Sr can also exceed the elastic
c         limit, therefore must be checked and trimmed regularly.
c
c*************************************************************************************
 					if (f.le.0.d0) then
 					   tt1=t1
					   tt2=t2
					   tt3=t3
					   tt4=t4
					end if
					if (f.le.0.d0) fac = 1.0d0  !! this point has been yielding from previous iteration
c         IF (s1 .gt. -UCS .and. s2 .gt. -UCS ) THEN
c15         IF (sm1 .gt. -UCS .and. sm3 .gt. -UCS ) THEN
!	open(7778,file='plastic.t19')
!	write(7778,*) ns,fac
					IF (sm1 .gt. -tS .and. sm3 .gt. -tS ) THEN !! has not exceeded tension strength yet
301						tr1 = t1 + (1.D0 - fac)*sigma(1)
						tr2 = t2 + (1.D0 - fac)*sigma(2)
						tr3 = t3 + (1.D0 - fac)*sigma(4)
						tr4 = t4 + (1.D0 - fac)*sigma(3)  !!PULL back to yielding surface
c  
c         Determine if stress correction is necessary
c
!!Shunfu.Z 2010.03.11
						if ( failuretype .eq. 2) then
							call invar(tr1,tr2,tr3,tr4,sigm,dsbar,alode)
							call mocouf(PHI,COH,sigm,dsbar,alode,fw)
						else if ( failuretype .eq. 3) then
							call MCY(phi,coh,tr1,tr2,tr4,fw)
						!!Shunfu.Z 2010.04.16
						else if ( failuretype .eq. 1) then
							CALL DRUCKERPRAGER(PHI,COH,SIGM,DSBAR,Fw)
						!!Shunfu.Z 2010.04.16
						end if
!					call invar(tr1,tr2,tr3,tr4,sigm,dsbar,alode)
!					call mocouf(PHI,COH,sigm,dsbar,alode,fw)
c	if( dabs(fw) .gt. 0.01D0.and.p.eq.1.and.q.eq.1) then
c	if( p.eq.1.and.q.eq.1.and.ig.eq.1) then
!						write(9999,*) ns,iter
!						write(9999,*) fnew,f,fw
c	endif

						if( dabs(fw) .lt. 0.01D0 ) go to 214
						go to 214
c-----------------------------------------------------------------------------
c
c********** adding something to correct yield surface stress ****************
c
c-----------------------------------------------------------------------------
 						tt1 = tr1
						tt2 = tr2
						tt3 = tr3
						tt4 = tr4

						nlim = 100
						Ncount = 100
						RH = 8.D0
						RTOL = 0.0001D0
						FTOL = 0.0001D0
						if ( failuretype .eq. 2 ) then
							call correctDP1(NLIM, RTOL, FTOL, RH, tt1,
     +							tt2,tt4,tt3,v,phi,coh,angle, NCOUNT)
						else if ( failuretype .eq. 1 ) then
							call correct(tt1,tt2,tt4,phi,coh,angle)
						else
						end if
						delso(1) = (tr1 - tt1)/1.d0
						delso(2) = (tr2 - tt2)/1.d0
						delso(3) = (tr4 - tt4)/1.d0
c Oct9 EQ	      delso(4) = (tr3 - v*(tt1+tt2))/1.D0
						delso(4) = (tr3 - tt3)/1.D0
c             ------------update stress ---------
						tr1=tt1
						tr2=tt2
						tr3=tt3
						tr4=tt4
c             ------------ end of adding --------
214						continue
					ELSE
c*************************************************************************
						sandg(p,q,ig) = 1
c****record status for sanding
						nsandi(ns)=1

						if ( q .gt. iq(p) ) THEN
							iq(p) = q
						End if
						fac =  1.0D0
c	      IF ( s1 .lt. s3 ) THEN
						IF ( sm1 .lt. sm3 ) THEN
c15                trp1 = -UCS
							trp1 = -tS   ! Shunfu.z trp1 =sigma3
							trp2 = trp1*(1.0D0 - coef)  ! Shunfu.z trp2 =sigma1-sigma3
							call transform1(s1,s2,s4,srr,stt,angle)
							call tranback1(tr1,tr2,tr4,trp1,trp2,angle)
							tr3=trp2
c	          call transform1(s1,s2,s4,srr,stt,angle)
c15	          dsrr = - srr + ucs
c15	          dstt = - stt + ucs*(1.0D0 - coef)
							dsrr = - srr + ts
							dstt = - stt + ts*(1.0D0 - coef)
							call tranback1(dsxx,dsyy,dsxy,dsrr,dstt,angle)
c15	          delso(1) =  -dsxx + UCS
c15	          delso(2) =  -dsyy + UCS*(1.0D0 - coef)
c15	          delso(3) =  -dsxy 
c15	          delso(4) =  -dsyy + UCS*(1.0D0 - coef)
							delso(1) =  -dsxx + tS
							delso(2) =  -dsyy + tS*(1.0D0 - coef)
							delso(3) =  -dsxy 
							delso(4) =  -dsyy + tS*(1.0D0 - coef)
						ELSE
c****please check this part*****
c15                trp1 = -UCS
							trp1 = -tS
							trp2 = trp1*(1.0D0 - coef) 
							call transform1(s1,s2,s4,srr,stt,angle)
							call tranback1(tr1,tr2,tr4,trp1,trp2,angle)
							tr3=trp2
							dsrr = -srr + trp1
							dstt = -stt + trp2
							call tranback1(dsxx,dsyy,dsxy,dsrr,dstt,angle)
							delso(1) =  dsxx
							delso(2) =  dsyy
							delso(3) =  dsxy 
							delso(4) =  dstt
c
c****end of check********June13,2000*****EQ*****************************
c
						END IF
					END IF
	!!Shunfu.Z 2010.03.26
					tr1zsf=-tr1
					tr2zsf=-tr2
					tr3zsf=-tr3
					tr4zsf=-tr4
!!Shunfu.Z 2010.04.16
 118					continue
					if   (failuretype.eq.2) then
	call mocopl(PHI,PSI,EN(p,q,ig),V,tr1zsf,tr2zsf,tr3zsf,tr4zsf,pl)
					elseif   (failuretype.eq.1) then
						CALL MOCOPLDP(PHI,E,V,tr1,tr2,tr3,tr4,PL)
					endif
!!Shunfu.Z 2010.04.16
	!!Shunfu.Z 2010.03.26
c  118				call mocopl(PHI,PSI,EN(p,q,ig),V,tr1,tr2,tr3,tr4,pl)

c	  if (q.ge.nye.and.ig.ge.3.and.p.eq.1) then
					if (q.ge.nye.and.ig.ge.3) then
					if (p.eq.1.or.p.eq.nxe) then
C	goto 7356
c--------------------                                                                     
c-------------------------------------------------------------------                      
c   define yielding point and sanding point                                            
					if (srp.le.0.d0.and.sanding_p(nn_s).eq.0.d0) then !! if sanding at this step                                        
						sanding_p(nn_s)=bhp  !! sanding record                                                                 
						coh_p(nn_s)=coh                                                                        
c	  psum=(dsqrt(er(p,q,1)**2.d0+et(p,q,1)**2.d0+ez(p,q,1)**2.d0)                          
c	1    +dsqrt(er(p,q,2)**2.d0+et(p,q,2)**2.d0+ez(p,q,1)**2.d0)                            
c	1    +dsqrt(er(p,q,3)**2.d0+et(p,q,3)**2.d0+ez(p,q,1)**2.d0)                            
c	1    +dsqrt(er(p,q,4)**2.d0+et(p,q,4)**2.d0+ez(p,q,1)**2.d0))/4.d0                      
						psum=(volstr(p,q,1)+volstr(p,q,2)                                   
	1					+volstr(p,q,3)+volstr(p,q,4))/4.d0 !!average volume strain                                                             
c	  plastic_p(nn_s)=psum                                                                  
c	  plastic_p(nn_s)=volstra                                                               
						plastic_p(nn_s)=pre0                                                                   
C	WRITE (*,*) '*********sand_p*********',nn_s,sanding_p(nn_s)                          
					end if                                                                                   
c	plastic_s=nelem(p,q,1)+nelem(p,q,2)+nelem(p,q,3)+nelem(p,q,4)                           
					p_s1=st(p,1,1)+nelem(p,1,2)+nelem(p,1,3)
					p_s1=p_s1+nelem(p,1,4)                             
					p_s2=st(p,2,1)+nelem(p,2,2)+nelem(p,2,3)
					p_s2=p_s2+nelem(p,2,4)                             
					if (p_s1.le.p_s2.and.yielding_p(nn_s).eq.0.d0) then                                 
						yielding_p(nn_s)=bhp   !! plastic record                                                       
					end if                                                                                   
c-------------------- 
C7356	continue
c	WRITE(T_OUT,*) '***WARNING: All Elements Have Yielded!!***'
					!commented by Bashar WRITE(T_OUT,*)'***WARNING: All Elements Have Yielded!!***'                     
					!commented by Bashar CALL WRTF(T_OUT)
c					write(2,*) '***WARNING: All Elements Have Yielded!!***'
					print*, '***WARNING: All Elements Have Yielded!!***'

					go to 5000
					end if
					end if
				ENDIF
 128				call msmult(pl, fac, ipl, 4)
				call mvmult(pl, ipl, eps, 4, elso) !! eps from previous iteration
c************************* Plastic stresses are elso   *****************
cc     The real incremental stress is then = sigma - elso
c-----------------------------------------------------------------------
				call vecadd(elso,delso,elso,4)
				call mvmult(bt,ibt,elso,h,eload)
				call quotca ( samp,gp,det,term,quot,i,j )
				do 16 k=1,dof
 16				bload(k)=bload(k) + eload(k)*quot

 101	if( iz .ne. 1 .and. iter .lt. its ) go to 117
c
c******************** Update the element stresses and strains **********
c
 				IF ( nelem(p,q,ig) .eq. 0 ) THEN  !! elastic status 
 					elso(1) = 0.0D0
 					elso(2) = 0.0D0
 					elso(3) = 0.0D0
 					elso(4) = 0.0D0
				END IF
				IF ( sandg(p,q,ig) .ne. 1 ) THEN !! having not sanding 
					sx(p,q,ig) = sx(p,q,ig)  + sigma(1) - elso(1)  
					sy(p,q,ig) = sy(p,q,ig)  + sigma(2) - elso(2) 
					sz(p,q,ig) = sz(p,q,ig)  + sigma(4) - elso(4) 
					sxy(p,q,ig)= sxy(p,q,ig) + sigma(3) - elso(3)
       			ELSE
c             if ( s1 .gt. s2 ) then
					if ( sm1 .gt. sm3 ) then
c15                dsrr = -UCS
						dsrr = -Ts
						dstt = dsrr*(1.0D0 - coef) 
	call tranback1(sx(p,q,ig),sy(p,q,ig),sxy(p,q,ig),dsrr,dstt,angle)
						sz(p,q,ig) = dstt
					else
c15                dsrr = -UCS
						dsrr = -Ts
						dstt = dsrr*(1.0D0 - coef) 
	call tranback1(sx(p,q,ig),sy(p,q,ig),sxy(p,q,ig),dsrr,dstt,angle)
						sz(p,q,ig) = dstt
					end if
				END IF


				EN(p,q,ig) = E
				IF ( modeltype .eq. 2) then
 					snorm = sx(p,q,ig)+sy(p,q,ig)+sz(p,q,ig)
					If ( snorm .le. 0.0 ) Then
						EN(p,q,ig) = 0.1D0*E
					Else
						EN(p,q,ig) = E*(snorm/(sh1+sh3+sz0))**0.7D0
						EN(p,q,ig) = E*(sr(p,q,ig)/snorm)**0.27D0
					End if
				ENDIF
c******record initial strain, because it need to use
c accedentle strain for calculte kay and qcom
				if (ns.eq.1) then
c       if (ns.eq.0) then
					ex0 = ex(p,q,ig)
					ey0 = ey(p,q,ig)
					ez0 = ez(p,q,ig)
					volstr0(p,q,ig)=ex(p,q,ig)+ey(p,q,ig)+ez(p,q,ig)
				end if
c     Canceled on Sept3,2000 by EQ
c******************************************************

				ex(p,q,ig) = ex(p,q,ig)  + eps(1)
				ey(p,q,ig) = ey(p,q,ig)  + eps(2)
				ez(p,q,ig) = ez(p,q,ig)  + eps(4) 
				exy(p,q,ig) = exy(p,q,ig) + eps(3)

				yw =0.5d0*exy(p,q,ig)
				call transform1(ex(p,q,ig),ey(p,q,ig),yw
	1               ,er(p,q,ig),et(p,q,ig),angle)
 	volstr(p,q,ig)=ex(p,q,ig)+ey(p,q,ig)+ez(p,q,ig)-volstr0(p,q,ig)
c	  volstr(p,q,ig) = dsqrt(( ex(p,q,ig) - ex0 )**2.D0 + 
c	1                 ( ey(p,q,ig) - ey0 )**2.D0 )

c	1                 ( ez(p,q,ig) - ez0 )**2.0D0)
				ap = 1.D0
!				poro(p,q,ig) = (porosity + ap*volstr(p,q,ig))/
!	1	               (1.0D0 + ap*volstr(p,q,ig))
	!!SHunfu.Z 2009.12.24
				poron(p,q,ig)=poro(p,q,ig)
!				poro(p,q,ig) = (porosity - ap*volstr(p,q,ig))/
!	1	               (1.0D0 - ap*volstr(p,q,ig))
				poro(p,q,ig)=porosity
	            porom(p,q,ig)=porositym
c******Aug24   EQ
!!===============================================
	kxym(p,q,ig,1)=RKEm_X
	kxym(p,q,ig,2)=RKEm_Y
!	  kxym(p,q,ig,1) = RKEm_X*(1.0D0-volstr(p,q,ig)/porositym)**ak/
!	1                        (1.D0-volstr(p,q,ig))
!	  kxym(p,q,ig,2) = RKEm_Y*(1.0D0-volstr(p,q,ig)/porositym)**ak/
!	1                        (1.D0-volstr(p,q,ig))
!!===========================================================

! 			kxy(p,q,ig,1) = RKE_X*(1.0D0+volstr(p,q,ig)/porosity)**ak/
! 	1                        (1.D0+volstr(p,q,ig))
! 			kxy(p,q,ig,2) = RKE_Y*(1.0D0+volstr(p,q,ig)/porosity)**ak/
! 	1                        (1.D0+volstr(p,q,ig))
! 			kxy(p,q,ig,1) = RKE_X*(1.0D0+volstr(p,q,ig)/porosity)**ak/
! 	1                        (1.D0+volstr(p,q,ig))
! 			kxy(p,q,ig,2) = RKE_Y*(1.0D0+volstr(p,q,ig)/porosity)**ak/
! 	1                        (1.D0+volstr(p,q,ig))
	 !!Carman Model
! 				kxy(p,q,ig,1) = RKE_X*(poro(p,q,ig)/porosity)**3*
!     $						((1.0D0-porosity)/(1.0D0-poro(p,q,ig)))**2
! 				kxy(p,q,ig,2) = RKE_Y*(poro(p,q,ig)/porosity)**3*
!     $						((1.0D0-porosity)/(1.0D0-poro(p,q,ig)))**2
c      kxy(p,q,ig,1)=RKE_X
c	kxy(p,q,ig,2)=RKE_Y
	!!Shunu.Z 
!	  kxy(p,q,ig,1) = RKE_X*(1.0D0-volstr(p,q,ig)/porosity)**ak/
!	1                        (1.D0-volstr(p,q,ig))
!	  kxy(p,q,ig,2) = RKE_Y*(1.0D0-volstr(p,q,ig)/porosity)**ak/
!	1                        (1.D0-volstr(p,q,ig))
      if(itype.eq.0) then
        kxy(p,q,ig,1)=RKE_X
	  kxy(p,q,ig,2)=RKE_Y
	else
       a2=nodep(p,q,ig*2-1)
	  if(imesh.eq.1) then
	    ag1=(p-1/2.0)*pi/2.0/nxe
	 elseif(imesh.gt.3) then
	    ag1=(p-1/2.0)*pi/nxe-pi/2.0
	  endif	 
	  srr = sx(p,q,ig)*dsin(ag1)-sy(p,q,ig)*dcos(ag1)

        if(k_s.eq.1) then   !!幂函数形式
          kxy(p,q,ig,1) = RKE_X*coff1*(st(p,q,ig)-dump(a2))**coff2
          kxy(p,q,ig,2) = RKE_Y*coff1*(st(p,q,ig)-dump(a2))**coff2
	  elseif(k_s.eq.2) then   !!指数形式
          kxy(p,q,ig,1) = RKE_X*coff1*exp(coff2*(sy(p,q,ig)-dump(a2)))
          kxy(p,q,ig,2) = RKE_Y*coff1*exp(coff2*(sx(p,q,ig)-dump(a2)))
        endif
	endif
c********************** Compute total bodyloads vector *****************
 117				continue
 15			continue   !!Gauss point recycle
			do 100 m=1,dof
				if(g(m) .eq. 0) go to 100
				bdylds(g(m))=bdylds(g(m))+bload(m)
 100			continue
  7		continue !plasticity-element-loop
789		continue
c  If no plastic yielding, and the effective radial stress <-UCS, get out the loop!
          if ( ix .eq. 1 ) go to 297
          if ( iz .ne. 1 .and. iter .ne. its) go to 200
c
c******************* Compute elemental effective stresses **************
c
 297		continue
c**********let bdylds=0 when it is convergence or iter=ite in load step ********************
c            recoder the unbalence force of this load step to tbdyld               
          if (iterator.eq.n_iterator) then
		call vecadd(tbdyld,bdylds,tbdyld,n)
		endif
	    
		call nulvec(bdylds,n)
c*******************************************************************************************               
	    do 499 p = 1, nxe
	    do 499 q = 1, nye
	        ig=0
			do 499 i=1,gp
			do 499 j=1,gp
				ig=ig+1
 499		continue
          ip = 1         
		CALL CALC_P_GRAD(force,isamp, 
	1   ider,nxe,nye,x,y,nodep,nodeg,tot,icordf                                    
     1   ,ijac,ijac1,PW_GRAD, PO_GRAD,KRW,KRO,psand,szg,imesh) 
			write(3999,*) Q_W,Q_O,PW_GRAD,PO_GRAD,x,y,nodep,                                        
     1			n_bkfix,iabkfix,nxe,nye                   
		CALL CALC_FLOW(Q_W,Q_O,PW_GRAD,PO_GRAD,x,y,nodep,                                        
     1			n_bkfix,iabkfix,nxe,n ye)

		if (iterator.eq.n_iterator.and.ns.gt.20) then

			CALL QUANTITY(IMESH,nxe,nye,nodep,nodep4,
     $			loads,x,y,kro,krw,qwater,qoil) !for imesh=1, to trick Quantity to calculate both inflow and outflow
			W_t=W_t+DTIM*(qwater(1)+qwater(2)+qoil(1)+qoil(2))/2.0
			write(671,54864),ns,timenow,qwater(1),qwater(2),qoil(1),qoil(2)
     $			,qwater(1)+qoil(1),qwater(2)+qoil(2),W_t
	  			
		endif
54864			format(I5,8e20.12)
c		CALL QUANTITY(IMESH,nxe,nye,nodep,nodep4,
c     $    loads,x,y,kro,krw,qwater,qoil)
		if (geometrytype.eq.1) then
!		IF (IMESH.GE.4) THEN
			IF (IMESH.GE.3) THEN   !! =============Lxd================
				do ii=1,2
					qwater(ii)=qwater(ii)*packerl*2.0D0
					qoil(ii)=qoil(ii)*packerl*2.0D0
				enddo
			ELSE
				do ii=1,2
					qwater(ii)=qwater(ii)*packerl*4.0D0
					qoil(ii)=qoil(ii)*packerl*4.0D0
				enddo				
			ENDIF
		else if (geometrytype.eq.2) then
				do ii=1,2
	qwater(ii)=qwater(ii)*2*pi*radius(1)*packerl/(radius(nye+1)-radius(1))
	qoil(ii)=qoil(ii)*2*pi*radius(1)*packerl/(radius(nye+1)-radius(1))
				enddo
		endif

		if(ns.gt.-1) then
c	write(3999,4012) ns,TIME_CAL-20.0d0,Q_W(1),qwater(1),Q_O(1),qoil(1)
C	write(3999,4012) ns,TIME_CAL-20.0d0,Q_W(1),qwater(1)
!			write(3999,4012) ns,qwater(1),qoil(1),qwater(2),qoil(2)
		endif
!		write(15,*) TIME_CAL,' ',PTIME,
!     1	' ',qwater(1),' ',qoil(1),' ',qwater(2),' ',qoil(2)
c	 CALL QUANTITYnew(IMESH,nxe,nye,nodep,nodep4,
c     $ loads,x,y,kro,krw,qwater,qoil)
!		IF (IMESH.GE.4) THEN
		IF (IMESH.GE.3) THEN    !! =============Lxd================
			do ii=1,2
				qwater(ii)=qwater(ii)*packerl*2.0D0
				qoil(ii)=qoil(ii)*packerl*2.0D0
			enddo
		ELSE
			do ii=1,2
				qwater(ii)=qwater(ii)*packerl*4.0D0
				qoil(ii)=qoil(ii)*packerl*4.0D0
			enddo				
		ENDIF



!			write(15,*) TIME_CAL,' ',PTIME,
!     1	' ',Q_W(1),' ',Q_O(1),' ',Q_W(2),' ',Q_O(2)	
C		write(3999,4012) ns,TIME_CAL,Q_W(1),qwater(1),Q_O(1),qoil(1)

4012		FORMAT(i10,5e20.12)
		IF ( bhp .lt. p0 .and. nelem(1,1,1) .eq. 1) then !if-C1
C********Move this part to above****************
c	      IF ( completiontype .eq. 1 ) then
c	        psand   = pi*packerl
c            ELSE
c	        psand   = pi*packerl*pd*pi*pradius**2.D0
c	      END IF
C*************end of this part*********Sept3,2000 EQ
c              nps1    =  nf(1,1)
c	        nps2    =  nf(2*nxe+1,2)
c              sandcut = (radius(1)**2.D0 - (rw - (loads(nps1)+
c	1		          loads(nps2))/2.D0)**2.D0)*psand
c*****???????Aug24 EQ
c
c    July 26th, 2001 YW: Qs = 2*pi*(ny-no)*Rp*dRp/dt
c                        Vs = pi*(ny-n0)*(Rp*Rp-rw*rw)

!          rateo = psand*(p0-bhp)/(radius(NYE)-radius(1))*krw(1,1,1,1)
!          ratew = psand*(p0-bhp)/(radius(NYE)-radius(1))*kro(1,1,1,1)
	    rateo = psand*(p0-bhp)/(radius(NYE)-radius(1))*kro(1,1,1,1)   
          ratew = psand*(p0-bhp)/(radius(NYE)-radius(1))*krw(1,1,1,1)
	    qw = qw + ratew*dtim
	    qo = qo + rateo*dtim
	
		if ((imesh.eq.4).or.(imesh.eq.5)) then   !! produce well                                       
			if (lq2(p).gt.nye0) then                                                          
				sandrate = 2.D0*pi*dsqrt(rad2(lq2(1))**2.D0 +                                
	1			rad2(nye+1-lq2(p))**2.D0)*(poro(1,1,1) - porosity)*                                 
	1			(dsqrt(rad2(lq2(1))**2.D0 + rad2(nye+1-lq2(p))**2.D0)                            
	2			- ad)                                                                      
			else                                                                              
				sandrate = 2.D0*pi*dsqrt(radius(lq(1))**2.D0 +                               
	1			radius(lq(p))**2.D0)*(poro(1,1,1) - porosity)*                                
	1			(dsqrt(radius(lq(1))**2.D0 + radius(lq(p))**2.D0)                           
	2			- ad)                                                                      
			end if                                                                            
		else                                                                                
				sandrate = 2.D0*pi*dsqrt(radius(lq(1))**2.D0 +                               
	1			radius(lq(p))**2.D0)*(poro(1,1,1) - porosity)*                                
	1			(dsqrt(radius(lq(1))**2.D0 + radius(lq(p))**2.D0)                           
	2			- ad)                                                                      
		end if   
C             sandrate = 2.D0*pi*dsqrt(radius(lq(1))**2.D0 + 
C	1	         radius(lq(p))**2.D0)*(poro(1,1,1) - porosity)*
C	1            (dsqrt(radius(lq(1))**2.D0 + radius(lq(p))**2.D0)
C	2             - ad)
c	      XL   = radius(lq(p))
	    DT0  = DTim
	    TMAX = 100
          Time_E = TIME_CAL - 20.D0 + 0.001D0+1.d0
c	      WRITE(T_OUT,*) 'Check CEPS->', volstr(1,1,1), cs,bhp
	    IF ( volstr(1,1,1) .lt. cs ) then !if-C2
			con   = 0.0D0
			conq  = 0.0D0
			conqf = rateo
	    ELSE !if-C2
c***
c Take average pore pressure at inner and outer edges
c this part is just for calculation of sand production. Real 
c distribution of pore pressure dump will be rewrittn later on
			nnn0=0
			do i2=1,nxe+1,nxe
				if ( i2 .eq. nxe+1) then
					lq(i2) = lq(i2-1)
					iq(i2) = iq(i2-1)
				end if
				if(imesh.GE.4) then                                                                      
					lq2(i2) = lq2(i2-1)                                                              
					iq2(i2) = iq2(i2-1)                                                         
				endif
C	 do i1=1,nye+1
C	    nnn0=nnn0+1
C	      nn=2*i2-1+(2*nxe+2+nxe)*(i1-1)
C	    if( nf(nn,3).ne.0) then
C		  dump(nnn0)=loads(nf(nn,3))
C	    else
C	      dump(nnn0)=0.d0
C	    end if
C	 end do
				do i1=1,NYL                                                                             
					nnn0=nnn0+1                                                                          
					nn=i_vnodes(i1,i2)                                                                 
					if(nn.GT.0) then                                                                         
						if( nf(nn,3).ne.0) then                                                              
							dump(nnn0)=loads(nf(nn,3))                                                            
						else                                                                                 
							dump(nnn0)=0.d0                                                                    
						end if                                                                               
					endif                                                                                    
				end do
			end do
c           pwn=(dump(1)+dump(3))/2.d0
c	     pdn=(dump(2)+dump(4))/2.d0
c	     xl=radius(nye+1)-radius(1)
c***
			summc=0.d0
			summq=0.d0
			sumco=0.d0
c***	do ik=1,nye
c***	pdn=dump(ik+1)
c***	pwn=dump(ik)
c***	xl=radius(ik+1)-radius(ik)
c***	dxl=radius(ik)
			do ik=1,nxe
				XL   = radius(lq(ik))
				pdn=dump(lq(ik))
				pwn=dump(1)
				dxl=radius(1)

				volstr1=0.d0
				snorm0=0.d0
c       do ik1=1,nye
                  ig=0
				do i11=1,gp
				do j11=1,gp
					ig=ig+1
c***	   snorm0 = snorm0+sxy(1,ik,ig)
c***         exy00=sxy(1,ik,ig)/en(1,ik,ig)*2.d0*(1.d0+v)
c***	   volstr1 = volstr1 + exy(1,ik,ig)-exy00
c	   snorm0 = snorm0+sxy(1,lq(ik),ig)
c         exy00=sxy(1,lq(ik),ig)/en(1,lq(ik),ig)*2.d0*(1.d0+v)
c	   volstr1 = volstr1 + exy(1,lq(ik),ig)-exy00
	snorm0=snorm0+dsqrt(sx(1,lq(ik),ig)**2.D0 +sy(1,lq(ik),ig)**2.D0
	1 		     + sz(p,q,ig)**2.D0)
	    volstr1 = volstr1+dsqrt(( ex(1,lq(ik),ig) - ex0 )**2.D0 + 
	1                            + ( ey(1,lq(ik),ig) - ey0 )**2.D0 ) 
c***	WRITE(T_OUT,*) 'plastic strain=', exy(1,ik,ig),exy00
c***	WRITE(T_OUT,*) '               ', sxy(1,ik,ig),en(1,ik,ig)
c	WRITE(T_OUT,*) 'plastic strain=', exy(1,lq(ik),ig),exy00
c	WRITE(T_OUT,*) '               ', sxy(1,lq(ik),ig),en(1,lq(ik),ig)
				end do
				end do
c	 end do
				volstr1=dabs(volstr1/4.d0)
				snorm0=dabs(snorm0/4.d0)
	
c      set sand production coefficient
				css0 = cs + snorm0*csco*1.d-3
c        css0 = (cs + snorm0*csco)*0.3d0
c          css0=cs
c           css0=0.001d0

				slind01=5.d4
				slind02=75.d0
				temp1=slind02/slind01 
				temp2=css0+temp1
            
				IF ( volstr1 .lt. css0) then
					snmd=0.d0
				ELSE IF ( volstr1 .gt. temp2) then
					snmd=slind02
				ELSE
					snmd=slind01*(volstr1-css0)
				END IF
	CALL WORM_CAL(PWN,PDN,XL,DT0,TMAX,Time_E,RW,CON,CONQ,CONQF,dxl)
				if ( conq .lt. 0.D0 .or. conqf .lt. 0.D0 )then
					conq  = 0.0D0
					conqf = 0.0D0
				end if
				summc=summc+conq 
				summq=summq+conqf
				sumco=sumc0+con
			end do
c***	end do
c
			conq=summc/nxe
			conqf=summq/nxe
			con=sumco/nxe

          END IF !if-C2
          if ( conq .lt. 0.D0 .or. conqf .lt. 0.D0 )then
              conq  = 0.0D0
	        conqf = 0.0D0
	    end if

          sandrate =  2.D0*pi*(poro(1,1,1) - porosity)*(radius(lq(1))
	1               - radius(rpold(1)))/dtim
c
c   End of adding
c
          sandcut = sandrate/(sandrate + rateo + ratew)*100.D0
         ELSE !if-C1
           sandcut = 0.D0
	   END IF !if-C1
c	       sandp = sandp + sandrate*dtim
             sandp = sandp + conq*dtim

	      DO 309 p = 1, nxe
	      DO 309 q = 1, nye
c
c------YW June 3rd, add radius and angle for gaussian and nodes --------
c       Note, the middle of the element is used which should be changed
c       into the distances on gaussian point for two gaussian point
c
C	            radiusn = radius(q) 
C	            radiusg = radius(q) + (radius(q+1) - radius(q))/2.D0
			if ((imesh.eq.4).or.(imesh.eq.5)) then  !!@@1                                           
				!! if (q.le.nye) then   --Shunfu.Z 2009.07.20
				if (q.le.nye0) then                                                               
					radiusn = radius(q)                                                          
					radiusg = radius(q)+(radius(q+1)-radius(q))/2.D0                         
				else                                                                            
					radiusn = rad2(nye-q+1)                                                      
					radiusg = rad2(nye-q+1) + (rad2(nye-q+2) -                                   
	1                 rad2(nye-q+1))/2.D0                                               
				end if                                                                          
			else                                                                               
				radiusn = radius(q)                                                          
				radiusg = radius(q)+(radius(q+1)-radius(q))/2.D0                         
			end if     !!@@2  
c-------------------End of changes-------------------------------------
                  ig=0
	      do 359 i=1,gp
	      do 359 j=1,gp
	            ig=ig+1
	           yw =0.5d0*exy(p,q,ig)
	   call transform1(ex(p,q,ig),ey(p,q,ig),yw,
	1                 er(p,q,ig),et(p,q,ig),angle)
	   call transform1(sx(p,q,ig),sy(p,q,ig),sxy(p,q,ig),sr(p,q,ig)
	1                 ,st(p,q,ig),angle)
!============Li==========
!	 call transform11(sx(p,q,ig),sy(p,q,ig),sxy(p,q,ig),sr(p,q,ig)  
!	1                 ,st(p,q,ig),angle,imesh,nxe,p)
c----------------------------------------------------------------------------------
c    Determine if sand production has occurred by checking the effective radial
c    stress or sandg = 1
c----------------------------------------------------------------------------------
           if  ( sandg(p,q,ig) .eq. 1 )     ip = 0
	           snorm = dsqrt(sx(p,q,ig)**2.D0 +sy(p,q,ig)**2.D0
	1 		               + sz(p,q,ig)**2.D0)
	volstr(p,q,ig)=ex(p,q,ig)+ey(p,q,ig)+ez(p,q,ig)-volstr0(p,q,ig)
c	           volstr(p,q,ig) = dsqrt(( ex(p,q,ig) - ex0 )**2.D0 + 
c	1                            + ( ey(p,q,ig) - ey0 )**2.D0 ) 
c	1                            + ( ez(p,q,ig) - ez0 )**2.0D0)
         css = cs + snorm*csco

c	     IF ( volstr(p,q,ig) .gt. css ) then
			IF ( volstr(p,q,ig) .gt. css ) then                                                 
				if (imesh.ge.4) then                                                              
					if (q.le.nye0) then                                                            
						if( iq(p) .lt. q ) then                                                       
							iq(p) = q                                                                       
						end if                                                                        
					else                                                                           
						if( iq2(p) .gt. q ) then                                                      
							iq2(p) = q                                                                      
						end if                                                                        
					end if                                                                         
				else                                                                              
					if( iq(p) .lt. q ) then                                                       
						iq(p) = q                                                                       
					end if                                                                        
				end if                                                                            
			END IF
C	           if( iq(p) .lt. q ) then 
C			       iq(p) = q
C	           end if
c	     END IF
c	     IF ( SNORM .lt. 0.0D0 ) THEN
c	           snorm = 0.0D0
c	     End if
c*** move it up	           css = cs + snorm*csco
c 	           if  ( dabs(et(1,1,ig)) .gt. css ) ip = 0
c 	           if  ( dabs(et(1,9,ig)) .gt. css ) ip = 0
c 	           if  ( dabs(er(1,1,ig)) .gt. css ) ip = 0
c 	           if  ( dabs(er(1,9,ig)) .gt. css ) ip = 0
 	           if  ( dabs(et(1,1,ig))   .gt. css ) nsandi(ns)=1
 	           if  ( dabs(et(1,nxe,ig)) .gt. css ) nsandi(ns)=1
 	           if  ( dabs(er(1,1,ig))   .gt. css ) nsandi(ns)=1
 	           if  ( dabs(er(1,nxe,ig)) .gt. css ) nsandi(ns)=1
359     continue
c-------------------------------------------------------------------------
	      disp  = -(er(1,1,1)+er(9,1,1))*rw*0.5D0
c
c------YW June 3rd, add angle for gaussian and nodes --------
c       Note, the middle of the element is used which should be changed
c       into the angle on gaussian point for two gaussian point
c
c  multimesh mode  QUESTION                                                                        
			if (imesh.eq.2) then                                                                
				agg = 0.0D0                                                                      
			elseif (imesh.eq.4) then                                                                 
				if (q.le.nye0) then                                                            
					agg = (p - 1)*90.D0/NXE + 90.d0/(2.d0*nxe)                                   
				else                                                                           
C					agg = 90-((p - 1)*90.D0/NXE + 90.d0/(2.d0*nxe)) ! error? 
					agg = (p - 1)*90.D0/NXE +  90.d0/(2.d0*nxe)  !!Shunfu.Z 2009.09.07                    
				end if                                                                         
			else ! including mesh5                                                                   
				agg = get2_ang(p,imesh,nxe)                                                            
			end if                                                                                   
c-------------------End of changes-------------------------------------                   
C	          agg = (p - 1)*90.D0/NXE + + 90.d0/(2.d0*nxe)
c-------------------End of changes-------------------------------------
c****************************************************************************
c
c****        Print out Results for Cold Production and Sand Control      ****
c
c****************************************************************************
            ng    = gp**2.D0
	      srp   = 0.d0
	      stp   = 0.D0
	      sxg   = 0.D0
	      syg   = 0.D0
	      sxyp  = 0.D0
	      szgg  = 0.D0
	      exg   = 0.D0
	      eyg   = 0.D0
	      exyg  = 0.D0
	      erp   = 0.0D0
	      etp   = 0.D0
	      EG    = 0.D0
	      porop = 0.0D0
	      kxp   = 0.0D0
c***                                                                                      
		cqcom=0.d0                                                                    
c***                                                                                      
c      Moved by Ma.Z                                                                      
C 8-31 CHANGE                                                                             
c			if(ndrill.eq.0) then                                                                     
c				IF(NS.LE.NT) TIME_CAL=NS                                                           
c				IF(NS.GT.NT) TIME_CAL=NT+PTIME                                                     
c			else                                                                                     
c				IF(NS.LE.NT) TIME_CAL=20
c				IF(NS.GT.NT) TIME_CAL=20+PTIME
c			end if                                                                            
c			RKE=dsqrt(RKE_X*dcos(agg*pi/180.0d0)**2.d0+
c     $		RKE_Y*dsin(agg*pi/180.0d0)**2.d0)                             
                                                
        do lg = 1, NG
	      srp   = srp   + sr(p,q,lg)/4.D0
	      stp   = stp   + st(p,q,lg)/4.D0
	      sxg   = sxg   + sx(p,q,lg)/4.D0
	      syg   = syg   + sy(p,q,lg)/4.D0
	      sxyp  = sxyp  + sxy(p,q,lg)/4.D0
	      szgg  = szgg  + sz(p,q,lg)/4.D0
	      exg   = exg   + ex(p,q,lg)*100.0D0/4.D0
	      eyg   = eyg   + ey(p,q,lg)*100.0D0/4.D0
	      exyg  = exyg  + exy(p,q,lg)*100.D0/4.D0
	      erp   = erp   + er(p,q,lg)*100.0D0/4.D0
	      etp   = etp   + et(p,q,lg)*100.D0/4.D0
	      EG    = EG    + EN(p,q,lg)/4.D0
	      porop = porop + poro(p,q,lg)*1.D0/4.D0
	      volstra= volstra + volstr(p,q,lg)/4.D0
c*****?????????????Aug24  EQ
	      kxp   = kxp   + kxy(p,q,lg,1)*1.D0/4.D0
c***                                                                                      
		cqcom=cqcom+qcom(p,q,lg)                                                      
c***  
c***
c       By Ma.Z
C       write(14,6015)coh*1.0d3,TIME_CAL,radiusg,agg,sr(p,q,lg)*1.0d3,
       write(14,6015)coh,TIME_CAL,radiusg,agg,sr(p,q,lg),
     1st(p,q,lg),sx(p,q,lg),sy(p,q,lg),
     1 sxy(p,q,lg),sz(p,q,lg),
	1 ex(p,q,lg),ey(p,q,lg),volstr(p,q,lg),
     1 er(p,q,lg),et(p,q,lg),qcom(p,q,lg),
     1 poro(p,q,lg)*1.D0/porosity*100.D0,
     1   kxy(p,q,lg,1)*1.D0/RKE*100.D0, nelem(p,q,lg),sandg(p,q,lg),
     1   sw(p,q,LG),
     1 PW_GRAD(p,q,lg,1),PW_GRAD(p,q,lg,2),
	1 PO_GRAD(p,q,lg,1),PO_GRAD(p,q,lg,2)
C     1st(p,q,lg)*1.0d3,sx(p,q,lg)*1.0d3,sy(p,q,lg)*1.0d3,
C     1 sxy(p,q,lg)*1.0d3,sz(p,q,lg)*1.0d3,
c*	1   exg,eyg,volstr(p,q,4),erp,etp,EG,porop/porosity*100.D0,
CC	1 ex(p,q,lg)*100.0D0,ey(p,q,lg)*100.0D0,volstr(p,q,lg),
C     1 er(p,q,lg)*100.0D0,et(p,q,lg)*100.D0,qcom(p,q,lg)*100.d0,
C     1 poro(p,q,lg)*1.D0/porosity*100.D0,
C     1   kxy(p,q,lg,1)*1.D0/RKE*100.D0, nelem(p,q,lg),sandg(p,q,lg),
C     1   sw(p,q,1),
C     1 PW_GRAD(p,q,lg,1),PW_GRAD(p,q,lg,2),
C	1 PO_GRAD(p,q,lg,1),PO_GRAD(p,q,lg,2)
	!below by Bashar for output purposes only
c	if (ns.ne.0) then
c	write(101,5600),"sx(",ns,",",p,",",q,",",ig,")=",sx(p,q,ig),";"
c	write(101,5600),"sy(",ns,",",p,",",q,",",ig,")=",sy(p,q,ig),";"
c	write(101,5600),"sz(",ns,",",p,",",q,",",ig,")=",sz(p,q,ig),";"
c	write(101,5600),"sxy(",ns,",",p,",",q,",",ig,")=",sxy(p,q,ig),";"
c	write(101,5600), "pw_gradx(",ns,",",p,",",q,",",ig,")=" ,
c    *	pw_grad(p,q,lg,1), ";"
c	write(101,5600), "pw_grady(",ns,",",p,",",q,",",ig,")=" ,
c     *	pw_grad(p,q,lg,2), ";"
c	write(101,5600), "po_gradx(",ns,",",p,",",q,",",ig,")=" ,
c    *	po_grad(p,q,lg,1), ";"
c	write(101,5600), "po_grady(",ns,",",p,",",q,",",ig,")=" ,
c     *	po_grad(p,q,lg,2), ";"
c	endif
	  end do
C 8-31 CHANGE
	if(ndrill.eq.0) then
       IF(NS.LE.NT) TIME_CAL=NS
       IF(NS.GT.NT) TIME_CAL=NT+PTIME
	else
       IF(NS.LE.NT) TIME_CAL=20
       IF(NS.GT.NT) TIME_CAL=20+PTIME
	end if
	
       RKE=dsqrt(RKE_X*dcos(agg)**2.d0+RKE_Y*dsin(agg)**2.d0)


      if (sandg(p,q,1).eq.1.or.sandg(p,q,2).eq.1.or.sandg(p,q,3).eq.1.
     1    or.sandg(p,q,4).eq.1) then
	end if
       write(12,6015)coh,TIME_CAL,radiusg,agg,srp,stp,
     1sxg,syg,sxyp,szgg,
C       write(12,6015)coh*1.0d3,TIME_CAL,radiusg,agg,srp*1.0d3,stp*1.0d3,
C     1sxg*1.0d3,syg*1.0d3,sxyp*1.0d3,szgg*1.0d3,
c*	1               exg,eyg,volstr(p,q,4),erp,etp,EG,
	1               exg,eyg,volstr(p,q,4),erp,etp,cqcom*100.d0,
     1               porop/porosity*100.D0,kxp/RKE*100.D0,
c     1               nelem(p,q,4),sandg(p,q,4),sw(p,q)
     1               nelem(p,q,4),sandg(p,q,4),sw(p,q,1)


C       write(12,6015)coh,TIME_CAL,radiusg,agg,srp,stp,sxg,syg,sxyp,szgg,
C	1               exg,eyg,volstr(p,q,4),erp,etp,EG,
C     1               porop/porosity*100.D0,kxp/RKE*100.D0,
C     1               nelem(p,q,4),sandg(p,q,4)
c-------------------------------------------------------------------                      
c   define yielding point and sanding point                                               
				if (srp.le.0.d0.and.sanding_p(nn_s).eq.0.d0) then                                        
					sanding_p(nn_s)=bhp                                                                    
					coh_p(nn_s)=coh                                                                        
c	  psum=(dsqrt(er(p,q,1)**2.d0+et(p,q,1)**2.d0+ez(p,q,1)**2.d0)                          
c	1    +dsqrt(er(p,q,2)**2.d0+et(p,q,2)**2.d0+ez(p,q,1)**2.d0)                            
c	1    +dsqrt(er(p,q,3)**2.d0+et(p,q,3)**2.d0+ez(p,q,1)**2.d0)                            
c	1    +dsqrt(er(p,q,4)**2.d0+et(p,q,4)**2.d0+ez(p,q,1)**2.d0))/4.d0                      
					psum=(volstr(p,q,1)+volstr(p,q,2)+volstr(p,q,3)                                   
	1					+volstr(p,q,4))/4.d0                                                             
c	  plastic_p(nn_s)=psum                                                                  
c	  plastic_p(nn_s)=volstra                                                               
					plastic_p(nn_s)=pre0                                                                   
C	WRITE (*,*) '*********sand_p*********',nn_s,sanding_p(nn_s)                          
				end if                                                                                   
c	plastic_s=nelem(p,q,1)+nelem(p,q,2)+nelem(p,q,3)+nelem(p,q,4)
				!!$$@@                           
				p_s1=st(p,1,1)+nelem(p,1,2)+nelem(p,1,3)+nelem(p,1,4)                             
				p_s2=st(p,2,1)+nelem(p,2,2)+nelem(p,2,3)+nelem(p,2,4)                             
				if (p_s1.le.p_s2.and.yielding_p(nn_s).eq.0.d0) then                                 
					yielding_p(nn_s)=bhp                                                            
				end if                                                                                   
c------------------------------------------------------------------- 
C END OF 8-31 CHANGE
6015   format(f10.4,f15.7,19e20.5E3, 4e20.5E3)
309    continue

c*****************ouput valuses in nodes************************June2
c------output according to r direction at first and t direction at secound---------	     
	do i2=1,nxe+1
	      if ( i2 .eq. nxe+1) then
	        lq(i2) = lq(i2-1)
              iq(i2) = iq(i2-1)
					if (imesh.GE.5) then                                                                
						lq2(i2) = lq2(i2-1)                                                           
						iq2(i2) = iq2(i2-1)                                                      
					endif 
	      end if
c	 do i1=1,nye+1
c	      nn=2*i2-1+(2*nxe+2+nxe)*(i1-1)
		do i1=1,NYL                                                                             
			nn=i_vnodes(i1,i2)                                                                 
			I_NODE=i_v4nodes(I1,I2)
			if(nn.gt.0) then  
				if( nf(nn,1).ne.0) then
					dumux(nn)=loads(nf(nn,1))
				else
					dumux(nn)=0.d0
				end if
				if( nf(nn,2).ne.0) then
					dumuy(nn)=loads(nf(nn,2))
				else
					dumuy(nn)=0.d0
				end if
				if( nf(nn,3).ne.0) then
					dump(nn)=loads(nf(nn,3))
				else
					dump(nn)=0.d0
				end if
c	if (ns.eq.21.and.nn.gt.0) write(703,999) nn,x(nn),y(nn), dump(nn)
c	if (ns.eq.20.and.nn.gt.0) write(704,999) nn,x(nn),y(nn), dumux(nn)
c	if (ns.eq.20.and.nn.gt.0) write(705,999) nn,x(nn),y(nn), dumuy(nn)
999		format(i10,3f20.12)
c				agn=(i2-1)*90.d0/nxe
 		agn=get_ang(i2,imesh,nxe)
				dumu(nn)  = dumux(nn)*dcos(agn*pi/180.D0)
	1						+ dumuy(nn)*dsin(agn*pi/180.D0)
				dumut(nn) = dumuy(nn)*dcos(agn*pi/180.D0)
	1						- dumux(nn)*dsin(agn*pi/180.D0)
C 8-31 CHANGE
c****let plastic radius = total plastic radius - radius of wellbore**EQ
c
c
C       write(13,6013) coh,TIME_CAL,radius(i1),agn,dumux(nn)*100.D0, 
C	1 dumuy(nn)*100.d0,dumu(nn)*100.D0,dumut(nn)*100.D0,dump(nn),
C     1 drawdown,bhp,sandp,con,conq,conqf,ratew,qo,qw,
c     1 drawdown,bhp,sandp,sandcut,sandrate,rateo,ratew,qo,qw,
C	2 (radius(lq(i2))-radius(1))*100.D0, 
C     1 (radius(lq(i2))-radius(1))*100.D0
c     1 (radius(iq(i2))-radius(1))*100.D0
		if (lq(i2).le.nye0) then    !!$$@@                                                           
C           WRITE (13,6013) coh,TIME_CAL,radius(i1),agn,dumux(nn)*100.D0,                   
C	1  dumuy(nn)*100.d0,dumu(nn)*100.D0,dumut(nn)*100.D0,dump(nn),!P_new(I_NODE),                                       
C     1         drawdown,bhp,sandp,con,conq,conqf,ratew,qo,qw,                             
C	2         (radius(lq(i2))-radius(1))*100.D0,                                             
C!Ma.Z     1         (radius(lq(i2))-radius(1))*100.D0,st1(nn),A_SAT(I_NODE)   ! st1 is T  
C     1      (radius(lq(i2))-radius(1))*100.D0,st1(I_NODE),A_SAT(I_NODE)   ! st1 is T 

	write(13,6013) coh,TIME_CAL,radius(i1),agn,dumux(nn)*100.D0, 
	1  dumuy(nn)*100.d0,dumu(nn)*100.D0,dumut(nn)*100.D0,dump(nn),!P_new(I_NODE),
C     1 drawdown,bhp,sandp,sandcut,sandrate,rateo,ratew,qo,qw,
     1        drawdown,bhp,sandp,con,conq,conqf,ratew,qo,qw,
	2         (radius(lq(i2))-radius(1))*100.D0, 
!Ma.Z     1         (radius(lq(i2))-radius(1))*100.D0,st1(nn),A_SAT(I_NODE)   ! st1 is T
     1      (radius(lq(i2))-radius(1))*100.D0,st1(I_NODE),A_SAT(I_NODE)   ! st1 is T     
		else 
           write(13,6013) coh,TIME_CAL,rad2(NYL+1-i1),
     *		 agn,dumux(nn)*100.D0, 
	1  dumuy(nn)*100.d0,dumu(nn)*100.D0,dumut(nn)*100.D0,dump(nn),!P_new(I_NODE),
C     1 drawdown,bhp,sandp,sandcut,sandrate,rateo,ratew,qo,qw,
     1        drawdown,bhp,sandp,con,conq,conqf,ratew,qo,qw,
	2         (rad2(lq2(i2)-nye)-rad2(1))*100.D0, 
!Ma.Z     1         (rad2(lq2(i2)-nye)-rad2(1))*100.D0,st1(nn),A_SAT(I_NODE)   ! st1 is T
     1      (rad2(lq2(i2)-nye)-rad2(1))*100.D0,st1(I_NODE),A_SAT(I_NODE)   ! st1 is T                                                                                  
C           WRITE (13,6013) coh,TIME_CAL,rad2(NYL+1-i1),                                    
C     *		 agn,dumux(nn)*100.D0,                                                            
C	1  dumuy(nn)*100.d0,dumu(nn)*100.D0,dumut(nn)*100.D0,dump(nn),!P_new(I_NODE),            
C     1 drawdown,bhp,sandp,sandcut,sandrate,rateo,ratew,qo,qw,                            
C     1         drawdown,bhp,sandp,con,conq,conqf,ratew,qo,qw,                             
C	2         (rad2(lq2(i2)-nye)-rad2(1))*100.D0,                                            
!Ma.Z     1         (rad2(lq2(i2)-nye)-rad2(1))*100.D0,st1(nn),A_SAT(I_NODE)   ! st1 is T 
C     1      (rad2(lq2(i2)-nye)-rad2(1))*100.D0,st1(I_NODE),A_SAT(I_NODE)   ! st1 is T     
		end if                                                                                 
c     1 (radius(iq(i2))-radius(1))*100.D0                                                 
C END OF 8-31 CHANGE                                                                      
C					endif
C END OF 8-31 CHANGE
	if (ns.ne.0) then
c	write(101,*), "dump(" ,ns, "," ,nn , ")=" , dump(nn), ";"
c	write(101,*), "dumux(" ,ns, "," ,nn , ")=" , dumux(nn), ";"
c	write(101,*), "dumuy(" ,ns, "," ,nn , ")=" , dumuy(nn), ";"
c	write(101,*), "dumu(" ,ns, "," ,nn , ")=" , dumu(nn), ";"
c	write(101,*), "dumut(" ,ns, "," ,nn , ")=" , dumut(nn), ";"
c	write(101,*), "dumT(" ,ns, "," ,nn , ")=" , st1(I_node), ";"
c	write(101,*), "v_waterx(" ,ns, "," ,nn , ")=" , v_water(1,I_node), ";"
c	write(101,*), "v_watery(" ,ns, "," ,nn , ")=" , v_water(2,I_node), ";"
c	write(101,*), "v_oilx(" ,ns, "," ,nn , ")=" , v_oil(1,I_node), ";"
c	write(101,*), "v_oily(" ,ns, "," ,nn , ")=" , v_oil(2,I_node), ";"

c	write(101,*), "time_cal(", ns , ")=", time_cal, ";"
	endif
	endif
	 end do
      end do
c6013  format(f7.4,f15.7,18e13.5)
6013  format(f10.4,f15.7,20e20.5E3)
c****************************

	if ( ns .ge. 0 ) then
	do kk1=1,n
	end do
		do kk1=1,1
          do kk2=1,nye
	    ig=0
	    do i=1,gp
	    do j=1,gp
	    ig=ig+1
	    end do
	    end do
	    end do
	    end do
		do kk1=1,1
          do kk2=1,nye
          ig=0
	    do i=1,gp
	    do j=1,gp
	    ig=ig+1
	    end do
	    end do
	    end do
	    end do
	end if
            if ( il .eq. 101 ) THEN
               rkf(ns,1) = ptime
	         rkf(ns,2) = sandp
	         rkf(ns,3) = sandcut
	         rkf(ns,4) = flowo
	         rkf(ns,5) = flowf
	      end if
c************* Drawdown and stress calculations  ************************
	      IF ( il .eq. 101 ) THEN
               rkf(ns,6) = ptime
  	         rkf(ns,7) = drawdown
	         rkf(ns,8) = sx(1,1,4)
	         rkf(ns,9) = sy(1,1,4)
	         rkf(ns,10)= sz(1,1,4)
	      END IF
c
c   Loop for additional load will !!stop if ip = 0 which indicates that the
c   sanding has taken place. The critical drawdown and wellbore wall 
c   displacement can be written after this critical loading.
c
               rplas = radius(1)
	      IF ( ip .eq. 0 .and. problemtype .eq. 1) go to 88


c*****************************recorde loads******************************
       if (Iterator.eq.n_iterator) then 
	 do ii1=1,n
	  tload(ii1)=loads(ii1)
	 end do
	endif

c******** decide if record sanding status  Sept26,2000   EQ
       if(nrec.eq.0) then
        if(ns.eq.1.and.nsandi(1).eq.1) then  !! sanding at current timestep         
	    reptime=time_cal
	    rebhp=bhp
	    recoh=coh
	    nrec=1
	  else
	    if(ns.gt.1.and.nsandi(ns).eq.1.and.nsandi(ns-1).eq.0) then 
	     reptime=time_cal
	     rebhp=bhp
	     recoh=coh
	     nrec=1
	    end if
	  end if
	 end if
c****
c******** decide if record yielding status  Sept28,2000   EQ
       if(nrecp.eq.0) then
        if(ns.eq.1.and.npelasti(1).eq.1) then !! yield at current timestep
	    reptimep=time_cal
	    rebhpp=bhp
	    recohp=coh
	    nrecp=1
	  else
	    if(ns.gt.1.and.npelasti(ns).eq.1.and.npelasti(ns-1).eq.0) then
	     reptimep=time_cal
	     rebhpp=bhp
	     recohp=coh
	     nrecp=1
	    end if
	  end if
	 end if
c****
c-------------------------------------------------------------------------
C*** IF PTIME >= TIMEF !!stop
      IF(NS.GT.NT) THEN
      IF(L_TMP.EQ.0) THEN	 
ccc 		WRITE(2,*)'---------- PRODUCTION TIME ----------'
ccc          WRITE(*,*)'---------- PRODUCTION TIME ----------'
	    WRITE(T_OUT,*)'---------- PRODUCTION TIME ----------'                           
		! CALL WRTF(T_OUT)
		L_TMP=1
	END IF
c      WRITE(T_OUT,998) PTIME/PDATE(NPOINT+1)*100.D0,PTIME,DTIM,BHP
	 WRITE(T_OUT,998) PTIME/PDATE(NPOINT+1)*100.D0,PTIME,DTIM,BHP                    
	! CALL WRTF(T_OUT)
ccc	WRITE(*,998) PTIME/PDATE(NPOINT+1)*100.D0,PTIME,DTIM,BHP 
ccc	WRITE(2,998) PTIME/PDATE(NPOINT+1)*100.D0,PTIME,DTIM,BHP
      END IF	 
      IF(PTIME.GE.TIMEF) GO TO 88
c
c      Define the previous plastic Radius
c
       Do p = 1, NYE
	         RPold(p) = lq(p)
       END DO

	call fract(kay,krw,fracwater,nxe,nye,nodep)

c***                                                                                      
C CALL SATURATION CALCULATION when it is production  
		                                     
c			do i4=1,nn4
c				i8=i_4_8_nodes(i4)
c				dumt(i8)=st1(i4)
c			enddo
14001 continue 
	if (ns.gt.nt) then 
	kkfl=0
	if (kkfl.eq.0) then 
 
 	
	CALL GETIW(IW4,NXE,NYE,NODEP4)
!************************************************************************************
	if (if_sat_pass.eq.0) then
	if (if_sat45_orig.eq.0) then
	!CALL SAT_NEW45_v8(ns,iw4,nn4,nn8,nxe,nye,NGRAV,
     1 !         N_BKFIX,IABKFIX,i_4_8_nodes,i_8_4_nodes,
     1!		  term,Dtim,COMP_W,SZG,vis_w,dump,DUMP_OLD,x,y,A_SAT,ww,
     1!		n1_deload,i1elemx,i1elemy,n3_deload,i3elemx,i3elemy,
     1!		kxy,vis_w_mat,vis_o_mat,SWT,SKROT,SKRWT,NUMOW,dkw_dsT
     1!		 ,d2kw_ds2T,a_sat_new,RKE_X,RKE_Y,
     1!			v_inodes,i_vnodes,a_satp,a_satpp,dtmp
     1!	,dt_min,it_total,nsub,A_C_GLOBE2,iterator,errp,errsw)
		
     		do ii=1,nn4
			a_sat_f2(ii)=(a_sat_new(ii)-a_sat(ii))/dtmp*dtim
     1			+a_sat_new(ii)
		enddo					
		if (iterator.eq.n_iterator) then
			do ii=1,nn4
				a_sat(ii)=a_sat_new(ii)
			enddo
		endif
	elseif (if_sat45_orig.eq.1) then
!************************************************************************************
		!CALL SAT_NEW45_ORIG_v2(ns,iw4,nn4,nn8,nxe,nye,NGRAV,
     1     !     N_BKFIX,IABKFIX,i_4_8_nodes,i_8_4_nodes,
     1	!	  term,Dtim,COMP_W,SZG,vis_w,dump,DUMP_OLD,x,y,A_SAT,ww)
		do ii=1,nn4
				a_sat_new(ii)=a_sat(ii)
		enddo
	elseif (if_sat45_orig.eq.2) then
!		call SAT_NEW5(NODEP,poro,dump,x,y,kxy,volste,volst0,
!	1       term,ns,ptime,
!     1		A_SAT,NODEP4,dump_old,i_8_4_nodes,i_4_8_nodes
!     1		,N_BKFIX,IABKFIX,radius,
!     1    n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                   
!     1          n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
!     1         n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload)

	     call SAT_NEW05(NODEP,poro,dump,x,y,kay,volste,volst0,
	1       term,ns,ptime,i_8_4_nodes,i_4_8_nodes,
     1		A_SAT,NODEP4,dump_old,number)

		do ii=1,nn4
				a_sat_new(ii)=a_sat(ii)
		enddo
	elseif (if_sat45_orig.eq.3) then
		!call SAT_NEW45_ZMA(NODEP,poro,dump,x,y,kxy,volste,volst0,
	1    !  term,ns,ptime,
     1	!	A_SAT,NODEP4,dump_old)
		do ii=1,nn4
				a_sat_new(ii)=a_sat(ii)
		enddo
	elseif (if_sat45_orig.eq.4) then
		!call SAT_OIL_ZMA(NODEP,poro,dump,x,y,kxy,volste,volst0,
	1    !   term,ns,ptime,
     1	!	A_SAT,NODEP4,dump_old)
		do ii=1,nn4
				a_sat_new(ii)=a_sat(ii)
		enddo
	elseif (if_sat45_orig.eq.5) then
		!call SAT_NEW45_2013A(ns,iw4,nn4,nn8,nxe,nye,NGRAV,
     1     !     N_BKFIX,IABKFIX,i_4_8_nodes,i_8_4_nodes,
     1	!	  term,Dtim,COMP_W,SZG,vis_w,dump,DUMP_OLD,x,y,A_SAT,ww)
		do ii=1,nn4
				a_sat_new(ii)=a_sat(ii)
		enddo
	elseif (if_sat45_orig.eq.6) then
		!call SAT_NEW56(NODEP,poro,dump,x,y,kxy,volste,volst0,
	1     !  term,ns,ptime,
     1	!	A_SAT,NODEP4,dump_old,number)

		do ii=1,nn4
				a_sat_new(ii)=a_sat(ii)
		enddo

	endif
	else
		do ii=1,nn4
			a_sat_new(ii)=sat_pass_value
		enddo
		do ii=1,nn4
			a_sat(ii)=sat_pass_value
		enddo
	endif

!************************************************************************************

	
		do ii=1,nxe
		do jj=1,nye
		ig=0
		do i=1,gp
		do j=1,gp
			ig=ig+1
			call formln(derf,iderf,funf,samp,gp,i,j)
			asum=0.0d0
			do kk=1,nodf
				asum=asum+funf(kk)*a_sat_new(nodep4(ii,jj,kk))
			enddo
			sw(ii,jj,ig)=asum
		enddo
		enddo
		enddo
		enddo
c----------------------------------Carl Change-------------------------
	  DO Q=1,NYE  !permeabilty update-loop
		radial_qw(q)=0.0
		radial_qo(q)=0.0
		radial_qt(q)=0.0
	  DO P=1,NXE
C======================================================================
C Tempreture from kevin
C         Temperature viscosity-temp relationship kevin 2006-06-22
          temp_element=0.0   ! Element temperature
          do in=1,4
	        temp_element=temp_element+st1(nodep4(p,q,in))/4.0
          end do
C         interpolation to get Vis_0 and Vis_w
          if(temp_element.LT.vis_temp(1,1)) then
             Vis_w=vis_temp(1,2)
             Vis_o=vis_temp(1,3)
c	    write (*,*)"Warnning temperature is out of viscosity-temperature"
	    else if(temp_element.GT.vis_temp(Num_T_V,1)) then
             Vis_w=vis_temp(Num_T_V,2)
             Vis_o=vis_temp(Num_T_V,3)
c	    write (*,*)"Warnning temperature is out of viscosity-temperature"
	    else
             DO 1100 I=2,Num_T_V !changed by bashar from I=1,...
             IF(temp_element.GE.vis_temp(I,1)) GO TO 1100
                Vis_W=vis_temp(I-1,2)+(temp_element-vis_temp(I-1,1))
     &                *(vis_temp(I,2)-vis_temp(I-1,2))/ 
     &                (vis_temp(I,1)-vis_temp(I-1,1))
				vis_w_mat(p,q)=Vis_W
                Vis_O=vis_temp(I-1,3)+(temp_element-vis_temp(I-1,1))
     &                *(vis_temp(I,3)-vis_temp(I-1,3))/
     &                (vis_temp(I,1)-vis_temp(I-1,1))
				vis_o_mat(p,q)=Vis_o
			goto 1101
 1100        continue
 1101			continue
           end if
		 if(temp_element.gt.265.and.if_steam_mu.eq.1)
     1		  Vis_W=steam_vis !steam instead of water
C End of Kevin added
C======================================================================
	  pmax_node=1
	  pmax_pressure=dump(nodep(p,q,1))
	  do i=2,4
	  j=2*i-1
	  if (dump(nodep(p,q,j)).gt.pmax_pressure) then
			pmax_pressure=dump(nodep(p,q,j))
			pmax_node=i
	  endif
	  enddo

	  pmax_node_sw=a_sat_new(nodep4(p,q,pmax_node))
c	  if (if_extrapolate2.eq.1) then
c		pmax_node_sw=a_sat_f2(nodep4(p,q,pmax_node))
c	  endif
  	  
	  ig=0
	  qw_mat(p,q)=0.0
	  qo_mat(p,q)=0.0
	  qt_mat(p,q)=0.0
	  do i=1,gp   !KAY-GP-LOOP
	  do j=1,gp
	  ig=ig+1
		
	
	if (if_kr_wells.ne.0) then
		if (if_simple_ups_per2.eq.0) then 
			call KR_finder(SW(P,Q,ig),SKRW,dkw_ds,SKRO,dko_ds,if_kr_wells)
		else
			call KR_finder(pmax_node_sw,SKRW,dkw_ds,SKRO,dko_ds,if_kr_wells)
		endif
	else
		if (if_simple_ups_per2.eq.0) then
			CALL INTERP(SWT,SKROT,NUMOW,SW(P,Q,ig),SKRO)
			CALL INTERP(SWT,SKRWT,NUMOW,SW(P,Q,ig),SKRW)
		else
			CALL INTERP(SWT,SKROT,NUMOW,pmax_node_sw,SKRO)
			CALL INTERP(SWT,SKRWT,NUMOW,pmax_node_sw,SKRW)
		endif
	endif
	if (if_const_kr.eq.1) then
		sw0=0.5
		call KR_finder(SW0,SKRW,dkw_ds,SKRO,dko_ds,1)
	endif

	  
        SLWO=SKRO/VIS_O+SKRW/VIS_W
7778  format(i5,3f20.9)
        kay(p,q,ig,1) = KXY(P,Q,IG,1)*SLWO*0.08527D0 
        krw(p,q,ig,1) = SKRw/VIS_w*KXY(p,q,ig,1)*0.08527D0 
        kro(p,q,ig,1) = SKRo/VIS_o*KXY(p,q,ig,1)*0.08527D0 
        kay(p,q,ig,2) = KXY(P,Q,IG,2)*SLWO*0.08527D0 
        krw(p,q,ig,2) = SKRw/VIS_w*KXY(p,q,ig,2)*0.08527D0 
        kro(p,q,ig,2) = SKRo/VIS_o*KXY(p,q,ig,2)*0.08527D0 
	  
c***
c      if (q.eq.1) then
c        kay(p,q,ig,1) = KXY(P,Q,IG,1)*SLWO*0.08527D0*0.01d0
c        kay(p,q,ig,2) = KXY(P,Q,IG,2)*SLWO*0.08527D0*0.01d0
c	end if
c***		
			GP=2
			call formln(derf,iderf,funf,samp,gp,i,j)
			call matmul(derf,iderf,coordf,icordf,jac,ijac)
			call twoby2(jac,ijac,jac1,ijac1,det)
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf)
			x_gp=0.0
			y_gp=0.0
			do ii=1,4
				p_element(ii)=dump(nodep(p,q,2*ii-1))
				x_gp=x_gp+funf(ii)*x(nodep(p,q,2*ii-1))
				y_gp=y_gp+funf(ii)*y(nodep(p,q,2*ii-1))
			enddo
			do ii=1,2
				p_grad(ii)=0.0
				do jj=1,4
					p_grad(ii)= p_grad(ii)+derivf(ii,jj)*p_element(jj)
				enddo
			enddo
			qwx=-krw(p,q,ig,1)*p_grad(1)
			qwy=-krw(p,q,ig,2)*p_grad(2)	
			qox=-kro(p,q,ig,1)*p_grad(1)
			qoy=-kro(p,q,ig,2)*p_grad(2)	
			qtx=-kay(p,q,ig,1)*p_grad(1)
			qty=-kay(p,q,ig,2)*p_grad(2)
			qwr=(qwx*x_gp+qwy*y_gp)/sqrt(x_gp*x_gp+y_gp*y_gp)
			qor=(qox*x_gp+qoy*y_gp)/sqrt(x_gp*x_gp+y_gp*y_gp)	
			qtr=(qtx*x_gp+qty*y_gp)/sqrt(x_gp*x_gp+y_gp*y_gp)
			qw_mat(p,q)=qw_mat(p,q)+0.25*qwr
			qo_mat(p,q)=qo_mat(p,q)+0.25*qor
			qt_mat(p,q)=qt_mat(p,q)+0.25*qtr
			
	  end do
	  end do !KAY-GP-LOOP

	  
			radial_qw(q)=radial_qw(q)+qw_mat(p,q)
			radial_qo(q)=radial_qo(q)+qo_mat(p,q)
			radial_qt(q)=radial_qt(q)+qt_mat(p,q)
	  END DO
			radial_qw(q)=radial_qw(q)*0.5*3.14159
     1			*(0.5*radius(q)+0.5*radius(q+1))/NXE
			radial_qo(q)=radial_qo(q)*0.5*3.14159
     1			*(0.5*radius(q)+0.5*radius(q+1))/NXE
			radial_qt(q)=radial_qt(q)*0.5*3.14159
     1			*(0.5*radius(q)+0.5*radius(q+1))/NXE
	  END DO !permeabilty update-loop

		endif
		endif

	if (iteraor.eq.1) then
		do ii=1,nn8
			DUMPim1(ii)=DUMP(ii)
		enddo
		do ii=1,nn4
			a_sat_newim1(ii)=a_sat_new(ii)
		enddo
	endif

	if (iterator.gt.1.and.iterator.ne.n_iterator) then
		errp=0.0
		errsw=0.0
		do ii=1,nn8
			if(abs(DUMPim1(ii)-DUMP(ii)).gt.errp) 
     1				errp=abs(DUMPim1(ii)-DUMP(ii))
		enddo
			do ii=1,nn4
			if(abs(a_sat_newim1(ii)-a_sat_new(ii)).gt.errsw) 
     1				errsw=abs(a_sat_newim1(ii)-a_sat_new(ii))
		enddo
		if (errp.lt.errpmax.and.errsw.lt.errswmax) iterator=n_iterator-1
	endif
	if (iterator.eq.n_iterator) if_finish2=1
	if (if_auto_iter.eq.0) if_finish2=1


	do ii=1,nn8
		DUMPim1(ii)=DUMP(ii)
	enddo
	do ii=1,nn4
		a_sat_newim1(ii)=a_sat_new(ii)
	enddo


10000	 CONTINUE  !ITERATOR
		if (ns.gt.nt) then !if-Z1
			kkfl=0
			if (kkfl.eq.0) then !if-Z2

  			call velocitynew(nxe,nye,nn4,nn,nodep4,nodep,
     $				TERM,krw,dump,x,y,v_water,1000.0d0,igrav)
	!!Velocity of oil   in  m/Day
  			call velocitynew(nxe,nye,nn4,nn,nodep4,nodep,
     $				TERM,kro,dump,x,y,v_oil,900.0d0,igrav)


			velocit=v_water+v_oil

	CALL LINMUL(bkreac,LOADS,NEWLO,n,nW)
	do ii=1,n
		forcereac(ii)=NEWLO(ii)-forcereac(ii)
!		if (abs(forcereac(ii)).le.1.0d-8) forcereac(ii)=0.0d0
	enddo
!	open(8567,file='quantity.t19')
c	write(8567,*) '-------------',ns
	do i=1,n
c		write(8567,*) i,forcereac(i)/dt
	enddo
	do i=1,nn4
		CALL INTERP(SWT,SKROT,NUMOW,A_SAT_new(i),SKRO)
		CALL INTERP(SWT,SKRWT,NUMOW,A_SAT_new(i),SKRW)
		unv=SKRW/(SKRW+SKRo)*forcereac(nf(i_4_8_nodes(i),3))
c		forcereac(nf(i_4_8_nodes(i),3))=unv
	enddo

	do ii=1,nn8
		if(nf(ii,3).ne.0) then
c			write(8567,*) ii,forcereac(nf(ii,3))/dtim
		endif
	enddo
	qwater=0.0d0
	qoil=0.0d0
	do ii=1,n_bkfix
		if(nf(iabkfix(ii,0),3).ne.0) then
			if (iabkfix(ii,2).eq.1) then
				qwater(1)=qwater(1)+forcereac(nf(iabkfix(ii,0),3))
     $					/dtim*fracwater(iabkfix(ii,0))
				qoil(1)=qoil(1)+forcereac(nf(iabkfix(ii,0),3))
     $					/dtim*(1.0d0-fracwater(iabkfix(ii,0)))
			else
			endif
			if (iabkfix(ii,2).eq.2) then
			qwater(2)=qwater(2)+forcereac(nf(iabkfix(ii,0),3))
     $					/dtim*fracwater(iabkfix(ii,0))
			qoil(2)=qoil(2)+forcereac(nf(iabkfix(ii,0),3))
     $					/dtim*(1.0d0-fracwater(iabkfix(ii,0)))
			else
			endif	
		endif
	enddo
		if (geometrytype.eq.1) then !if-Z3
			IF (IMESH.GE.4) THEN
				do ii=1,2
					qwater(ii)=qwater(ii)*packerl*2.0D0
					qoil(ii)=qoil(ii)*packerl*2.0D0
				enddo
			ELSE
				do ii=1,2
					qwater(ii)=qwater(ii)*packerl*4.0D0
					qoil(ii)=qoil(ii)*packerl*4.0D0
				enddo				
			ENDIF
		else if (geometrytype.eq.2) then !if-Z3
				do ii=1,2
	qwater(ii)=qwater(ii)*2*pi*radius(1)*packerl/(radius(nye+1)-radius(1))
	qoil(ii)=qoil(ii)*2*pi*radius(1)*packerl/(radius(nye+1)-radius(1))
				enddo
		endif !if-Z3



	end if !if-Z2
	write(15,*) TIME_CAL,' ',PTIME,
     1	' ',qwater(1),' ',qoil(1),' ',qwater(2),' ',qoil(2)

	endif !if-Z1
811         continue 
!!	Gas Saturation update
	goto 8111
!	include 'gas_saturation_update.for'
8111	continue

        if (ns.eq.1) then
	!!SHunfu.Z 2010.01.25
			do p=1,nxe                                                                              
			do q=1,nye
	!!SHunfu.Z 2010.01.25
	       ex0 = ex(p,q,ig)
	       ey0 = ey(p,q,ig)
	       ez0 = ez(p,q,ig)
	     volstr0(p,q,ig)=ex(p,q,ig) + ey(p,q,ig) + ez(p,q,ig)
	!!SHunfu.Z 2010.01.25
			end do                                                                                  
			end do
	!!SHunfu.Z 2010.01.25
	 end if
1239      format(3f20.12) 	
			do p=1,nxe                                                                              
			do q=1,nye                                                                              
				do k=1,ng                                                                             
					volst0(p,q,k)=volstr(p,q,k) !! update the volume strain for all elements 
c					porop0(p,q,k)=poro(p,q,k)                                                         
				end do                                                                                
			end do                                                                                  
			end do                                                                                  
	!!nn8= the number of nodes in eight-node system                                                                                                                                                                         
			DO i=1,nn8                                                                        
				DUMP_OLD(i)=DUMP(i)                                                                      
			END DO
c 	call calstress(nxe,nye,nn,nodep,x,y,sy,sx,sxy,srzsf,stzsf,taozsf)
	!!单元结果顺序输出ex,ey,ez,exy,eyz,volstr (Unit results order output)
	!!                sx,sy,taozsf,sxy,srzsf,stzsf
	!!节点结果顺序输出dispx,dispy,press,saturation  (The node results order output)
!	call mscmarcout(nxe,nye,nn,n,nn4,nodep,
c     $sx,sy,sxy,sz,ex,ey,exy,ez,loads,volstr,sr,st,a_sat8)
!     $sx,sy,sxy,taozsf,ex,ey,exy,ez,loads,volstr,srzsf,stzsf,a_sat8)
c     $sx,sy,sxy,taozsf,ex,ey,exy,ez,loads,volstr,sr,st,a_sat8) 

!	include 'recordings.for'


32654 continue

C----------------------ANALYTICAL SOLUTION FOR ENERGY EQUATION -----------------------------------------
	if (ns.eq.22) then
	do i=1,101
	r1A=radius(1)
	r1B=radius(NYE+1)
	r11=(i-1.0)/100.0*(r1B-r1A)+r1A
	T1A=Tiner
	T1B=Tout
	T11cond=T1A+(T1B-T1A)*log(r11/r1a)/log(r1B/r1A) !steady-state conductive
	if(if_q1.eq.1) 
     1	T11cond=T1B-q1*r1A/permx*log(r11/r1B)
	
	P1A=PDELTA(2)
	P1B=p0
	P11=P1A+(P1B-P1A)*log(r11/r1a)/log(r1B/r1A) !solution to Lap(p)=0.0
	if(if_GRAV_BC_pass.eq.0) then
	if(IMESH.eq.1) then
	P11=P11+rho*9.8e-6*(0.0-r11*sqrt(2.0)/2.0)
	elseif (IMESH.eq.3) then
	P11=P11+rho*9.8e-6*0.0
	endif
	endif
	n11=-(P1B-P1A)/log(r1B/r1A)/PERMX*RKE_X*0.08527/POROSITY
	T11=T1A+(T1B-T1A)*((r11/r1A)**n11-1.0)/((r1B/r1A)**n11-1.0)
c	read*,a1
	T11test=r11**2.0

	enddo
	endif

C---------------------------------------------------------------------
! >>>> Dr. Fotios Karaoulanis >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
c      call to_vtk(ns, nn, nn4,x, y, nxe, nye, nodep, nf,
c     &            loads, sx, sy, sz, sxy, ex, ey, ez, exy,
c     &            volstr, nelem, sw, so, sg, poro,st,sr,tt,kxy,dd)
8     continue !time-loop


c**************** Cohesion vs drawdown and porosity change  **************
 88   continue  !! produce finished  
            rkf(ico,11) = coh
	      rkf(ico,12) = drawdown
	      rkf(ico,13) = bhp
	      rkf(ico,14) = rep
	do i=1,nn_s
ccc	WRITE(*,3335) i,coh_p(i),yielding_p(i),sanding_p(i),plastic_p(i)                                                                     
	WRITE(T_OUT,3335) i,coh_p(i),yielding_p(i),sanding_p(i),plastic_p(i)
 3335 format('sanding_P=',i3,4(1x,f15.5))           
	! CALL WRTF(T_OUT)                                                                
	end do
	if( il .eq. 101) ntl = ns
c**************** Stresses Distributions  ********************************
c**** Recording bhp etc for sanding.  sept28 EQ
      if (nrec.eq.0) then
	    reptime=0.d0
	    rebhp=0.d0
	    recoh=coh
	END IF
	write(10,882) recoh,reptime,rebhp,nrec
 882  format(1x,'1',3E14.4,I3)
c**** Recording bhp etc for yielding.  sept28 EQ 
      if (nrecp.eq.0) then
	    reptimep=0.d0
	    rebhpp=0.d0
	    recohp=coh
	end if
	write(10,881) recohp,reptimep,rebhpp,nrecp
 881  format(1x,'0',3E14.4,I3)
 5000   continue

      do i=1,nn_s
ccc	WRITE(*,3335) i,coh_p(i),yielding_p(i),sanding_p(i),plastic_p(i)                                                                     
	WRITE(T_OUT,3335) i,coh_p(i),yielding_p(i),sanding_p(i),plastic_p(i)
      ! CALL WRTF(T_OUT)                                                                
	end do

5600	format(A,I2,A,I2,A,I2,A,I2,A,F,A)  


c	call Bashar_test_2013_08_28

	call itime(current_time)
78910	return
	end ! subroutine FEM
