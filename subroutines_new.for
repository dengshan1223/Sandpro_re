c============================================================================================
	subroutine nodal_to_GP_file_exporter(dump,nxe,nye,nodep,x,y
!     1 		,fname,IMESH)
     1 		,IMESH) 
	real*8 P_GP(40,80,2,2),x_GP(40,80,2,2),y_GP(40,80,2,2)
	real*8 dump(5000),p_elem(4)
	integer ii,jj,nxe,nye,in,i,j,gp2,ig,ider,nodep(40,80,8)
	real*8 coordf(4,2),samp2(10,2),der(2,8),fun(8)
	real*8 x(4000),y(4000)
	character*256 fname
	integer IMESH
	
	ider=2
	do 21 ii=1,nxe !element-loop                                                                    
	do 21 jj=1,nye
		do in=1,4                                                                         
              p_elem(in)=dump(nodep(ii,jj,(2*in-1)))
			coordf(in,1)=x(nodep(ii,jj,(2*in-1)))
			coordf(in,2)=y(nodep(ii,jj,(2*in-1)))
		end do
	
		GP2=2                                                                                                                                                                 
		do 3001 i=1,gp2 !Gauss point-loop                                                                            
		do 3001 j=1,gp2                                                                            
			call gauss2(samp2,gp2)
			call formln2(der,ider,fun,samp2,gp2,i,j)                                              
			p_GP(ii,jj,i,j)=0.0
			x_GP(ii,jj,i,j)=0.0
			y_GP(ii,jj,i,j)=0.0
			do in=1,4
				p_GP(ii,jj,i,j)=p_GP(ii,jj,i,j)+FUN(in)*p_elem(in)
				x_GP(ii,jj,i,j)=x_GP(ii,jj,i,j)+FUN(in)*coordf(in,1)
				y_GP(ii,jj,i,j)=y_GP(ii,jj,i,j)+FUN(in)*coordf(in,2)
              enddo
			
 3001		continue                                                                                                                                                                   
 21   continue
	


	end subroutine nodal_to_GP_file_exporter
c=============================================================================================
	subroutine GP_to_GP_file_exporter(dump,nxe,nye,nodep,x,y
!     1 		,fname,IMESH) 
     1 		,IMESH)
	real*8 P_GP(40,80,2,2),x_GP(40,80,2,2),y_GP(40,80,2,2)
	real*8 dump(40,80,4),p_elem(4)
	integer ii,jj,nxe,nye,in,i,j,gp2,ig,ider,nodep(40,80,8)
	real*8 coordf(4,2),samp2(10,2),der(2,8),fun(8)
	real*8 x(4000),y(4000)
	character*256 fname
	integer IMESH
	

	ider=2
	do 21 ii=1,nxe !element-loop                                                                    
	do 21 jj=1,nye
		do in=1,4                                                                         
			coordf(in,1)=x(nodep(ii,jj,(2*in-1)))
			coordf(in,2)=y(nodep(ii,jj,(2*in-1)))
		end do
	
		GP2=2
		                                                                                                                                                                
		do 3001 i=1,gp2 !Gauss point-loop                                                                            
		do 3001 j=1,gp2                                                                            
			call gauss2(samp2,gp2)
			call formln2(der,ider,fun,samp2,gp2,i,j)                                              
			x_GP(ii,jj,i,j)=0.0
			y_GP(ii,jj,i,j)=0.0
			do in=1,4
				x_GP(ii,jj,i,j)=x_GP(ii,jj,i,j)+FUN(in)*coordf(in,1)
				y_GP(ii,jj,i,j)=y_GP(ii,jj,i,j)+FUN(in)*coordf(in,2)
              enddo
			

 3001		continue  
 			p_GP(ii,jj,1,1)=dump(ii,jj,1)
              p_GP(ii,jj,1,2)=dump(ii,jj,2)
			p_GP(ii,jj,2,1)=dump(ii,jj,3)   
			p_GP(ii,jj,2,2)=dump(ii,jj,4)                                                                                                                                             
 21   continue
	

	end subroutine GP_to_GP_file_exporter
c=============================================================================================
	subroutine nodal_to_nodal_file_exporter(dump,nxe,nye,nodep,x,y
!     1 		,fname,IMESH)
     1 		,IMESH) 
	real*8 P_GP(40,80,2,2),x_GP(40,80,2,2),y_GP(40,80,2,2)
	real*8 dump(5000),p_elem(4)
	integer ii,jj,nxe,nye,in,i,j,gp2,ig,ider,nodep(40,80,8)
	real*8 coordf(4,2),samp2(10,2),der(2,8),fun(8)
	real*8 x(4000),y(4000)
	character*256 fname
	integer IMESH
	

	ider=2
	do 21 ii=1,nxe !element-loop                                                                    
	do 21 jj=1,nye
		do in=1,4                                                                         
              p_elem(in)=dump(nodep(ii,jj,(2*in-1)))
			coordf(in,1)=x(nodep(ii,jj,(2*in-1)))
			coordf(in,2)=y(nodep(ii,jj,(2*in-1)))
		end do
	
		GP2=2                                                                                                                                                                 
		do 3001 i=1,gp2 !Gauss point-loop                                                                            
		do 3001 j=1,gp2                                                                            
			call gauss2(samp2,gp2)
			call formln2(der,ider,fun,samp2,gp2,i,j)                                              
			p_GP(ii,jj,i,j)=0.0
			x_GP(ii,jj,i,j)=0.0
			y_GP(ii,jj,i,j)=0.0
			do in=1,4
				p_GP(ii,jj,i,j)=p_GP(ii,jj,i,j)+FUN(in)*p_elem(in)
				x_GP(ii,jj,i,j)=x_GP(ii,jj,i,j)+FUN(in)*coordf(in,1)
				y_GP(ii,jj,i,j)=y_GP(ii,jj,i,j)+FUN(in)*coordf(in,2)
              enddo
			
 3001		continue                                                                                                                                                                   
 21   continue
	
	if (IMESH.eq.1.or.IMESH.eq.5) then
	do jj=1,NYE
		do ii=1,NXE
			i=nodep(ii,jj,3)
		enddo
		ii=NXE
		i=nodep(ii,jj,5)
	enddo
	jj=NYE
	do ii=1,NXE
		i=nodep(ii,jj,1)
	enddo
	ii=NXE
	i=nodep(ii,jj,7)

	endif




	if (IMESH.eq.5) then
	endif
	

	end subroutine nodal_to_nodal_file_exporter
c=============================================================================================

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine gauss2(samp2,gp2)
c 
c*** This subroutine forms Gaussian quadrature abscissae and weights ***
c
       Implicit real*8 (a-h, o-z)
       integer gp2,i
       real*8 samp2(10,2)
	 
	 do i=1,gp2
	 samp2(i,1)=(i-1)*2.0/(gp2-1)-1.0
	 samp2(i,2)=1.0
       enddo
		
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine formln2(derf,ider,funf,samp2,isamp,i,j)
c
c***** This subroutine forms the shape functions FUN and their derivatives          *****
c***** DER for 4-Noded quadrilateral elements at Gaussian point(I,J)                *****
c
       Implicit real*8 (a-h, o-z)
       integer  ider,isamp,i,j
       real*8 derf(2,4),funf(4),samp2(10,2)
       real*8 eta,xi,etam,etap,xim,xip
       eta=samp2(i,1)
       xi=samp2(j,1)
       etam=0.25D0*(1.0D0-eta)
       etap=0.25D0*(1.0D0+eta)
       xim =0.25D0*(1.0D0-xi)
       xip= 0.25D0*(1.0D0+xi)
       funf(1)=4.0D0*xim*etam
       funf(2)=4.0D0*xim*etap
       funf(3)=4.0D0*xip*etap
       funf(4)=4.0D0*xip*etam
       derf(1,1)=-etam
       derf(1,2)=-etap
       derf(1,3)=etap
       derf(1,4)=etam
       derf(2,1)=-xim
       derf(2,2)=xim
       derf(2,3)=xip
       derf(2,4)=-xip
       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine formreg(kb,ikb,km,ikm,g,w,dof,printer)
c
c******** This subroutine assembles element matrices into        *******
c******** unsymmetrical band globle matix(stored as a rectangle) *******
c
       Implicit NONE
       real*8 kb(400,400),km(4,4)
       integer ikb,ikm,g(20),w,dof,i,j,cd,k,l,printer
		
       do 1 i=1,dof
       if(g(i).eq.0) go to 1
       do 2 j=1,dof
       if(g(j).eq.0) go to 2

       kb(g(i),g(j))=kb(g(i),g(j))+km(i,j)
  2    continue
  1    continue
c	!!stop '======'
       return
       end
c======================================================
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c============================================================================================
	subroutine nodal_to_GP(dumx,X_GP2,X_GP3,nxe,nye,nodep) 
	real*8 X_GP2(40,80,4),X_GP3(40,80,9),dumx(5000),x_elem(4)
	integer ii,jj,nxe,nye,in,i,j,gp,ig,ider,nodep(40,80,8)
	real*8 coordf(4,2),samp(5,2),der(2,8),fun(8)
	ider=2
	do 21 ii=1,nxe !element-loop                                                                    
	do 21 jj=1,nye
		do in=1,4                                                                         
              x_elem(in)=dumx(nodep(ii,jj,(2*in-1)))
						                                                           
		end do
	
		gp=2
		ig=0                                                                                     
		do 31 i=1,gp !Gauss point-loop                                                                            
		do 31 j=1,gp                                                                            
			ig=ig+1
			call gauss(samp,gp)
			call formln(der,ider,fun,samp,gp,i,j)                                              
			X_GP2(ii,jj,ig)=0.0
			do in=1,4
				X_GP2(ii,jj,ig)=X_GP2(ii,jj,ig)+FUN(in)*x_elem(in)
              enddo

 31		continue
 
		GP=3                                                                                                                                                                   
		ig=0                                                                                     
		do 3001 i=1,gp !Gauss point-loop                                                                            
		do 3001 j=1,gp                                                                            
			ig=ig+1
			call gauss(samp,gp)
			call formln(der,ider,fun,samp,gp,i,j)                                              
			X_GP3(ii,jj,ig)=0.0
			do in=1,4
				X_GP3(ii,jj,ig)=X_GP3(ii,jj,ig)+FUN(in)*x_elem(in)
              enddo

 3001		continue                                                                                                                                                                   

 21   continue


	end subroutine nodal_to_GP
c=============================================================================================
	subroutine GP2_to_nodal(dumx,X_GP2,nxe,nye,nodep,ind) 
c	The following code lines are partly copied from SAT_GAUSS
	implicit none
	integer nxe,nye,nodep(40,80,8),nodep4(40,80,4)
	real*8 samp(2,2),derf(2,4),funf(4),pp(4),ppn(4),coor(4,2)
	real*8 press,pressn,temp,temp1
	integer gp,ii,jj,i,j,ig,ind
	real*8 X_GP2(40,80,4,2),X_GP3(40,80,9),dumx(2000),x_elem(4)
	integer in,ider
	real*8 coordf(4,2),der(2,8),fun(8)

	gp=2
	call gauss(samp,gp)

	coor(1,1)=-dsqrt(3.0d0)
	coor(1,2)=-dsqrt(3.0d0)
	coor(2,1)=-dsqrt(3.0d0)
	coor(2,2)=dsqrt(3.0d0)
	coor(3,1)=dsqrt(3.0d0)
	coor(3,2)=dsqrt(3.0d0)
	coor(4,1)=dsqrt(3.0d0)
	coor(4,2)=-dsqrt(3.0d0)
	do ii=1,nxe
	do jj=1,nye
		do i=1,4
c			if (ind.eq.0) pp(i)=X_GP2(ii,jj,i)
			if (ind.eq.1) pp(i)=X_GP2(ii,jj,i,1)
			if (ind.eq.2) pp(i)=X_GP2(ii,jj,i,2)
		enddo
		ig=0
		do i=1,gp
		do j=1,gp
			ig=ig+1
			funf(1)=(coor(ig,1)+1.0d0)*(coor(ig,2)+1.0d0)/4.0d0
			funf(2)=(-coor(ig,1)+1.0d0)*(coor(ig,2)+1.0d0)/4.0d0
			funf(3)=(coor(ig,1)+1.0d0)*(-coor(ig,2)+1.0d0)/4.0d0
			funf(4)=(-coor(ig,1)+1.0d0)*(-coor(ig,2)+1.0d0)/4.0d0
			call matmul(funf,1,pp,4,press,1)
			dumx(nodep(ii,jj,2*ig-1))=press
		enddo
		enddo		
	enddo
	enddo

	end subroutine GP2_to_nodal
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
       subroutine formln_uw(derf2,ider,funf2,samp,isamp,i,j,uwr1,uwr2
     1 ,nxi,dump_g)
		
c***** This subroutine forms the shape functions FUN and their derivatives          *****
c***** DER for 4-Noded quadrilateral elements at Gaussian point(I,J)                *****
c
c      Implicit real*8 (a-h, o-z)
      integer  ider,isamp,i,j,nxi
      real*8 derf2(2,4),funf2(4),samp(isamp,2)
      real*8 eta,xi,alpha1,alpha2,beta1,beta2,beta0
	real*8 uwr1,uwr2,dump_g(4)
	real*8 etam1,etap1,xim1,xip1,etam2,etap2,xim2,xip2
	real*8 dxim1,dxip1,detam1,detap1,dxim2,dxip2,detam2,detap2,a111
		
	if (abs(dump_g(1)-dump_g(2)).lt.1.0e-3) then
		alpha1=0.0
	elseif (dump_g(1).gt.dump_g(2)) then
		alpha1=3.0*uwr1
	else
		alpha1=-3.0*uwr1
	endif

	if (abs(dump_g(3)-dump_g(4)).lt.1.0e-3) then
		alpha2=0.0
	elseif (dump_g(4).gt.dump_g(3)) then
		alpha2=3.0*uwr1
	else
		alpha2=-3.0*uwr1
	endif

	if (abs(dump_g(3)-dump_g(2)).lt.1.0e-3) then
		beta1=0.0
	elseif (dump_g(2).gt.dump_g(3)) then
		beta1=3.0*uwr2
	else
		beta1=-3.0*uwr2
	endif

	if (abs(dump_g(1)-dump_g(4)).lt.1.0e-3) then
		beta2=0.0
	elseif (dump_g(1).gt.dump_g(4)) then
		beta2=3.0*uwr2
	else
		beta2=-3.0*uwr2
	endif
c	alpha1=0.0
c	alpha2=0.0
c	beta1=0.0
c	beta2=0.0

	if(j.eq.12) then
		xi=-1.0
		eta=0.0
	elseif(j.eq.23) then
		xi=0.0
		eta=1.0
	elseif(j.eq.34) then
		xi=1.0
		eta=0.0
	elseif(j.eq.41) then
		xi=0.0
		eta=-1.0
	else
		xi=samp(j,1)
		if (i.eq.0) then                                                                            n
			eta=1.0
		else
			eta=samp(i,1)
		endif
	endif

	 		

c	if (nxi.eq.1) xi=1.d0
c	if (nxi.eq.2) xi=-1.d0
c	if (nxi.eq.3) eta=1.d0
c	if (nxi.eq.4) eta=-1.d0

      etam1=0.5D0*(1.0D0-eta)-alpha1/4.0*(1.0-eta*eta)
      etap1=0.5D0*(1.0D0+eta)+alpha1/4.0*(1.0-eta*eta)
      xim1 =0.5D0*(1.0D0-xi)-beta1/4.0*(1.0-xi*xi)
      xip1= 0.5D0*(1.0D0+xi)+beta1/4.0*(1.0-xi*xi)
      etam2=0.5D0*(1.0D0-eta)-alpha2/4.0*(1.0-eta*eta)
      etap2=0.5D0*(1.0D0+eta)+alpha2/4.0*(1.0-eta*eta)
      xim2 =0.5D0*(1.0D0-xi)-beta2/4.0*(1.0-xi*xi)
      xip2= 0.5D0*(1.0D0+xi)+beta2/4.0*(1.0-xi*xi)
	 
	detam1=0.5D0*(-1.0)-alpha1/4.0*(-2.0*eta)
      detap1=0.5D0*(+1.0)+alpha1/4.0*(-2.0*eta)
      dxim1 =0.5D0*(-1.0)-beta1/4.0*(-2.0*xi)
      dxip1= 0.5D0*(+1.0)+beta1/4.0*(-2.0*xi)
	detam2=0.5D0*(-1.0)-alpha2/4.0*(-2.0*eta)
      detap2=0.5D0*(+1.0)+alpha2/4.0*(-2.0*eta)
      dxim2 =0.5D0*(-1.0)-beta2/4.0*(-2.0*xi)
      dxip2= 0.5D0*(+1.0)+beta2/4.0*(-2.0*xi)
       
	 funf2(1)=xim2*etam1
       funf2(2)=xim1*etap1
       funf2(3)=xip1*etap2
       funf2(4)=xip2*etam2

       derf2(1,1)=dxim2*etam1
       derf2(1,2)=dxim1*etap1
       derf2(1,3)=dxip1*etap2
       derf2(1,4)=dxip2*etam2

       derf2(2,1)=xim2*detam1
       derf2(2,2)=xim1*detap1
       derf2(2,3)=xip1*detap2
       derf2(2,4)=xip2*detam2

		if (nxi.eq.100) then
	write(691,*),"[dump_g]=",dump_g
	write(691,*),"[alpha1,alpha2,beta1,beta2]=",alpha1,alpha2,beta1,beta2
		endif

       return
       end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc.................Carl Change.................
      SUBROUTINE INTERP2(X,Y,N,XO,YO)
	 IMPLICIT NONE
C	 REAL*8  X(200),Y(200),XO,YO
       INTEGER N,I
	 REAL*8  X(N),Y(N),XO,YO


      IF(XO.GE.X(N)) then
		YO=Y(N)
	endif 
      
	IF(XO.GE.X(N)) then
	RETURN
	endif
	
	IF(XO.LE.X(1)) then
	YO=Y(1)
      endif

	IF(XO.LE.X(1)) then
	RETURN
	endif

      DO 11 I=2,N
      IF(XO.GE.X(I)) then
	GO TO 11
	endif

      YO=Y(I-1) +(XO-X(I-1))*(Y(I)-Y(I-1))/(X(I)-X(I-1))
      RETURN
  11  CONTINUE
      END
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	subroutine dy_dxT_maker(xt,yt,n,dy_dxT)

	integer n,i
	real*8 xt(n),yt(n),dy_dxT(n)

	i=1
	dy_dxT(i)=(yt(i+1)-yt(i))/(xt(i+1)-xt(i))

	do i=2,n-1
	dy_dxT(i)=(yt(i+1)-yt(i-1))/(xt(i+1)-xt(i-1))
	enddo

	i=n
	dy_dxT(i)=(yt(i)-yt(i-1))/(xt(i)-xt(i-1))

	end subroutine dy_dxT_maker
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	subroutine KR_finder(sw1,SKRW1,dkw_ds1,SKRO1,dko_ds1,ref_ind)

	real*8 sw1,SKRW1,dkw_ds1,SKRO1,dko_ds1,Swc,Sor,nw,no,krw_end
	real*8 swa,swb,SKRWa,SKRWb,SKROa,SKROb,dkw_dsa,dkw_dsb,dko_dsa
	real*8 dko_dsb,sw11
	integer ref_ind !1 for Wells, 2 for Emily reprot
	!Schrefler & Scotta 2001 paper
c	SKRW1=1.0-2.207*(1.0-sw1)**1.0121
c	dkw_ds=2.207*1.0121*(1.0-sw1)**0.0121
c	SKRO1=0.0
c	dko_ds=0.0

	if (ref_ind.eq.1) then
	!Wells et al. 2008 paper
	sw11=sw1
	if (sw1.lt.0.16) sw11=0.16
	if (sw1.gt.0.84) sw11=0.84
	krw_end=0.5
	Swc=0.15
	Sor=0.15
	nw=3.5
	no=1.5
	SKRW1=krw_end*((sw11-Swc)/(1.0-Swc-Sor))**nw
	dkw_ds1=krw_end*nw/((1.0-Swc-Sor))**nw*(sw11-Swc)**(nw-1)
	SKRO1=((1.0-Sor-sw11)/(1.0-Swc-Sor))**no
	dko_ds1=-no/((1.0-Swc-Sor))**no*(1.0-Sor-sw11)**(no-1)

	goto 12213
c	Linear functions
C	swa=0.2
C	swb=0.8
C	SKRWa=krw_end*((swa-Swc)/(1.0-Swc-Sor))**nw
C	dkw_dsa=krw_end*3.5/((1.0-Swc-Sor))**nw*(swa-Swc)**(nw-1)
C	SKROa=((1.0-Sor-swa)/(1.0-Swc-Sor))**no
C	dko_dsa=1.5/((1.0-Swc-Sor))**no*(1.0-Sor-swa)**(no-1)
C	
C	SKRWb=krw_end*((swb-Swc)/(1.0-Swc-Sor))**nw
C	dkw_dsb=krw_end*3.5/((1.0-Swc-Sor))**nw*(swb-Swc)**(nw-1)
C	SKROb=((1.0-Sor-swb)/(1.0-Swc-Sor))**no
C	dko_dsb=1.5/((1.0-Swc-Sor))**no*(1.0-Sor-swb)**(no-1)
C
C	SKRW1=SKRWa+(sw11-swa)*(SKRWb-SKRWa)/(swb-swa)
C	SKRO1=SKROa+(sw11-swa)*(SKROb-SKROa)/(swb-swa)
C	dkw_ds1=dkw_dsa+(sw11-swa)*(dkw_dsb-dkw_dsa)/(swb-swa)
C	dko_ds1=dko_dsa+(sw11-swa)*(dko_dsb-dko_dsa)/(swb-swa)
12213  continue
	else  !if (ref_ind.eq.2) then
	sw11=sw1
	if (sw1.lt.0.01) sw11=0.01
	if (sw1.gt.0.99) sw11=0.99
	krw_end=1.0
	Swc=0.0
	Sor=0.0
	nw=3.0
	no=3.0
	SKRW1=krw_end*((sw11-Swc)/(1.0-Swc-Sor))**nw
	dkw_ds1=krw_end*nw/((1.0-Swc-Sor))**nw*(sw11-Swc)**(nw-1)
	SKRO1=((1.0-Sor-sw11)/(1.0-Swc-Sor))**no
	dko_ds1=-no/((1.0-Swc-Sor))**no*(1.0-Sor-sw11)**(no-1)

	endif
	end subroutine KR_finder

c=============================================================
	subroutine exchange(a1,a2)
	real*8 a0,a1,a2
	
	a0=a1
	a1=a2
	a2=a0
	end subroutine exchange 
c====================================================
