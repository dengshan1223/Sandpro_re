
	subroutine fract(kay,krw,fracwater,nxe,nye,nodep)
	implicit none
	real*8 kay(40,80,4,2),krw(40,80,4,2),fracwater(5000)
	integer nxe,nye,nodep(40,80,8)
	integer ii,jj,i
	real*8 funf(4),coor(8,2),pp(4),press,ppw(4),pressw
	!! extrapolation
	coor(1,1)=dsqrt(3.0d0)
	coor(7,1)=dsqrt(3.0d0)
	coor(8,1)=dsqrt(3.0d0)

	coor(2,1)=0.0d0
	coor(6,1)=0.0d0

	coor(3,1)=-dsqrt(3.0d0)
	coor(4,1)=-dsqrt(3.0d0)
	coor(5,1)=-dsqrt(3.0d0)

	coor(5,2)=dsqrt(3.0d0)
	coor(6,2)=dsqrt(3.0d0)
	coor(7,2)=dsqrt(3.0d0)

	coor(4,2)=0.0d0
	coor(8,2)=0.0d0

	coor(1,2)=-dsqrt(3.0d0)
	coor(2,2)=-dsqrt(3.0d0)
	coor(3,2)=-dsqrt(3.0d0)
	do ii=1,nxe
	do jj=1,nye
		do i=1,4
			pp(i)=kay(ii,jj,i,1)
			ppw(i)=krw(ii,jj,i,1)
		enddo
		do i=1,8
			funf(1)=(coor(i,1)+1.0d0)*(coor(i,2)+1.0d0)/4.0d0
			funf(2)=(-coor(i,1)+1.0d0)*(coor(i,2)+1.0d0)/4.0d0
			funf(3)=(coor(i,1)+1.0d0)*(-coor(i,2)+1.0d0)/4.0d0
			funf(4)=(-coor(i,1)+1.0d0)*(-coor(i,2)+1.0d0)/4.0d0
			call matmul(funf,1,pp,4,press,1)
			call matmul(funf,1,ppw,4,pressw,1)
			fracwater(nodep(ii,jj,i))=pressw/press
		enddo		
	enddo
	enddo




	end


       !P-G法，权函数与形函数不一样，考虑重力
	 SUBROUTINE SAT_NEW_PG(NODEP,poro,dump,x,y,kay,volste,volst0,
	1       term,ns,ptime,i_8_4_nodes,i_4_8_nodes,IABKFIX,N_BKFIX,
     1		A_SAT,NODEP4,dump_old,number,
     1         n1_deload,i1elemx,i1elemy,i1elm_deload,ang1_elm_deload,                  
     1        n3_deload,i3elemx,i3elemy,i3elm_deload,ang3_elm_deload,                   
     1        n2_deload,i2elemx,i2elemy,i2elm_deload,ang2_elm_deload)
      Implicit real*8 (a-h, o-z)
      integer p,q,r,cdmax,dof,steps,tot,band,h,t,gp,g(20),nk(3000),
     &        dt(200),sandg(40,80,4),nelem(40,80,4)
      real*8 det,kxp,E,v,dtim,quot,theta,hards,ou,dee(4,4)
     & ,samp(3,2),coord(8,2),jac(2,2),jac1(2,2)
     & ,derivt(4,2),poro(40,80,4),kxy(40,80,4,2),der(2,8),deriv(2,8)
     & ,kay1(2,2),qcom1(2,2),kderiv(2,4),bee(4,16),dbee(4,16),bt(16,4)
     & ,btdb(16,16),dtkd(4,4),km(16,16),kp(4,4),ke(20,20),kd(20,20)
     & ,fun(8),funf(4),coordf(4,2),derf(2,4),derivf(2,4),tforce(5000)
     & ,c(16,4),volf(16,4),eldf,eldfp,mc(3,2),fmderiv(3,4),eld(20)
     & ,vol(16),eps(4),sigma(4),tbdyld(5000),dpl(4,4),eload(16)
     & ,bload(16),tload(5000),dloads(5000),bdylds(5000),oldlds(5000)
     & ,sx(40,80,4),EN(40,80,4),sy(40,80,4),sz(40,80,4),sxy(40,80,4)
     & ,ex(40,80,4),ey(40,80,4),ez(40,80,4),exy(40,80,4),diff(900)
     & ,er(40,80,4),sr(40,80,4),pl(4,4),elso(4),delso(4),et(40,80,4)
     & ,st(40,80,4),SOI,SWI,SWWC,SKRO,SKRW,volstr(40,80,4)
     & ,OUTOIL,OUTWAT,bhp,sh3,mud,np,coh,COHI,COEFR,BETA,COEFP,PS0,KR2
C     & ,SM,S0MAX,KR1,P0,SHV,loads(18000),ans(18000),pb(18000,851)
C     & ,bk(7000000),dump(18000),dumu(18000),loads0(18000)
C     & ,dumux(18000),dumuy(18000),dumut(18000)
C     & ,force(18000),x(18000),y(18000),prelds(18000),qcom(40,80,4)
     & ,qderiv(2,8),dtkq(4,4),PNOW,RSO0,kay(40,80,4,2)
	1 ,seld(4),RSOO(40,80),krw(40,80,4,2),kro(40,80,4,2)
	1 ,volste(40,80,4),volst0(40,80,4),dump(5000),x(5000),y(5000)

	integer lq(50),Rpold(50),iq(50),nodep(40,80,8),nodeg(40,80,20)
c******* nsandi for rescording the time of sanding for each cohesion
c******* npelasti for rescording the time of yielding for each cohesion
	integer mcc(1000,4),nsandi(101),npelasti(101)
      integer problemtype,modeltype,damagetype,geometrytype,ctype
     & ,completiontype,reservoirtype,failuretype,stresstype,filtercake
c
      data ijac,ijac1,ikay,ider,ideriv,ikderv,T,iderf,idervf/9*2/
      data isamp,nodof/2*3/
c********* idee,ibee,idbee should be 3 in plan stress and plan strain *************
      data idee,ibee,idbee/3*3/
      data icordf,idtkd,ikp,nodf/4*4/
      data icoord,idervt,nod/3*8/
      data ike,ikd,tot/3*20/
      data ibt,ikm,ic,ivolf,dof,ibtdb/6*16/
      data ipl,idpl/2*4/
	COMMON/TMP1/nf(8000,3), nft(8000,1)
      COMMON/INITIAL/shmaxg,shming,szg,pbg,rw,rw2,pw,z,rb
      COMMON/GEOMECH/E,v,cou,col,phi,csco,psi,Biot,Biotp,cs,Ts,icode
      COMMON/RESERVIOR/PO(20),RSO(20),RKE_X,RKE_Y,rkp,VIS_O,VIS_W,qf,
     &                bm,COMP_O,COMP_W,porosity,porey,BUBP,NUMO
      COMMON/SUBNONL/Sc,Sv,CM,Qc,AP,AB,AL,am,ai
      COMMON/SUBFIELD/TIMEF,vs,pores,wks,CT,WD,Frac,S0,
     &                C_DISPLACEMENT,C_DSP2
      COMMON/SUBINITIAL/sigmah,pa,af,az,amhs,mud,pd,pay,pradius,packerl
      COMMON/SUBRESER/w0,ww,ak,BB,vu
      COMMON/OPTNUMERI1/NT,NX,NLOAD
      COMMON/OPTNUMERI2/dr,T1,Dtim
      COMMON/OPTFEM1/NXE,NYE,NYE0,nyl,NN,NN4,N,NW,GP,NL,Its,nntemp,ntemp
      COMMON/OPTFEM2/THETA,TOL
      COMMON/DATAIT/nxN, It
      COMMON/DATA/PHI0,PHIMAX,SK0,SKC,YS,YF,UF,C0,SNMD,SNMD0,
     &            Err,YA0,WORM_PHI(1000),
     &      WORM_SK(1000),WORM_C(1000),
     &      WORM_YA(1000),WORM_X(1000),WORM_T_PHI(1000),
     &      WORM_T_SK(1000),WORM_T_C(1000),WORM_Q(1000),
     &      WORM_TMPC(1000)
      COMMON/OPTIONS/problemtype,modeltype,damagetype,geometrytype,
     *                completiontype,reservoirtype,failuretype,ctype,
     *                stresstype,filtercake,imethod,iflowtype,iproblem
      COMMON/PHASE2/SKROT(20),SKRWT(20),SWWC,SWT(20),SO(100,100),
     &              SW(100,100),SG(100,100),NUMOW
      COMMON/VAR_ADD1/COEFR,BETA,COEFP,PS0,SM,S0MAX,P0,SHV
c*******added data******************************************************
	COMMON/LOAD1/radius(51),rad2(51),pofix(1000),point(1000,2),gtheta,
	1gravy,pressa(1000,6),spoke(51),dofix(1000,3)
	COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)
	1,lodpt(1000),nbound,ndrill,ndirect
	COMMON/flow/flowqi,flowqo
	COMMON/flowc/nflowi,nflowo
	COMMON/loads1/pdate(1000),pdelta(1000),PDLTA2(1000)
	COMMON/loads2/npoint,npr(1000),npr2(1000),nstep(1000)

C
      COMMON/SOLUELE/S_X(1000),S_Y(1000),N_S_ELE(1000,4),
     *	N_S_NOD(100,100,4),
	1               S_PORO(1000),S_KAY(1000),S_DUMP(1000),S_SOLVE(1000),
	1               S_SOLVE0(1000)
C
!      dimension aa(380,30),soluca(380,380),solua(380),nbb(380),sww(380)
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82, 82),
     *i_v4nodes(82,42),N_N
	 dimension ia_sat_nodes(100,100),iaPQ(1000,0:1)

	DIMENSION A_SAT(1000), A_SAT1(1000),FUNFT(4)
	DIMENSION NODEP4(40,80,4)

	REAL*8 A_KW(2,2),A_C(4,4),A_F2(4),V_F(4),A_C_GLOBE(1000, 1000)
     * , V_F_GLOBE(1000), A_F2_GLOBE(1000),FAI_ROW_FUNF(4)
	REAL*8 A_L(4,4),A_L_GLOBE(1000, 1000),A_L2(4,4),FAI_ROW
	real*8 FAI_ROW_DVT_FUNF(4),FAI_ROW_DVT
	real*8 dump_old(5000),dump_gauss,DUMP_gauss_OLD
	real*8 dump_max,dump_min,dump_old_max
	common /iconsider/igrav
	integer i_8_4_nodes(1000),i_4_8_nodes(1000)
	real*8 GCOM(2),P_GRAD(2),F2BODY(2),DUMP_G(4)

	REAL*8 A_F2_1(2),A_F2_2(2),A_F2_3,P_NODE(4),KAY_NODE(4),pmax,pmin
	integer if_PC,if_g,if_BLE, if_AL2,jj,mm,if_BC_out,if_BC1,if_BC2
	real*8 A_BLE(4),A_BLE_GLOBE(1000)
	real*8 lambda_w,lambda_o,fw,x0,y0,mu_w,mu_o,SWGP,krwGP,kroGP
	real*8 ut,ut_vec(2),ww,sw1,sw2,sw0		
	integer n_bkfix,iabkfix(500,0:3),in,inn,if_lump_mass,if_BIntegral
      REAL*8 FUNF2(4),DERF2(2,4),DERIVF2(2,4),DERIVT2(4,2)
	real*8 uwr1,uwr2,uwr,PRESS(4),PRESS_nom(4),lumped_mass
	real*8 Sw_max,Sw_min
	integer if_limit_SW_BCbased
      integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	real*8 ang1_elm_deload(80,3)                                                           
      integer i2elemx(80),i2elemy(80),i2elm_deload(80,3),ip                           
	real*8 ang2_elm_deload(80,3)
	integer i3elemx(80),i3elemy(80),i3elm_deload(80,3),j12,m1                           
	real*8 ang3_elm_deload(80,3)
	integer n1_deload,n2_deload,n3_deload,if_PQ_BC,IBC,node_BC(2),jmax
	real*8 ang_BC,normal(2),side_length,flux,r0,radius1(80),dfw_ds 
	real*8 dkrwGP_ds,dkroGP_ds,dlambda_w_ds,dlambda_o_ds
     
	if_BC_out=0
	if_BC1=0
	if_BC2=1
	sw0=0.4
	sw1=0.4
	sw2=0.75
	uwr1=1.0
	uwr2=1.0
	uwr=abs(uwr1)+abs(uwr2)

	call nulvec(GCOM,2)
	GCOM(2) = -1.0D-6*9.8d0*900.0d0

	!A_SAT1 用于迭代
	DO I = 1, NN4
	A_SAT1(I) = A_SAT(I)
	END DO

      DO 1 K_ITER = 0, 50
      
	number=number+1
	CALL NULL(A_C_GLOBE,1000,1000)
	CALL NULL(A_L_GLOBE,1000,1000)
	CALL NULvec(A_F2_GLOBE,1000)
	CALL NULvec(V_F_GLOBE,1000)
	
	gp=2
      do 3 p=1,nxe
      do 3 q=1,nye

		!4节点
		do in=1,icordf
			inn=2*in-1
			coordf(in,1)=x(nodep(p,q,inn))
			coordf(in,2)=y(nodep(p,q,inn))
		end do
!*************************************************************************

		call null(A_C, 4, 4) !  A_C---- 单刚
		call null(A_L, 4, 4) !  A_L---- 单刚
		call null(A_L2, 4, 4) !  A_L2---- 单刚
		call nulvec(A_F2, 4)!  A_F2---- 单刚
		call nulvec(V_F, 4) !  V_F---- 右端项
	    
	    call nulvec(DUMP_G,4)
	    call nulvec(ELEMNET_2,4)
	    call nulvec(FAI_ROW_DVT_FUNF,4)
          ! 上游权
          SSK = 0.D0
	    DUMP_MAX=0.D0
	    DUMP_OLD_MAX=0.D0
		DO IP = 1, 4
		    NNP=NODEP(P,Q,IP*2-1)
			NNP4=NODEP4(P,Q,IP)
              if(DUMP(NNP).GT.DUMP_MAX) then
	           SSK=A_SAT(NNP4)
	        endif
			DUMP_MAX = MAX(DUMP_MAX, DUMP(NNP))
			DUMP_OLD_MAX = MAX(DUMP_OLD_MAX, DUMP_OLD(NNP))
		END DO
		DO II=1,4
			DUMP_G(II)=DUMP(NODEP(P,Q,2*II-1))
		ENDDO
          
		ig=0
		do 4 i = 1, gp
		do 4 j = 1, gp
			ig=ig+1
			CALL GAUSS(samp,gp)
	        call nulvec(f2body,2)

			call formln(derf,iderf,funf,samp,gp,i,j)
			call matmul(derf,iderf,coordf,icordf,jac,ijac)
			call twoby2(jac,ijac,jac1,ijac1,det)
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf)
              call matran(derivt1,nodf,derivf1,idervf)
	        call quotca(samp,gp,det,term,quot,i,j)

			nxi=0
			CALL FORMLN_UW(DERF2,IDERF,FUNF2,SAMP,GP,I,J,uwr1
     1				,uwr2,nxi,DUMP_G)
 			CALL MATMUL(JAC1,IJAC1,DERF2,IDERF,DERIVF2,NODF)
 			CALL MATRAN(DERIVT2,nodf,DERIVF2,IDERIVF)
			
			! 以下来源于钱博士的subroutine SATURATION
              PC=8.763D-3*SSK**(-1.D0/1.754D0)
	        pcpie=8.763D-3*(-1.D0/1.754D0)*SSK**(-1.D0/1.754D0-1.D0)

              ! 生成[L] 和 做右端项Po的系数阵一部分代码是共用的
              A_KW(1,1) = krw(p,q,ig,1)*1000.0
			A_KW(2,2) = krw(p,q,ig,2)*1000.0
			A_KW(1,2) = 0
			A_KW(2,1) = 0 

              !A_L
	        call matmul(A_KW,ikay,derivf,idervf,kderiv,nodf)
	        call matmul(derivt2,nodf,kderiv,ikderv,dtkd,idtkd)
	        PROD = QUOT * DTIM
	        CALL MSMULT(DTKD,PROD,IDTKD,NODF)
	        CALL MSMULT(DTKD,pcpie,IDTKD,NODF)
	        call matadd(A_L,ikp,DTKD,idtkd)

	        !A_F2
              call matmul(derivf,idervf,DUMP_G,nodf,f2body,1)
	        do KK=1,2
				P_GRAD(KK)= f2body(KK) + GCOM(KK)
			enddo
			call matmul(A_KW,ikay,P_GRAD,idervf,Tderiv,1)
			call matmul(derivt2,nodf,kderiv,ikderv,ELEMENT_2,1)
			call msmult(ELEMENT_2,PROD,nodf,1)
			call matadd(A_F2,nodf,ELEMENT_2,1)
			
			!做左端Sw的刚度阵
	        !A_C
			DO I1 = 1, 4
				FUNFT(I1)=FUNF2(I1)
			END DO 
	        FAI_ROW = PORO(P,Q,ig)*1000.0*QUOT
			CALL MATMUL(FAI_ROW,1,FUNF,1,FAI_ROW_FUNF,4)
			CALL MATMUL(FUNFT,4,FAI_ROW_FUNF,1,DTKD,4)
			call matadd(A_C,ikp,DTKD,idtkd)

	        !A_L2
			FAI_ROW_DVT=(DUMP_MAX-DUMP_OLD_MAX)*COMP_W*PORO(P,Q,ig)
     1		  *1000.0*QUOT+1000.0*(volstr(p,q,ig)-volst0(p,q,ig))*QUOT
			CALL MATMUL(FAI_ROW_DVT,1,FUNF,1,FAI_ROW_DVT_FUNF,4)
			CALL MATMUL(FUNFT,4,FAI_ROW_DVT_FUNF,1,DTKD,4)
			call matadd(A_L2,ikp,DTKD,idtkd)

		goto 4
4		continue

          ! 组装总刚
		do I=1,NODF
		do J=1,NODF
			I2=NODEP4(P,Q,I)
			J2=NODEP4(P,Q,J)
			A_F2_GLOBE(I2)=A_F2_GLOBE(I2)+A_F2(i)
			A_C_GLOBE(I2,J2)=A_C_GLOBE(I2,J2)+A_C(i,j)
			A_L_GLOBE(I2,J2)=A_L_GLOBE(I2,J2)-A_L(i,j)
			A_L_GLOBE(I2,J2)=A_L_GLOBE(I2,J2)+A_L2(i,j) ! consider d(ROU_w)/dt only
	    END DO
	    END DO

 3		CONTINUE

	! 生成总的右端项[A_F2_GLOBE]
	CALL NULvec(V_F_GLOBE,1000)
	DO I = 1, NN4
	DO J = 1, NN4
		V_F_GLOBE(I) = V_F_GLOBE(I) - A_F2_GLOBE(I)
     *		 + A_C_GLOBE(I,J) * A_SAT(J)
	ENDDO
	ENDDO

	! 合并C, L 阵
	do I=1,NN4
	do J=1,NN4
		A_C_GLOBE(I,J)=A_C_GLOBE(I,J)+A_L_GLOBE(I,J)
	END DO
	END DO

	!-----------  Boundary conditions ------------------------------------------

	 do ii=1,nn4
	 do jj=1,n_bkfix
		if (if_BC_out.eq.1) then
		if(iabkfix(jj,2).eq.256) then
			if(i_4_8_nodes(ii).eq.iabkfix(jj,0)) then
				do mm=1,nn4
					A_C_GLOBE(ii,mm)=0.0
				enddo
				V_F_GLOBE(ii)=sw0
				A_C_GLOBE(ii,ii)=1.0
			endif	
		endif
		endif
		
		if (if_BC1.eq.1) then
		if(iabkfix(jj,2).eq.1) then
			if(i_4_8_nodes(ii).eq.iabkfix(jj,0)) then
				do mm=1,nn4
					A_C_GLOBE(ii,mm)=0.0
				enddo
				V_F_GLOBE(ii)=sw1
				A_C_GLOBE(ii,ii)=1.0

			endif	
		endif
		endif

		if (if_BC2.eq.1) then
		if(iabkfix(jj,2).eq.2) then
			if(i_4_8_nodes(ii).eq.iabkfix(jj,0)) then
				do mm=1,nn4
					A_C_GLOBE(ii,mm)=0.0
				enddo
				V_F_GLOBE(ii)=sw2
				A_C_GLOBE(ii,ii)=1.0
			endif	
		endif
		endif
	 enddo
	 enddo
	!-----------  end of Boundary conditions ------------------------------------------

	! 求解
	CALL GASSSO(A_C_GLOBE,V_F_GLOBE,NN4)

	if (IMESH.eq.5) then
	if (Sw1.le.Sw2.and.Sw2.le.ww) then
		Sw_min=Sw1
		Sw_max=ww
	elseif(Sw1.le.ww.and.ww.le.Sw2) then
		Sw_min=Sw1
		Sw_max=Sw2
	elseif(Sw2.le.ww.and.ww.le.Sw1) then
		Sw_min=Sw2
		Sw_max=Sw1
	elseif(Sw2.le.Sw1.and.Sw1.le.ww) then
		Sw_min=Sw2
		Sw_max=ww
	elseif(ww.le.Sw1.and.Sw1.le.Sw2) then
		Sw_min=ww
		Sw_max=Sw2
	else
		Sw_min=ww
		Sw_max=Sw1
	endif
	endif

	if (IMESH.eq.1.or.IMESH.eq.3) then
	if (Sw1.le.ww) then
		Sw_min=Sw1
		Sw_max=ww
	else
		Sw_min=ww
		Sw_max=Sw1
	endif
	endif

	do I=1,nn4
		if(V_F_GLOBE(I).lt.Sw_min) V_F_GLOBE(I)=Sw_min
		if(V_F_GLOBE(I).gt.Sw_max) V_F_GLOBE(I)=Sw_max
	enddo


      !收敛准则
	fMAX_S = -1E10
	fMIN_S = 1E10
	DO I = 1, NN4
		fMAX_S=MAX(V_F_GLOBE(I),fMAX_S)
		fMIN_S=MAX(V_F_GLOBE(I),fMIN_S)
	ENDDO

	F_MAX_ERR = 0
	DO I = 1, NN4
	  F_SAT1 = A_SAT1(I)
        A_SAT1(I)=V_F_GLOBE(I)
	  F_MAX_ERR=MAX(F_MAX_ERR,DABS(A_SAT1(I)-F_SAT1))
	ENDDO

	DO I = 1, NN4
	IF(A_SAT1(I).LT.0.15) THEN
		A_SAT1(I)=0.15
	ELSEIF(A_SAT1(I).GT.0.85) THEN
		A_SAT1(I)=0.85
	ENDIF
	ENDDO

	IF(F_MAX_ERR.LT.1E-6) GOTO 8

1	CONTINUE

8	DO I = 1, NN4
		IF(A_SAT1(I).LT.0.15) THEN
			A_SAT(I)=0.15
		ELSEIF(A_SAT1(I).GT.0.85) THEN
			A_SAT(I)=0.85
		ELSE
			A_SAT(I) = A_SAT1(I)
		ENDIF
	ENDDO



	RETURN
	END             


       !毛管力，不含重力
      SUBROUTINE SAT_NEW05(NODEP,poro,dump,x,y,kay,volste,volst0,
	1       term,ns,ptime,i_8_4_nodes,i_4_8_nodes,
     1		A_SAT,NODEP4,dump_old,number)
      Implicit real*8 (a-h, o-z)
C      REAL*8 RKF(5000,20)
      integer p,q,r,cdmax,dof,steps,tot,band,h,t,gp,g(20),nk(3000),
     &        dt(200),sandg(40,80,4),nelem(40,80,4)
      real*8 det,kxp,E,v,dtim,quot,theta,hards,ou,dee(4,4)
     & ,samp(3,2),coord(8,2),jac(2,2),jac1(2,2)
     & ,derivt(4,2),poro(40,80,4),kxy(40,80,4,2),der(2,8),deriv(2,8)
     & ,kay1(2,2),qcom1(2,2),kderiv(2,4),bee(4,16),dbee(4,16),bt(16,4)
     & ,btdb(16,16),dtkd(4,4),km(16,16),kp(4,4),ke(20,20),kd(20,20)
     & ,fun(8),funf(4),coordf(4,2),derf(2,4),derivf(2,4),tforce(5000)
     & ,c(16,4),volf(16,4),eldf,eldfp,mc(3,2),fmderiv(3,4),eld(20)
     & ,vol(16),eps(4),sigma(4),tbdyld(5000),dpl(4,4),eload(16)
     & ,bload(16),tload(5000),dloads(5000),bdylds(5000),oldlds(5000)
     & ,sx(40,80,4),EN(40,80,4),sy(40,80,4),sz(40,80,4),sxy(40,80,4)
     & ,ex(40,80,4),ey(40,80,4),ez(40,80,4),exy(40,80,4),diff(900)
     & ,er(40,80,4),sr(40,80,4),pl(4,4),elso(4),delso(4),et(40,80,4)
     & ,st(40,80,4),SOI,SWI,SWWC,SKRO,SKRW,volstr(40,80,4)
     & ,OUTOIL,OUTWAT,bhp,sh3,mud,np,coh,COHI,COEFR,BETA,COEFP,PS0,KR2
     & ,qderiv(2,8),dtkq(4,4),PNOW,RSO0,kay(40,80,4,2)
	1 ,seld(4),RSOO(40,80),krw(40,80,4,2),kro(40,80,4,2)
	1 ,volste(40,80,4),volst0(40,80,4),dump(5000),x(5000),y(5000)

	integer lq(50),Rpold(50),iq(50),nodep(40,80,8),nodeg(40,80,20)
c******* nsandi for rescording the time of sanding for each cohesion
c******* npelasti for rescording the time of yielding for each cohesion
	integer mcc(1000,4),nsandi(101),npelasti(101)
      integer problemtype,modeltype,damagetype,geometrytype,ctype
     & ,completiontype,reservoirtype,failuretype,stresstype,filtercake
c
      data ijac,ijac1,ikay,ider,ideriv,ikderv,T,iderf,idervf/9*2/
      data isamp,nodof/2*3/
c********* idee,ibee,idbee should be 3 in plan stress and plan strain *************
      data idee,ibee,idbee/3*3/
      data icordf,idtkd,ikp,nodf/4*4/
      data icoord,idervt,nod/3*8/
      data ike,ikd,tot/3*20/
      data ibt,ikm,ic,ivolf,dof,ibtdb/6*16/
      data ipl,idpl/2*4/
	COMMON/TMP1/nf(8000,3), nft(8000,1)
      COMMON/INITIAL/shmaxg,shming,szg,pbg,rw,rw2,pw,z,rb
      COMMON/GEOMECH/E,v,cou,col,phi,csco,psi,Biot,Biotp,cs,Ts,icode
      COMMON/RESERVIOR/PO(20),RSO(20),RKE_X,RKE_Y,rkp,VIS_O,VIS_W,qf,
     &                bm,COMP_O,COMP_W,porosity,porey,BUBP,NUMO
      COMMON/SUBNONL/Sc,Sv,CM,Qc,AP,AB,AL,am,ai
      COMMON/SUBFIELD/TIMEF,vs,pores,wks,CT,WD,Frac,S0,
     &                C_DISPLACEMENT,C_DSP2
      COMMON/SUBINITIAL/sigmah,pa,af,az,amhs,mud,pd,pay,pradius,packerl
      COMMON/SUBRESER/w0,ww,ak,BB,vu
      COMMON/OPTNUMERI1/NT,NX,NLOAD
      COMMON/OPTNUMERI2/dr,T1,Dtim
      COMMON/OPTFEM1/NXE,NYE,NYE0,nyl,NN,NN4,N,NW,GP,NL,Its,nntemp,ntemp
      COMMON/OPTFEM2/THETA,TOL
      COMMON/DATAIT/nxN, It
      COMMON/DATA/PHI0,PHIMAX,SK0,SKC,YS,YF,UF,C0,SNMD,SNMD0,
     &            Err,YA0,WORM_PHI(1000),
     &      WORM_SK(1000),WORM_C(1000),
     &      WORM_YA(1000),WORM_X(1000),WORM_T_PHI(1000),
     &      WORM_T_SK(1000),WORM_T_C(1000),WORM_Q(1000),
     &      WORM_TMPC(1000)
      COMMON/OPTIONS/problemtype,modeltype,damagetype,geometrytype,
     *                completiontype,reservoirtype,failuretype,ctype,
     *                stresstype,filtercake,imethod,iflowtype,iproblem
      COMMON/PHASE2/SKROT(20),SKRWT(20),SWWC,SWT(20),SO(100,100),
     &              SW(100,100),SG(100,100),NUMOW
      COMMON/VAR_ADD1/COEFR,BETA,COEFP,PS0,SM,S0MAX,P0,SHV
c*******added data******************************************************
	COMMON/LOAD1/radius(51),rad2(51),pofix(1000),point(1000,2),gtheta,
	1gravy,pressa(1000,6),spoke(51),dofix(1000,3)
	COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)
	1,lodpt(1000),nbound,ndrill,ndirect
	COMMON/flow/flowqi,flowqo
	COMMON/flowc/nflowi,nflowo
	COMMON/loads1/pdate(1000),pdelta(1000),PDLTA2(1000)
	COMMON/loads2/npoint,npr(1000),npr2(1000),nstep(1000)

      COMMON/SOLUELE/S_X(1000),S_Y(1000),N_S_ELE(1000,4),
     *	N_S_NOD(100,100,4),
	1               S_PORO(1000),S_KAY(1000),S_DUMP(1000),S_SOLVE(1000),
	1               S_SOLVE0(1000)

	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82, 82),
     *i_v4nodes(82,42),N_N
	 dimension ia_sat_nodes(100,100),iaPQ(1000,0:1)

	DIMENSION A_SAT(1000), A_SAT1(1000),FUNFT(4)
	DIMENSION NODEP4(40,80,4)

	REAL*8 A_KW(2,2),A_C(4,4),A_F2(4,4),V_F(4),
     * A_C_GLOBE(1000, 1000), V_F_GLOBE(1000), A_F2_GLOBE(1000,1000),
     * FAI_ROW_FUNF(4), P_OIL(1000)
	REAL*8 A_L(4,4),A_L_GLOBE(1000, 1000),A_L2(4,4)
	integer i_8_4_nodes(1000),i_4_8_nodes(1000)

	real*8 dump_old(5000)
	REAL*8 A_F2_1(2),A_F2_2(2),A_F2_3,P_NODE(4),KAY_NODE(4)
	real*8 sw0,sw1,sw2,sw_min,sw_max
      
	sw0=0.4
      Sw1=0.75
      Sw2=0.4
      sw_min=0.4
      sw_max=0.75

	!A_SAT1 用于迭代
	DO I = 1, NN4
	A_SAT1(I) = A_SAT(I)
	END DO

	DO 1 K_ITER = 0, 50
      
	number=number+1
	CALL NULL(A_C_GLOBE,1000,1000)
	CALL NULL(A_L_GLOBE,1000,1000)
	CALL NULL(A_F2_GLOBE,1000,1000)
	CALL NULL(V_F_GLOBE,1000,1)
	CALL NULL(P_OIL,1000,1)
	
	do i2=1,nxe+1
	do i1=1,NYL
		nn=i_vnodes(i1,i2)
		IF(nn.GT.0) THEN
			I_NODE=i_8_4_nodes(nn)
			P_OIL(I_NODE) = DUMP(NN)
		ENDIF
	ENDDO
	ENDDO

	gp=2
      do 3 p=1,nxe
      do 3 q=1,nye

		!4节点
		do in=1,icordf
			inn=2*in-1
			coordf(in,1)=x(nodep(p,q,inn))
			coordf(in,2)=y(nodep(p,q,inn))
		end do
		!*************************************************************************

		call null(A_C, 4, 4) !  A_C---- 单刚
		call null(A_L, 4, 4) !  A_L---- 单刚
		call null(A_L2, 4, 4) !  A_L2---- 单刚
		call null(A_F2, 4, 4)!  A_F2---- 单刚用于乘Po算f2
		call null(V_F, 4, 1) !  V_F---- 右端项
		ig=0
		do 4 i = 1, gp
		do 4 j = 1, gp
			ig=ig+1
			CALL GAUSS(samp,gp)
			call formln(derf,iderf,funf,samp,gp,i,j)
			call matmul(derf,iderf,coordf,icordf,jac,ijac)
			call twoby2(jac,ijac,jac1,ijac1,det)
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf)

	       ! 上游权
             SSK = 0.D0
	        DUMP_G = 0
	        DUMP_G_OLD = 0
			DO IP = 1, 4
				NNP=NODEP(P,Q,IP*2-1)
				NNP4=NODEP4(P,Q,IP)
                  if(DUMP(NNP).GT.DUMP_G) then
	               SSK=A_SAT(NNP4)
	            endif
				DUMP_G = MAX(DUMP_G, DUMP(NNP))
				DUMP_G_OLD = MAX(DUMP_G_OLD, DUMP_OLD(NNP))
			END DO

			! 以下来源于钱博士的subroutine SATURATION
              PC=8.763D-3*SSK**(-1.D0/1.754D0)
	        pcpie=8.763D-3*(-1.D0/1.754D0)*SSK**(-1.D0/1.754D0-1.D0)

        ! 生成[L] 和 做右端项Po的系数阵一部分代码是共用的
        ! 做右端项Po的系数阵
              A_KW(1,1) = kay(p,q,ig,1)*SSK**3.D0*YF
			A_KW(2,2) = kay(p,q,ig,2)*SSK**3.D0*YF
!			A_KW(1,1) = krw(p,q,ig,1)*SSK**3.D0*1000.0
!			A_KW(2,2) = krw(p,q,ig,2)*SSK**3.D0*1000.0
			A_KW(1,2) = 0
			A_KW(2,1) = 0

			call matmul(A_KW,ikay,derivf,idervf,kderiv,nodf)
			call matran(derivt,nodf,derivf,idervf)
			call matmul(derivt,nodf,kderiv,ikderv,dtkd,idtkd)
			call quotca ( samp,gp,det,term,quot,i,j )
			PROD = QUOT / VIS_W / 1000 * DTIM
!	        PROD = QUOT * DTIM
			CALL MSMULT(DTKD, PROD,IDTKD, NODF)
			call matadd(A_F2,ikp,DTKD,idtkd)
			CALL MSMULT(DTKD,pcpie,IDTKD, NODF)
			call matadd(A_L,ikp,DTKD,idtkd)


			!做左端Sw的刚度阵
			DO I1 = 1, 4
				FUNFT(I1)=FUNF(I1)
			END DO
			!不用YF了。YF好像是混合密度。我们用的是纯水的密度。故以下用到密度的地方都采用1000.0
			FAI_ROW = PORO(P,Q,ig)*1000.0*QUOT
			CALL MATMUL(FAI_ROW,1,FUNF,1,FAI_ROW_FUNF,4)
			CALL MATMUL(FUNFT,4,FAI_ROW_FUNF,1,DTKD,4)
			call matadd(A_C,ikp,DTKD,idtkd)

			!做[L]的第二项，压缩部分
			FAI_ROW_DVT=(DUMP_G-DUMP_G_OLD)/DTIM*COMP_W*PORO(P,Q,ig)
     1			*1000.0*QUOT*DTIM ! derivt 是对时间的导数
			CALL MATMUL(FAI_ROW_DVT,1,FUNF,1,FAI_ROW_DVT_FUNF,4)
			CALL MATMUL(FUNFT,4,FAI_ROW_DVT_FUNF,1,DTKD,4)
			call matadd(A_L2,ikp,DTKD,idtkd)
			
	goto 4
4		continue

		! 组装总刚
		do I=1,NODF
		do J=1,NODF
			I2=NODEP4(P,Q,I)
			J2=NODEP4(P,Q,J)
			A_F2_GLOBE(I2,J2)=A_F2_GLOBE(I2,J2)+A_F2(i,j)
			A_C_GLOBE(I2,J2)=A_C_GLOBE(I2,J2)+A_C(i,j)
			A_L_GLOBE(I2,J2)=A_L_GLOBE(I2,J2)-A_L(i,j)
			A_L_GLOBE(I2,J2)=A_L_GLOBE(I2,J2)+A_L2(i,j) ! consider d(ROU_w)/dt only
	    END DO
	    END DO

3		CONTINUE
	! 生成总的右端项[A_F2_GLOBE]*{Po}
	CALL NULL(V_F_GLOBE,1000,1)
	DO I = 1, NN4
	DO J = 1, NN4
		V_F_GLOBE(I) = V_F_GLOBE(I) - A_F2_GLOBE(I,J) * P_OIL(J)
     *		 + A_C_GLOBE(I,J) * A_SAT(J)
	ENDDO
	ENDDO

	! 合并C, L 阵
	do I=1,NN4
	do J=1,NN4
		A_C_GLOBE(I,J)=A_C_GLOBE(I,J)+A_L_GLOBE(I,J)
	END DO
	END DO


       !饱和度边界
	if(imesh.eq.1.or.imesh.eq.3) then
        DO I=1,NXE+1
	    DO J=1,NN4
	      if(I.eq.J) then
	        A_C_GLOBE(I,J)=1.0d0
            else
	        A_C_GLOBE(I,J)=0.0
	      endif
	    ENDDO
	  ENDDO
        DO I=1,NXE+1
	    V_F_GLOBE(I)=Sw0
	  ENDDO

	  DO I=NN4-NXE,NN4
	    DO J=1,NN4
	      if(I.eq.J) then
	        A_C_GLOBE(I,J)=1.0d0
           else
	        A_C_GLOBE(I,J)=0.0
	      endif
	    ENDDO
	  ENDDO
	  DO I=NN4-NXE,NN4
	    V_F_GLOBE(I)=Sw1
	  ENDDO

	elseif(imesh.eq.5) then
	        DO I=1,NXE+1
	    DO J=1,NN4
	      if(I.eq.J) then
	        A_C_GLOBE(I,J)=1.0d0
            else
	        A_C_GLOBE(I,J)=0.0
	      endif
	    ENDDO
	  ENDDO
        DO I=1,NXE+1
	    V_F_GLOBE(I)=0.4
	  ENDDO

	  DO I=NN4-NXE,NN4
	    DO J=1,NN4
	      if(I.eq.J) then
	        A_C_GLOBE(I,J)=1.0d0
           else
	        A_C_GLOBE(I,J)=0.0
	      endif
	    ENDDO
	  ENDDO
	  DO I=NN4-NXE,NN4
	    V_F_GLOBE(I)=0.85
	  ENDDO
	endif

	! 求解
	CALL GASSSO(A_C_GLOBE,V_F_GLOBE,NN4)

	do I=1,nn4
		if(V_F_GLOBE(I).lt.Sw_min) V_F_GLOBE(I)=Sw_min
		if(V_F_GLOBE(I).gt.Sw_max) V_F_GLOBE(I)=Sw_max
	enddo


	fMAX_S = -1E10
	fMIN_S = 1E10
	DO I = 1, NN4
		fMAX_S=MAX(V_F_GLOBE(I),fMAX_S)
		fMIN_S=MAX(V_F_GLOBE(I),fMIN_S)
	ENDDO

	F_MAX_ERR = 0
	DO I = 1, NN4
	F_SAT1 = A_SAT1(I)
      A_SAT1(I)=V_F_GLOBE(I)
	F_MAX_ERR=MAX(F_MAX_ERR,DABS(A_SAT1(I)-F_SAT1))
	ENDDO

	IF(F_MAX_ERR.LT.1E-6) GOTO 8
1	CONTINUE

8	DO I = 1, NN4
		IF(A_SAT1(I).LT.0.15) THEN
			A_SAT(I)=0.15
		ELSEIF(A_SAT1(I).GT.0.85) THEN
			A_SAT(I)=0.85
		ELSE
			A_SAT(I) = A_SAT1(I)
		ENDIF
	ENDDO

	RETURN
	END


       !井眼及外边界流量计算，重力按角度分配，Li
	 subroutine QUANTITY1(IMESH,nxe,nye,nodep,nodep4,
     $ dump,x,y,kro,krw,qwater,qoil,term)
	 implicit none
	 integer IMESH,nxe,nye,n_bkfix,iabkfix(500,0:3)
	 INTEGER nodep(40,80,8),nodep4(40,80,4)                                                                                   
       integer n3_deload                        
	 real*8 dump(5000),x(5000),y(5000)
	 real*8 kro(40,80,4,2),krw(40,80,4,2)

	 INTEGER GP,IG,IN,INN,NODEID4(4),NODEID8(4)
	 INTEGER II,JJ,NNP
	 REAL*8 coordf(4,2),coord(8,2),DUMP_G(4),A_KW(2,2)
	 INTEGER ICORDF,icord,nodf
	 DATA ICORDF,icord,nodf/4,8,4/
	 INTEGER ijac,ijac1,ikay,iderf,idervf
	 data ijac,ijac1,ikay,iderf,idervf/5*2/
	 real*8 det,quot,samp(2,2),jac(2,2),jac1(2,2)
	 REAL*8 funf(4),derf(2,4),derivf(2,4)
	 REAL*8 P_GRAD(2),Tderiv(2),KWP_GRAD(2,4),KOP_GRAD(2,4)
	 real*8 qwater(3),qoil(3)
	 INTEGER I,J,KK
	 integer nf, nfT
	 COMMON /TMP1/ nf(5000,3), nfT(5000,1)
	!! FOR BOUNDARY INTEGRATION
	 REAL*8 SAMPB(2),N(4),WATER_Grad(2,2),OIL_Grad(2,2),elcod(2,3)
	 REAL*8 der(2,8),FUN(8),DIRECTION(2),NORMAL(2)
	 INTEGER iodeg,I_WELL,IYE,igrav,IYE1
	REAL*8 GTHETA,GRAVY,GCOMWATER(2),GCOMOIL(2)
	real*8 PW_GRAD(2),Po_GRAD(2),ang,pi
	common /iconsider/igrav
	integer mm,nn,if_ang
	real*8 element_w,element_o,tempw,tempo,term

	pi=3.1415926
	if_ang=0 !0为正常，1为边界中点角度，2为两节点角度

      !zhang shunfu
      if(if_ang.eq.0) then
      Gtheta=0.0d0
	IF(igrav.EQ.1) THEN
		GRAVY= 10000.0D0
		GCOMWATER(1) = 1.0D-6*GRAVY*DSIN(GTHETA)
		GCOMWATER(2) =1.0D-6*GRAVY*DCOS(GTHETA)
		GRAVY= 9000.0D0
		GCOMOIL(1)   = 1.0D-6*GRAVY*DSIN(GTHETA)
		GCOMOIL(2)   =1.0D-6*GRAVY*DCOS(GTHETA)
	ELSE
		GCOMWATER(1) = 0.0D0
		GCOMWATER(2) = 0.0D0
		GCOMOIL(1)   = 0.0D0
		GCOMOIL(2)   = 0.0D0
	ENDIF
	endif
	
	qwater(1)=0.0d0
	qwater(2)=0.0d0
	qwater(3)=0.0d0
	qoil(1)=0.0d0
	qoil(2)=0.0d0
	qoil(3)=0.0d0
!!	FOR THE FIRST WELL
	GP=2
	I_WELL=0
100	CONTINUE
	I_WELL=I_WELL+1  !I_WELL=1 hole1,I_WELL=2 outer boundary,I_WELL=3 hole2
	IF(I_WELL.EQ.1) THEN
		IYE=1
	    IYE1=1
	ELSEIF(I_WELL.EQ.2) THEN
		if(imesh.eq.5) then
		  IYE=NYE/2
		  IYE1=nye/2+1
		elseif(imesh.eq.1.or.imesh.eq.3) then
		  IYE=NYE
		  IYE1=NYE
		endif
	else
	   IYE=NYE
	   IYE1=NYE  
	ENDIF
	DO II=1,nxe
	DO JJ=IYE,IYE1 !JJ is eigher 1 or 8
		do in=1,icordf !!get the coordinate information                                                                      
			inn=2*in-1
			nodeid4(in)= nodep4(II,JJ,in)
			nodeid8(in)=nodep(II,JJ,inn)                                                                            
			coordf(in,1)=x(nodep(II,JJ,inn))                                                         
			coordf(in,2)=y(nodep(II,JJ,inn))                                                        
		end do
		do in=1,icord
			coord(in,1)=x(nodep(II,JJ,in))
			coord(in,2)=y(nodep(II,JJ,in)) 
		enddo
		DO IN=1,4
			NNP=NODEP(II,JJ,IN*2-1)
			DUMP_G(IN)= DUMP(NF(NNP,3)) 

		ENDDO 
           
	!边界中心角度
	    if(if_ang.eq.1) then
            if(imesh.EQ.5) then
		    ang=((0.5D0+ii-1)*180.D0/NXE-90.D0)*pi/180.D0
            elseif (imesh.eq.2) then
	        ang = 0.0d0
	      elseif(imesh.EQ.3) then
		    ang=((0.5D0+ii-1)*180.D0/NXE-90.D0)*pi/180.D0
	      elseif(imesh.EQ.1) then
		    ang=((0.5D0+ii-1)*90.D0/NXE)*pi/180.D0
	      else
	      endif
	      IF(igrav.EQ.1) THEN
		    GRAVY= 10000.0D0
		    GCOMWATER(1) = 0
	        GCOMWATER(2) = 1.0D-6*GRAVY*dabs(DSIN(ang))
		    GRAVY= 9000.0D0
		    GCOMOIL(1)   = 0
		    GCOMOIL(2)   = 1.0D-6*GRAVY*dabs(DSIN(ang))	
	      ELSE
		    GCOMWATER(1) = 0.0D0
		    GCOMWATER(2) = 0.0D0
		    GCOMOIL(1)   = 0.0D0
		    GCOMOIL(2)   = 0.0D0
	      ENDIF
	    endif

		CALL GAUSS(samp,gp)
		IG=0
		do 4 i = 1, gp                                                   
		do 4 j = 1, gp
			ig=ig+1

	!单元两边角度
	     if(if_ang.eq.2) then
		   if(imesh.EQ.5) then
		     ang=((j+ii-2)*180.D0/NXE-90.D0)*pi/180.D0
             elseif (imesh.eq.2) then
	         ang = 0.0d0
	       elseif(imesh.EQ.3) then
		     ang=((j+ii-2)*180.D0/NXE-90.D0)*pi/180.D0
	       elseif(imesh.EQ.1) then
		     ang=((j+ii-2)*90.D0/NXE)*pi/180.D0
	       else
	       endif
	       IF(igrav.EQ.1) THEN
		     GRAVY= 10000.0D0
		     GCOMWATER(1) = 0
	         GCOMWATER(2) = 1.0D-6*GRAVY*dabs(DSIN(ang))
		     GRAVY= 9000.0D0
		     GCOMOIL(1)   = 0
		     GCOMOIL(2)   = 1.0D-6*GRAVY*dabs(DSIN(ang))	
	       ELSE
		     GCOMWATER(1) = 0.0D0
		     GCOMWATER(2) = 0.0D0
		     GCOMOIL(1)   = 0.0D0
		     GCOMOIL(2)   = 0.0D0
	       ENDIF
		 endif  
		                     
	!! FOR WATER PRODUCTION
		    A_KW(1,1) = krw(II,JJ,ig,1)
			A_KW(2,2) = krw(II,JJ,ig,2)
			A_KW(1,2) = 0.0d0                                                                          
			A_KW(2,1) = 0.0d0
			call nulvec(P_GRAD,2)                                                                                                                                                   
			call formln(derf,iderf,funf,samp,gp,i,j)                                               
			call matmul(derf,iderf,coordf,icordf,jac,ijac)                                                                                                     
			call twoby2(jac,ijac,jac1,ijac1,det)                                                                                                 
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf) 
			call matmul(derivf,idervf,DUMP_G,nodf,P_GRAD,1)

			do KK=1,2
				PW_GRAD(KK)=  P_GRAD(KK) + GCOMWATER(KK)
			enddo 
!	        write(331,*) ii,ig,PW_GRAD(2),P_GRAD(2),GCOMWATER(2)
			call matmul(A_KW,ikay,PW_GRAD,idervf,Tderiv,1)
			do KK=1,2
				KWP_GRAD(KK,ig)= -Tderiv(KK)
			enddo

	!! FOR OIL PRODUCTION
		    A_KW(1,1) = krO(II,JJ,ig,1)
			A_KW(2,2) = krO(II,JJ,ig,2)                                                                            
			A_KW(1,2) = 0.0d0                                                                          
			A_KW(2,1) = 0.0d0
			do KK=1,2
				Po_GRAD(KK)=  P_GRAD(KK) +GCOMOIL(KK)
			enddo
			call matmul(A_KW,ikay,Po_GRAD,idervf,Tderiv,1)
			do KK=1,2
				KOP_GRAD(KK,ig)= -Tderiv(KK) 
			enddo	
		 
4		CONTINUE
	!! BOUNDARY INTEGRATION FOR WATER
		SAMPB(1)=dsqrt(3.0d0)/3.0
		SAMPB(2)=-SAMPB(1)
	    if(i_WELL.eq.2) then !外边界
	      N(1)=(-1.0d0+SAMPB(1))/(2.0d0*SAMPB(1)) 
		  N(2)=(-1.0d0+SAMPB(2))/(2.0d0*SAMPB(2))
	    else   !井眼
		  N(1)=(1.0d0+SAMPB(1))/(2.0d0*SAMPB(1)) !! shape functiom for point 1/sart(3.0)
		  N(2)=(1.0d0+SAMPB(2))/(2.0d0*SAMPB(2)) !! shape functiom for point -1/sart(3.0)
	    endif
		WATER_grad=0.0d0
		OIL_grad=0.0D0	
		do i=1,2 !! for gauss point ii in one dimension
		do j=1,2 !!Direction x, y
		WATER_grad(j,i)=KWP_GRAD(j,i)*n(1)+KWP_GRAD(j,i+2)*n(2)
		OIL_grad(j,i)=KOP_GRAD(j,i)*n(1)+KOP_GRAD(j,i+2)*n(2)
		enddo	
		enddo
		do in=1,3                                                                        
	      elcod(1,IN)=coord(2+IN,1)                                                         
		  elcod(2,IN)=coord(2+IN,2)
		enddo

	    element_w=0.0d0
	    element_o=0.0d0

		do 9105 ig=1,gp                                                                        
			call gauss(samp,gp)
c************** xi=weigh in gauss in Owen                                                 
			call fmquad(der,2,fun,samp,gp,ig,1,2) !! xi can equal to -1.0 only (so gauss quadrature was limited to one dimension)
			do IN=1,2
				direction(IN)=0.0d0	
 				do iodeg=1,3
 			direction(IN)=direction(IN)+elcod(IN,iodeg)*der(2,iodeg) !! [d x/ d eta; d y/ d eta]
 				enddo
 			enddo
	!! NORMAL DIRECTION
	        if(i_WELL.eq.2) then  !外边界
	          normal(1)=direction(2) 
			  normal(2)=-direction(1)
	        else  !井眼
			  normal(1)=-direction(2) 
			  normal(2)=direction(1)
	        endif

	       tempw=WATER_grad(1,ig)*normal(1)+WATER_grad(2,ig)*normal(2)
	       tempo=OIL_grad(1,ig)*normal(1)+OIL_grad(2,ig)*normal(2)
	       funf(1)=(1.0d0-sampb(ig))/2.0d0
	       funf(2)=(1.0d0+sampb(ig))/2.0d0
	       do i=1,2
	         element_w=element_w+funf(i)*tempw*term
	         element_o=element_o+funf(i)*tempo*term
	       enddo
	       qwater(i_WELL)=qwater(i_WELL)+element_w
	       qOIL(i_WELL)=qOIL(i_WELL)+element_o

!			qwater(i_WELL)=qwater(i_WELL)+
!     $			WATER_grad(1,ig)*normal(1)+WATER_grad(2,ig)*normal(2)
!			qOIL(i_WELL)=qOIL(i_WELL)+
!     $			OIL_grad(1,ig)*normal(1)+OIL_grad(2,ig)*normal(2)		                 
9105		continue
	ENDDO
	ENDDO
!	IF(IMESH.GE.4.AND.I_WELL.EQ.1) GOTO 100 
!	IF(I_WELL.EQ.1) GOTO 100
      IF(I_WELL.EQ.1) GOTO 100
	if(imesh.eq.5.and.I_WELL.eq.2) GOTO 100  
	RETURN
	end subroutine


!!=========================Li================================
!!==================孔隙介质压力计算=========================
!!===========================================================
	subroutine  pressure_m4(nxe,nye,nn,nntemp,ntemp,bhp,nyl,
     *nodep, dtim,dumpm0,dump,dumpm1,term,x,y ,POROm,nodep4
     * ,vis_o,VIS_W,SW, NUMOW ,kxym,ns ,IMESH,n_bkfix,iabkfix,
     * COMP_O,alpha_t,pm0,SWT,SKROT,SKRWT,i_4_8_nodes,i_vnodes)

	IMPLICIT NONE                                    
	INTEGER IW,nxe,nye,nodeg(40,80,4),nntemp,ntemp,ngrav,igrav,nn,nyl
	integer nodep(40,80,8),ia_t_fix(500,0:3),n_t_fix ,imesh,ns,nn4
	integer nodep4(40,80,4),i11
	integer i_8_4_nodes(1000),i_4_8_nodes(1000)
	integer n_bkfix,iabkfix(500,0:3) 
	REAL*8  PERMX,PERMY,dtim,time_sagd_prod,bhp
	REAL*8 x(5000),y(5000),dumpm(5000), dumpm0(5000) ,dump(5000)
	REAL*8 porom(40,80,4),dumpm1(5000),dumpf0(5000),pm0                                                                      
      real*8 det,kxp,E,v,quot,theta,samp(3,2),coord(8,2),jac(2,2),
     &    jac1(2,2),derivt(4,2),derf(2,4),derivf(2,4),funf(4),funft(4) 
     &    ,LOADS(5000),LOADf(5000),coordf(4,2),kderiv(2,4)
      REAL*8 A_KW(2,2),KP1(4,4),S1(4,4),M(4,4),VIS_O,VIS_W,dtkd(4,4),
     * KP1_GLOBE(1000, 1000), S1_GLOBE(1000,1000),M_GLOBE(1000,1000),
     * FAI_ROW, FAI_ROW_1,NEWLONS(1000),KNS(1000,1000),
     * dump_g, F_MAX_ERR
      real*8 SKROT(1000),SKRWT(1000), SKRO,SKRW,SLWO,SWT(1000)
	real*8 kxym(40,80,4,2),alfa_t,SW(40,80,4),kaym(40,80,4,2)
     1       ,krom(40,80,4,2),krwm(40,80,4,2)
	real*8 i_v4nodes(82, 82),i_vnodes(82, 82)
      integer i1elemx(80),i1elemy(80),i1elm_deload(80,3)                           
	real*8 ang1_elm_deload(80,3)                                                           
      integer i2elemx(80),i2elemy(80),i2elm_deload(80,3)                           
	real*8 ang2_elm_deload(80,3)
	integer i3elemx(80),i3elemy(80),i3elm_deload(80,3),j12                           
	real*8 ang3_elm_deload(80,3)
	integer n1_deload,n2_deload,n3_deload,K_ITER, K_ITER_MAX,I_NODE
	integer ip,iq,in,inn,ig,gp ,i,j,numow,i1,nnp,p,q,ii,jj,mm,i2,j2
	real*8 TERM,prod,COMP_O,V_L,P_L,F_SAT1,alpha_t,pho
	real*8 FAI_ROW_FUNf(4),FAI_ROW_FUNf_1(4)
	integer ijac,ijac1,ikay,ider,ideriv,ikderv,iderf,iderv,
     &  isamp,nodof,idee,ibee,idbee,icordf,idtkd,ikp,nodf,
     &  icoord,idervt,nod,ike,ikd,tot, ibt,ikm,ic,ivolf,dof,ibtdb,
     &  ipl,idpl,idervf

      data ijac,ijac1,ikay,ider,ideriv,ikderv,iderf,idervf/8*2/
      data isamp,nodof/2*3/
      data idee,ibee,idbee/3*3/
      data icordf,idtkd,ikp,nodf/4*4/
      data icoord,idervt,nod/3*8/
      data ike,ikd,tot/3*20/
      data ibt,ikm,ic,ivolf,dof,ibtdb/6*16/
      data ipl,idpl/2*4/
	Real*8 dtim_min,DDTIM,time           
	Integer timenum

      K_ITER_MAX=1
	gp=2
	pho=1000.0
	theta=1.0

c	dtim_min=0.01D0
c	dtim_min=dtim
c	if (dtim.LT.dtim_min) then
c		ddtim=dtim
c		timenum=1
c	else
c		ddtim=dtim_min
c		timenum=dtim/dtim_min
c	end if
      CALL NULVEC(dumpm1,ntemp)
	do i=1,ntemp
	   dumpm1(i) = DUMPm0(i)
	ENDDO
      CALL NULVEC(dumpf0,ntemp)
	do ip=1,nxe
      do iq=1,nye
      do in=1,4
        inn=2*in-1
	  dumpf0(nodep4(ip,iq,in))=dump(nodep(ip,iq,inn))
	end do
      enddo
      enddo

      DO 1 K_ITER = 1, K_ITER_MAX

        CALL NULL(KP1_GLOBE,1000,1000)
        CALL NULL(S1_GLOBE,1000,1000)
        CALL NULL(M_GLOBE,1000,1000)
        CALL NULL(KNS,1000,1000)
        CALL NULvec(NEWLONS,1000)

        CALL NULVEC(LOADS,ntemp)
        CALL NULVEC(LOADf,ntemp)
        do i=1,ntemp
	     loads(i)=dumpm0(i)
           loadf(i)=dumpf0(i)
        end do

        DO 10 IP=1,NXE  
        DO 10 IQ=1,NYE 
	    do in=1,4
	      inn=2*in-1
	      coordf(in,1)=x(nodep(ip,iq,inn))
	      coordf(in,2)=y(nodep(ip,iq,inn))
	    end do
	    DO IN=1,8
	      COORD(IN,1)=X(nodep(ip,iq,in))
	      COORD(IN,2)=Y(nodep(ip,iq,in))
	    ENDDO

	    call null(KP1, 4, 4) 
	    call null(S1, 4, 4)
	    call null(M, 4, 4)
		ig=0
		do 4 i = 1, gp
		do 4 j = 1, gp
			ig=ig+1
	        CALL GAUSS(SAMP,GP)
			call formln(derf,iderf,funf,samp,gp,i,j)
			call matmul(derf,iderf,coordf,icordf,jac,ijac)
			call twoby2(jac,ijac,jac1,ijac1,det)
			call matmul(jac1,ijac1,derf,iderf,derivf,nodf)

            CALL INTERP(SWT,SKROT,NUMOW,SW(iP,iQ,ig),SKRO)
		  CALL INTERP(SWT,SKRWT,NUMOW,SW(iP,iQ,ig),SKRW)
	      SLWO=SKRO/VIS_o+SKRW/VIS_W
		  kaym(ip,iq,ig,1)= KXYm(ip,iq,IG,1)*SLWO*0.08527D0 
		  krwm(ip,iq,ig,1) = SKRw/VIS_w*KXYm(ip,iq,ig,1)*0.08527D0 
		  krom(ip,iq,ig,1) = SKRo/VIS_o*KXYm(ip,iq,ig,1)*0.08527D0
		  kaym(ip,iq,ig,2)= KXYm(ip,iq,IG,2)*SLWO*0.08527D0 
		  krwm(ip,iq,ig,2) = SKRw/VIS_w*KXYm(ip,iq,ig,2)*0.08527D0 
		  krom(ip,iq,ig,2) = SKRo/VIS_o*KXYm(ip,iq,ig,2)*0.08527D0 

	      A_KW(1,2) = 0.0d0
	      A_KW(2,1) = 0.0d0
            A_KW(1,1) = KXYm(iP,iQ,IG,1)*0.08527D0/VIS_O
            A_KW(2,2) = KXYm(iP,iQ,IG,2)*0.08527D0/VIS_O
c            A_KW(1,1) = Krom(iP,iQ,IG,1)
c            A_KW(2,2) = Krom(iP,iQ,IG,2)

		  call matmul(A_KW,ikay,derivf,idervf,kderiv,nodf)
		  call matran(derivt,nodf,derivf,idervf)
		  call matmul(derivt,nodf,kderiv,ikderv,dtkd,idtkd)
		  call quotca ( samp,gp,det,term,quot,i,j )
		  PROD = QUOT * DTIM *pho
		  CALL MSMULT(DTKD, PROD,IDTKD, NODF)
		  call matadd(KP1,ikp,DTKD,idtkd)

	      DUMP_G = 0.0d0
	      DO I1 = 1, 4
		    NNP=NODEP4(i,j,I1)
		    DUMP_G = MAX(DUMP_G, DUMPm1(NNP))
	   	  END DO
            DO I1 = 1, 4
			FUNfT(I1)=FUNf(I1)
		  END DO
         FAI_ROW=POROm(i,j,ig)*COMP_O*pho*QUOT
c	      FAI_ROW=POROm(i,j,ig)*COMP_O*QUOT*pho
            CALL MATMUL(FAI_ROW,1,FUNf,1,FAI_ROW_FUNf,4)
		  CALL MATMUL(FUNfT,4,FAI_ROW_FUNf,1,DTKD,4)
		  call matadd(S1,ikp,DTKD,idtkd)

            FAI_ROW_1=pho*alpha_t*KXYm(i,j,IG,1)/VIS_o*QUOT*dtim
c            FAI_ROW_1=pho*alpha_t*Krom(i,j,IG,1)*QUOT*dtim
            CALL MATMUL(FAI_ROW_1,1,FUNf,1,FAI_ROW_FUNf_1,4)
	      CALL MATMUL(FUNfT,4,FAI_ROW_FUNf_1,1,DTKD,4)
	      call matadd(M,ikp,DTKD,idtkd)

4     continue
c      write(*,*) S1(1,1),KP1(1,1),M(1,1)

	    do p=1,4
	    do q=1,4
		  I2=NODEP4(ip,iq,p)
		  J2=NODEP4(ip,iq,q)
		  KP1_GLOBE(I2,J2)=KP1_GLOBE(I2,J2)+KP1(p,q)
		  S1_GLOBE(I2,J2)=S1_GLOBE(I2,J2)+S1(p,q)
		  M_GLOBE(I2,J2)=M_GLOBE(I2,J2)+M(p,q)
	    END DO
	    END DO
10	  CONTINUE

      DO 444 I=1,ntemp
	DO 444 J=1,ntemp
 	  KNS(I,J)= S1_GLOBE (I,J)+theta*KP1_GLOBE(I,J)                                                         
        NEWLONS(I)=NEWLONS(I)
     1		+( S1_GLOBE(I,J)-(1.0-theta)* KP1_GLOBE(I,J))*LOADS(J)
     1        - M_GLOBE(I,J)*( LOADS(J)-LOADf(J))
 444  CONTINUE

c      write(*,*) '============'
c      write(*,*) prod,FAI_ROW,FAI_ROW_1
c      write(*,*) S1_GLOBE(1,1),KP1_GLOBE(1,1),M_GLOBE(1,1)
c      write(*,*) KNS(1,1),NEWLONS(1),A_KW(1,1)
c	write(*,*) loads(1),loadf(1),dumpm1(1)

      !!边界条件
	 do ii=1,ntemp
	 do jj=1,n_bkfix
		if(iabkfix(jj,2).eq.256) then
			if(i_4_8_nodes(ii).eq.iabkfix(jj,0)) then
				do mm=1,ntemp
					KNS(ii,mm)=0.0
				enddo
				NEWLONS(ii)=pm0
				KNS(ii,ii)=1.0
			endif	
		endif
		
		if(iabkfix(jj,2).eq.1) then
			if(i_4_8_nodes(ii).eq.iabkfix(jj,0)) then
				do mm=1,ntemp
					KNS(ii,mm)=0.0
				enddo
				NEWLONS(ii)=bhp
				KNS(ii,ii)=1.0
			endif	
		endif

		if(iabkfix(jj,2).eq.2) then
			if(i_4_8_nodes(ii).eq.iabkfix(jj,0)) then
				do mm=1,ntemp
					KNS(ii,mm)=0.0
				enddo
				NEWLONS(ii)=70
				KNS(ii,ii)=1.0
			endif	
		endif
	 enddo
	 enddo
c	write(*,*) nodep(nxe,nye,1),nodep(nxe,nye,3)
      !!==================
c      DO 60 J=1,timenum
c	  TIME=J*DdTIM
        call GASSSO(KNS,NEWLONS,NTEMP)
        DO II=1,ntemp
	    LOADS(II)=NEWLONS(II)
        ENDDO
c	  GOTO 60
c 60  CONTINUE

      F_MAX_ERR = 0
	DO I = 1, ntemp
	  F_SAT1 = dumpm1(I)
        Dumpm1(I)=loads(I)
	  F_MAX_ERR=MAX(F_MAX_ERR,DABS(dumpm1(I)-F_SAT1))
	ENDDO
	IF(F_MAX_ERR.LT.1E-6) GOTO 8
1	CONTINUE

8     continue
!      DO I = 1, NN
!		dumpm(I) = pm0
!      ENDDO
!	do p=1,nxe
!      do q=1,nye
!      do in=1,4
!        inn=2*in-1
!	  dumpm(nodep(p,q,inn))=dumpm1(nodep4(p,q,in))
!	end do
!      enddo
!      enddo

	return
      END
