C***********************************************************************
C      Unpack input arrays and assign to variables
C***********************************************************************         
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc      
	SUBROUTINE Unpack
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 kf,mud,mu,COEFR,BETA,COEFP,PS0,KR2,SM,S0MAX,
     &       KR1,P0,SHV,pm0,PBGm,RKEm_X,RKEm_Y,alpha_t
      REAL*8 t1,dr,dit,theta,tol
      integer problemtype,modeltype,damagetype,geometrytype,ctype
	integer completiontype,reservoirtype,failuretype,stresstype
	integer filtercake,CODE
	integer gp
c      character*80 OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5
	CHARACTER*256 OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5,INPUT1,INPUT2
c      COMMON /INITIAL   /shmaxg,shming,szg,pbg,ra,pw,z,rb
	COMMON/INITIAL/shmaxg,shming,szg,pbg,ra,rw2,pw,z,rb,pbgm,alpha_t
      COMMON /INITIALHOL/s2,s1,p2,bb,aa,sa
      COMMON /INITIALTRI/AStress1,AStress2,Aeps1,Aeps2,UCS,CCS,CAS
      COMMON /GEOMECH /E0,mu,co0,cor,phip,csco,da,Biot,Biotp,cs,Ts,Biotm
      COMMON/RESERVIOR/PO(20),RSO(20),RKE_X,RKE_Y,RKP,VIS_O,VIS_W,qf,bm,
     &             COMP_O,COMP_W,porosity,porey,BUBP,NUMO,RKEm_X,RKEm_Y
      COMMON /WELL      /fl,fe,pc,kf,ft
c      COMMON /SUBINITIAL/sigmah,pa,af,az,amhs,mud,pd,pay,pradius,pl 
      COMMON /SUBINITIAL/sigmah,pa,af,az,amhs,mud,pd,pl,pradius,packerl
      COMMON /OPTTHERMAL/Tw,Tb,etf,ets,tdiff,tcp,vf,cct,SWC
      COMMON /SUBRESER  /w0,ww,rks,B,vu
      COMMON /SUBNONL   /Sc,S,CM,Qc,AP,AB,AL,am,ai
      COMMON/SUBFIELD/TIMEF,vs,pores,wks,CT,WD,Frac,S0,
     &                C_DISPLACEMENT,C_DSP2
      COMMON /OPTNUMERI1/NT,NX,NLOAD
      COMMON /OPTNUMERI2/dr,T1,Dit
c      COMMON /OPTFEM1/NXE,NYE,NN,N,NW,NGauss,NL,Iter
	COMMON/OPTFEM1/NXE,NYE,NYE0,nyl,NN,NN4,N,NW,GP,NL,Iter,nntemp,ntemp	 
      COMMON /OPTFEM2/THETA,TOL,length,wideth
      COMMON /OPTIONS/problemtype,modeltype,damagetype,geometrytype,
     *                completiontype,reservoirtype,failuretype,ctype,
     *                stresstype,filtercake,imethod,iflowtype,iproblem
c      COMMON/OUTPUT/OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5
	COMMON/OUTPUT/OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5,INPUT1,INPUT2 
      COMMON/PHASE2/SKROT(20),SKRWT(20),SWWC,SWT(20),SO(40,80,4),
     &              SW(40,80,4),SG(40,80,4),NUMOW
      COMMON/VAR_ADD1/COEFR,BETA,COEFP,PS0,SM,S0MAX,P0,SHV,pm0
      character*80 COMPANY,DATE,WELL_NAME,LOCATION,COMMENTS
      COMMON/USE_INFO/COMPANY,DATE,WELL_NAME,LOCATION,COMMENTS
	COMMON/LOAD1/radius(82),rad2(42),pofix(1000),point(1000,2),gtheta,
	1  gravy,pressa(1000,6),spoke(41),dofix(1000,3),radius1(82),
     1  radius2(82),rad21(42),rad22(42)
	COMMON/LOAD2/nvfix,nplod,ngrav,nedge,nodpt(1000),nopress(1000,3)
	1,lodpt(1000),nbound,ndrill,ndirect
	COMMON/flow/flowqi,flowqo
	COMMON/flowc/nflowi,nflowo
	COMMON/loads1/pdate(1000),pdelta(1000),PDLTA2(1000)
	COMMON/loads2/npoint,npr(1000),npr2(1000),nstep(1000)

	CHARACTER CHAR$*80
	!!SHunfu.Z 2009.09.02
	character*80 SITE_CONT,PERSON_CONT,PHONE,FAX,OPERATOR,JOB_NO
	REAL*8 MIN_R,MAX_R
C	COMMON/vistemp/vis_temp(50,3)
	real*8 K
	integer ANG_CODE
	COMMON/MESH/IMESH,n_node,NELEMENT,n_4node,i_vnodes(82,82),
     *i_v4nodes(82,42),
     *	i_8_4_nodes(5000),i_4_8_nodes(5000),N_N

C======================================================================                   
C Temperature from kevin                                                                  
	COMMON/temperature/COMP_B,COMP_S,alfaT,COMP_WT,COMP_OT,                                  
     &      VAL0,Tiner,Tiner2,Tout,Num_T_V                                                
      COMMON/vistemp/vis_temp(50,3)
	COMMON/GAS/GASP0 
	real*8 kk,porom(40,80,4)                                                   
C====================================================================== 
! >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      real*8 e_L, p_L, V_L,fra_den,coff1,coff2,matprops
	integer k_s,fra_num,itype,elemmats
      COMMON /FOTIS/ e_L, p_L, V_L,fra_num,fra_den,coff1,coff2,k_s,itype
      common/materials/elemmats(40,80),matprops(2000,6)

! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
c      OPEN(1,FILE="COUP_SIMU.CPS", STATUS='UNKNOWN')!in 4_PHASE
      OPEN(1,FILE="SAND_PRO.$08",STATUS='UNKNOWN') !in SAND_PRO

20    FORMAT(A80)
C I/O CONTROL ----------------------------------------------------------
      READ(1,20)CHAR$ !-------------- OUTPUT CONTROL --------------
	READ(1,20)OUTPUT1 !=SAND_PRO2.G08
	READ(1,20)OUTPUT2 !=SAND_PRO2.R08
	READ(1,20)OUTPUT3 !=SAND_PRO2.S08
	READ(1,20)OUTPUT4 !=SAND_RATE01.E08
C User Info. -----------------------------------------------------------
      READ(1,20)CHAR !$-------------- USER INFO ------------------- 	
      READ(1,20)CHAR$
 	READ(1,20)COMPANY
      READ(1,20)CHAR$
 	READ(1,20)SITE_CONT
      READ(1,20)CHAR$
 	READ(1,20)PERSON_CONT
      READ(1,20)CHAR$
 	READ(1,20)PHONE
      READ(1,20)CHAR$
 	READ(1,20)FAX
      READ(1,20)CHAR$
 	READ(1,20)WELL_NAME
      READ(1,20)CHAR$
	READ(1,20)LOCATION
      READ(1,20)CHAR$
	READ(1,20)OPERATOR
      READ(1,20)CHAR$
	READ(1,20)JOB_NO
      READ(1,20)CHAR$
	READ(1,20)DATE
      READ(1,20)CHAR$
	READ(1,20)COMMENTS
C MODELTYPE ------------------------------------------------------------
      READ(1,20)CHAR$ !-------------- MODEL TYPE -------------------
      READ(1,20)CHAR$ !*Model Type
	READ(1,*)MODELTYPE
      READ(1,20)CHAR$ !*Geometry Type
	READ(1,*)GEOMETRYTYPE
      READ(1,20)CHAR$ !*Failure Type
	READ(1,*)FAILURETYPE
c	failuretype=2
      READ(1,20)CHAR$ !*CompletionType
	READ(1,*)COMPLETIONTYPE
	! Moved from below by Ma.Z 2007-10-23 
	READ(1,20)CHAR$  !**Mesh style, 1---traditional sylinder,2--edge cylinder                                                                   
      READ(1,*) IMESH
	READ(1,20)CHAR$    !model style
      READ(1,*) itype
C RESERVOIR DATA -------------------------------------------------------
      READ(1,20)CHAR$ !-------------- RESERVOIR DATA ----------------
      READ(1,20)CHAR$ !*POREPG               - Virgin Pore Pressure Gradient, MPa/m
	READ(1,*)PBG      !Virgin Pore Pressure Gradient, MPa/m
c	pbg=pbg*1.0d-3 
      READ(1,20)CHAR$ !*DEPTH                - Depth, m
	READ(1,*)Z        
      READ(1,20)CHAR$ !*INI_KX               - Intact permeability at X Direction,mD
	READ(1,*)RKE_X      !Intact permeability,mD
      READ(1,20)CHAR$ !*INI_KY               - Intact permeability at Y Direction,mD
	READ(1,*)RKE_Y      !Intact permeability,mD
      READ(1,20)CHAR$ !*INI_POR              - Intact porosity input in fraction,f
	READ(1,*)POROSITY !Intact porosity input in FRACTION
c	POROSITY=POROSITY/100.
!!============================================
!!=====================LI=====================
      if(itype.gt.0) then
      if(itype.eq.1.or.itype.eq.2) then
        READ(1,20)CHAR$
	  READ(1,*) V_L
        READ(1,20)CHAR$
	  READ(1,*) P_L
	elseif(itype.eq.3) then
      !!poro
        READ(1,20)CHAR$ !*POREPG               - Virgin Pore Pressure Gradient, MPa/m
	  READ(1,*)PBGm
        READ(1,20)CHAR$ !*INI_KX               - Intact permeability at X Direction,mD
	  READ(1,*)RKEm_X      !Intact permeability,mD
        READ(1,20)CHAR$ !*INI_KY               - Intact permeability at Y Direction,mD
	  READ(1,*)RKEm_Y      !Intact permeability,mD
        READ(1,20)CHAR$ !*INI_POR              - Intact porosity input in fraction,f
	  READ(1,*)POROSITYm !Intact porosity input in FRACTION
        READ(1,20)CHAR$
	  READ(1,*) fra_num
        READ(1,20)CHAR$
	  READ(1,*) fra_den
	  alpha_t=fra_num*fra_den**2*1e-15
	endif
      READ(1,20)CHAR$
	READ(1,*) k_s
      READ(1,20)CHAR$
	READ(1,*) coff1,coff2
      endif
!!============================================
!!=====================LI=====================
C FLUID DATA -----------------------------------------------------------
      READ(1,20)CHAR$  !--------------- FLUID DATA -------------------
      READ(1,20)CHAR$ !*VISCOSITY_OIL        - cp
	READ(1,*)VIS_O    !VISCOSITY OF OIL
      READ(1,20)CHAR$ !*COMPRESS_OIL         - 1/MPa
	READ(1,*)COMP_O   !COMPRESSIBILITY OF OIL
c 	COMP_O =COMP_O *1.0d3
c	COMP_O =COMP_O *1.0d-3
      READ(1,20)CHAR$ !*OIL BUBBLE_PRESSURE  - MPa
	READ(1,*)BUBP     !BUBBLE PRESSURE OF OIL
c	BUBP=BUBP*1.0d-3
      READ(1,20)CHAR$ !*GAS_COMP             - 1/MPa
	READ(1,*)BM       !COMPRESSIBILITY OF GAS
c 	bm=bm*1.0d3
c	bm=bm*1.0d-3
      READ(1,20)CHAR$ !*VISCOSITY_WATER      - cp
	READ(1,*)VIS_W    !VISCOSITY OF WATER
	READ(1,20)CHAR$ !*COMPRESS_WATER       - 1/MPa
	READ(1,*)COMP_W   !COMPRESSIBILITY OF WATER
c 	COMP_W=COMP_W*1.0d3
c	COMP_W=COMP_W*1.0d-3
      READ(1,20)CHAR$   !*SW-KRW-KRO, RELATIVE PERMEABILITY OF OIL-WATER SYSTEM
      READ(1,*)NUMOW !11
      DO I=1,NUMOW
CC		READ(1,*)SWT(I),SKROT(I),SKRWT(I)
		!!Shunfu.Z 2009.09.02
         READ(1,*)SWT(I),SKROT(I),SKRWT(I)
      END DO

      READ(1,20)CHAR$ !*PO-RSO, SOLUTION GAS-OIL RATIO, RSO: Sg(Po)
      READ(1,*)NUMO !4
      DO I=1,NUMO
         READ(1,*)PO(I),RSO(I)
      END DO
c	PO=PO*1.0d-3
C GEOMECHANICS PROPERTIES ----------------------------------------------
      READ(1,20)CHAR$ !-------------- GEOMECHANICS PROPERTIES -------   
      READ(1,20)CHAR$ !*YOUNGS            - Young's Modulus, MPa 
	READ(1,*)E0       
c	E0=E0*1.0d-3
      READ(1,20)CHAR$ !*POISSON           - Poisson Ratio
	READ(1,*)MU      
      READ(1,20)CHAR$ !*C_PEAK            - Peak Cohesion, MPa
	READ(1,*)CO0      
c	CO0=CO0*1.0d-3
      READ(1,20)CHAR$ !*C_REDL            - Residual Cohesion, MPa
	READ(1,*)COR  
c	COR=COR*1.0d-3
      READ(1,20)CHAR$ !*PHI_PEAK          - Peak Friction Angle, Deg
	READ(1,*)PHIP     
      READ(1,20)CHAR$ !*DILATION          - Dilation Angle, Deg
	READ(1,*)DA       
      READ(1,20)CHAR$ !*CSCO              - Coefficient for the critical strain
	READ(1,*)CSCO     
      READ(1,20)CHAR$ !*BIOTI             - Biot's Constant
	READ(1,*)BIOT
	if(itype.eq.3) then
        READ(1,20)CHAR$ !*BIOTI             - Biot's Constant of matrix
	  READ(1,*)BIOTm
	endif	     
      READ(1,20)CHAR$ !*CRTSS             - Critical Sanding Strain, 
	READ(1,*)CS       !Critical Sanding Strain, %
	CS=CS/100.
      READ(1,20)CHAR$ !*K_CHANGE_EXP      - Exponential constant for permeability change
	READ(1,*)RKS      
      READ(1,20)CHAR$ !*SHMAXG            - Max. Horizontal Stress Gradient, MPa/m
	READ(1,*)SHMAXG   
c	shmaxg=1.0d-3*shmaxg
      READ(1,20)CHAR$ !*SHMING            - Min. Horizontal Stress Gradient, MPa/m
	READ(1,*)SHMING   
c	SHMING=1.0d-3*SHMING
!	if (geometrytype.eq.2) then
!		average=(SHMAXG+SHMING)/2.0d0
!		SHMAXG=average
!		SHMING=average
!	endif
      READ(1,20)CHAR$  !*SZG               - Vertical Stress Gradient, MPa/m
	READ(1,*)SZG      
c	SZG=1.0d-3*SZG
C WELL CONDITION -------------------------------------------------------
      READ(1,20)CHAR$  !-------------- Well Conditions ---------------
      READ(1,20)CHAR$ !*RW                - Wellbore Radius, m
      if(imesh.EQ.4.OR.imesh.EQ.5)then                                                         
		READ(1,*)RA,rw2       !Wellbore Radius, m                                               
	else
		READ(1,*)RA       !Wellbore Radius, m                                                   
	endif
      READ(1,20)CHAR$ !*SWI_WELL          - Water saturation near wellbore
	READ(1,*)WW       !Near wellbore water content
      READ(1,20)CHAR$ !*BHP               - Bottom Hole Pressure, MPa
	READ(1,*)PW       !Bottom Hole Pressure, MPa
c	PW=1.0d-3*PW
      READ(1,20)CHAR$ !*PERF_LENG         - Perforation Tunnel Length
	READ(1,*)PL       !Perforation tunnel length
      READ(1,20)CHAR$ !*PERF_RADIUS       - Perforation Tunnel Radius
	READ(1,*)PRADIUS  !Perforation tunnel radius
      READ(1,20)CHAR$ !*PERF_DEN          - Perforation Density
	READ(1,*)PD       !Perforation density
      READ(1,20)CHAR$ !*C_DISPLACEMENT    - Critical Displacement,cm
	if(imesh.EQ.4.OR.imesh.EQ.5)then                                                         
		READ(1,*)C_DISPLACEMENT,C_DSP2 !Critical displacement,cm                                
		C_DSP2=C_DSP2/100.0                                                                       
	else                                                                                     
		READ(1,*)C_DISPLACEMENT !Critical displacement,cm                                       
	endif 
      C_DISPLACEMENT=C_DISPLACEMENT/100.0d0
	
      READ(1,20) CHAR$ !*TIMEF             - Final Time at Production Stop, day
	READ(1,*)TIMEF   !Final time for production prediction, min
      READ(1,20) CHAR$ !*DRILL_CODE        - =0 UnBalanced drilling; =1 Balanced drilling
	READ(1,*)NDRILL   !0=negatival drilling;1=un-negatival drilling
C RUN CONTROL ----------------------------------------------------------
      READ(1,20)CHAR$ !-------------- RUN CONTROL -------------------
      READ(1,20)CHAR$ !*NX_FE             - Total Element number at tangential direction
	!!Shunfu.Z 2010.08.10
	if (GEOMETRYTYPE.eq.2) imesh=1
	!!Shunfu.Z 2010.08.10
	READ(1,*)NXE      !Total Element number at TANGANTIAL direction
	if (((imesh.eq.4).or.(imesh.eq.5)).and.mod(nxe,4).ne.0) then
c		WRITE(T_OUT,*) 'data input error:'
c		WRITE(T_OUT,*) 'Total Element number in TANGANTIAL direction should
c 		WRITE(T_OUT,*) 'data input error:'
		!commented by Bashar CALL WRTF(T_OUT)
 		!commented by Bashar WRITE(T_OUT,*) 'Total Element number in TANGANTIAL direction should
    !commented by Bashar $ be N times 4'
		!CALL WRTF(T_OUT)   commented by Bashar
		stop 
	endif
	READ(1,20)CHAR$ !*NY_FE             - Total Element number at radial direction
	READ(1,*)NYE      !Total Element number at RADIUS direction
	READ(1,20)CHAR$  !*C_N               - Crank-Nicolson paramter
	READ(1,*)THETA    !Crank-Nicolson paramter
      READ(1,20)CHAR$  !*CONVERG_TOLER     - Convergence toleration on displacement and pore pressure
	READ(1,*)TOL      !Convergence toleration
      READ(1,20)CHAR$ !*MAX_ITER          - Maximum iteration number
	READ(1,*)ITER     !Maximum iteration number
      READ(1,20)CHAR$ !*OUT_CODE          - OUTER BOUNDARY CODE (0-SPECIFY PRESSURE, 1-SPECIFY RATE)
	READ(1,*)NFLOWO   !OUTER BOUNDARY OPTIONS, =0 SPECIFY Pp, =1 SPECIFY RATE
	READ(1,*)FLOWQO  !RATE
C 	READ(1,*)NFLOWI,NFLOWO   !OUTER BOUNDARY OPTIONS, =0 SPECIFY Pp, =1 SPECIFY RATE
C 	READ(1,*)FLOWQI,FLOWQO  !RATE
      READ(1,20)CHAR$ !INI_CODE           - INITIAL CONDITION CODE (0-SPECIFY DISPLACEMENT, 1-SPECIFY STRESS)
	READ(1,*)NBOUND   !INITIAL CODE (0-SPECIFY DISPLACEMENT, 1-SPECIFY STRESS)
      READ(1,20)CHAR$ !WELL_TYPE           - WELL TYPE CODE (0-VRITICAL, 1-HORIZONTAL)
	READ(1,*)NGRAV
      GRAVY=NGRAV*9.8D0
	!!Shunfu.Z 2010.06.13 for axissymetric situation
		if(geometrytype.eq.2) NGRAV=0
	!!Shunfu.Z 2010.06.13
      READ(1,20)CHAR$ !ANG_CODE           - ANGLE BETWEEN MAX PRINCIPAL STERSS AND WELL AXIS (0-90 DEG, 1-0 DEG)
	READ(1,*)ANG_CODE !MAX PRINCIPAL STERSS ^ WELL AXIS (0-90 DEG, 1-0 DEG)
      NDIRECT=0
	IF(NGRAV.EQ.1.AND.ANG_CODE.EQ.1) NDIRECT=1
      READ(1,20)CHAR$ !LENGTH             - MODEL LENGTH,m
	READ(1,*) Length
	READ(1,20)CHAR$ !WIDETH             - MODEL WIDETH,m
	READ(1,*) wideth
      READ(1,20)CHAR$ !MIN_R              - MINIMUM RADIUS,m
	READ(1,*) MIN_R
	READ(1,20)CHAR$ !MAX_R              - MAXIMUM RADIUS,m
	READ(1,*) MAX_R
	if(imesh.le.5) then
	READ(1,20)CHAR$ !RADIUS,m
      DO I=1,NYE+1
	    READ(1,*) RADIUS(I)		
cc		RADIUS(I)=MIN_R+(MAX_R-MIN_R)
cc     1		*((sin((I-1.0)/NYE*3.14159-3.14159/2)+1.0)/2.0)**(1.0)
cc		RADIUS1(I)=MIN_R+(wideth*10-MIN_R)
cc     1		*((sin((I-1.0)/NYE*3.14159-3.14159/2)+1.0)/2.0)**(1.0)
cc		RADIUS2(I)=MIN_R+(length/2.0d0*10-MAX_R-MIN_R)
cc     1		*((sin((I-1.0)/NYE*3.14159-3.14159/2)+1.0)/2.0)**(1.0)
	END DO
	endif
! >>>> Dr. Fotios Karaoulanis (18/06/13)>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
! >>>> Read spoke for imesh = 6
      if(imesh.eq.6) then
	READ(1,20)CHAR$ !RADIUS,m
	DO I=1, NYE+1	
		READ(1,*) RADIUS(I)
	END DO
	endif
	if (imesh.eq.6) then
	READ(1,20)CHAR$
      DO I=1, NXE+1	
		READ(1,*) spoke(I)
	END DO
      else
	DO I=1,NXE+1
		SPOKE(I)=1.0
	END DO
	end if
! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	!!!radius=30.0*radius

      READ(1,20)CHAR$ !WELL BOUNDARY (0-SPECIFY PRESSURE, 1-SPECIFY RATE)
      READ(1,20)CHAR$ !TIME        S_P/S_R       ITER          CODE
      READ(1,*)NPOINT !5

c******setting intinal date and poreporessure
	pdate(1)=0.0
	pdelta(1)=pbg*z
	PDLTA2(1)=pbg*z
	nstep(1)=0
	npr(1)=0
	npr2(1)=0

	if(imesh.EQ.4.OR.imesh.EQ.5) then                                                        
		DO I=2,NPOINT+1                                                                       
	READ(1,*) PDATE(I),PDELTA(I),NSTEP(I),NPR(I),PDLTA2(I),NPR2(I)
		END DO                                                                                  
	else                                                                                     
		DO I=2,NPOINT+1                                                                         
			READ(1,*) PDATE(I),PDELTA(I),NSTEP(I),NPR(I)
		END DO                                                                                  
	endif
	gp=2

      READ(1,20)CHAR$ !*Pay Zone Thickness, m
      READ(1,*) packerl
      READ(1,20)CHAR$ !*Tensile Strength, MPa
      READ(1,*) TS
	  
      DO I=1,NUMOW
		IF(SKRWT(I).LE.0.00001d0) SWWC=SWT(I)
      END DO
C new input parametr gas initial density                                                  
      READ(1,20)CHAR$ !*Gas initial density                                                                 
      READ(1,*) GASP0 
      READ(1,20)CHAR$  !* Initial temperature                                                                   
      READ(1,*) VAL0      ! reservoir initial temperature                                                                                     
                                                                                          
	IF ((IMESH.EQ.4).OR.(IMESH.EQ.5)) THEN                                                   
		READ(1,20)CHAR$ !* BHT Steam temperature                                                                         
          READ(1,*) Tiner,Tiner2     ! wellbore temperature
      ELSE                                                                                     
	    READ(1,20)CHAR$    !* BHT Steam temperature                                                                  
          READ(1,*) Tiner     ! wellbore temperature                                      
	END IF                                                                                                                                                                     
	READ(1,20)CHAR$ !* outer temperature                                                                    
      READ(1,*) Tout      ! outside temperature                                           
	READ(1,20)CHAR$   !* Cb, Bulk compressibility                                                                
      READ(1,*) COMP_B    
	READ(1,20)CHAR$     !* cs, Skeleton compressibility                                                               
      READ(1,*) COMP_S      
	COMP_S=5.0d-4 
	COMP_B=2.0d-5                               
	READ(1,20)CHAR$ !* thermal expansion, solid                                                                    
      READ(1,*) alfaT     ! thermal expamsion coefficient                                 
	READ(1,20)CHAR$ !* water heat conductivity                                                                    
      READ(1,*) COMP_WT   ! water heat conductivity       
	READ(1,20)CHAR$   !* oil heat conductivity                                                                 
      READ(1,*) COMP_OT   ! oil heat conductivity          
C Viscosity-Temperature relation Table                                                    
      READ(1,20)CHAR$  !* Viscosity-temperature (temp  water  oil)                                                                   
      READ(1,*)Num_T_V !27                                                                 
	DO I=1,Num_T_V                                                                           
		READ(1,*) vis_temp(I,1),vis_temp(I,2),vis_temp(I,3)                                                                      
		if(vis_temp(I,2).LE.0.0) then  ! water viscosity                                        
			vis_temp(I,2)=1.0                                                                 
		end if                                                                                  
		if(vis_temp(I,3).LE.0.0) then  ! Oil viscosity                                     
			vis_temp(I,3)=1.0                                                                 
		end if 
c		vis_temp(I,3)=10.0                                                                                  
	END DO
	
      do i = 1, 40
        do j = 1, 80
          elemmats(i, j) = 1
        end do
      end do
      ! init material
      matprops(1, 1) = E0
      matprops(1, 2) = mu
	READ(1, 20) CHAR$   ! Materials
	if(imesh.eq.5) then
	  do i=1, nxe*nye*2
	    READ(1,  *) (matprops(i, ii), ii = 1, 2)
	  enddo
	else
	  do i=1, nxe*nye
	    READ(1,  *) (matprops(i, ii), ii = 1, 2)
	  enddo
	endif
	                                                                                                             
C======================================================================                   
	CLOSE(1)
C
c       Initiaize variables
c
      cp = 3.1415926D0*phip/180.D0
      cw = 3.1415926D0*phip/180.D0
      P0  = pbg*z !DEPTH EFFECT
	Pm0  = pbgm*z
	shv = szg*z !DEPTH EFFECT
      Pa=Pw
      smin=shming*z !DEPTH EFFECT
      smax=shmaxg*z !DEPTH EFFECT
c***Re-arrenged shv smax,smin on x,y,z-direstions for horizontal well
	if(NGRAV.eq.1.and.ANG_CODE.eq.1) then
		smax=szg*z
		smin=shmaxg*z
		shv=shming*z
	end if
	if(NGRAV.eq.1.and.ANG_CODE.eq.2) then
		smax=szg*z
		smin=shming*z
		shv=shmaxg*z
	end if
	!! For axial symmetry 
	if (GEOMETRYTYPE.eq.2) then
		smax=szg*z
		smin=shming*z
		shv=shmaxg*z		
	endif
c**********end **********
c
c     Choose Active State First
c
	coefp = (1.0D0+dsin(cp))/(1.0D0-dsin(cp))
	coefr = (1.0D0+dsin(cw))/(1.0D0-dsin(cw))
	Sr    = - 2.0D0*cor*dcos(cw)/(1.0D0-dsin(cw))
	SA    = - 2.0D0*co0*dcos(cp)/(1.0D0-dsin(cp))
	Nk = 1      
	S0 = (smax - smin)/2.D0
	PS0= (smax + smin)/2.D0
      K = E0/(3.D0*(1.0D0-2.0D0*mu))
      G = E0/(2.0D0*(1+mu))
      row = K - 2.0D0*G/3.0D0
c       
c  All parameters have been defined as passive states, but the loading
c  state will be checked and redefined if they are active
c
      beta = 0.5D0*(1.0D0-2.0D0*mu)*BIOT/(1.0D0-mu)
      test = (PS0 - 2.D0*S0 - beta*P0)/(1.D0 - beta)
      If( test .lt. 0.00001D0) Then
c
C   Choose passive or active loading
C
		coefp = (1.D0-dsin(cp))/(1.D0+dsin(cp))
		coefr = (1.D0-dsin(cw))/(1.D0+dsin(cw))
		sa = 2.D0*co0*dcos(cp)/(1.D0+dsin(cp))
		sr = 2.D0*cor*dcos(cw)/(1.D0+dsin(cw))
		Nk = 0
C
C     Define the parameters for the elliptical plastic zone
C
      End If 
      S0max = (coefp - 1.D0)*(PS0-P0 + S0/(coefp - 1.D0))/(coefp + 1.D0) 
      sm = S0/S0max

C****************** End Initial Calculations ************************** 	
      Return
      End

