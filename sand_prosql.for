
      !!˫�ؽ���
        program  sand
        IMPLICIT REAL*8 (A-H, O-Z)
        CHARACTER*256 OPT51,INPUT11,INPUT21,OPTMESH1
	  real*8 n_args

	  call SAND_PROSQL(n_Args,INPUT11,INPUT21,OPT51,OPTMESH1)

	  end program sand

!		program SAND_PROSQL !(n_Args,INPUT11,INPUT21,OPT51,OPTMESH1)
	SUBROUTINE SAND_PROSQL(n_Args,INPUT11,INPUT21,OPT51,OPTMESH1)
c*** this program is finished on item1,item5and item2********************
c*** 1--options for porepressure or rate of flow at outside and table for input
c          at inside of borehole
c    2--options for initial stress ( stress of far field is added in outside 
c       edge of borehole) or initial displacement ( outside edge is fixed as 
c       initinal displacement) 
c    3--options for negatival drilling or un-negatival drilling
c   Aug.10 2000 EQ
c    4-- change kay as two dimensions.
c    5-- porepressure reduces at inside edge of the borehole is contruled by table
c    6-- gravity effect has been added in the initial stress for horizontal well
c    7-- intinal stress and strain are calculated.
c   Sept.7 2000 EQ
c    8--subroutine correctDP1 was modified in order to fit the general case. 
c       So sz can be the minimum in 3 principle stresses. When sz<sr, let 
c       st=sz/v_sr, then get sx sy,sxy,sz and return.r
c    9-- UCS should be changed as TS. It should be done after Carl adds TS as 
c        input data. In this program, temparely let TS=0.5UCS. Now stress doesn't
c        jump again.
c    10--kxy(p,q,ig,1) and kxy(p,q,ig,2) should each equal to RKF-x and RKF-y.
c        This also should be changed after Carl change RKF as RKF-x and RKF-y.
c         This is done on sept28,2000
c    11--sanding time for each coh is been wroten in file SAANDTIME.$$$. The values
c         are recorded are coh and bhp.
c    12--yielding time for each coh is been wroten in file YIELTIME.$$$. The values
c         are recorded are coh and bhp.
c
cThis program is finished on Sept28,2000,all A items has been done
C---------------------------------------
C    MAIN PROGRAM TO SIMPLY STAB_WELL
C---------------------------------------
      IMPLICIT REAL*8 (A-H, O-Z)
c	CHARACTER*80 OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5,Q_OUTPUT
	CHARACTER*256 OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5,INPUT1,INPUT2
	CHARACTER*256 Q_OUTPUT
	CHARACTER*256 OPT51,INPUT11,INPUT21,OPTMESH1
	!CHARACTER*4096 T_OUT   commented by Bashar
	CHARACTER*100 HEAD
      integer problemtype,modeltype,damagetype,geometrytype,ctype
     & ,completiontype,reservoirtype,failuretype,stresstype,filtercake
c      COMMON/OUTPUT/OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5
	COMMON/OUTPUT/OUTPUT1,OUTPUT2,OUTPUT3,OUTPUT4,OPT5,INPUT1,INPUT2 
      character*80 COMPANY,DATE,WELL_NAME,LOCATION,COMMENTS
      COMMON/USE_INFO/COMPANY,DATE,WELL_NAME,LOCATION,COMMENTS
      COMMON/OPTIONS/problemtype,modeltype,damagetype,geometrytype,
     *      completiontype,reservoirtype,failuretype,ctype,stresstype,
     *      filtercake,imethod,iflowtype,iproblem
      COMMON/DATAIT/nxN,It
      COMMON/DATA/PHI0,PHIMAX,SK0,SKC,YS,YF,UF,C0,SNMD,
     &            Err,YA0,WORM_PHI(1000),
     &      WORM_SK(1000),WORM_C(1000),
     &      WORM_YA(1000),WORM_X(1000),WORM_T_PHI(1000),
     &      WORM_T_SK(1000),WORM_T_C(1000),WORM_Q(1000),
     &      WORM_TMPC(1000)
	 COMMON /STEPINF/ PERMX,PERMY,tsdrill0,tsdrill,NNSTEP
	common /iconsider/igrav
	common /itemp/Tin1,Tin2,tou
		COMMON/temperature/COMP_B,COMP_S,alfaT,COMP_WT,COMP_OT,                                  
     &      VAL0,Tiner,Tiner2,Tout,Num_T_V  
C---------------------------------------
C    INPUT DATA BASED KEYWORD
C---------------------------------------
	INTEGER(2) tmpday, tmpmonth, tmpyear
	INTEGER(2) tmphour, tmpminute, tmpsecond, tmphund

	OPTMESH1='output'
	n_Args=0
	if(n_Args.LT.2) then
	INPUT1="SAND_PRO.$08"
	INPUT2='INPUT.DAT'
	OPT5='OUTPUT.G04'
	elseif(n_Args.EQ.4) then
!	CALL GETARG(1,INPUT1)
!	CALL GETARG(2,INPUT2)
!	CALL GETARG(3,OPT5)
	INPUT1=INPUT11
	INPUT2=INPUT21
	OPT5=OPT51
	elseif(n_Args.EQ.5) then
!	CALL GETARG(1,INPUT1)
!	CALL GETARG(2,INPUT2)
!	CALL GETARG(3,OPT5)
	INPUT1=INPUT11
	INPUT2=INPUT21
	OPT5=OPT51
	call meshing(OPTMESH1)
 	stop
      return
	else
	!WRITE(T_OUT,*) "Param err." commented by Bashar
 	!CALL WRTF(T_OUT)  commented by Bashar
c	return       
	endif
	TIME_CAL=0.0d0


	CALL GETDAT(tmpyear, tmpmonth, tmpday)
	CALL GETTIM(tmphour, tmpminute, tmpsecond, tmphund)
      N_EXP=0
	IF (TMPYEAR.GT.2015) N_EXP=1
	IF (TMPYEAR.EQ.2015.AND.TMPMONTH.GT.7) N_EXP=1
	IF (TMPYEAR.EQ.2015.AND.TMPMONTH.EQ.7.AND.TMPDAY.GT.1) N_EXP=1
      IF (N_EXP.EQ.1) THEN
      WRITE(T_OUT,*)" I am sorry, Your license has expired"                           
c      CALL WRTF(T_OUT)                                                                
      WRITE(T_OUT,*)" Please contact PGI for further information ..."                 
c      CALL WRTF(T_OUT)                                                               
      WRITE(T_OUT,*)"-------------------------------------------------"            
c      CALL WRTF(T_OUT)                                                                
      WRITE(T_OUT,*)" 300, 840-6th Avenue S.W."                                       
c      CALL WRTF(T_OUT)                                                                
      WRITE(T_OUT,*)" Calgary, Alberta T3C 0K1"                                       
c      CALL WRTF(T_OUT)                                                                
      WRITE(T_OUT,*)" Tel: 1-(403)-313-6401"                                          
c      CALL WRTF(T_OUT)                                                                
      WRITE(T_OUT,*)" Fax: 1-(403)-313-1238"                                          
c      CALL WRTF(T_OUT)                                                                
      WRITE(T_OUT,*)" E-mail: PGI@Canada.com"                                         
c      CALL WRTF(T_OUT)                                                                
      WRITE(T_OUT,*)" Http://www.pgi-ca.com"                                          
c      CALL WRTF(T_OUT)  
      END IF
	
 	CALL Unpack
	
!@@@@	OPT5='OUTPUT.G04'
c	Q_OUTPUT='Q.txt'
      Q_OUTPUT='q.txt'
	if(lzhangshunfu.eq.2) then

	endif
	OPEN(unit=15,file=Q_OUTPUT,status='unknown')
      OPEN(unit=12,file=OUTPUT1,status='unknown')
      OPEN(unit=14,file=OPT5,status='unknown') !.G04 by Ma.Z
      OPEN(unit=13,file=OUTPUT2,status='unknown')
      OPEN(unit=10,file=OUTPUT3,status='unknown')
      OPEN(unit=2,file=OUTPUT4,status='unknown')
c****   set a file for recording the time of sending for each cohision
c      OPEN(unit=10,file=OUTPUT3,status='unknown')

c***set a file for testing output
c      open(unit=15,file='testo.dat',status='unknown')
c***
!@@@@      OPEN(1,FILE='INPUT.DAT',STATUS='UNKNOWN')
	OPEN(1,FILE=INPUT2,STATUS='UNKNOWN')
	READ(1,143)HEAD !*WELLBORE PRESSURE,ATM
	READ(1,*) PWN     !Inner cavity/wormhole pressure
c	PWN=1.0d-3*PWN
	READ(1,143)HEAD !*DRAINAGE BOUNDARY PRESSURE,ATM
	READ(1,*) PDN     !External wormhole pressure
c	PdN=1.0d-3*PdN
	READ(1,143)HEAD !*SPACIMEN LENGTH,M
	READ(1,*) XL     !length of wormhole channel
	READ(1,143)HEAD !*INITIAL POROSITY
	READ(1,*) PHI0   
	READ(1,143)HEAD !*MAXIMUM POROSITY OF WORMHOLE
	READ(1,*) PHIMAX !Maximum porosity
	READ(1,143)HEAD !*INITIAL PERMEABILITY(D)
	READ(1,*) SK0    !Permeability inside a wormhole
	SK0 = SK0 * 0.987D0 * 0.000000000000001D0 ! Darcy -> m^2
	SKC = SK0 * (1.D0 - PHI0)**2.D0 / PHI0**3.D0
	READ(1,143)HEAD !*DENSITY OF SOLID
	READ(1,*) YS  ! Solid Density
	YS = YS * 1000.D0
c	ys=ys*1.33959d-19
	READ(1,143)HEAD !*DENSITY OF FLUID
	READ(1,*) YF   ! Fluid Density
	YF = YF * 1000.D0
c	yf=yf*1.33959d-19
	READ(1,143)HEAD !*FLUID VISCOSITY(CP)
	READ(1,*) UF !'cp/rhou -> m2/day
	uf=uf*1.0d-12/24.0d0/3600.0d0
c	UF = UF / YF * 0.00000000001D0
	UF = UF / YF
	READ(1,143)HEAD !*INITIAL TRANSPORT CONCENTRATION OF SOLID
	READ(1,*) C0
	YA0 = YF + C0 * (YS - YF)
	READ(1,143)HEAD !*Erosion Parameter
	READ(1,*) SNMD  ! Erosional Parameter
	READ(1,143)HEAD !*THE NUMBER OF SPATIAL POINT
	READ(1,*) nxN
	READ(1,143)HEAD !*MAXIMUM CALCULATION TIME, DAY
	READ(1,*) TMAX
	READ(1,143)HEAD !*INITIAL TIME STEP
	READ(1,*) DT0
	DT0 = DT0 * 86400.D0
	READ(1,143)HEAD !*CALCULATION ERROR
	READ(1,*) Err
	!!Shunfu.Z 2010.06.03
	READ(1,143)HEAD !*TOTAL STEPS FOR THE DRILLING TIME
	READ(1,*) NNSTEP
	READ(1,143)HEAD ! *Thermal diffusivity (m2/Day) !!!
	READ(1,*) PERMX,PERMY
	READ(1,143)HEAD !* gravity in consideration(1---yes, 0---no)
	READ(1,*) Igrav
	READ(1,143)HEAD !* Steam temperature for production well and injection well
	READ(1,*) Tiner,Tiner2     ! wellbore temperature 
	READ(1,143)HEAD !* outer temperature
	READ(1,*) Tout     
	READ(1,143)HEAD !tsdrill0,tsdrill
	READ(1,*) tsdrill0,tsdrill  	 
	CLOSE(1)
	!!Shunfu.Z 2010.06.03

143	FORMAT(A100)
	SKC = SK0 * (1.D0 - PHI0) ** 2.D0 / PHI0 ** 3.D0
	YA0 = YF + C0 * (YS - YF)

         
c      WRITE(T_OUT,*) PWN,PDN,XL,PHI0,PHIMAX,SK0,SKC,YS,YF,UF,C0,SNMD,
c     &            TMAX,DT0,Err,nxN,YA0,IT

      WRITE(10,*)'0-YIELDING, 1-SANDING'
      WRITE(10,*)'COH,TIME,BHP,INDEX'
      WRITE(12,*) 21
      WRITE(12,1)
1     format(1x,'Cohesion, MPa'/,
     &       1X,'Production Time, Day'/,
     &       1X,'Distance, m'/,
     &       1x,'Angle from Sigma, Degree'/,
     &       1x,'3rd Principle Stress, MPa'/,
     &       1x,'1st Principle Stress, MPa'/,
     &       1x,'X-Stress, MPa'/
     &       1x,'Y-Stress, MPa'/
     &       1x,'Shear Stress, MPa'/
     &       1x,'Vertical Stress, MPa'/
     &       1x,'X-Strain, %'/
     &       1x,'Y-Strain, %'/
     &       1x,'Vol-Strain, %'/
     &       1x,'Radial Strain, %'/
     &       1x,'Tangential Strain, %'/
     &       1x,'Elastic Stiffness, MPa'/
     &       1x,'Porosity Change, %'/
     &       1x,'Permeability Change, %'/
     &       1x,'Plastic Radius Domain'/
     &       1x,'sanding radius Domain'/
     &       1x,'Saturation')
      WRITE(14,*) 25
      WRITE(14,2)
2     format(1x,'Cohesion, MPa'/,
     &       1X,'Production Time, Day'/,
     &       1X,'Distance, m'/,
     &       1x,'Angle from Sigma, Degree'/,
     &       1x,'3rd Principle Stress, MPa'/,
     &       1x,'1st Principle Stress, MPa'/,
     &       1x,'X-Stress, MPa'/
     &       1x,'Y-Stress, MPa'/
     &       1x,'Shear Stress, MPa'/
     &       1x,'Vertical Stress, MPa'/
     &       1x,'X-Strain, %'/
     &       1x,'Y-Strain, %'/
     &       1x,'Vol-Strain, %'/
     &       1x,'Radial Strain, %'/
     &       1x,'Tangential Strain, %'/
     &       1x,'Elastic Stiffness, MPa'/
     &       1x,'Porosity Change, %'/
     &       1x,'Permeability Change, %'/
     &       1x,'Plastic Radius Domain'/
     &       1x,'sanding radius Domain'/
     &       1x,'Saturation'/
     &       1x,'Water X-Flow'/
     &       1x,'Water Y-Flow'/
     &       1x,'Oil X-Flow'/
     &       1x,'Oil Y-Flow')
      WRITE(13,*)22
      WRITE(13,3)
3     format(1x,'Cohesion, MPa'/,
     &       1X,'Production Time, Day'/,
     &       1X,'Distance, m'/,
     &       1x,'Angle to X-Axis, Degree'/,
     &       1x,'X-Displacement, cm'/
     &       1x,'Y-Displacement, cm'/
     &       1x,'Radial Displacement, cm'/
     &       1x,'Tangential Displacement, cm'/
     &       1x,'Pore Pressure, MPa'/
     &       1x,'Well Drawdown, MPa'/
     &       1x,'BHP, MPa'/
     &       1x,'sand production, m^3'/
     &       1x,'sand cut, %'/
     &       1x,'sand rate, m^3/Day'/
     &       1x,'Oil Rate, M^3/Day'/
     &       1x,'Water Rate, m^3/Day'/
     &       1x,'Oil Production, m^3'/
     &       1x,'Water Production, m^3'/
     &       1x,'Plastic Radius, cm'/
     &       1x,'Sanding Radius, cm'/
     &       1x,'Temperature, C Deg'/
     &       1x,'Saturation')

C END OF 8-31 CHANGE
C---------------------------------------
C             CALCULATION
C---------------------------------------
	CALL fem(0,OPTMESH1)
c	WRITE(T_OUT,888)
c	WRITE(T_OUT,888)                                                                
	!BAshar CALL WRTF(T_OUT)  

	write(2,888)
888   FORMAT('Calculation is completed!!')
      CLOSE(2)
	CLOSE(14)
	CLOSE(13)
      CLOSE(12)
	close(11)
	close(10)
c***
      close(15)
c***
!	END program SAND_PROSQL
      END
